package com.orugga.transunionsdk.helper.session

import com.orugga.transunionsdk.repository.AuthRepository
import com.orugga.transunionsdk.vo.createApp.CreateAppResponse
import com.orugga.transunionsdk.vo.TokenResponse

interface SessionHelper {

    suspend fun getAppToken() : TokenResponse
    fun getDCApplicationId() : String
    suspend fun createDCApplication(firstName: String, lastName: String): CreateAppResponse

    private class SessionHelperImp(private val authRepository: AuthRepository) : SessionHelper {

        override suspend fun getAppToken(): TokenResponse {
            return authRepository.getOauthToken()
        }

        override suspend fun createDCApplication(firstName: String, lastName: String) : CreateAppResponse {
            return authRepository.createDCApplication(firstName, lastName)
        }

        override fun getDCApplicationId() : String {
            return authRepository.getDCApplicationId()
        }
    }

    companion object {
        fun get(authRepository: AuthRepository) : SessionHelper {
            return SessionHelperImp(authRepository)
        }
    }
}
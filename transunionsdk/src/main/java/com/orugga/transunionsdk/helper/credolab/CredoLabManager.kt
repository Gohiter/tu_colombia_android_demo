package com.orugga.transunionsdk.helper.credolab

import android.content.Context
import credoapp.CredoAppException
import credoapp.CredoAppService


object CredoLabManager {
//    private var url = ""
    private var referenceKey = ""
    fun collectData(appContext: Context, referenceKey: String, url: String) : String {
        var result = ""
        Thread(Runnable {
            try {
                val credoAppService = CredoAppService(appContext, url, referenceKey)
                result = credoAppService.collectData(referenceKey)
                return@Runnable
            } catch (e: CredoAppException) {
                result = "CredoApp collect data failure. ${e.message}"
                return@Runnable
            }
        }).start()
        return result
    }
    fun setReferenceKey(referenceKey: String) {
        this.referenceKey = referenceKey
    }
}
package com.orugga.transunionsdk.helper.documents

import com.orugga.transunionsdk.repository.DocumentsRepository
import com.orugga.transunionsdk.vo.UploadableDocumentType
import com.orugga.transunionsdk.vo.createDocument.CreateAttachDocumentResponse
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceResponse
import com.orugga.transunionsdk.vo.docAuthenticationReport.DocAuthReportResponse
import com.orugga.transunionsdk.vo.facialSimilarityInvokeService.FacialSimilarityInvokeServiceResponse
import com.orugga.transunionsdk.vo.facialSimilarityReport.FacialSimilarityReportResponse
import com.orugga.transunionsdk.vo.pullReport.PullReportResponse
import okhttp3.MediaType
import java.io.File

interface DocumentsHelper {

    suspend fun documentAuthentication(firstName: String, lastName: String) : DocAuthInvokeServiceResponse
    suspend fun invokeFacialSimilarityService(firstName: String, lastName: String): FacialSimilarityInvokeServiceResponse
    suspend fun createDocument(uploadableDocumentType: UploadableDocumentType, fileName: String) : CreateAttachDocumentResponse
    suspend fun uploadDocument(file: File, type: MediaType, uploadableDocumentType: UploadableDocumentType) : CreateAttachDocumentResponse
    suspend fun processFrontDocument() : DocAuthInvokeServiceResponse
    suspend fun processSelfie() : DocAuthInvokeServiceResponse
    suspend fun getFrontDocumentReport(docType: String): DocAuthReportResponse
    suspend fun getFacialSimilarityReport() : FacialSimilarityReportResponse
    suspend fun pullReport() : PullReportResponse

    private class DocumentsHelperImpl(private val documentsRepository: DocumentsRepository) : DocumentsHelper {
        override suspend fun documentAuthentication(firstName: String, lastName: String) : DocAuthInvokeServiceResponse {
            return documentsRepository.documentAuthentication(firstName, lastName)
        }

        override suspend fun invokeFacialSimilarityService(firstName: String, lastName: String): FacialSimilarityInvokeServiceResponse {
            return documentsRepository.facialSimilarityInvokeService(firstName, lastName)
        }

        override suspend fun createDocument(uploadableDocumentType: UploadableDocumentType, fileName: String): CreateAttachDocumentResponse {
            return documentsRepository.createDocument(uploadableDocumentType, fileName)
        }

        override suspend fun uploadDocument(file: File, type: MediaType, uploadableDocumentType: UploadableDocumentType): CreateAttachDocumentResponse {
            return documentsRepository.uploadDocument(file, type, uploadableDocumentType)
        }

        override suspend fun processFrontDocument(): DocAuthInvokeServiceResponse {
            return documentsRepository.processFrontDocument()
        }

        override suspend fun processSelfie(): DocAuthInvokeServiceResponse {
            return documentsRepository.processSelfie()
        }

        override suspend fun getFrontDocumentReport(docType: String): DocAuthReportResponse {
            return documentsRepository.getFrontDocumentReport("tax_id")
        }

        override suspend fun getFacialSimilarityReport(): FacialSimilarityReportResponse {
            return documentsRepository.facialSimilarityReport()
        }

        override suspend fun pullReport(): PullReportResponse {
            return documentsRepository.pullReport()
        }
    }

    companion object {
        fun get(documentsRepository: DocumentsRepository) : DocumentsHelper {
            return DocumentsHelperImpl(documentsRepository)
        }
    }
}
package com.orugga.transunionsdk.helper.iovation

import android.content.Context
import com.iovation.mobile.android.FraudForceConfiguration
import com.iovation.mobile.android.FraudForceManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object FraudManager {
    fun getBlackBoxFromDevice(appContext: Context, iovationSubscriberKey: String?) : String {
        return try {
            val configuration = FraudForceConfiguration.Builder()
                .subscriberKey(iovationSubscriberKey)
                .enableNetworkCalls(true) // Defaults to false if left out of configuration
                .build()
            val fraudForceManager = FraudForceManager.getInstance()
            fraudForceManager.initialize(configuration, appContext)
            FraudForceManager.getInstance().refresh(appContext)
            FraudForceManager.getInstance().getBlackbox(appContext)
        } catch (e: Exception) {
            "Blackbox generation failure - network issue or other. ${e.message}"
        }
    }
}
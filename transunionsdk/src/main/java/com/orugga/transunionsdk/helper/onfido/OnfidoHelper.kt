package com.orugga.transunionsdk.helper.onfido

import android.content.Context
import androidx.fragment.app.Fragment
import com.onfido.android.sdk.capture.Onfido
import com.onfido.android.sdk.capture.OnfidoConfig
import com.onfido.android.sdk.capture.OnfidoFactory
import com.onfido.android.sdk.capture.ui.camera.face.stepbuilder.FaceCaptureStepBuilder
import com.onfido.android.sdk.capture.ui.options.FlowStep


object OnfidoHelper {
    fun runOnfidoVideoLiveness(context: Context, fragment: Fragment,sdkToken:String ) {
        val flow: FlowStep = FaceCaptureStepBuilder.forVideo().withIntro(false).build()
        val steps : Array<FlowStep> = arrayOf(flow)

        val config = OnfidoConfig.builder(context)
            .withSDKToken(sdkToken)
            .withCustomFlow(steps)
            .build()
        val onfido = OnfidoFactory.create(context).client
        onfido.startActivityForResult(fragment, 1, config)

    }

}
package com.orugga.transunionsdk.helper.verification

import com.orugga.transunionsdk.repository.VerificationRepository
import com.orugga.transunionsdk.vo.biocatch.BiocatchResponse
import com.orugga.transunionsdk.vo.credoLab.CredoLabTokenResponse
import com.orugga.transunionsdk.vo.credoLab.DataSetInsightsResponse
import com.orugga.transunionsdk.vo.completeTransaction.CompleteTransactionResponse
import com.orugga.transunionsdk.vo.deviceVerification.DeviceVerificationResponse
import com.orugga.transunionsdk.vo.emailVerification.EmailVerificationResponse
import com.orugga.transunionsdk.vo.offers.OffersResponse
import com.orugga.transunionsdk.vo.phoneVerification.PhoneVerificationResponse
import com.orugga.transunionsdk.vo.videoliveness.OnfidoSDKResponse
import com.orugga.transunionsdk.vo.videoliveness.OnfidoVideoResponse
import com.orugga.transunionsdk.vo.watchlist.WatchlistResponse

interface VerificationHelper {

    suspend fun sendEmail(email: String): EmailVerificationResponse
    suspend fun sendPhone(number: String, type: String, countryCode: String): PhoneVerificationResponse
    suspend fun deviceVerification(blackBox: String): DeviceVerificationResponse
    suspend fun watchListVerification(firstName: String, lastName: String) : WatchlistResponse
    suspend fun getBiocatchScore(): BiocatchResponse
    suspend fun getOffers(firstName: String, lastName: String) : OffersResponse
    suspend fun getCredoLabToken(): CredoLabTokenResponse
    suspend fun getCredoLabDataSetInsight(referenceNumber: String): DataSetInsightsResponse
    suspend fun completeTransaction(): CompleteTransactionResponse
    suspend fun getSDKToken(onfidoSdkToken: String, applicantId: String,applicationBundleName: String) : OnfidoSDKResponse
    suspend fun getVideo (applicantId: String): OnfidoVideoResponse

    private class VerificationHelperImp(private val verificationRepository: VerificationRepository) :
        VerificationHelper {

        override suspend fun sendEmail(email: String) : EmailVerificationResponse {
            return verificationRepository.sendEmail(email)
        }

        override suspend fun deviceVerification(blackBox: String): DeviceVerificationResponse {
            return verificationRepository.validateDevice(blackBox)
        }

        override suspend fun sendPhone(
            number: String,
            type: String,
            countryCode: String
        ): PhoneVerificationResponse {
            return verificationRepository.sendPhone(number, type, countryCode)
        }

        override suspend fun watchListVerification(
            firstName: String,
            lastName: String
        ): WatchlistResponse {
            return verificationRepository.watchlistVerification(firstName, lastName)
        }

        override suspend fun getBiocatchScore(): BiocatchResponse {
            return verificationRepository.getBiocatchScore()
        }

        override suspend fun getOffers(
            firstName: String,
            lastName: String
        ): OffersResponse {
            return verificationRepository.getOffers(firstName, lastName)
        }

        override suspend fun getCredoLabToken(): CredoLabTokenResponse {
            return verificationRepository.getCredoLabToken()
        }

        override suspend fun getCredoLabDataSetInsight(referenceNumber: String): DataSetInsightsResponse {
            return verificationRepository.getCredoLabDatasetInsight(referenceNumber)
        }

        override suspend fun completeTransaction(): CompleteTransactionResponse {
            return verificationRepository.completeTransaction()
        }
        override suspend fun getSDKToken(onfidoSdkToken: String, applicantId: String,applicationBundleName: String) : OnfidoSDKResponse {

            return verificationRepository.getSDKToken(onfidoSdkToken, applicantId, applicationBundleName)
        }
        override suspend fun getVideo (applicantId: String) : OnfidoVideoResponse {
            return verificationRepository.getVideo(applicantId)
        }
    }

    companion object {
        fun get(verificationRepository: VerificationRepository) : VerificationHelper {
            return VerificationHelperImp(verificationRepository)
        }
    }
}
package com.orugga.transunionsdk.vo.docAuthentication

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.fileAttach.DocAttachPhoto
import com.orugga.transunionsdk.vo.ResponseLink

data class DocAttachResponseIdentityVerification(
        @field:SerializedName("Link") val link: ResponseLink,
        @field:SerializedName("SwitchDevice") val switchDevice: String,
        @field:SerializedName("Action") val action: String,
        @field:SerializedName("RefreshInMS") val refreshInMs: String,
        @field:SerializedName("Phone") val phone: String,
        @field:SerializedName("Email") val email: String,
        @field:SerializedName("CurrentStep") val currentStep: String,
        @field:SerializedName("Type") val type: String,
        @field:SerializedName("Step") val step: String,
        @field:SerializedName("Selfie") val selfie: DocAttachPhoto,
        @field:SerializedName("Front") val front: DocAttachPhoto,
        @field:SerializedName("Back") val back: DocAttachPhoto,
        @field:SerializedName("DocIType") val docIType: String,
        @field:SerializedName("DefaultCountryId") val defaultCountryId: String,
        @field:SerializedName("SendPhone") val sendPhone: String,
        @field:SerializedName("sendEmail") val sendEmail: String,
        @field:SerializedName("Error") val error: String
)
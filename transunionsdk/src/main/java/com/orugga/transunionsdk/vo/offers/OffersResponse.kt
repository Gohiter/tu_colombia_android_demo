package com.orugga.transunionsdk.vo.offers

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo

data class OffersResponse(
    @field:SerializedName("Status") val status: String,
    @field:SerializedName("ResponseInfo") val responseInfo: ResponseInfo,
    @field:SerializedName("Fields") val fields: OffersResponseFields
)
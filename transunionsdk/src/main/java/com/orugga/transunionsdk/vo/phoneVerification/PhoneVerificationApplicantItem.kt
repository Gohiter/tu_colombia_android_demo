package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class PhoneVerificationApplicantItem(
    @field:SerializedName("Telephones") val telephones: PhoneVerificationTelephones
)
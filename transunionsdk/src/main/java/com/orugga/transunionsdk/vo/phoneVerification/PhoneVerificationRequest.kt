package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class PhoneVerificationRequest(
        @field:SerializedName("Fields") val fields: PhoneVerificationFields
)
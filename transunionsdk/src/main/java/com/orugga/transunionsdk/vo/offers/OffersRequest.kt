package com.orugga.transunionsdk.vo.offers

import com.google.gson.annotations.SerializedName

data class OffersRequest(
        @field:SerializedName("Fields") val fields: OffersFields
)
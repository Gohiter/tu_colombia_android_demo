package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSDocAuthentication(
        @field:SerializedName("DAApplicationId") val daApplicationId: String,
        @field:SerializedName("DAIdDocumentReportId2") val daIdDocumentReportId2: String,
        @field:SerializedName("DAIdDocumentReportId") val daIdDocumentReportId: String,
        @field:SerializedName("DAApplication2Id") val daApplication2Id: String,
        @field:SerializedName("DADocumentFrontId") val daDocumentFrontId: String,
        @field:SerializedName("DADocumentFrontId2") val daDocumentFrontId2: String,
        @field:SerializedName("DADocumentBackId") val daDocumentBackId: String,
        @field:SerializedName("DADocumentBackId2") val daDocumentBackId2: String,
        @field:SerializedName("DAIdDocumentCheckId") val daIdDocumentCheckId: String,
        @field:SerializedName("DAIdDocumentCheckId2") val daIdDocumentCheckId2: String
)
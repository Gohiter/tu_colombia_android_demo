package com.orugga.transunionsdk.vo.docAuthenticationReport

import com.google.gson.annotations.SerializedName

data class DocAuthReportStatus(
        @field:SerializedName("Outcome") val outcome: String,
        @field:SerializedName("IsSuccess") val isSuccess: String,
        @field:SerializedName("ErrorMessage") val errorMessage: String,
        @field:SerializedName("Errorcode") val errorCode: String
)
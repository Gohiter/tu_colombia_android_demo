package com.orugga.transunionsdk.vo.facialSimilarityReport

import com.google.gson.annotations.SerializedName

data class FacialSimilarityReportApplicant(
        @field:SerializedName("Attributes") val attributes: FacialSimilarityReportAttributes,
        @field:SerializedName("DSFacialSimilarityStatus") val dsFacialSimilarityStatus: DSFacialSimilarityReportStatus
)
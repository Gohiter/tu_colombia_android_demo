package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class WatchlistResponseFields(
        @field:SerializedName("ApplicationId") val applicationId: String,
        @field:SerializedName("Applicants_IO") val applicantIO: ApplicantIOResponse
)
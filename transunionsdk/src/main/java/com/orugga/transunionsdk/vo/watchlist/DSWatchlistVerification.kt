package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class DSWatchlistVerification(
        @field:SerializedName("Match") val matches: List<WatchlistMatch> = ArrayList(),
        @field:SerializedName("Severity") val severity: String,
        @field:SerializedName("StatusCode") val statusCode: String
)
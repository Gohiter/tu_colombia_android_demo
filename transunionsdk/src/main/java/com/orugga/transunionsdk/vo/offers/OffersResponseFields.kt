package com.orugga.transunionsdk.vo.offers

import com.google.gson.annotations.SerializedName

data class OffersResponseFields(
        @field:SerializedName("Applicants_IO") val applicantsIO: OffersResponseApplicants,
        @field:SerializedName("ApplicationId") val applicationId: String

)
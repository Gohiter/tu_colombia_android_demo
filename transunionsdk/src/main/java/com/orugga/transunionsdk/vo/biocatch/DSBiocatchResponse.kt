package com.orugga.transunionsdk.vo.biocatch

import com.google.gson.annotations.SerializedName

data class DSBiocatchResponse(
        @field:SerializedName("score") val score: String,
        @field:SerializedName("riskFactors") val riskFactors: String,
        @field:SerializedName("platformType") val platformType: String,
        @field:SerializedName("muid") val muid: String,
        @field:SerializedName("isSupported") val isSupported: String,
        @field:SerializedName("isRecentRat") val isRecentRat: String,
        @field:SerializedName("isMobileRat") val isMobileRat: String,
        @field:SerializedName("isEmulator") val isEmulator: String,
        @field:SerializedName("isBot") val isBot: String,
        @field:SerializedName("isAggregator") val isAggregator: String,
        @field:SerializedName("ipGeoLongtitude") val ipGeoLongtitude: String,
        @field:SerializedName("ipGeoLatitude") val ipGeoLatitude: String,
        @field:SerializedName("ipGeoCountryCode") val ipGeoCountryCode: String,
        @field:SerializedName("ipGeoCountry") val ipGeoCountry: String,
        @field:SerializedName("ipGeoCity") val ipGeoCity: String,
        @field:SerializedName("ipGeoASN") val ipGeoASN: String,
        @field:SerializedName("ip") val ip: String,
        @field:SerializedName("genuineFactors") val genuineFactors: String,
        @field:SerializedName("customerSessionID") val customerSessionID: String,
        @field:SerializedName("bcStatus") val bcStatus: String,
        @field:SerializedName("requestDateTime") val requestDateTime: String

)
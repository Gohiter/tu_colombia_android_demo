package com.orugga.transunionsdk.vo.docAuthInvokeService

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.PhotoResponse
import com.orugga.transunionsdk.vo.ResponseLink

data class DocAuthInvokeServiceResponseIdentityVerification(
        @field:SerializedName("Link") val link: ResponseLink,
        @field:SerializedName("SwitchDevice") val switchDevice: String,
        @field:SerializedName("Action") val action: String,
        @field:SerializedName("RefreshInMS") val refreshInMS: String,
        @field:SerializedName("Phone") val phone: String,
        @field:SerializedName("Email") val email: String,
        @field:SerializedName("CurrentStep") val currentStep: String,
        @field:SerializedName("Type") val type: String,
        @field:SerializedName("Step") val step: String,
        @field:SerializedName("Selfie") val selfie: PhotoResponse,
        @field:SerializedName("Back") val back: String,
        @field:SerializedName("Front") val front: String,
        @field:SerializedName("DocIType") val docIType: String,
        @field:SerializedName("DefaultCountryId") val defaultCountryId: String,
        @field:SerializedName("SendPhone") val sendPhone: String,
        @field:SerializedName("SendEmail") val sendEmail: String
)
package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class CompleteTransactionRequest(
        @field:SerializedName("Fields") val requestFields: CompleteTransactionFields
)
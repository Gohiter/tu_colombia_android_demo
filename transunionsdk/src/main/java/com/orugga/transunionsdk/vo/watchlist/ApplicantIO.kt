package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class ApplicantIO(
        @field:SerializedName("Applicant") val applicant: Applicant
)
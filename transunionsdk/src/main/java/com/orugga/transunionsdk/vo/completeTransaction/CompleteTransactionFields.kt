package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class CompleteTransactionFields(
        @field:SerializedName("ServiceType") val serviceType: String? = "Complete"
)
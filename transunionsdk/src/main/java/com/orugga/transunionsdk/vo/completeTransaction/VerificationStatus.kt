package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class VerificationStatus(
        @field:SerializedName("Outcome") val outcome: String,
        @field:SerializedName("isSuccess") val isSuccess: String,
        @field:SerializedName("ErrorMessage") val errorMessage: String,
        @field:SerializedName("Errorcode") val errorCode: String
)
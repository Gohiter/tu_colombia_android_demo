package com.orugga.transunionsdk.vo.createDocument

import com.google.gson.annotations.SerializedName

data class CreateAttachDocumentResponse(
        @field:SerializedName("Message") val message: String
)
package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSBureauData(
        @field:SerializedName("Response") val response: DSBureauDataResponse
)
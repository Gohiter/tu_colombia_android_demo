package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.deviceVerification.DeviceRequest
import com.orugga.transunionsdk.vo.docAuthentication.DocAttachResponseIdentityVerification
import com.orugga.transunionsdk.vo.offers.OffersResponseMyOffers
import com.orugga.transunionsdk.vo.phoneVerification.PhoneVerificationTelephones

data class CompleteTransactionResponseApplicant(
        @field:SerializedName("ApplicantFirstName") val applicantFirstName: String,
        @field:SerializedName("ApplicantLastName") val applicantLastName: String,
        @field:SerializedName("EmailAddress") val email: String,
        @field:SerializedName("Gender") val gender: String,
        @field:SerializedName("DateOfBirth") val dateOfBirth: String,
        @field:SerializedName("ApplicantAadhaar") val aadhaar: String,
        @field:SerializedName("Attributes") val attributes: CompleteTransactionResponseAttributes,
        @field:SerializedName("IdentityVerification") val identityVerification: CompleteIdentityVerification,
        @field:SerializedName("DSDACreateApplicantData") val dsdaCreateApplicantData: DSDACreateApplicantData,
        @field:SerializedName("DSDACreateApplicantStatus") val dsdaCreateApplicantStatus: VerificationStatus,
        @field:SerializedName("DSDAPrefillStatus") val dsdaPrefillStatus: VerificationStatus,
        @field:SerializedName("DSDocAuthenticationData") val dsDocAuthenticationData: DSDocAuthenticationData,
        @field:SerializedName("DSDocAuthenticationStatus") val dsDocAuthenticationStatus: VerificationStatus,
        @field:SerializedName("Documents") val documents: IdentityVerificationDocuments,
        @field:SerializedName("DSFacialSimilarityData") val dsFacialSimilarityData: DSFacialSimilarityData,
        @field:SerializedName("DSFacialSimilarityStatus") val dsFacialSimilarityStatus: VerificationStatus,
        @field:SerializedName("Identifiers") val identifiers: CompleteTransactionIdentifiers,
        @field:SerializedName("DeviceRequest") val deviceRequest: DeviceRequest,
        @field:SerializedName("DSDeviceVerification_FraudForceData") val dsDeviceFraudForceData: DSDeviceVerificationFraudForceData,
        @field:SerializedName("DSDeviceVerification_FraudForceStatus") val dsDeviceFraudForceStatus: VerificationStatus,
        @field:SerializedName("DSWatchlistVerificationData") val dsWatchlistVerificationData: DSWatchlistVerificationData,
        @field:SerializedName("DSWatchlistVerificationStatus") val dsWatchlistVerificationStatus: VerificationStatus,
        @field:SerializedName("DSEmailVerificationData") val dsEmailVerificationData: DSEmailVerificationData,
        @field:SerializedName("DSEmailVerificationStatus") val dsEmailVerificationStatus: VerificationStatus,
        @field:SerializedName("Telephones") val telephones: PhoneVerificationTelephones,
        @field:SerializedName("DSPhoneVerificationData") val dsPhoneVerificationData: DSPhoneVerificationData,
        @field:SerializedName("DSPhoneVerificationStatus") val dsPhoneVerificationStatus: VerificationStatus,
        @field:SerializedName("DSBureauData") val dsBureauData: DSBureauData,
        @field:SerializedName("MyOffers") val myOffers: OffersResponseMyOffers,
        @field:SerializedName("DSBehavioralBiometricsData") val dsBehavioralBiometricsData: DSBehavioralBiometricsData,
        @field:SerializedName("DSBehavioralBiometricsStatus") val dsBehavioralBiometricsStatus: VerificationStatus

)
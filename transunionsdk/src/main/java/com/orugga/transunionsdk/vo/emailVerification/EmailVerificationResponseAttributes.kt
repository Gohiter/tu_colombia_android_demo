package com.orugga.transunionsdk.vo.emailVerification

import com.google.gson.annotations.SerializedName

data class EmailVerificationResponseAttributes(
        @field:SerializedName("DSEmailVerification") val dsVerification: DSEmailVerificationResponse,
        @field:SerializedName("DREmailVerification") val drVerification: DREmailVerificationResponse
)
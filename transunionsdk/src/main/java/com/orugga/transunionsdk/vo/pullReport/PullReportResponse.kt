package com.orugga.transunionsdk.vo.pullReport

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo

data class PullReportResponse(
        @field:SerializedName("Status") val status: String,
        @field:SerializedName("ResponseInfo") val responseInfo: ResponseInfo,
        @field:SerializedName("Fields") val fields: PullReportFields
)
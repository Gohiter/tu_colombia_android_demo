package com.orugga.transunionsdk.vo.offers

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.watchlist.ApplicantIO

data class OffersFields(
        @field:SerializedName("Applicants_IO") val applicantIO: ApplicantIO,
        @field:SerializedName("ServiceType") val serviceType: String = "OffersEngine"
)
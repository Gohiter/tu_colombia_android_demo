package com.orugga.transunionsdk.vo

enum class ServiceType(name: String) {
    EMAILVERIFICATION("EmailVerification")
}

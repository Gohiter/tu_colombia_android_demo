package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class PhoneVerificationApplicants(
        @field:SerializedName("Applicant") val applicants: List<PhoneVerificationApplicantItem>
)
package com.orugga.transunionsdk.vo.offers

import com.google.gson.annotations.SerializedName

data class OffersResponseApplicants(
        @field:SerializedName("Applicant") val applicants: List<OffersResponseApplicantItem>
)
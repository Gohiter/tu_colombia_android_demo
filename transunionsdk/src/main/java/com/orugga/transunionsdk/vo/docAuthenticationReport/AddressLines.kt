package com.orugga.transunionsdk.vo.docAuthenticationReport

import com.google.gson.annotations.SerializedName

data class AddressLines (
        @field:SerializedName("street_address") val streetAddress: String,
        @field:SerializedName("city") val city: String,
        @field:SerializedName("postal_code") val postalCode: String,
        @field:SerializedName("state") val state: String,
        @field:SerializedName("country") val country: String,
        @field:SerializedName("country_code") val countryCode: String
)
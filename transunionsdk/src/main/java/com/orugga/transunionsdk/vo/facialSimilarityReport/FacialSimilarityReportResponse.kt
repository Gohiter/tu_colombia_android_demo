package com.orugga.transunionsdk.vo.facialSimilarityReport

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo

data class FacialSimilarityReportResponse(
        @field:SerializedName("Status") val status: String,
        @field:SerializedName("ResponseInfo") val responseInfo: ResponseInfo,
        @field:SerializedName("Fields") val dsFacialSimilarityApplicanFields: FacialSimilarityReportApplicanFields
)
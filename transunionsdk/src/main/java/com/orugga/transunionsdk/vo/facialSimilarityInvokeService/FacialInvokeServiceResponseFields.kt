package com.orugga.transunionsdk.vo.facialSimilarityInvokeService

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceResponseIdentityVerification

data class FacialInvokeServiceResponseFields(
        @field:SerializedName("identityVerification") val identityVerificationDocAuthInvokeService: DocAuthInvokeServiceResponseIdentityVerification,
        @field:SerializedName("Applicants_IO") val applicantsIO: String,
        @field:SerializedName("ApplicationId") val applicationId: String
)
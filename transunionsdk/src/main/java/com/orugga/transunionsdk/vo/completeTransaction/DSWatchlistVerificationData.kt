package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSWatchlistVerificationData(
        @field:SerializedName("Request") val request: DSWatchlistVerificationDataRequest,
        @field:SerializedName("Response") val response: DocResponse
)
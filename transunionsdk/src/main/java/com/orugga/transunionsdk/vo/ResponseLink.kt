package com.orugga.transunionsdk.vo

import com.google.gson.annotations.SerializedName

data class ResponseLink(
        @field:SerializedName("ResendLinkWaitTime") val resendLinkWaitTime: String,
        @field:SerializedName("RetryAttempts") val retryAttemps: String,
        @field:SerializedName("ErrorMessage") val errorMessage: String,
        @field:SerializedName("IsSuccess") val isSuccess: String
)
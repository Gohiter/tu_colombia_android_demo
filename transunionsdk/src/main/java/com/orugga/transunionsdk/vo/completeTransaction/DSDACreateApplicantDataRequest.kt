package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSDACreateApplicantDataRequest(
        @field:SerializedName("Authorization") val request: String,
        @field:SerializedName("first_name") val firstName: String,
        @field:SerializedName("last_name") val lastName: String
)
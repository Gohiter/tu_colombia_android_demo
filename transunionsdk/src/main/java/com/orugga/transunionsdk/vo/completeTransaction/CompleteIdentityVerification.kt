package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class CompleteIdentityVerification(
        @field:SerializedName("Link") val link: CompleteTransactionResponseLink,
        @field:SerializedName("RefreshInMS") val refreshInMs: String,
        @field:SerializedName("Phone") val phone: String,
        @field:SerializedName("Email") val email: String,
        @field:SerializedName("CurrentStep") val currentStep: String,
        @field:SerializedName("DocIType") val docIType: String,
        @field:SerializedName("DefaultCountryId") val defaultCountryId: String,
        @field:SerializedName("SendPhone") val sendPhone: String,
        @field:SerializedName("sendEmail") val sendEmail: String,
        @field:SerializedName("Error") val error: String
)
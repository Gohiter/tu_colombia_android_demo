package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSWatchlistCredentialsRequestHeader(
        @field:SerializedName("secTokenLogin") val secTokenLogin: DSWatchlistCredentialsToken
)
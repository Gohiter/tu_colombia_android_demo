package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class WatchlistMatch(
        @field:SerializedName("MatchScore") val matchScore: String,
        @field:SerializedName("RiskScore") val riskScore: String,
        @field:SerializedName("RiskPriority") val riskPriority: String,
        @field:SerializedName("MatchType") val matchType: String,
        @field:SerializedName("EntityName") val entityName: String,
        @field:SerializedName("EntityId") val entityId: String,
        @field:SerializedName("Gender") val gender: String,
        @field:SerializedName("DatesOfBirth") val datesOfBirth: List<String>,
        @field:SerializedName("Alias") val alias: List<Alias>,
        @field:SerializedName("CountryCode") val countryCode: String,
        @field:SerializedName("Event") val events: List<WatchlistEvent>
)
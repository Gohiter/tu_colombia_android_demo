package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class ApplicantResponse(
        @field:SerializedName("Attributes") val attributes: WatchlistResponseAttributes,
        @field:SerializedName("DSWatchlistVerificationStatus") val dsWatchlistVerificationStatus: DSWatchlistVerificationStatus
)
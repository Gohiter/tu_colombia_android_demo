package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DocResponse(
        @field:SerializedName("Success") val success: String,
        @field:SerializedName("Data") val data: DocResponseData
)
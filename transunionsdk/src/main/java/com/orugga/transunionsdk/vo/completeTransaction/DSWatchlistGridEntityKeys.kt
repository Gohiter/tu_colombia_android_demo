package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSWatchlistGridEntityKeys(
        @field:SerializedName("gridEntityCriteria") val gridEntityCriteria: DSWatchlistGridEntityCriteria
)
package com.orugga.transunionsdk.vo.videoliveness

import com.google.gson.annotations.SerializedName

data class OnfidoSDKResponse(
        @field:SerializedName("token") val token: String?
)
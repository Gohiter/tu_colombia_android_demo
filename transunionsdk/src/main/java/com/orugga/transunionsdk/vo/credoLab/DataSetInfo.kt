package com.orugga.transunionsdk.vo.credoLab

import com.google.gson.annotations.SerializedName

data class DataSetInfo(
    @field:SerializedName("referenceNumber") val recordNumber: String?,
    @field:SerializedName("uploadDate") val uploadDate: String,
    @field:SerializedName("collectionStartDate") val collectionStartDate: String,
    @field:SerializedName("collectionEndDate") val collectionEndDate: String,
    @field:SerializedName("source") val source: String,
    @field:SerializedName("device") val device: DataSetInfoDevice,
    @field:SerializedName("permissions") val permissions: List<CredoLabPermission>,
    @field:SerializedName("rawDataSetFileSize") val rawDataSetFileSize: String,
    @field:SerializedName("osVersion") val osVersion: String,
    @field:SerializedName("isRepeatableUpload") val isRepeatableUpload: String
)
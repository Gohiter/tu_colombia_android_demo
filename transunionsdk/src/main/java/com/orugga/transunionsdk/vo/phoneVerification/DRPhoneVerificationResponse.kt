package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class DRPhoneVerificationResponse(
        @field:SerializedName("IsBlackListedPhoneType") val isBlackListedPhoneType: String,
        @field:SerializedName("IsPhoneNumberValid") val isPhoneNumberValid: String,
        @field:SerializedName("IsPhoneBlackListCheckSuccess") val isPhoneBlackListCheckSuccess: String,
        @field:SerializedName("IsBlackListedPhoneNumber") val isBlackListedPhoneNumber: String,
        @field:SerializedName("IsBlacklistedCountryCode") val isBlacklistedCountryCode: String,
        @field:SerializedName("IsPhoneAPISuccess") val isPhoneAPISuccess: String,
        @field:SerializedName("PNKRiskLevel") val pnkRiskLevel: String

)
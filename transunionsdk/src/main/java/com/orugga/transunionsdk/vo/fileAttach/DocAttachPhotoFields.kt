package com.orugga.transunionsdk.vo.fileAttach

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.docAuthentication.DocAuthIdentityVerification

// TODO SET STEP AS FRONT OR BACK AND CURREENT AS FRONT/BACK AND CONTINUE
data class DocAttachPhotoFields(
        @field:SerializedName("IdentityVerification") val identityVerification: DocAuthIdentityVerification
)
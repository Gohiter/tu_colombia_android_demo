package com.orugga.transunionsdk.vo.offers

import com.google.gson.annotations.SerializedName

data class OffersResponseOffer(
        @field:SerializedName("Product") val product: String,
        @field:SerializedName("ActiveFlag") val activeFlag: String,
        @field:SerializedName("OfferId") val offerId: String,
        @field:SerializedName("OfferName") val offerName: String,
        @field:SerializedName("OfferImage") val offerImage: String,
        @field:SerializedName("OfferPdf") val offerPdf: String,
        @field:SerializedName("LoanAmount") val loanAmount: String,
        @field:SerializedName("InterestRate") val interestRate: String,
        @field:SerializedName("ProcessingFee") val processingFee: String,
        @field:SerializedName("OfferStartDate") val offerStartDate: String,
        @field:SerializedName("OfferEndDate") val offerEndDate: String,
        @field:SerializedName("ClientRedirectUrl") val clientRedirectUrl: String,
        @field:SerializedName("Attribute1") val attribute1: String,
        @field:SerializedName("Attribute2") val attribute2: String,
        @field:SerializedName("Attribute3") val attribute3: String,
        @field:SerializedName("Attribute4") val attribute4: String,
        @field:SerializedName("Attribute5") val attribute5: String,
        @field:SerializedName("Attribute6") val attribute6: String,
        @field:SerializedName("Attribute7") val attribute7: String,
        @field:SerializedName("Attribute8") val attribute8: String,
        @field:SerializedName("PdfNameField") val pdfNameField: String,
        @field:SerializedName("ImageNameField") val imageNameField: String,
        @field:SerializedName("OfferSequence") val offerSequence: Int

)
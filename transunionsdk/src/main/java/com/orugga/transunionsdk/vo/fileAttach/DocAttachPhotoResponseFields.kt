package com.orugga.transunionsdk.vo.fileAttach

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.docAuthentication.DocAttachResponseIdentityVerification

data class DocAttachPhotoResponseFields(
        @field:SerializedName("IdentityVerification") val identityVerification: DocAttachResponseIdentityVerification,
        @field:SerializedName("Applicants_IO") val applicantsIo: String,
        @field:SerializedName("ApplicationId") val applicationId: String
)
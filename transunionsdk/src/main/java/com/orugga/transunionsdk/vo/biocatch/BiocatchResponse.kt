package com.orugga.transunionsdk.vo.biocatch

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo

data class BiocatchResponse(
    @field:SerializedName("Status") val status: String,
    @field:SerializedName("ResponseInfo") val responseInfo: ResponseInfo,
    @field:SerializedName("Fields") val fields: BiocatchResponseFields
)
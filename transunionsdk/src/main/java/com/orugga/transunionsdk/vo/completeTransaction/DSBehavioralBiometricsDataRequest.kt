package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSBehavioralBiometricsDataRequest(
        @field:SerializedName("customerID") val customerID: String,
        @field:SerializedName("customerSessionID") val customerSessionID: String,
        @field:SerializedName("activityName") val activityName: String,
        @field:SerializedName("activityType") val activityType: String,
        @field:SerializedName("uuid") val uuid: String,
        @field:SerializedName("userAgent") val userAgent: String,
        @field:SerializedName("action") val action: String,
        @field:SerializedName("solution") val solution: String,
        @field:SerializedName("yearOfBirth") val yearOfBirth: String
)
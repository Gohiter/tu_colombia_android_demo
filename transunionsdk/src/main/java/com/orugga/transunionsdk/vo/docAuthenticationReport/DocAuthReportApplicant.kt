package com.orugga.transunionsdk.vo.docAuthenticationReport

import com.google.gson.annotations.SerializedName

data class DocAuthReportApplicant (
        @field:SerializedName("Attributes") val reportAttributes: DocAuthReportAttributes
)
package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSDACreateApplicantData(
        @field:SerializedName("Request") val request: DSDACreateApplicantDataRequest,
        @field:SerializedName("Response") val response: DSDocAuthenticationDataResponse
)
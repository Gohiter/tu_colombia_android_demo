package com.orugga.transunionsdk.vo.pullReport

import com.google.gson.annotations.SerializedName

data class PullReportRequestFields(
        @field:SerializedName("ServiceType") val serviceType: String = "PullReport",
)
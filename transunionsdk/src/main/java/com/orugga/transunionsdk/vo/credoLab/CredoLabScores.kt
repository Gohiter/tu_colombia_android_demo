package com.orugga.transunionsdk.vo.credoLab

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CredoLabScores(
    @field:SerializedName("code") val code: String? = null,
    @field:SerializedName("value") val value: String? = null,
    @field:SerializedName("probability") val probability: String? = null
): Parcelable
package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName

data class DeviceVerificationRequest(
        @field:SerializedName("Fields") val applicants: DeviceVerificationFields
)
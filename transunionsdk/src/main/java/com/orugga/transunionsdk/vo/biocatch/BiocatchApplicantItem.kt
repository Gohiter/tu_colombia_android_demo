package com.orugga.transunionsdk.vo.biocatch

import com.google.gson.annotations.SerializedName

data class BiocatchApplicantItem(
    @field:SerializedName("MessageType") val messageType: String = "T",
    @field:SerializedName("UUID") val uuid: String = "no_uuid",
    @field:SerializedName("UserAgent") val userAgent: String = "AndroidTSOApp",
    @field:SerializedName("ActivityType") val activityType: String = "NEWAPP",
    @field:SerializedName("ActivityName") val activityName: String = "Application Submitted",
    @field:SerializedName("Solution") val solution: String = "AO",
    @field:SerializedName("Action") val action: String = "GetScore",
    @field:SerializedName("CSID") val csid: String,
    @field:SerializedName("YearOfBirth") val yearOfBirth: String = "1984"
)
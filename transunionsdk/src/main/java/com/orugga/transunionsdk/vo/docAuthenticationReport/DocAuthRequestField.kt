package com.orugga.transunionsdk.vo.docAuthenticationReport

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo
import com.orugga.transunionsdk.vo.docAuthentication.DocAuthIdentityVerification

data class DocAuthRequestField (
        @field:SerializedName("IdentityVerification") val identityVerification: DocAuthIdentityVerification
)
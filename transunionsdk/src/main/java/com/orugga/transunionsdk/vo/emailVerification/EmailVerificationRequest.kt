package com.orugga.transunionsdk.vo.emailVerification

import com.google.gson.annotations.SerializedName

data class EmailVerificationRequest(
        @field:SerializedName("Fields") val fields: EmailVerificationFields
)
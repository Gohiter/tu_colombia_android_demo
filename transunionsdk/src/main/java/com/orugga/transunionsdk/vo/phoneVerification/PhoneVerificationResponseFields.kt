package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class PhoneVerificationResponseFields(
        @field:SerializedName("ApplicationId") val applicationId: String,
        @field:SerializedName("Applicants_IO") val applicantsIO: PhoneVerificationResponseApplicants

)
package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSDocAuthenticationDataResponse(
        @field:SerializedName("UploadIdDocumentFront") val uploadIdDocumentFront: DocResponse,
        @field:SerializedName("CreateCheckRequest") val createCheckRequest: DocResponse,
        @field:SerializedName("IDDocumentReportRequest") val iDDocumentReportRequest: DocResponse
)
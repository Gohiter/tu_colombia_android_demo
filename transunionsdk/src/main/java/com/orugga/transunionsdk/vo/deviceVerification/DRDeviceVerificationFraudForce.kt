package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName

data class DRDeviceVerificationFraudForce(
        @field:SerializedName("APIResponseTime") val apiResponseTime: String
)
package com.orugga.transunionsdk.vo.emailVerification

import com.google.gson.annotations.SerializedName

data class EmailVerificationFields(
        @field:SerializedName("Applicants_IO") val applicantsIO: EmailVerificationApplicants,
        @field:SerializedName("ServiceType") val serviceType: String
)
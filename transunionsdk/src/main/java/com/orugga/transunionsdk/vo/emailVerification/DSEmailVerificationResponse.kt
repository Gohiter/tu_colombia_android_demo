package com.orugga.transunionsdk.vo.emailVerification

import com.google.gson.annotations.SerializedName

data class DSEmailVerificationResponse(
        @field:SerializedName("RequestDateTime") val requestDateTime: String,
        @field:SerializedName("BasicCheckStatus") val basicCheckStatus: String,
        @field:SerializedName("BasicCheckStatusCode") val basicCheckStatusCode: String,
        @field:SerializedName("LongevityScore") val longevityScore: String,
        @field:SerializedName("VelocityScore") val velocityScore: String,
        @field:SerializedName("PopularityScore") val popularityScore: String,
        @field:SerializedName("DateFirstSeen") val dateFirstSeen: String,
        @field:SerializedName("BasicCheckStatusCodeDesc") val basicCheckStatusCodeDesc: String

)
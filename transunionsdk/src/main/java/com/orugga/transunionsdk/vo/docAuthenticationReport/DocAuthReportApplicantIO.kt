package com.orugga.transunionsdk.vo.docAuthenticationReport

import com.google.gson.annotations.SerializedName

data class DocAuthReportApplicantIO (
        @field:SerializedName("Applicant") val attributes: List<DocAuthReportApplicant>
)
package com.orugga.transunionsdk.vo.docAuthenticationReport

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.Document

data class Properties (
        //@field:SerializedName("nationality") val nationality: String,
        @field:SerializedName("last_name") val lastName: String,
        @field:SerializedName("issuing_country") val issuingCountry: String,
        @field:SerializedName("gender") val gender: String,
        @field:SerializedName("first_name") val firstName: String,
        @field:SerializedName("document_type") val documentType: String,
        @field:SerializedName("document_numbers") val documentNumbers: List<Document>,
        @field:SerializedName("date_of_expiry") val dateOfExpiry: String,
        @field:SerializedName("date_of_birth") val dateOfBirth: String,
        @field:SerializedName("address_lines") val addresslines: AddressLines?
)
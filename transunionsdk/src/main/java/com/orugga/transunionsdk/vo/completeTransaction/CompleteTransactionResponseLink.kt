package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class CompleteTransactionResponseLink(
        @field:SerializedName("ResendLinkWaitTime") val resendLinkWaitTime: String,
        @field:SerializedName("RetryAttempts") val retryAttemps: String
)
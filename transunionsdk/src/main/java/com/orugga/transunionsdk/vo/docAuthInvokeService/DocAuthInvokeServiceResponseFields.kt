package com.orugga.transunionsdk.vo.docAuthInvokeService

import com.google.gson.annotations.SerializedName

data class DocAuthInvokeServiceResponseFields(
        @field:SerializedName("identityVerification") val identityVerificationDocAuthInvokeService: DocAuthInvokeServiceResponseIdentityVerification,
        @field:SerializedName("Applicants_IO") val applicantsIO: String,
        @field:SerializedName("ApplicationId") val applicationId: String
)
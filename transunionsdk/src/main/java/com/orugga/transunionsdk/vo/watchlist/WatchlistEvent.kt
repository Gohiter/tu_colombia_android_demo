package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class WatchlistEvent(
        @field:SerializedName("category") val category: EventCategory,
        @field:SerializedName("eventDesc") val eventDesc: String,
        @field:SerializedName("eventDt") val eventDate: String,
        @field:SerializedName("subCategory") val subCategory: EventCategory,
        @field:SerializedName("EntityName") val entityName: String,
        @field:SerializedName("EntityId") val entityId: String,
        @field:SerializedName("Gender") val gender: String,
        @field:SerializedName("DatesOfBirth") val datesOfBirth: List<String>,
        @field:SerializedName("Alias") val alias: List<Alias>,
        @field:SerializedName("CountryCode") val countryCode: String
)
package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName

data class DeviceVerificationApplicantIO(
        @field:SerializedName("Applicant") val deviceVerificationApplicant: DeviceVerificationApplicant
)
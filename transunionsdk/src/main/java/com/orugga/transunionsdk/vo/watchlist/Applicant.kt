package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class Applicant(
        @field:SerializedName("ApplicantFirstName") val applicantFirstName: String,
        @field:SerializedName("ApplicantLastName") val applicantLastName: String
)
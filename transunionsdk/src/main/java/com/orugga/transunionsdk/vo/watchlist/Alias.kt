package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class Alias(
        @field:SerializedName("aliasTyp") val aliasType: String,
        @field:SerializedName("aliasName") val aliasName: String
)
package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSDeviceVerificationCKAddRegistration(
        @field:SerializedName("apiAccount") val apiAccount: String,
        @field:SerializedName("code") val code: String,
        @field:SerializedName("responseType") val responseType: String,
        @field:SerializedName("status") val status: String,
        @field:SerializedName("headsubscriberId") val headSubscriberId: String,
        @field:SerializedName("transactionId") val transactionId: String,
        @field:SerializedName("result") val result: String,
        @field:SerializedName("version") val version: String,
        @field:SerializedName("bodysubscriberId") val bodySubscriberId: String,
        @field:SerializedName("userAccountCode") val userAccountCode: String,
        @field:SerializedName("ttlSeconds") val ttlSeconds: String,
        @field:SerializedName("ttlType") val ttlType: String,
        @field:SerializedName("regDate") val regDate: String,
        @field:SerializedName("deviceType") val deviceType: String
)
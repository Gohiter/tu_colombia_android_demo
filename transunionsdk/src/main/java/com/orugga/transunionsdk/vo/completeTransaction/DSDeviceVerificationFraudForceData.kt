package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.deviceVerification.DeviceRequest

data class DSDeviceVerificationFraudForceData(
        @field:SerializedName("Request") val request: DeviceRequest,
        @field:SerializedName("Response") val response: DocResponse
)
package com.orugga.transunionsdk.vo.videoliveness

import com.google.gson.annotations.SerializedName

data class OnfidoVideoResponse(
        @field:SerializedName("live_videos") val live_videos: List<OnfidoVideoResponseOnfidoVideo>?
)
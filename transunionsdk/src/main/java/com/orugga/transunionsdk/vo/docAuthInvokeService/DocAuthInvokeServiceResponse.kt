package com.orugga.transunionsdk.vo.docAuthInvokeService

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo

data class DocAuthInvokeServiceResponse(
        @field:SerializedName("Status") val status: String,
        @field:SerializedName("ResponseInfo") val responseInfo: ResponseInfo,
        @field:SerializedName("Fields") val fields: DocAuthInvokeServiceResponseFields
)
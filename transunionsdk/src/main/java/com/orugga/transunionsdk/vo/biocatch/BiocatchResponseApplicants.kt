package com.orugga.transunionsdk.vo.biocatch

import com.google.gson.annotations.SerializedName

data class BiocatchResponseApplicants(
        @field:SerializedName("Applicant") val applicants: List<BiocatchResponseApplicantItem>
)
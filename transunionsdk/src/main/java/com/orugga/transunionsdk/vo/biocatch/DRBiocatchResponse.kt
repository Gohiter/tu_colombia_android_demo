package com.orugga.transunionsdk.vo.biocatch

import com.google.gson.annotations.SerializedName

data class DRBiocatchResponse(
        @field:SerializedName("IsBioCatchAPICallSuccess") val isBioCatchApiCallSuccess: String,
        @field:SerializedName("HighRiskScore") val highRiskScore: String,
        @field:SerializedName("APIResponseTime") val apiResponseTime: String,
        @field:SerializedName("BioCatchDecision") val biocatchDecision: String

)
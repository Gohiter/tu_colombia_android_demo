package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class WatchlistResponseAttributes(
        @field:SerializedName("DSWatchlistVerification") val dsWatchlistVerification: DSWatchlistVerification,
        @field:SerializedName("DRWatchlistVerification") val drWatchlistVerification: DRWatchlistVerification
)
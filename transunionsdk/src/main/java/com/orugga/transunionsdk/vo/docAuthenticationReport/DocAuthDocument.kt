package com.orugga.transunionsdk.vo.docAuthenticationReport

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo

data class DocAuthDocument (
        @field:SerializedName("Id") val id: String,
        @field:SerializedName("Error") val error: String
)
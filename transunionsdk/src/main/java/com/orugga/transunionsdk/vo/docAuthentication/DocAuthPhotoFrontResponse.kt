package com.orugga.transunionsdk.vo.docAuthentication

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.fileAttach.DocAttachPhotoResponseFields
import com.orugga.transunionsdk.vo.ResponseInfo

data class DocAuthPhotoFrontResponse(
        @field:SerializedName("Status") val status: String,
        @field:SerializedName("ResponseInfo") val responseInfo: ResponseInfo,
        @field:SerializedName("Fields") val fields: DocAttachPhotoResponseFields
)
package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DocMultiPartFormData(
        @field:SerializedName("Type") val type: String,
        @field:SerializedName("Key") val key: String,
        @field:SerializedName("Value") val value: String,
        @field:SerializedName("DocumentId") val documentId: String,
        @field:SerializedName("ContentType") val contentType: String
)
package com.orugga.transunionsdk.vo.facialSimilarityInvokeService

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo

data class FacialInvokeServiceResponse(
        @field:SerializedName("Status") val status: String,
        @field:SerializedName("ResponseInfo") val responseInfo: ResponseInfo,
        @field:SerializedName("Fields") val fields: FacialInvokeServiceResponseFields
)
package com.orugga.transunionsdk.vo.emailVerification

import com.google.gson.annotations.SerializedName

data class DREmailVerificationResponse(
        @field:SerializedName("IsEmailValid") val isEmailValid: String,
        @field:SerializedName("IsEmailBlackListCheckSuccess") val isEmailBlackListCheckSuccess: String,
        @field:SerializedName("IsEmailBlackListedEmailDomain") val isEmailBlackListedEmailDomain: String,
        @field:SerializedName("IsEmailBlackListedEmailAddress") val isEmailBlackListedEmailAddress: String,
        @field:SerializedName("IsEmailAPICallSuccess") val isEmailAPICallSuccess: String,
        @field:SerializedName("APIResponseInMS") val apiResponseInMS: String,
        @field:SerializedName("EmailBasic") val emailBasic: String,
        @field:SerializedName("EmailAdvance") val emailAdvance: String
)
package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class CompleteTransactionResponseApplicants(
        @field:SerializedName("Applicant") val applicant: List<CompleteTransactionResponseApplicant>
)
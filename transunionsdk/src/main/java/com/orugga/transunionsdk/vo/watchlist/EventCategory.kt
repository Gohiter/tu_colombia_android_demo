package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class EventCategory(
        @field:SerializedName("categoryDesc") val categoryDesc: String,
        @field:SerializedName("categoryCode") val categoryCode: String
)
package com.orugga.transunionsdk.vo.docAuthenticationReport

import com.google.gson.annotations.SerializedName

data class DocAuthReportResponseFields (
        @field:SerializedName("Applicants_IO") val reportApplicantIO: DocAuthReportApplicantIO,
        @field:SerializedName("ApplicationId") val applicationId: String
)
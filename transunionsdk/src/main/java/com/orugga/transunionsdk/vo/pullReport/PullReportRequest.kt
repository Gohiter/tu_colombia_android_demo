package com.orugga.transunionsdk.vo.pullReport

import com.google.gson.annotations.SerializedName

data class PullReportRequest(
        @field:SerializedName("Fields") val fields: PullReportRequestFields,
)
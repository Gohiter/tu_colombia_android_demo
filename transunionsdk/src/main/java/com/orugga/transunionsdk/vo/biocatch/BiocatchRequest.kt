package com.orugga.transunionsdk.vo.biocatch

import com.google.gson.annotations.SerializedName

data class BiocatchRequest(
        @field:SerializedName("Fields") val fields: BiocatchFields
)
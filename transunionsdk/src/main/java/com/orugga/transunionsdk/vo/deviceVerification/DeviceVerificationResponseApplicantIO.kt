package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName

data class DeviceVerificationResponseApplicantIO(
        @field:SerializedName("Applicant") val applicant: List<DeviceVerificationResponseApplicant>
)
package com.orugga.transunionsdk.vo.credoLab

import com.google.gson.annotations.SerializedName

data class CredoLabTokenRequest(
    @field:SerializedName("userEmail") val userEmail: String,
    @field:SerializedName("password") val password: String
)
package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo

data class DeviceVerificationResponse(
        @field:SerializedName("Status") val status: String,
        @field:SerializedName("ResponseInfo") val deviceVerificationResponseInfo: ResponseInfo,
        @field:SerializedName("Fields") val deviceVerificationResponseFields: DeviceVerificationResponseFields
)
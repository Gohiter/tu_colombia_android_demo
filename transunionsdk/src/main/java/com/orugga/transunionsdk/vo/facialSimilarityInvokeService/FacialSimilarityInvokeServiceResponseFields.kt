package com.orugga.transunionsdk.vo.facialSimilarityInvokeService

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceResponseFieldsApplicantsIO
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceResponseIdentityVerification

data class FacialSimilarityInvokeServiceResponseFields(
    @field:SerializedName("identityVerification") val identityVerificationDocAuthInvokeService: DocAuthInvokeServiceResponseIdentityVerification,
//    @field:SerializedName("Applicants_IO") val applicantsIO: DocAuthInvokeServiceResponseFieldsApplicantsIO,
//    @field:SerializedName("ApplicationId") val applicationId: String
)
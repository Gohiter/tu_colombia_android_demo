package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class WatchlistFields(
        @field:SerializedName("ServiceType") val serviceType: String = "WatchlistVerification",
        @field:SerializedName("Applicants_IO") val applicantIO: ApplicantIO
)
package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class Address(
        @field:SerializedName("AddressType") val addressType: String,
        @field:SerializedName("AddressLine1") val addressLine1: String,
        @field:SerializedName("AddressLine2") val addressLine2: String,
        @field:SerializedName("AddressLine3") val addressLine3: String,
        @field:SerializedName("City") val city: String,
        @field:SerializedName("PinCode") val pinCode: String,
        @field:SerializedName("State") val state: String,
        @field:SerializedName("StateCode") val stateCode: String
)
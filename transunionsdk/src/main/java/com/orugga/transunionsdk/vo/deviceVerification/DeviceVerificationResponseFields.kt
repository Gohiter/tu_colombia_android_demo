package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName

data class DeviceVerificationResponseFields(
        @field:SerializedName("Applicants_IO") val applicantIO: DeviceVerificationResponseApplicantIO,
        @field:SerializedName("ApplicationId") val applicationId: String
)
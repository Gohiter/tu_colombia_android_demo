package com.orugga.transunionsdk.vo.pullReport

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.docAuthenticationReport.DocAuthReportVerification
import com.orugga.transunionsdk.vo.facialSimilarityReport.DSFacialSimilarityReport

data class PullReportAttributes(
        @field:SerializedName("DSFacialSimilarityReport") val dSFacialSimilarityReport: DSFacialSimilarityReport?,
        @field:SerializedName("DSIdDocumentReport") val dSIdDocumentReport: DocAuthReportVerification?
)
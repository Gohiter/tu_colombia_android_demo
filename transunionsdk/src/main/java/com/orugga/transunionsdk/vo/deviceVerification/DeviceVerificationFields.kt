package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName

data class DeviceVerificationFields(
        @field:SerializedName("ServiceType") val serviceType: String? = "DeviceVerification_FraudForce",
        @field:SerializedName("Applicants_IO") val applicantsIO: DeviceVerificationApplicantIO
)
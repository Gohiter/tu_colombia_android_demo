package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSEmailVerificationDataRequest(
        @field:SerializedName("APIKey") val apiKey: String,
        @field:SerializedName("Email") val email: String
)
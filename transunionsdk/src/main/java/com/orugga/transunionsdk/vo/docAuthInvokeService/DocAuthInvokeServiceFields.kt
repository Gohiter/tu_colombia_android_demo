package com.orugga.transunionsdk.vo.docAuthInvokeService

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.createApp.ApplicantIO

data class DocAuthInvokeServiceFields(
        @field:SerializedName("ServiceType") val serviceType: String = "DocAuthVerification",
        @field:SerializedName("Applicants_IO") val applicantIO: ApplicantIO
)
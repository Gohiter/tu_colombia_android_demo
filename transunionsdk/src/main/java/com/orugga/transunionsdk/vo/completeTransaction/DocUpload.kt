package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DocUpload(
        @field:SerializedName("Authorization") val authorization: String,
        @field:SerializedName("applicant_id") val applicantId: String,
        @field:SerializedName("MultiPartFormData") val multiPartFormData: List<DocMultiPartFormData>
)
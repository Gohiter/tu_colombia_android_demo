package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.biocatch.DRBiocatchResponse
import com.orugga.transunionsdk.vo.biocatch.DSBiocatchResponse
import com.orugga.transunionsdk.vo.deviceVerification.DRDeviceVerificationFraudForce
import com.orugga.transunionsdk.vo.deviceVerification.DSDeviceVerificationFraudForce
import com.orugga.transunionsdk.vo.docAuthenticationReport.DocAuthReportVerification
import com.orugga.transunionsdk.vo.emailVerification.DREmailVerificationResponse
import com.orugga.transunionsdk.vo.emailVerification.DSEmailVerificationResponse
import com.orugga.transunionsdk.vo.facialSimilarityReport.DSFacialSimilarityReport
import com.orugga.transunionsdk.vo.phoneVerification.DRPhoneVerificationResponse
import com.orugga.transunionsdk.vo.phoneVerification.DSPhoneVerificationResponse
import com.orugga.transunionsdk.vo.watchlist.DRWatchlistVerification
import com.orugga.transunionsdk.vo.watchlist.DSWatchlistVerification

data class CompleteTransactionResponseAttributes(
    @field:SerializedName("DSEmailVerification") val dsEmailVerification: DSEmailVerificationResponse,
    @field:SerializedName("DREmailVerification") val drEmailVerification: DREmailVerificationResponse,
    @field:SerializedName("CreditBureau") val creditBureau: CompleteTransactionResponseCreditBureau,
    @field:SerializedName("DSDeviceVerification_FraudForce") val dsDeviceVerification: DSDeviceVerificationFraudForce,
    @field:SerializedName("DRDeviceVerification_FraudForce") val drDeviceVerification: DRDeviceVerificationFraudForce,
    @field:SerializedName("DSDeviceVerification_CKAddRegistration") val dsDeviceVerificationCKAddRegistration: DSDeviceVerificationCKAddRegistration,
    @field:SerializedName("DRDeviceVerification_CKAddRegistration") val drDeviceVerificationCKAddRegistration: DRDeviceVerificationCKAddRegistration,
    @field:SerializedName("DSDeviceVerification_CKCheckRegistration") val dsDeviceVerificationCKCheckRegistration: DSDeviceVerificationCKCheckRegistration,
    @field:SerializedName("DRDeviceVerification_CKCheckRegistration") val drDeviceVerificationCKCheckRegistration: DRDeviceVerificationCKCheckRegistration,
    @field:SerializedName("DSPhoneVerification") val dsPhoneVerification: DSPhoneVerificationResponse,
    @field:SerializedName("DRPhoneVerification") val drPhoneVerification: DRPhoneVerificationResponse,
    @field:SerializedName("DSWatchlistVerification") val dsWatchlistVerification: DSWatchlistVerification,
    @field:SerializedName("DRWatchlistVerification") val drWatchlistVerification: DRWatchlistVerification,
    @field:SerializedName("DRDocAuthentication") val drDocAuthentication: DRDocAuthentication,
    @field:SerializedName("DSDocAuthentication") val dsDocAuthentication: DSDocAuthentication,
    @field:SerializedName("DSFacialSimilarity") val dsFacialSimilarity: DSFacialSimilarity,
    @field:SerializedName("DSFacialSimilarityReport") val dSFacialSimilarityReport: DSFacialSimilarityReport?,
    @field:SerializedName("DSIdDocumentReport") val dSIdDocumentReport: DocAuthReportVerification?,
    @field:SerializedName("DSBehavioralBiometrics") val dsBiocatch: DSBiocatchResponse,
    @field:SerializedName("DRBehavioralBiometrics") val drBiocatch: DRBiocatchResponse
)
package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class CompleteTransactionIdentifiers(
        @field:SerializedName("Identifier") val identifier: CompleteTransactionIdentifier
)
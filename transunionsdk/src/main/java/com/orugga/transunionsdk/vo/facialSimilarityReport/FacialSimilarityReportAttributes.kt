package com.orugga.transunionsdk.vo.facialSimilarityReport

import com.google.gson.annotations.SerializedName

data class FacialSimilarityReportAttributes(
        @field:SerializedName("DSFacialSimilarity") val dSFacialSimilarity: DSFacialSimilarityReport
)
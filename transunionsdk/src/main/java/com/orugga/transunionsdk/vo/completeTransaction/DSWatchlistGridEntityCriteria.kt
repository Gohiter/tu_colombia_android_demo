package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSWatchlistGridEntityCriteria(
        @field:SerializedName("name") val name: String,
        @field:SerializedName("entityTyp") val entityType: String,
        @field:SerializedName("exactNameMatch") val exactNameMatch: String,
        @field:SerializedName("birthDt") val birthDate: String
)
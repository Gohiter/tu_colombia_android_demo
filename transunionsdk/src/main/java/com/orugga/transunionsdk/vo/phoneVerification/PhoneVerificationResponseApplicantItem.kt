package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class PhoneVerificationResponseApplicantItem(
        @field:SerializedName("Attributes") val attributes: PhoneVerificationResponseAttributes,
        @field:SerializedName("DSPhoneVerificationStatus") val status: DSPhoneVerificationStatusResponse
)
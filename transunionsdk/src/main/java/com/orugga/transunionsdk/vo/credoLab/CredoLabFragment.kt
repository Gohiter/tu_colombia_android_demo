package com.orugga.transunionsdk.vo.credoLab

import com.google.gson.annotations.SerializedName

data class CredoLabFragment(
    @field:SerializedName("code") val code: String,
    @field:SerializedName("items") val items: List<CredoLabFragmentItem>
)
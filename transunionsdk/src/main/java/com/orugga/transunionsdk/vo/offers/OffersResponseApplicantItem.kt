package com.orugga.transunionsdk.vo.offers

import com.google.gson.annotations.SerializedName

data class OffersResponseApplicantItem(
        @field:SerializedName("MyOffers") val myOffers: OffersResponseMyOffers
)
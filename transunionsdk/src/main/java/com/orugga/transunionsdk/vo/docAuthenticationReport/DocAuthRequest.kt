package com.orugga.transunionsdk.vo.docAuthenticationReport

import com.google.gson.annotations.SerializedName

data class DocAuthRequest (
        @field:SerializedName("Fields") val status: DocAuthRequestField
)
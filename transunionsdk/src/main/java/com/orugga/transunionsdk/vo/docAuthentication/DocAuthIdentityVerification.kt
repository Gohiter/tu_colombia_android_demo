package com.orugga.transunionsdk.vo.docAuthentication

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.fileAttach.DocAttachPhoto

data class DocAuthIdentityVerification(
        @field:SerializedName("Selfie") val selfie: DocAttachPhoto? = null,
        @field:SerializedName("Front") val front: DocAttachPhoto? = null,
        @field:SerializedName("Back") val back: DocAttachPhoto? = null,
        @field:SerializedName("Type") val type: String? = null,
        @field:SerializedName("Step") val step: String,
        @field:SerializedName("CurrentStep") val currentStep: String
)
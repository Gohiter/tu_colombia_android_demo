package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSFacialSimilarityDataResponse(
        @field:SerializedName("UploadSelfieResponse") val uploadSelfieFront: DocResponse,
        @field:SerializedName("CreateFRCheckResponse") val createCheckRequest: DocResponse
)
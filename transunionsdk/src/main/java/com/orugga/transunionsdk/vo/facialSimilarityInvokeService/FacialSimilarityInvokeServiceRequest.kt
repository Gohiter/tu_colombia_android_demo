package com.orugga.transunionsdk.vo.facialSimilarityInvokeService

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.createApp.ApplicantIO

data class FacialSimilarityInvokeServiceRequest(
        @field:SerializedName("ServiceType") val ServiceType: String = "FacialSimilarityReport",
        @field:SerializedName("Applicants_IO") val applicantIO: ApplicantIO
)
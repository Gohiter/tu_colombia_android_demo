package com.orugga.transunionsdk.vo.createApp

import com.google.gson.annotations.SerializedName

data class Applicant(
        @field:SerializedName("ApplicantFirstName") val applicantFirstName: String,
        @field:SerializedName("ApplicantLastName") val applicantLastName: String
)
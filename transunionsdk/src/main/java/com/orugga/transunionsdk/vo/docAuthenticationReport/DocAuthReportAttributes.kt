package com.orugga.transunionsdk.vo.docAuthenticationReport

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.Document

data class DocAuthReportAttributes (
        @field:SerializedName("DSDocAuthVerification") val dSDocAuthReportVerification: DocAuthReportVerification,
        @field:SerializedName("DSDocAuthenticationStatus") val dSDocAuthReportStatus: DocAuthReportStatus,
)
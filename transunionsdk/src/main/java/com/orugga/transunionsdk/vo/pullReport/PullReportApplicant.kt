package com.orugga.transunionsdk.vo.pullReport

import com.google.gson.annotations.SerializedName

data class PullReportApplicant(
        @field:SerializedName("Attributes") val attributes: PullReportAttributes
)
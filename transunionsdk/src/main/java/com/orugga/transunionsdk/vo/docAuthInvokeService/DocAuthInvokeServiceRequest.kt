package com.orugga.transunionsdk.vo.docAuthInvokeService

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.createApp.ApplicantIO

data class DocAuthInvokeServiceRequest(
        @field:SerializedName("Fields") val fields: DocAuthInvokeServiceFields
)
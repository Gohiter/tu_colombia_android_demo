package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSDocAuthenticationIdDocumentReportRequest(
        @field:SerializedName("Authorization") val authorization: String,
        @field:SerializedName("check_id") val checkId: String,
        @field:SerializedName("report_id") val reportId: String
)
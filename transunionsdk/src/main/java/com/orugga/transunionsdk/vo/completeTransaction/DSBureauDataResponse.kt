package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSBureauDataResponse(
        @field:SerializedName("IsSuccess") val isSuccess: String,
        @field:SerializedName("RawResponse") val rawResponse: String
)
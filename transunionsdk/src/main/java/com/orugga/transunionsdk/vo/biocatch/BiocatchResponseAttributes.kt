package com.orugga.transunionsdk.vo.biocatch

import com.google.gson.annotations.SerializedName

data class BiocatchResponseAttributes(
        @field:SerializedName("DSBehavioralBiometrics") val dsBiocatch: DSBiocatchResponse,
        @field:SerializedName("DRBehavioralBiometrics") val drBiocatch: DRBiocatchResponse
)
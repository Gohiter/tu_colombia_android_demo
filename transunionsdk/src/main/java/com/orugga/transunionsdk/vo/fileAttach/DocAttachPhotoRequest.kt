package com.orugga.transunionsdk.vo.fileAttach

import com.google.gson.annotations.SerializedName

//TODO: SET STEP FRONT AND CURRENT FRONT
data class DocAttachPhotoRequest(
        @field:SerializedName("Fields") val fields: DocAttachPhotoFields
)
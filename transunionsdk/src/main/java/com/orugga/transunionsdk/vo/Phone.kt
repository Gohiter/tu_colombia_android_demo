package com.orugga.transunionsdk.vo

data class Phone(
        val countryCode: String,
        val nationalNumber: String
) {
        fun getPhoneNumber(): String {
                return "+".plus(countryCode).plus(nationalNumber)
        }
}
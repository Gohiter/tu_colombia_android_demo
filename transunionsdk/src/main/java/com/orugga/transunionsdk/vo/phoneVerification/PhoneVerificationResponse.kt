package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo

data class PhoneVerificationResponse(
    @field:SerializedName("Status") val status: String,
    @field:SerializedName("ResponseInfo") val responseInfo: ResponseInfo,
    @field:SerializedName("Fields") val fields: PhoneVerificationResponseFields
)
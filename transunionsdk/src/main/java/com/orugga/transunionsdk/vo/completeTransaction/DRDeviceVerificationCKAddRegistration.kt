package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DRDeviceVerificationCKAddRegistration(
        @field:SerializedName("APIResponseTime") val apiResponseTime: String
)
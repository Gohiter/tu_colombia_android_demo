package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class PhoneVerificationResponseAttributes(
        @field:SerializedName("DSPhoneVerification") val dsVerification: DSPhoneVerificationResponse,
        @field:SerializedName("DRPhoneVerification") val drVerification: DRPhoneVerificationResponse
)
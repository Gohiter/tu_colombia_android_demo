package com.orugga.transunionsdk.vo.createApp

import com.google.gson.annotations.SerializedName

data class ApplicantIO(
        @field:SerializedName("Applicant") val applicant: Applicant
)
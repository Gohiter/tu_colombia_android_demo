package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class CompleteTransactionIdentifier(
        @field:SerializedName("IdType") val idType: String,
        @field:SerializedName("IdNumber") val idNumber: String
)
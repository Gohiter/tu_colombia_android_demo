package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName

data class DSDeviceVerificationFraudForceStatus(
        @field:SerializedName("Outcome") val outcome: String,
        @field:SerializedName("isSuccess") val isSuccess: String
)
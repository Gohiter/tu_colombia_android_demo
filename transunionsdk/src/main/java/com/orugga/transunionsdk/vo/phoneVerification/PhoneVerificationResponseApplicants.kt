package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class PhoneVerificationResponseApplicants(
        @field:SerializedName("Applicant") val applicants: List<PhoneVerificationResponseApplicantItem>
)
package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class PhoneVerificationTelephoneItem(
    @field:SerializedName("TelephoneNumber") val telephoneNumber: String,
    @field:SerializedName("TelephoneType") val telephoneType: String,
    @field:SerializedName("TelephoneCountryCode") val telephoneCountryCode: String
)
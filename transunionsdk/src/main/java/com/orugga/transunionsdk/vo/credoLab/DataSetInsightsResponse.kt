package com.orugga.transunionsdk.vo.credoLab

import com.google.gson.annotations.SerializedName

data class DataSetInsightsResponse(
    @field:SerializedName("requestedDate") val requestedDate: String,
    @field:SerializedName("requestor") val requestor: String,
    @field:SerializedName("datasetInfo") val datasetInfo: DataSetInfo,
    @field:SerializedName("scores") val scores: List<CredoLabScores>,
    @field:SerializedName("fragments") val fragments: List<CredoLabFragment>
)
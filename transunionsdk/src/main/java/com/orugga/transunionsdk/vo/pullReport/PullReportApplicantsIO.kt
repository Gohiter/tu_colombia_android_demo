package com.orugga.transunionsdk.vo.pullReport

import com.google.gson.annotations.SerializedName

data class PullReportApplicantsIO(
        @field:SerializedName("Applicant") val applicant: List<PullReportApplicant>
)
package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class DSWatchlistVerificationStatus(
        @field:SerializedName("OutCome") val outCome: String,
        @field:SerializedName("IsSuccess") val isSuccess: String
)
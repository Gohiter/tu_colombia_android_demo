package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class ApplicantIOResponse(
        @field:SerializedName("Applicant") val applicant: List<ApplicantResponse>
)
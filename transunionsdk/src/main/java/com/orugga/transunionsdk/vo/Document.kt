package com.orugga.transunionsdk.vo

import com.google.gson.annotations.SerializedName

data class Document(
        @field:SerializedName("value") val value: String,
        @field:SerializedName("type") val type: String
)
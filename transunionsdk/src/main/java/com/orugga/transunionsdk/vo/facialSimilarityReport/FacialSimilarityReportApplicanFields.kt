package com.orugga.transunionsdk.vo.facialSimilarityReport

import com.google.gson.annotations.SerializedName

data class FacialSimilarityReportApplicanFields(
        @field:SerializedName("Applicants_IO") val dsFacialSimilarityApplicantIO: FacialSimilarityReportApplicantIO,
        @field:SerializedName("ApplicationId") val applicationId: String
)
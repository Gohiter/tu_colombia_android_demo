package com.orugga.transunionsdk.vo.biocatch

import com.google.gson.annotations.SerializedName

data class BiocatchResponseFields(
        @field:SerializedName("Applicants_IO") val applicantsIO: BiocatchResponseApplicants

)
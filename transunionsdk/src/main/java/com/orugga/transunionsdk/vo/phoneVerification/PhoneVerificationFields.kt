package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class PhoneVerificationFields(
        @field:SerializedName("Applicants_IO") val applicantsIO: PhoneVerificationApplicants,
        @field:SerializedName("ServiceType") val serviceType: String
)
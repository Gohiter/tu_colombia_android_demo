package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class DSPhoneVerificationResponse(
        @field:SerializedName("PhoneCountryCode") val phoneCountryCode: String,
        @field:SerializedName("PhoneNumber") val phoneNumber: String,
        @field:SerializedName("PhoneTypeCode") val phoneTypeCode: String,
        @field:SerializedName("PhoneTypeDescription") val phoneTypeDescription: String,
        @field:SerializedName("PhoneCity") val phoneCity: String,
        @field:SerializedName("PhoneState") val phoneState: String,
        @field:SerializedName("PhoneZip") val phoneZip: String,
        @field:SerializedName("PhoneCounty") val phoneCounty: String,
        @field:SerializedName("PhoneCountry") val phoneCountry: String,
        @field:SerializedName("PhoneCountryCodeISO2") val phoneCountryCodeISO2: String,
        @field:SerializedName("PhoneCountryCodeISO3") val phoneCountryCodeISO3: String,
        @field:SerializedName("Carrier") val carrier: String,
        @field:SerializedName("IsPhoneBlocked") val isPhoneBlocked: String,
        @field:SerializedName("PhoneBlockCode") val phoneBlockCode: String,
        @field:SerializedName("PhoneBlockDescription") val phoneBlockDescription: String,
        @field:SerializedName("PhoneRiskLevel") val phoneRiskLevel: String,
        @field:SerializedName("PhoneRiskScore") val phoneRiskScore: String,
        @field:SerializedName("PhoneRiskRecommendation") val phoneRiskRecommendation: String,
        @field:SerializedName("TimeOfRequest") val timeOfRequest: String,
        @field:SerializedName("APIResponseTime") val apiResponseTime: String

)
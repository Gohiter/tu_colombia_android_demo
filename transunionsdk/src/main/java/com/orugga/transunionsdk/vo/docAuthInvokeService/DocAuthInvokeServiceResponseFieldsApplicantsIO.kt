package com.orugga.transunionsdk.vo.docAuthInvokeService

import com.google.gson.annotations.SerializedName

data class DocAuthInvokeServiceResponseFieldsApplicantsIO(
    @field:SerializedName("ApplicationId") val applicationId: String
)
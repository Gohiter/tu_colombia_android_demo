package com.orugga.transunionsdk.vo.pullReport

import com.google.gson.annotations.SerializedName

data class PullReportFields(
        @field:SerializedName("Applicants_IO") val applicantIO: PullReportApplicantsIO,
        @field:SerializedName("ApplicationId") val applicationId: String
)
package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSFacialSimilarityData(
        @field:SerializedName("Request") val request: DSFacialSimilarityDataRequest,
        @field:SerializedName("Response") val response: DSFacialSimilarityDataResponse
)
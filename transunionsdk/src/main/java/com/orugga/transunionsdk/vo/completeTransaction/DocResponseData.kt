package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DocResponseData(
        @field:SerializedName("RawResponse") val rawResponse: String,
        @field:SerializedName("Status") val status: String,
        @field:SerializedName("GatewayTransactionId") val gatewayTransactionId: String
)
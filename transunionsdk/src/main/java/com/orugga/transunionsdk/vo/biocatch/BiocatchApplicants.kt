package com.orugga.transunionsdk.vo.biocatch

import com.google.gson.annotations.SerializedName

data class BiocatchApplicants(
        @field:SerializedName("Applicant") val applicants: List<BiocatchApplicantItem>
)
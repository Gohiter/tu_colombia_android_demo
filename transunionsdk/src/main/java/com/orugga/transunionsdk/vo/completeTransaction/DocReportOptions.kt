package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DocReportOptions(
        @field:SerializedName("name") val name: String
)
package com.orugga.transunionsdk.vo.credoLab

import com.google.gson.annotations.SerializedName

data class CredoLabPermission(
    @field:SerializedName("name") val name: String,
    @field:SerializedName("value") val value: String
)
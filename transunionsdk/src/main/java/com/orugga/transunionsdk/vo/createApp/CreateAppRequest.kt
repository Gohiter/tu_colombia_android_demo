package com.orugga.transunionsdk.vo.createApp

import com.google.gson.annotations.SerializedName

data class CreateAppRequest(
        @field:SerializedName("RequestInfo") val requestInfo: CreateAppRequestInfo,
        @field:SerializedName("Fields") val requestFields: CreateAppFields
)
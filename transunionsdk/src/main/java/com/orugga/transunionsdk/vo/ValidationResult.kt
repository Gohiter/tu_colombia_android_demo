package com.orugga.transunionsdk.vo

enum class ValidationResult() {
    TOO_DARK, BLURRED, OFF_CENTER, MOVE_AWAY, MOVE_CLOSER, NO_CONTOUR, ADJUST_ANGLE, NO_MESSAGE
}
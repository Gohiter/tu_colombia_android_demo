package com.orugga.transunionsdk.vo.videoliveness

import com.google.gson.annotations.SerializedName

data class OnfidoVideoResponseOnfidoVideo(
        @field:SerializedName("id") val id: String?,
        @field:SerializedName("created_at") val createdAt: String?,
        @field:SerializedName("file_name") val fileName: String?,
        @field:SerializedName("file_type") val fileType: String?,
        @field:SerializedName("file_size") val fileSize: String?,
        @field:SerializedName("href") val href: String?,
        @field:SerializedName("download_href") val downloadHref: String?,


)
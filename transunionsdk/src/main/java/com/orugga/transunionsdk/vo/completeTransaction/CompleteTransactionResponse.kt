package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo

data class CompleteTransactionResponse(
        @field:SerializedName("Status") val status: String,
        @field:SerializedName("ResponseInfo") val responseInfo: ResponseInfo,
        @field:SerializedName("Fields") val responseFields: CompleteTransactionResponseFields
)
package com.orugga.transunionsdk.vo
import com.google.gson.annotations.SerializedName

data class TokenResponse(
    @field:SerializedName("access_token") val access_token: String,
    @field:SerializedName("token_type") val token_type: String,
    @field:SerializedName("refresh_token") val refresh_token: String,
    @field:SerializedName("expires_in") val expires_in: String,
    @field:SerializedName("scope") val scope: String)

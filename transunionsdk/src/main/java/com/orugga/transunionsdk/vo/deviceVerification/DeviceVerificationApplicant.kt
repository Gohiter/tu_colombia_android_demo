package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName

data class DeviceVerificationApplicant(
        @field:SerializedName("DeviceRequest") val deviceRequest: DeviceRequest
)
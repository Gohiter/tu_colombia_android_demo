package com.orugga.transunionsdk.vo.biocatch

import com.google.gson.annotations.SerializedName

data class BiocatchFields(
        @field:SerializedName("Applicants_IO") val applicantsIO: BiocatchApplicants,
        @field:SerializedName("ServiceType") val serviceType: String = "BEHAVIORALBIOMETRICSVERIFICATION"
)
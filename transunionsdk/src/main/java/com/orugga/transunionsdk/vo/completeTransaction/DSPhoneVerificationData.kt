package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSPhoneVerificationData(
        @field:SerializedName("Request") val request: DSPhoneVerificationDataRequest,
        @field:SerializedName("Response") val response: DocResponse
)
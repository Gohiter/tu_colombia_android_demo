package com.orugga.transunionsdk.vo.createDocument

import com.google.gson.annotations.SerializedName

data class CreateDocumentRequest(
        @field:SerializedName("Description") val description: String,
        @field:SerializedName("FileName") val fileName: String,
        @field:SerializedName("Note") val note: String
)
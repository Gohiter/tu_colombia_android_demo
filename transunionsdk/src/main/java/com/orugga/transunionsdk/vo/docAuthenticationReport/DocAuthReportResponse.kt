package com.orugga.transunionsdk.vo.docAuthenticationReport

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo

data class DocAuthReportResponse (
        @field:SerializedName("Status") val status: String,
        @field:SerializedName("ResponseInfo") val responseInfo: ResponseInfo,
        @field:SerializedName("Fields") val fieldsReport: DocAuthReportResponseFields
)
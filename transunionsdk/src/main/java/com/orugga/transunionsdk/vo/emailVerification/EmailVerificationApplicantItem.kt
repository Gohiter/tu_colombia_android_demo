package com.orugga.transunionsdk.vo.emailVerification

import com.google.gson.annotations.SerializedName

data class EmailVerificationApplicantItem(
    @field:SerializedName("EmailAddress") val email: String
)
package com.orugga.transunionsdk.vo.emailVerification

import com.google.gson.annotations.SerializedName

data class EmailVerificationResponseFields(
        @field:SerializedName("Applicants_IO") val applicantsIO: EmailVerificationResponseApplicants,
        @field:SerializedName("ApplicationId") val applicationId: String

)
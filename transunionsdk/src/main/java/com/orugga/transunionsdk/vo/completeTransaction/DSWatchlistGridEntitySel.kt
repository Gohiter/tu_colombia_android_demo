package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSWatchlistGridEntitySel(
        @field:SerializedName("gridEntityKeys") val gridEntityKeys: DSWatchlistGridEntityKeys
)
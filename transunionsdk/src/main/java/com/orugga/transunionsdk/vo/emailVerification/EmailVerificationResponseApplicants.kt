package com.orugga.transunionsdk.vo.emailVerification

import com.google.gson.annotations.SerializedName

data class EmailVerificationResponseApplicants(
        @field:SerializedName("Applicant") val applicants: List<EmailVerificationResponseApplicantItem>
)
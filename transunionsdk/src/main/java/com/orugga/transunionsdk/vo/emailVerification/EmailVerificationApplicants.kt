package com.orugga.transunionsdk.vo.emailVerification

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.phoneVerification.PhoneVerificationApplicantItem

data class EmailVerificationApplicants(
        @field:SerializedName("Applicant") val applicant: EmailVerificationApplicantItem
)
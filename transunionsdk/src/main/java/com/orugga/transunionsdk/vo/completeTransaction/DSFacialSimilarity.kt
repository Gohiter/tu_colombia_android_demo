package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSFacialSimilarity(
        @field:SerializedName("DALivePhotoId") val daLivePhotoId: String,
        @field:SerializedName("DALivePhotoId2") val daLivePhotoId2: String,
        @field:SerializedName("DASelfieCheckId") val daSelfieCheckId: String,
        @field:SerializedName("DASelfieCheckId2") val daSelfieCheckId2: String,
        @field:SerializedName("DAFRReportId") val daFRReportId: String,
        @field:SerializedName("DAFRReportId2") val daFRReportId2: String
)
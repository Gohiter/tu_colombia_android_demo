package com.orugga.transunionsdk.vo.emailVerification

import com.google.gson.annotations.SerializedName

data class EmailVerificationResponseApplicantItem(
        @field:SerializedName("Attributes") val attributes: EmailVerificationResponseAttributes,
        @field:SerializedName("DSEmailVerificationStatus") val status: DSEmailVerificationStatusResponse
)
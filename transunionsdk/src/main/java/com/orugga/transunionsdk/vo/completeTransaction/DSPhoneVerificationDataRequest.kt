package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSPhoneVerificationDataRequest(
        @field:SerializedName("complete_phone_number") val completePhoneNumber: String,
        @field:SerializedName("Authorization") val authorization: String,
        @field:SerializedName("account_lifecycle_event") val accountLifecycleEvent: String,
        @field:SerializedName("MultiPartFormData") val multiPartFormData: List<DocMultiPartFormData>
)
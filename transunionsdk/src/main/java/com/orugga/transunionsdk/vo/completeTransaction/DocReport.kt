package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DocReport(
        @field:SerializedName("name") val name: String,
        @field:SerializedName("options") val options: List<DocReportOptions>
)
package com.orugga.transunionsdk.vo.biocatch

import com.google.gson.annotations.SerializedName

data class BiocatchResponseApplicantItem(
        @field:SerializedName("Attributes") val attributes: BiocatchResponseAttributes,
        @field:SerializedName("DSBehavioralBiometricsStatus") val status: DSBiocatchStatusResponse
)
package com.orugga.transunionsdk.vo.fileAttach

import com.google.gson.annotations.SerializedName

data class DocAttachPhoto(
        @field:SerializedName("Id") val id: String,
        @field:SerializedName("Error") val error: String
)
package com.orugga.transunionsdk.vo.credoLab

import com.google.gson.annotations.SerializedName

data class DataSetInfoDevice(
    @field:SerializedName("identifier") val identifier: String,
    @field:SerializedName("model") val model: String,
    @field:SerializedName("brand") val brand: String,
    @field:SerializedName("associatedDataSetsCount") val associatedDataSetsCount: String,
    @field:SerializedName("os") val os: String,
    @field:SerializedName("browser") val browser: String
)
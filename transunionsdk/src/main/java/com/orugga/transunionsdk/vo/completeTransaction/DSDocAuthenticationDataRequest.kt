package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSDocAuthenticationDataRequest(
        @field:SerializedName("UploadIdDocumentFront") val uploadIdDocumentFront: DocUpload,
        @field:SerializedName("CreateCheckRequest") val createCheckRequest: DocCheckRequest,
        @field:SerializedName("IDDocumentReportRequest") val iDDocumentReportRequest: DSDocAuthenticationIdDocumentReportRequest
)
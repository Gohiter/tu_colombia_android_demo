package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName

data class DeviceVerificationResponseApplicantAttributes(
        @field:SerializedName("DSDeviceVerification_FraudForce") val dsPhoneVerification: DSDeviceVerificationFraudForce
)
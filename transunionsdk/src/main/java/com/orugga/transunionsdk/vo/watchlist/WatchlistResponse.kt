package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo

data class WatchlistResponse(
        @field:SerializedName("Fields") val responseFields: WatchlistResponseFields,
        @field:SerializedName("ResponseInfo") val responseInfo: ResponseInfo,
        @field:SerializedName("Status") val status: String
)
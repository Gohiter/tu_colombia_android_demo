package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DRDocAuthentication(
        @field:SerializedName("CreateApplicant2APIResponseTime") val createApplicant2APIResponseTime: String,
        @field:SerializedName("CreateApplicantAPIResponseTime") val createApplicantAPIResponseTime: String,
        @field:SerializedName("DAResult") val daResult: String,
        @field:SerializedName("isFRChecked") val isFRChecked: String,
        @field:SerializedName("FSReport2APIResponseTime") val fsReport2APIResponseTime: String,
        @field:SerializedName("FSReportAPIResponseTime") val fsReportAPIResponseTime: String,
        @field:SerializedName("Id2DocCheckAPIResponseTime") val id2DocCheckAPIResponseTime: String,
        @field:SerializedName("Id2DocReportAPIResponseTime") val id2DocReportAPIResponseTime: String,
        @field:SerializedName("IdDocCheckAPIResponseTime") val idDocCheckAPIResponseTime: String,
        @field:SerializedName("IdDocReportAPIResponseTime") val idDocReportAPIResponseTime: String,
        @field:SerializedName("IdDocument2Type") val idDocument2Type: String,
        @field:SerializedName("IdDocumentType") val idDocumentType: String,
        @field:SerializedName("IdFrontUploadAPIResponseTime") val idFrontUploadAPIResponseTime: String,
        @field:SerializedName("Id2BackUploadAPIResponseTime") val id2BackUploadAPIResponseTime: String,
        @field:SerializedName("IdBackUploadAPIResponseTime") val idBackUploadAPIResponseTime: String,
        @field:SerializedName("Selfie2UploadAPIResponseTime") val selfie2UploadAPIResponseTime: String,
        @field:SerializedName("SelfieUploadAPIResponseTime") val selfieUploadAPIResponseTime: String,
        @field:SerializedName("Selfie2CheckAPIResponseTime") val selfie2CheckAPIResponseTime: String,
        @field:SerializedName("SelfieCheckAPIResponseTime") val selfieCheckAPIResponseTime: String
)
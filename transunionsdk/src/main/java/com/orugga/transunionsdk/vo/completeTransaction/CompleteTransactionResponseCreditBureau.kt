package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class CompleteTransactionResponseCreditBureau(
        @field:SerializedName("TotalOutstanding") val totalOutstanding: String,
        @field:SerializedName("NumberOfOpenAccounts") val numberOfOpenAccounts: String,
        @field:SerializedName("CreditScore") val creditScore: String
)
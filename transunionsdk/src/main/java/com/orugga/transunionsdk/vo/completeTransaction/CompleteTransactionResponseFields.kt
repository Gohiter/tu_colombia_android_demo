package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.offers.OffersResponseMyOffers

data class CompleteTransactionResponseFields(
        @field:SerializedName("Applicants") val applicants: CompleteTransactionResponseApplicants,
        @field:SerializedName("ApplicationId") val applicationId: String,
        @field:SerializedName("MyOffers") val myOffers: OffersResponseMyOffers,
        @field:SerializedName("FraudForce") val fraudForce: String
)
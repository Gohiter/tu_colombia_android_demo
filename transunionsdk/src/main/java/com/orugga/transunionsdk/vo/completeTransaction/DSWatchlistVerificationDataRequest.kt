package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSWatchlistVerificationDataRequest(
        @field:SerializedName("interfaceVersion") val interfaceVersion: String,
        @field:SerializedName("credentialsRqHdr") val credentialsRqHdr: DSWatchlistCredentialsRequestHeader,
        @field:SerializedName("gridEntitySel") val gridEntitySel: DSWatchlistGridEntitySel
)
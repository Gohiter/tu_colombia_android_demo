package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSWatchlistCredentialsToken(
        @field:SerializedName("loginName") val loginName: String,
        @field:SerializedName("pswd") val password: String
)
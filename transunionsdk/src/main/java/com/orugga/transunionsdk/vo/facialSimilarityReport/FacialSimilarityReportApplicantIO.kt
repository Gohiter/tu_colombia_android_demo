package com.orugga.transunionsdk.vo.facialSimilarityReport

import com.google.gson.annotations.SerializedName

data class FacialSimilarityReportApplicantIO(
        @field:SerializedName("Applicant") val dsFacialSimilarityApplicant: List<FacialSimilarityReportApplicant>
)
package com.orugga.transunionsdk.vo

import com.google.gson.annotations.SerializedName

data class ResponseInfo(
        @field:SerializedName("ApplicationId") val applicationId: String,
        @field:SerializedName("SolutionSetInstanceId") val solutionSetInstanceId: String,
        @field:SerializedName("CurrentQueue") val currentQueue: String
)
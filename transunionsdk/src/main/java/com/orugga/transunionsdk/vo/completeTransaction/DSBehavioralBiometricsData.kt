package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSBehavioralBiometricsData(
        @field:SerializedName("Request") val request: DSBehavioralBiometricsDataRequest,
        @field:SerializedName("Response") val response: DocResponse
)
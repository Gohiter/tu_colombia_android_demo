package com.orugga.transunionsdk.vo

import com.google.gson.annotations.SerializedName

data class PhotoResponse(
        @field:SerializedName("Status") val status: String,
        @field:SerializedName("Error") val error: String,
        @field:SerializedName("Id") val id: String
)
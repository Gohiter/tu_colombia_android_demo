package com.orugga.transunionsdk.vo.offers

import com.google.gson.annotations.SerializedName

data class OffersResponseMyOffers(
        @field:SerializedName("Offer") val offers: List<OffersResponseOffer>
)
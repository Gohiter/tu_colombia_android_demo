package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSFacialSimilarityDataRequest(
        @field:SerializedName("UploadSelfieRequest") val uploadSelfieRequest: DocUpload,
        @field:SerializedName("CreateFRCheckResult") val createFRCheckResult: DocCheckRequest
)
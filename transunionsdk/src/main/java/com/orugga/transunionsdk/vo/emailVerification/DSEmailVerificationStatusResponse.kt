package com.orugga.transunionsdk.vo.emailVerification

import com.google.gson.annotations.SerializedName

data class DSEmailVerificationStatusResponse(
        @field:SerializedName("Outcome") val outcome: String,
        @field:SerializedName("IsSuccess") val isSuccess: String,
        @field:SerializedName("ErrorMessage") val errorMessage: String,
        @field:SerializedName("Errorcode") val errorCode: String
)
package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class DRWatchlistVerification(
        @field:SerializedName("IsInputValid") val isInputValid: String,
        @field:SerializedName("IsWatchlistAPISuccess") val isWatchlistApiSuccess: String,
        @field:SerializedName("MatchCount") val matchCount: String,
        @field:SerializedName("APIResponseTime") val apiResponseTime: String,
        @field:SerializedName("WatchlistDecision") val watchListDecision: String
)
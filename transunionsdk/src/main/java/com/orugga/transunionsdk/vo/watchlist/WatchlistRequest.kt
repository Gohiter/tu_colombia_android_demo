package com.orugga.transunionsdk.vo.watchlist

import com.google.gson.annotations.SerializedName

data class WatchlistRequest(
        @field:SerializedName("Fields") val requestFields: WatchlistFields
)
package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DocCheckRequest(
        @field:SerializedName("Authorization") val authorization: String,
        @field:SerializedName("applicant_id") val applicantId: String,
        @field:SerializedName("type") val type: String,
        @field:SerializedName("docId") val docId: String,
        @field:SerializedName("reports") val reports: List<DocReport>
)
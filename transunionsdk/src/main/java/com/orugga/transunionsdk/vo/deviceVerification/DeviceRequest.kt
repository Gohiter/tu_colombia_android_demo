package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName

data class DeviceRequest(
        @field:SerializedName("statedIp") val statedIp: String?,
        @field:SerializedName("accountCode") val accountCode: String?,
        @field:SerializedName("blackbox") val blackBox: String?,
        @field:SerializedName("type") val type: String?,
        @field:SerializedName("transactionInsight") val transactionInsight: TransactionInsight


)
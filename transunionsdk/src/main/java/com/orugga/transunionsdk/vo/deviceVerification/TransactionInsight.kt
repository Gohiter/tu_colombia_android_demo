package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName

data class TransactionInsight(
        @field:SerializedName("email") val email: String?,
        @field:SerializedName("homePhoneNumber") val homePhoneNumber: String?
)
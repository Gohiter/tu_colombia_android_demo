package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.docAuthentication.DocAttachResponseIdentityVerification

data class IdentityVerificationDocuments(
        @field:SerializedName("IdentityVerification") val identityVerifications: List<CompleteIdentityVerification>
)
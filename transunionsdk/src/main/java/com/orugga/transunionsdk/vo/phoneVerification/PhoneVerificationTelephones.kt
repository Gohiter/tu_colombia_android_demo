package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class PhoneVerificationTelephones(
    @field:SerializedName("Telephone") val telephone: List<PhoneVerificationTelephoneItem>
)
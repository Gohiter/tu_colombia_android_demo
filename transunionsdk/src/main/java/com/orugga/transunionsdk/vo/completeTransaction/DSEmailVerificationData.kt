package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSEmailVerificationData(
        @field:SerializedName("Request") val request: DSEmailVerificationDataRequest,
        @field:SerializedName("Response") val response: DocResponse
)
package com.orugga.transunionsdk.vo.facialSimilarityInvokeService

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.ResponseInfo
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceResponseFields

data class FacialSimilarityInvokeServiceResponse(
        @field:SerializedName("Status") val status: String,
        @field:SerializedName("ResponseInfo") val responseInfo: ResponseInfo,
        @field:SerializedName("Fields") val fields: FacialSimilarityInvokeServiceResponseFields
        )
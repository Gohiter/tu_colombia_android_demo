package com.orugga.transunionsdk.vo.deviceVerification

import com.google.gson.annotations.SerializedName

data class DeviceVerificationResponseApplicant(
        @field:SerializedName("Attributes") val deviceVerificationResponseApplicantAttributes: DeviceVerificationResponseApplicantAttributes,
        @field:SerializedName("DSDeviceVerification_FraudForceStatus") val dsDeviceVerificationFraudForceStatus: DSDeviceVerificationFraudForceStatus
)
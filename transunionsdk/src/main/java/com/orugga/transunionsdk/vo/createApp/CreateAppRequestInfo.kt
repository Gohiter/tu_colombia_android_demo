package com.orugga.transunionsdk.vo.createApp

import com.google.gson.annotations.SerializedName

data class CreateAppRequestInfo(
        @field:SerializedName("SolutionSetName") val solutionSetName: String,
        @field:SerializedName("ExecuteLatestVersion") val executeLatestVersion: String = "true",
        @field:SerializedName("SolutionSetVersion") val solutionSetVersion: String = "0"
)
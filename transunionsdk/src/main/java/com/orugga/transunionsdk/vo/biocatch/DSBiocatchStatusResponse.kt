package com.orugga.transunionsdk.vo.biocatch

import com.google.gson.annotations.SerializedName

data class DSBiocatchStatusResponse(
        @field:SerializedName("Outcome") val outcome: String,
        @field:SerializedName("IsSuccess") val isSuccess: String
)
package com.orugga.transunionsdk.vo.videoliveness

import com.google.gson.annotations.SerializedName
import com.orugga.transunionsdk.vo.pullReport.PullReportAttributes

data class OnfidoSDKRequest(
        @field:SerializedName("applicant_id") val applicant_id: String,
        @field:SerializedName("application_id") val application_id: String
)
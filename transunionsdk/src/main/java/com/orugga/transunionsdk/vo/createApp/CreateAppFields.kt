package com.orugga.transunionsdk.vo.createApp

import com.google.gson.annotations.SerializedName

data class CreateAppFields(
        @field:SerializedName("ExternalApplicationId") val externalApplicationId: String,
        @field:SerializedName("Applicants_IO") val applicantIO: ApplicantIO
)
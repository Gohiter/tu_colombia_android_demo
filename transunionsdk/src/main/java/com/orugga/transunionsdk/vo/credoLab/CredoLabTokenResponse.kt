package com.orugga.transunionsdk.vo.credoLab
import com.google.gson.annotations.SerializedName

data class CredoLabTokenResponse(
    @field:SerializedName("access_token") val access_token: String,
    @field:SerializedName("token_type") val token_type: String,
    @field:SerializedName(".issued") val issued: String,
    @field:SerializedName(".expires") val expires: String,
    @field:SerializedName("refresh_token") val refresh_token: String)

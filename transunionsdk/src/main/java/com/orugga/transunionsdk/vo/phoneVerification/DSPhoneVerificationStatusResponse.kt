package com.orugga.transunionsdk.vo.phoneVerification

import com.google.gson.annotations.SerializedName

data class DSPhoneVerificationStatusResponse(
        @field:SerializedName("Outcome") val outcome: String,
        @field:SerializedName("IsSuccess") val isSuccess: String,
        @field:SerializedName("IsBlackListed") val isBlackListed: String,
        @field:SerializedName("ErrorMessage") val errorMessage: String,
        @field:SerializedName("Errorcode") val errorCode: String
)
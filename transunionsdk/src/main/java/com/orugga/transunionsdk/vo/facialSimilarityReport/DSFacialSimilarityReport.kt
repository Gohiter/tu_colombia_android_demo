package com.orugga.transunionsdk.vo.facialSimilarityReport

import com.google.gson.annotations.SerializedName

data class DSFacialSimilarityReport(
        @field:SerializedName("Result") val result: String,
        @field:SerializedName("SubResult") val subResult: String,
        @field:SerializedName("Status") val status: String,
        @field:SerializedName("FacialSimilarityScore") val facialSimilarityScore: String,
        @field:SerializedName("FaceComparisonResult") val faceComparisonResult: String,
        @field:SerializedName("ImageIntegrityResult") val imageIntegrityResult: String,
        @field:SerializedName("VisualAuthenticity") val visualAuthenticity: String
)
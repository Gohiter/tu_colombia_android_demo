package com.orugga.transunionsdk.vo.completeTransaction

import com.google.gson.annotations.SerializedName

data class DSDocAuthenticationData(
        @field:SerializedName("Request") val request: DSDocAuthenticationDataRequest,
        @field:SerializedName("Response") val response: DocResponse
)
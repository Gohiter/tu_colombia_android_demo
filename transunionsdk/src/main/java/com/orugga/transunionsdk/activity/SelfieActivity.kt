package com.orugga.transunionsdk.activity

import android.graphics.Paint
import android.graphics.Color
import android.graphics.Bitmap
import android.graphics.PointF
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.drawable.ColorDrawable
import android.hardware.display.DisplayManager
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.util.Size
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageButton
import androidx.camera.core.CameraSelector
import androidx.camera.core.Preview
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageAnalysis
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.orugga.transunionsdk.R
import com.orugga.transunionsdk.docCapture.DocCaptureLiveEdgeUtils
import com.orugga.transunionsdk.docCapture.FaceAnalyzer
import com.orugga.transunionsdk.util.ANIMATION_FAST_MILLIS
import com.orugga.transunionsdk.util.ANIMATION_SLOW_MILLIS
import com.orugga.transunionsdk.util.ImageUtils
import kotlinx.android.synthetic.main.activity_doc_capture.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.concurrent.Executors

class SelfieActivity : PhotoActivity() {

    private lateinit var container: FrameLayout
    private lateinit var viewFinder: PreviewView
    private lateinit var captureButton: ImageButton
    private var faceBitmap: Bitmap? = null
    private var lensFacing: Int = CameraSelector.LENS_FACING_FRONT
    private val paintStroke = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
        color = Color.RED
        strokeWidth = 5f
    }

    /**
     * We need a display listener for orientation changes that do not trigger a configuration
     * change, for example if we choose to override config change in manifest or for 180-degree
     * orientation changes.
     */
    private val displayListener = object : DisplayManager.DisplayListener {
        override fun onDisplayAdded(displayId: Int) = Unit
        override fun onDisplayRemoved(displayId: Int) = Unit
        override fun onDisplayChanged(displayId: Int) = container.rootView?.let { view ->
            if (displayId == this@SelfieActivity. displayId && view.display != null) {
                Log.d(TAG, "Rotation changed: ${view.display.rotation}")
                imageCapture?.targetRotation = view.display.rotation
                imageAnalyzer?.targetRotation = view.display.rotation
            }
        } ?: Unit
    }

    companion object {
        const val TAG = "SelfieActivity"
        private const val FILENAME = "yyyy-MM-dd-HH-mm-ss-SSS"
        private const val PHOTO_EXTENSION = ".jpg"
        private const val RATIO_4_3_VALUE = 4.0 / 3.0
        private const val RATIO_16_9_VALUE = 16.0 / 9.0
        private val EXTENSION_WHITELIST = arrayOf("JPG")
        const val KEY_FILE_PATH = "file_uri"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doc_capture)
        // Hide the status bar.
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        // Never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        actionBar?.hide()
        DocCaptureLiveEdgeUtils.loadFaceModel(this)
    }

    override fun initializeComponents() {
        container = findViewById(R.id.camera_container)
        viewFinder = container.findViewById(R.id.view_finder)
        // Initialize our background executor
        cameraExecutor = Executors.newSingleThreadExecutor()
        // Every time the orientation of device changes, update rotation for use cases
        displayManager.registerDisplayListener(displayListener, null)
        // Wait for the views to be properly laid out
        viewFinder.post {
            // Keep track of the display in which this view is attached
            displayId = viewFinder.display.displayId

            // Build UI controls
            updateCameraUi()

            // Set up the camera and its use cases
            setUpCamera()
        }
        faceBitmap = null
    }

    /** Initialize CameraX, and prepare to bind the camera use cases  */
    private fun setUpCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener(Runnable {

            // CameraProvider
            cameraProvider = cameraProviderFuture.get()

            // Select lensFacing depending on the available cameras
            lensFacing = when {
                hasFrontCamera() -> CameraSelector.LENS_FACING_FRONT
                hasBackCamera() -> CameraSelector.LENS_FACING_BACK
                else -> throw IllegalStateException("Back and front camera are unavailable")
            }
            // Build and bind the camera use cases
            bindCameraUseCases()
        }, ContextCompat.getMainExecutor(this))
    }

    /** Returns true if the device has an available back camera. False otherwise */
    private fun hasBackCamera(): Boolean {
        return cameraProvider?.hasCamera(CameraSelector.DEFAULT_BACK_CAMERA) ?: false
    }

    /** Returns true if the device has an available front camera. False otherwise */
    private fun hasFrontCamera(): Boolean {
        return cameraProvider?.hasCamera(CameraSelector.DEFAULT_FRONT_CAMERA) ?: false
    }

    /** Method used to re-draw the camera UI controls, called every time configuration changes. */
    override fun updateCameraUi() {
        // Remove previous UI if any
        container.findViewById<ConstraintLayout>(R.id.camera_selfie_ui_container)?.let {
            container.removeView(it)
        }
        // Inflate a new view containing all UI for controlling the camera
        val controls = View.inflate(this, R.layout.camera_selfie_ui_container, container)

        // Listener for button used to capture photo
        captureButton = controls.findViewById(R.id.camera_capture_button)
        captureButton.setOnClickListener { takePicture() }
        // Listener for button used to close the camera
        controls.findViewById<ImageButton>(R.id.btn_close_camera).setOnClickListener { onBackPressed() }
    }

    override fun takePicture() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Display flash animation to indicate that photo was captured
            container.postDelayed({
                container.foreground = ColorDrawable(Color.WHITE)
                container.postDelayed(
                    { container.foreground = null }, ANIMATION_FAST_MILLIS
                )
            }, ANIMATION_SLOW_MILLIS)
        }
        faceBitmap?.let { createURiResult(it) }
    }

    /** Declare and bind preview, capture and analysis use cases */
    override fun bindCameraUseCases() {

        // Get screen metrics used to setup camera for full screen resolution
        val metrics = DisplayMetrics().also { viewFinder.display.getRealMetrics(it) }

        val screenAspectRatio = aspectRatio(metrics.widthPixels, metrics.heightPixels)

        val rotation = viewFinder.display.rotation

        // CameraProvider
        val cameraProvider = cameraProvider
            ?: throw IllegalStateException("Camera initialization failed.")

        // CameraSelector
        val cameraSelector = CameraSelector.Builder().requireLensFacing(lensFacing).build()

        // Preview
        preview = Preview.Builder()
            // Set initial target rotation
            .setTargetRotation(rotation)
            .setTargetResolution(Size(metrics.widthPixels, metrics.heightPixels))
            .build()

        // ImageCapture
        imageCapture = ImageCapture.Builder()
            .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
            // We request aspect ratio but no resolution to match preview config, but letting
            // CameraX optimize for whatever specific resolution best fits our use cases
            .setTargetResolution(Size(metrics.widthPixels, metrics.heightPixels))
            // Set initial target rotation, we will have to call this again if rotation changes
            // during the lifecycle of this use case
            .setTargetRotation(rotation)
            .build()

        // ImageAnalysis
        val angle = if(lensFacing == CameraSelector.LENS_FACING_FRONT) -90f else 90f
        imageAnalyzer = ImageAnalysis.Builder()
            .setTargetRotation(rotation)
            .setTargetResolution(Size(metrics.widthPixels, metrics.heightPixels))
            .build()
            // The analyzer can then be assigned to the instance
            .also {
                it.setAnalyzer(cameraExecutor, FaceAnalyzer({ points, original, cropped ->
                    runBlocking {
                        faceBitmap = cropped
                        runOnUiThread {
                            captureButton.isEnabled = cropped != null
                        }
                        drawBoundaries(points, original)
                    }
                }, this, angle))
            }
        // Must unbind the use-cases before rebinding them
        cameraProvider.unbindAll()

        try {
            // A variable number of use-cases can be passed here -
            // camera provides access to CameraControl & CameraInfo
            camera = cameraProvider.bindToLifecycle(
                this, cameraSelector, preview, imageCapture, imageAnalyzer)

            // Attach the viewfinder's surface provider to preview use case
            preview?.setSurfaceProvider(viewFinder.createSurfaceProvider())
        } catch (exc: Exception) {
            Log.e(TAG, "Use case binding failed", exc)
        }
    }

    suspend fun drawBoundaries(points: List<PointF>?, image: Bitmap?) {
        if(image != null && image.width > 0 && image.height > 0) {
            var overlay = Bitmap.createBitmap(image.width, image.height, Bitmap.Config.ARGB_8888)
            withContext(Dispatchers.Unconfined) {
                if (overlay != null) {
                    val canvas = Canvas(overlay)
                    if(!points.isNullOrEmpty()){
                        val path = Path()
                        for (i in points.indices) {
                            if (i == 0) {
                                path.moveTo(points[i].x, points[i].y)
                            } else {
                                path.lineTo(points[i].x, points[i].y)
                            }
                        }
                        path.close()
                        canvas.drawPath(path, paintStroke)
                    }
                    runOnUiThread {
                        imageView.setImageBitmap(ImageUtils.flipX(overlay))
                    }
                    overlay.let { Canvas(it) }.apply {
                        canvas
                    }
                }
            }
        }

    }
}

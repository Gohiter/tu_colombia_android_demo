package com.orugga.transunionsdk.activity

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Paint
import android.graphics.Color
import android.graphics.PorterDuffXfermode
import android.graphics.PorterDuff
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.graphics.PointF
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.graphics.drawable.ColorDrawable
import android.hardware.display.DisplayManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.util.Size
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.Preview
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Camera
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.AspectRatio
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.orugga.transunionsdk.R
import com.orugga.transunionsdk.docCapture.DocCaptureAnalyzer
import com.orugga.transunionsdk.util.ANIMATION_FAST_MILLIS
import com.orugga.transunionsdk.util.ANIMATION_SLOW_MILLIS
import com.orugga.transunionsdk.util.ImageUtils
import com.orugga.transunionsdk.util.PermissionsHelper
import com.orugga.transunionsdk.vo.ValidationResult
import kotlinx.android.synthetic.main.activity_doc_capture.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

open abstract class PhotoActivity : AppCompatActivity() {

    private lateinit var container: FrameLayout
    private lateinit var viewFinder: PreviewView

    protected var torch = false
    protected var displayId: Int = -1
    protected var preview: Preview? = null
    protected var imageCapture: ImageCapture? = null
    protected var imageAnalyzer: ImageAnalysis? = null
    protected var camera: Camera? = null
    protected var cameraProvider: ProcessCameraProvider? = null

    protected val displayManager by lazy {
        this.getSystemService(Context.DISPLAY_SERVICE) as DisplayManager
    }

    /**
     * We need a display listener for orientation changes that do not trigger a configuration
     * change, for example if we choose to override config change in manifest or for 180-degree
     * orientation changes.
     */
    private val displayListener = object : DisplayManager.DisplayListener {
        override fun onDisplayAdded(displayId: Int) = Unit
        override fun onDisplayRemoved(displayId: Int) = Unit
        override fun onDisplayChanged(displayId: Int) = container.rootView?.let { view ->
            if (displayId == this@PhotoActivity. displayId) {
                Log.d(TAG, "Rotation changed: ${view.display.rotation}")
                imageCapture?.targetRotation = view.display.rotation
                imageAnalyzer?.targetRotation = view.display.rotation
            }
        } ?: Unit
    }

    companion object {
        const val TAG = "DocCaptureActivity"
        private const val FILENAME = "yyyy-MM-dd-HH-mm-ss-SSS"
        private const val PHOTO_EXTENSION = ".png"
        private const val RATIO_4_3_VALUE = 4.0 / 3.0
        private const val RATIO_16_9_VALUE = 16.0 / 9.0
        private val EXTENSION_WHITELIST = arrayOf("PNG")
        const val KEY_FILE_PATH = "file_uri"
    }

    /** Blocking camera operations are performed using this executor */
    protected lateinit var cameraExecutor: ExecutorService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doc_capture)
        // Hide the status bar.
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        // Never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        actionBar?.hide()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishExecutor()
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionsHelper.DOC_CAPTURE_PERMISSION) {
            if (PackageManager.PERMISSION_GRANTED == grantResults.firstOrNull()) {
                Toast.makeText(this, "Permission request granted", Toast.LENGTH_LONG).show()
                initializeComponents()
            } else {
                Toast.makeText(this, "Permission request denied", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (PermissionsHelper.checkDocCapturePermission(this)) {
            initializeComponents()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        // Shut down our background executor
       finishExecutor()
    }

    private fun finishExecutor() {
        cameraExecutor.shutdown()
        displayManager.unregisterDisplayListener(displayListener)
        imageAnalyzer?.clearAnalyzer()
    }

    /** Returns true if the device has an available back camera. False otherwise */
    private fun hasBackCamera(): Boolean {
        return cameraProvider?.hasCamera(CameraSelector.DEFAULT_BACK_CAMERA) ?: false
    }

    /** Returns true if the device has an available front camera. False otherwise */
    private fun hasFrontCamera(): Boolean {
        return cameraProvider?.hasCamera(CameraSelector.DEFAULT_FRONT_CAMERA) ?: false
    }

    protected fun toggleFlash() {
        torch = !torch
        val button = findViewById<ImageButton>(R.id.camera_flashlight_button)
        if (torch) {
            button.background = getDrawable(R.drawable.rounded_square_selected)
        } else {
            button.background = getDrawable(R.drawable.rounded_square)
        }
        camera?.cameraControl?.enableTorch(torch)
    }

    protected fun createURiResult(bitmap: Bitmap) {
        val outputDirectory = ImageUtils.getOutputDirectory(this)
        // Create output file to hold the image
        val photoFile = ImageUtils.createFile(outputDirectory,
            FILENAME,
            PHOTO_EXTENSION
        )
        try {
            val os = FileOutputStream(photoFile)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, os)
            os.flush()
            os.close()
            intent?.putExtra(DocCaptureActivity.KEY_FILE_PATH, photoFile.path)
            Log.i("PHOTOFILE", photoFile.toURI().toString())
            setResult(RESULT_OK, intent)
            // Shut down our background executor
            finishExecutor()
            finish()
        } catch (e: java.lang.Exception) {
            Log.e(DocCaptureActivity.TAG, e.message);
        } finally {
            setResult(RESULT_CANCELED)
        }
    }

    protected fun createURiResult(photoFile: File) {
        intent?.putExtra(DocCaptureActivity.KEY_FILE_PATH, photoFile.path)
        Log.i("PHOTOFILE", photoFile.toURI().toString())
        setResult(RESULT_OK, intent)
        // Shut down our background executor
        finishExecutor()
        finish()
    }

    /**
     *  [androidx.camera.core.ImageAnalysisConfig] requires enum value of
     *  [androidx.camera.core.AspectRatio]. Currently it has values of 4:3 & 16:9.
     *
     *  Detecting the most suitable ratio for dimensions provided in @params by counting absolute
     *  of preview ratio to one of the provided values.
     *
     *  @param width - preview width
     *  @param height - preview height
     *  @return suitable aspect ratio
     */
    protected fun aspectRatio(width: Int, height: Int): Int {
        val previewRatio = max(width, height).toDouble() / min(width, height)
        if (abs(previewRatio - RATIO_4_3_VALUE) <= abs(previewRatio - RATIO_16_9_VALUE)) {
            return AspectRatio.RATIO_4_3
        }
        return AspectRatio.RATIO_16_9
    }

    /** Take picture manually method */
    abstract fun takePicture()

    /** Declare and bind preview, capture and analysis use cases */
    abstract fun bindCameraUseCases()

    /** Method used to re-draw the camera UI controls, called every time configuration changes. */
    abstract fun updateCameraUi()

    /** Method to be called when te user grants the camera permissions */
    abstract fun initializeComponents()
}

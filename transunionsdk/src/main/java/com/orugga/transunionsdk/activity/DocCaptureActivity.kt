package com.orugga.transunionsdk.activity

import android.graphics.Paint
import android.graphics.Color
import android.graphics.PorterDuffXfermode
import android.graphics.PorterDuff
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.graphics.PointF
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.graphics.drawable.ColorDrawable
import android.hardware.display.DisplayManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.util.Size
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.TextView
import androidx.camera.core.CameraSelector
import androidx.camera.core.Preview
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageCaptureException
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.orugga.transunionsdk.R
import com.orugga.transunionsdk.docCapture.DocCaptureAnalyzer
import com.orugga.transunionsdk.util.ANIMATION_FAST_MILLIS
import com.orugga.transunionsdk.util.ANIMATION_SLOW_MILLIS
import com.orugga.transunionsdk.util.ImageUtils
import com.orugga.transunionsdk.vo.ValidationResult
import kotlinx.android.synthetic.main.activity_doc_capture.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.concurrent.Executors

class DocCaptureActivity : PhotoActivity() {

    private lateinit var container: FrameLayout
    private lateinit var viewFinder: PreviewView

    private var lensFacing: Int = CameraSelector.LENS_FACING_BACK
    private val paintFill = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = Color.GREEN
        alpha = 25
    }
    private val paintStroke = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.STROKE
        color = Color.GREEN
        strokeWidth = 5f
    }
    private val outsideFill = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = Color.GRAY
        alpha = 200
    }
    private val emptyFill = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
    }
    private var lastTime = 0L

    /**
     * We need a display listener for orientation changes that do not trigger a configuration
     * change, for example if we choose to override config change in manifest or for 180-degree
     * orientation changes.
     */
    private val displayListener = object : DisplayManager.DisplayListener {
        override fun onDisplayAdded(displayId: Int) = Unit
        override fun onDisplayRemoved(displayId: Int) = Unit
        override fun onDisplayChanged(displayId: Int) = container.rootView?.let { view ->
            if (displayId == this@DocCaptureActivity. displayId && view.display != null) {
                Log.d(TAG, "Rotation changed: ${view.display.rotation}")
                imageCapture?.targetRotation = view.display.rotation
                imageAnalyzer?.targetRotation = view.display.rotation
            }
        } ?: Unit
    }

    companion object {
        const val TAG = "DocCaptureActivity"
        private const val FILENAME = "yyyy-MM-dd-HH-mm-ss-SSS"
        private const val PHOTO_EXTENSION = ".png"
        private const val RATIO_4_3_VALUE = 4.0 / 3.0
        private const val RATIO_16_9_VALUE = 16.0 / 9.0
        private val EXTENSION_WHITELIST = arrayOf("PNG")
        const val KEY_FILE_PATH = "file_uri"
        const val MAX_TOO_DARK_MSG = 10000L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doc_capture)
        // Hide the status bar.
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        // Never show the action bar if the
        // status bar is hidden, so hide that too if necessary.
        actionBar?.hide()
    }

    override fun initializeComponents() {
        torch = false
        container = findViewById(R.id.camera_container)
        viewFinder = container.findViewById(R.id.view_finder)
        // Initialize our background executor
        cameraExecutor = Executors.newSingleThreadExecutor()
        // Every time the orientation of device changes, update rotation for use cases
        displayManager.registerDisplayListener(displayListener, null)
        // Wait for the views to be properly laid out
        viewFinder.post {

            // Keep track of the display in which this view is attached
            displayId = viewFinder.display.displayId

            // Build UI controls
            updateCameraUi()

            // Set up the camera and its use cases
            setUpCamera()
        }
    }

    /** Initialize CameraX, and prepare to bind the camera use cases  */
    private fun setUpCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(this)
        cameraProviderFuture.addListener(Runnable {

            // CameraProvider
            cameraProvider = cameraProviderFuture.get()

            // Select lensFacing depending on the available cameras
            lensFacing = when {
                hasBackCamera() -> CameraSelector.LENS_FACING_BACK
                hasFrontCamera() -> CameraSelector.LENS_FACING_FRONT
                else -> throw IllegalStateException("Back and front camera are unavailable")
            }
            // Build and bind the camera use cases
            bindCameraUseCases()
        }, ContextCompat.getMainExecutor(this))
    }

    /** Returns true if the device has an available back camera. False otherwise */
    private fun hasBackCamera(): Boolean {
        return cameraProvider?.hasCamera(CameraSelector.DEFAULT_BACK_CAMERA) ?: false
    }

    /** Returns true if the device has an available front camera. False otherwise */
    private fun hasFrontCamera(): Boolean {
        return cameraProvider?.hasCamera(CameraSelector.DEFAULT_FRONT_CAMERA) ?: false
    }

    /** Method used to re-draw the camera UI controls, called every time configuration changes. */
    override fun updateCameraUi() {
        // Remove previous UI if any
        container.findViewById<ConstraintLayout>(R.id.camera_ui_container)?.let {
            container.removeView(it)
        }
        // Inflate a new view containing all UI for controlling the camera
        val controls = View.inflate(this, R.layout.camera_ui_container, container)

        // Listener for button used to capture photo
        controls.findViewById<ImageButton>(R.id.camera_capture_button).setOnClickListener { takePicture() }
        // Listener for button used to close the camera
        controls.findViewById<ImageButton>(R.id.btn_close_camera).setOnClickListener { onBackPressed() }
        // Listener for button used to close the camera
        controls.findViewById<ImageButton>(R.id.camera_flashlight_button).setOnClickListener { toggleFlash() }
    }

    override fun takePicture() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // Display flash animation to indicate that photo was captured
            container.postDelayed({
                container.foreground = ColorDrawable(Color.WHITE)
                container.postDelayed(
                    { container.foreground = null }, ANIMATION_FAST_MILLIS
                )
            }, ANIMATION_SLOW_MILLIS)
        }
        val imageCapture = imageCapture ?: return
        val outputDirectory = ImageUtils.getOutputDirectory(this)
        val photoFile = ImageUtils.createFile(outputDirectory, FILENAME, PHOTO_EXTENSION)
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()
        imageCapture.takePicture(
            outputOptions, ContextCompat.getMainExecutor(this), object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    val savedUri = Uri.fromFile(photoFile)
                    val msg = "Photo capture succeeded: $savedUri"
                    Log.d(TAG, msg)
                    val bitmap = BitmapFactory.decodeFile(photoFile.path)

                    val width = bitmap.width
                    val height = bitmap.height
                    val marginH =  bitmap.width * 0.2f
                    val marginV =  bitmap.height * 0.2f
                    val points = listOf(PointF(marginH, marginV), PointF(width - marginH, marginV), PointF(width - marginH, height - marginV), PointF(marginH, height - marginV))
                    createURiResult(ImageUtils.getCroppedDocument(bitmap, points, 0.0))

                    photoFile.delete()
                }
            })
    }

    /** Declare and bind preview, capture and analysis use cases */
    override fun bindCameraUseCases() {

        // Get screen metrics used to setup camera for full screen resolution
        val metrics = DisplayMetrics().also { viewFinder.display.getRealMetrics(it) }
        Log.d(TAG, "Screen metrics: ${metrics.widthPixels} x ${metrics.heightPixels}")

        val screenAspectRatio = aspectRatio(metrics.widthPixels, metrics.heightPixels)
        Log.d(TAG, "Preview aspect ratio: $screenAspectRatio")

        val rotation = viewFinder.display.rotation

        // CameraProvider
        val cameraProvider = cameraProvider
            ?: throw IllegalStateException("Camera initialization failed.")

        // CameraSelector
        val cameraSelector = CameraSelector.Builder().requireLensFacing(lensFacing).build()

        // Preview
        preview = Preview.Builder()
            // Set initial target rotation
            .setTargetRotation(rotation)
            .setTargetResolution(Size(metrics.widthPixels, metrics.heightPixels))
            .build()
            .also {
                var overlay = Bitmap.createBitmap(imageView.width, imageView.height, Bitmap.Config.ARGB_8888)
                imageView.setImageBitmap(overlay)
                val canvas = Canvas(overlay)
                drawBox(canvas)
            }

        // ImageCapture
        imageCapture = ImageCapture.Builder()
            .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
            // We request aspect ratio but no resolution to match preview config, but letting
            // CameraX optimize for whatever specific resolution best fits our use cases
            .setTargetResolution(Size(metrics.widthPixels, metrics.heightPixels))
            // Set initial target rotation, we will have to call this again if rotation changes
            // during the lifecycle of this use case
            .setTargetRotation(rotation)
            .build()

        // ImageAnalysis
        imageAnalyzer = ImageAnalysis.Builder()
            .setTargetRotation(rotation)
            .setTargetResolution(Size(metrics.widthPixels, metrics.heightPixels))

            .build()
            // The analyzer can then be assigned to the instance
            .also {
                it.setAnalyzer(cameraExecutor, DocCaptureAnalyzer({ result, points, original, croppedDocument ->
                    runBlocking {
                        drawBoundaries(points, original, result)
                        croppedDocument?.let { document -> createURiResult(document) }
                        showFeedbackResult(result)
                    }
                }, this, true, false))
            }
        // Must unbind the use-cases before rebinding them
        cameraProvider.unbindAll()

        try {
            // A variable number of use-cases can be passed here -
            // camera provides access to CameraControl & CameraInfo
            camera = cameraProvider.bindToLifecycle(
                this, cameraSelector, preview, imageCapture, imageAnalyzer)

            // Attach the viewfinder's surface provider to preview use case
            preview?.setSurfaceProvider(viewFinder.createSurfaceProvider())
        } catch (exc: Exception) {
            Log.e(TAG, "Use case binding failed", exc)
        }
    }

    private fun showFeedbackResult(result: ValidationResult) {
        val currentTime = System.currentTimeMillis()
        val stringResult: String = when(result) {
            ValidationResult.NO_CONTOUR -> {
                if(lastTime == 0L){
                    lastTime = currentTime
                }
                if(currentTime - lastTime > MAX_TOO_DARK_MSG) {
                    getString(R.string.adjust_angle)
                } else {
                    getString(R.string.user_dark_background)
                }
            }
            ValidationResult.MOVE_AWAY -> getString(R.string.too_close)
            ValidationResult.MOVE_CLOSER -> getString(R.string.far_away)
            ValidationResult.OFF_CENTER -> getString(R.string.center_image)
            ValidationResult.TOO_DARK -> getString(R.string.too_dark)
            ValidationResult.BLURRED -> getString(R.string.hold_still)
            ValidationResult.ADJUST_ANGLE -> getString(R.string.adjust_angle)
            else -> ""
        }
        runOnUiThread {
            if (!TextUtils.isEmpty(stringResult)) {
                findViewById<FrameLayout>(R.id.lytInstructionsContainer).visibility = View.VISIBLE
                findViewById<TextView>(R.id.tvCaptureInstructions).text = stringResult
            } else {
                findViewById<FrameLayout>(R.id.lytInstructionsContainer).visibility = View.GONE
            }
        }
    }

    suspend fun drawBoundaries(points: List<PointF>?, image: Bitmap?, result: ValidationResult) {
        if(image != null && image.width > 0 && image.height > 0) {
            var overlay = Bitmap.createBitmap(image.width, image.height, Bitmap.Config.ARGB_8888)
            withContext(Dispatchers.Unconfined) {
                if (overlay != null) {
                    val canvas = Canvas(overlay)
                    canvas.drawPaint(emptyFill) //clear canvas
                    drawBox(canvas)

                    if(result != ValidationResult.NO_CONTOUR && !points.isNullOrEmpty()){
                        val path = Path()
                        for (i in points.indices) {
                            if (i == 0) {
                                path.moveTo(points[i].x, points[i].y)
                            } else {
                                path.lineTo(points[i].x, points[i].y)
                            }
                        }
                        path.close()
                        canvas.drawPath(path, paintFill)
                        canvas.drawPath(path, paintStroke)
                    }
                    runOnUiThread {
                        imageView.setImageBitmap(overlay)
                    }
                    overlay.let { Canvas(it) }.apply {
                        canvas
                    }
                }
            }
        }
    }

    private fun drawBox(canvas: Canvas) {
        val w = canvas.width
        val h = canvas.height
        val horizontalMargin =  canvas.width * 0.2f
        val verticalMargin =  canvas.height * 0.2f
        val rect = RectF(horizontalMargin, verticalMargin, (w - horizontalMargin), (h - verticalMargin))
        val radius = 20.0f
        val bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        val auxCanvas = Canvas(bitmap)
        auxCanvas.drawPaint(outsideFill)
        auxCanvas.drawRoundRect(rect, radius, radius, emptyFill)
        canvas.drawBitmap(bitmap, 0f, 0f, paintStroke)
    }
}

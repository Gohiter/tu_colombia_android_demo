package com.orugga.transunionsdk.activity

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log

import androidx.appcompat.app.AppCompatActivity
import com.orugga.transunionsdk.BiocatchSdk
import com.orugga.transunionsdk.BuildConfig
import com.orugga.transunionsdk.util.PermissionsHelper
import com.orugga.transunionsdk.util.SharedPreferencesHelper
import org.opencv.android.OpenCVLoader
import java.util.UUID

/**
 * The Main activity in the application should extends this activity in order to be sure
 * that all the permissions and configurations are set correctly before start using it.
 */
open class TransunionActivity : AppCompatActivity() {

    lateinit var userAnswer: PermissionsHelper.UserAnswer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val wupsUrl = BuildConfig.BIOCATCH_BASE_URL
        val cid = BuildConfig.BIOCATCH_CID
        BiocatchSdk.start(wupsUrl, application, cid, activity = this@TransunionActivity)
        val ocvLoaded = OpenCVLoader.initDebug()
        if (ocvLoaded) {
            Log.i("openCV", "OpenCV loaded")
        } else {
            Log.i("openCV", "loader: ${OpenCVLoader.initDebug()}")
        }

    }

    override fun onResume() {
        super.onResume()
        userAnswer = PermissionsHelper.checkLocationPermission(this) {onLocationPermissionDenied()}
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PermissionsHelper.LOCATION_PERMISSION -> {
                userAnswer?.userAnswered()
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    onLocationPermissionAccepted()
                } else {
                    onLocationPermissionDenied()
                }
                return
            }
        }
    }

    /**
     * Override this function with the action that should be executed if the user denies to the app
     * the location permission.
     */
    open fun onLocationPermissionDenied() {
        Log.i("cameraPermission", "Denied")
    }

    /**
     * Override this function with the action that should be executed if the user allows to the app
     * the location permission.
     */
    open fun onLocationPermissionAccepted() {
        Log.i("cameraPermission", "Granted")
    }
}

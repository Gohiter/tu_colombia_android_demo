package com.orugga.transunionsdk.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.orugga.transunionsdk.vo.createApp.CreateAppResponse
import com.orugga.transunionsdk.vo.Phone
import com.orugga.transunionsdk.vo.TokenResponse
import com.orugga.transunionsdk.vo.credoLab.CredoLabTokenResponse

/**
 * Class to access using methods to Shared preferences.
 */
class SharedPreferencesHelper(private val appContext: Context) {

    companion object {
        /**
         * String key for the auth model
         */
        private const val AUTH_KEY = "sharedPreferences.auth"
        private const val APPLICATION_ID = "sharedPreferences.applicationId"
        private const val FRONT_PICTURE_LOCATION_ID = "sharedPreferences.frontPictureLocationId"
        private const val SELFIE_PICTURE_LOCATION_ID = "sharedPreferences.selfiePictureLocationId"
        const val PHONES_LIST_KEY = "sharedPreferences.phones"
        const val EMAIL_LIST_KEY = "sharedPreferences.emails"
        const val LOCATION_PERMISSION_ASKED = "sharedPreferences.locationPermissionAsked"
        const val BIOCATCH_CURRENT_CSID = "sharedPreferences.currentCSID"
        private const val CREDOLAB_TOKEN = "sharedPreferences.credoLab.token"
        private const val ONFIDO_TOKEN = "sharedPreferences.onfido.token"
        private const val ONFIDO_SDK_TOKEN = "sharedPreferences.onfido.sdktoken"
        private const val PHONES_MAX_SIZE = 20
        private const val EMAIL_MAX_SIZE = 20


    }

    private var sharedPreferences = PreferenceManager.getDefaultSharedPreferences(appContext)
    private val gson = Gson()
    //Vamos a almacenar aca en MEMORIA
    private var oauth_user = ""
    private var oauth_password = ""
    private var oauth_grant_type = ""
    private var solution_name = ""
    private var external_application_id = ""
    private var credo_lab_key = ""
    private var credo_lab_user = ""
    private var credo_lab_password = ""

    /**
     * clears the shared prefs
     */
    fun setTransunionSDK(oauth_user: String, oauth_password: String, oauth_grant_type: String, external_application_id: String, solution_name: String ) {
        this.oauth_user = oauth_user
        this.oauth_password = oauth_password
        this.oauth_grant_type = oauth_grant_type
        this.external_application_id = external_application_id
        this.solution_name = solution_name
    }
    fun setCredoLab(credo_lab_key: String, credo_lab_user: String, credo_lab_password: String) {
        this.credo_lab_key = credo_lab_key
        this.credo_lab_user = credo_lab_user
        this.credo_lab_password = credo_lab_password
    }
    fun getUser() : String {
        return this.oauth_user
    }
    fun getPassword() : String {
        return this.oauth_password
    }
    fun getGrantType() : String {
        return this.oauth_grant_type
    }
    fun getAppId() : String {
        return this.external_application_id
    }
    fun getSolutionName() : String {
        return this.solution_name
    }
    fun getCredoLabKey() : String {
        return this.credo_lab_key
    }
    fun getCredoLabUser() : String {
        return this.credo_lab_user
    }
    fun getCredoLabPassword() : String {
        return this.credo_lab_password
    }
    fun clearSharedPrefs(): Boolean {
        val editor = sharedPreferences.edit()
        editor.clear()
        return editor.commit()
    }

    /**
     * saves the auth
     */
    fun saveAuth(tokenResponse: TokenResponse) {
        val editor = sharedPreferences.edit()
        editor.putString(AUTH_KEY, gson.toJson(tokenResponse)).commit()
    }

    /**
     * clears the auth
     */
    fun removeAuth(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(AUTH_KEY).commit()
    }

    /**
     * returns the auth
     */
    fun getAuth(): TokenResponse {
        val auth = sharedPreferences.getString(AUTH_KEY, "{}")
        return gson.fromJson(auth, TokenResponse::class.java)
    }

    fun addPhone(phone: Phone){
        val phones = getPhones()
        if(PHONES_MAX_SIZE == phones.size){
            phones.remove(phones.first())
        }
        phones.add(phone)
        sharedPreferences.edit().putString(PHONES_LIST_KEY, gson.toJson(phones)).commit()
    }

    fun getPhones(): MutableSet<Phone>{
        val phonesJson = sharedPreferences.getString(PHONES_LIST_KEY, "[]")
        val type = object : TypeToken<Set<Phone>>(){}.type
        return gson.fromJson(phonesJson, type)
    }

    fun addEmail(email: String){
        val emailList = getEmailList()
        if(EMAIL_MAX_SIZE == emailList.size){
            emailList.remove(emailList.first())
        }
        emailList.add(email)
        sharedPreferences.edit().putString(EMAIL_LIST_KEY, gson.toJson(email)).commit()
    }

    fun getEmailList(): MutableSet<String>{
        val emailListJson = sharedPreferences.getString(EMAIL_LIST_KEY, "[]")
        val type = object : TypeToken<Set<String>>(){}.type
        return gson.fromJson(emailListJson, type)
    }

    /**
     * Saves the application Id
     */
    fun saveApplicationId(createAppResponse: CreateAppResponse) {
        val editor = sharedPreferences.edit()
        editor.putString(APPLICATION_ID, gson.toJson(createAppResponse)).commit()
    }

    /**
     * Clears the application Id
     */
    fun removeApplicationId(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(APPLICATION_ID).commit()
    }

    /**
     * Retrieve the application Id
     */
    fun getApplicationId(): CreateAppResponse {
        val auth = sharedPreferences.getString(APPLICATION_ID, "{}")
        return gson.fromJson(auth, CreateAppResponse::class.java)
    }

    /**
     * Saves the front picture location id in server
     */
    fun saveFrontPictureLocationId(locationId: String) {
        val editor = sharedPreferences.edit()
        editor.putString(FRONT_PICTURE_LOCATION_ID, locationId).commit()
    }

    /**
     * Clear the front picture location id in server
     */
    fun removeFrontPictureLocationId(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(FRONT_PICTURE_LOCATION_ID).commit()
    }

    /**
     * Retrieves the front picture location id in server
     */
    fun getSelfiePictureLocationId(): String {
        return sharedPreferences.getString(SELFIE_PICTURE_LOCATION_ID, "")
    }

    /**
     * Saves the front picture location id in server
     */
    fun saveSelfiePictureLocationId(locationId: String) {
        val editor = sharedPreferences.edit()
        editor.putString(SELFIE_PICTURE_LOCATION_ID, locationId).commit()
    }

    /**
     * Clear the front picture location id in server
     */
    fun removeSelfiePictureLocationId(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(SELFIE_PICTURE_LOCATION_ID).commit()
    }

    /**
     * Retrieves the front picture location id in server
     */
    fun getFrontPictureLocationId(): String {
        return sharedPreferences.getString(FRONT_PICTURE_LOCATION_ID, "")
    }

    fun saveCurrentCSID(csid: String) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.putString(BIOCATCH_CURRENT_CSID, csid).commit()
    }

    fun getUserCurrentCSID():String{
        return sharedPreferences.getString(BIOCATCH_CURRENT_CSID, "")
    }

    fun removeCurrentCSID(){
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.remove(BIOCATCH_CURRENT_CSID).commit()
    }

    fun saveCredoLabToken(tokenResponse: CredoLabTokenResponse) {
        val editor = sharedPreferences.edit()
        editor.putString(CREDOLAB_TOKEN, gson.toJson(tokenResponse)).commit()
    }

    fun removeCredoLabToken(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(CREDOLAB_TOKEN).commit()
    }

    fun getCredoLabToken(): CredoLabTokenResponse {
        val auth = sharedPreferences.getString(CREDOLAB_TOKEN, "{}")
        return gson.fromJson(auth, CredoLabTokenResponse::class.java)
    }

    //Token de onfido para acceder al servicio
    fun saveOnfidoToken(token: String) {
        val editor = sharedPreferences.edit()
        editor.putString(ONFIDO_TOKEN, token).commit()
    }

    fun removeOnfidoToken(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(ONFIDO_TOKEN).commit()
    }

    fun getOnfidoToken(): String {
        val auth = sharedPreferences.getString(ONFIDO_TOKEN, "")
        return auth
    }

    //Token generado del de Onfido para generar una instancia de video liveness
    fun saveOnfidoSDKToken(token: String) {
        val editor = sharedPreferences.edit()
        editor.putString(ONFIDO_SDK_TOKEN, token).commit()
    }

    fun removeOnfidoSDKToken(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(ONFIDO_SDK_TOKEN).commit()
    }

    fun getOnfidoSDKToken(): String {
        val auth = sharedPreferences.getString(ONFIDO_SDK_TOKEN, "")
        return auth
    }

}
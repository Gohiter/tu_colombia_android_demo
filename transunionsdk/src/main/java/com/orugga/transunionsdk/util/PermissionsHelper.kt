package com.orugga.transunionsdk.util

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.preference.PreferenceManager
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.orugga.transunionsdk.R


class PermissionsHelper {
    companion object {
        const val LOCATION_PERMISSION = 42
        const val DOC_CAPTURE_PERMISSION = 43
        private val DOC_CAPTURE_PERMISSIONS_REQUIRED = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        private val userAnswer = object: UserAnswer {
            override fun userAnswered() {
                userAnswered = true
            }
        }
        private var userAnswered = false

        fun checkLocationPermission(
            activity: Activity,
            onCancel: (() -> Unit)
        ): UserAnswer {
            val sharedPreferences =
                PreferenceManager.getDefaultSharedPreferences(activity.applicationContext)
            if (ContextCompat.checkSelfPermission(
                    activity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        activity,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    )
                ) {
                    ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        LOCATION_PERMISSION
                    )
                }
            } else {
                val alreadyAskedForPermission = sharedPreferences.getBoolean(
                    SharedPreferencesHelper.LOCATION_PERMISSION_ASKED,
                    false
                )
                if (alreadyAskedForPermission) {
                    openAppSettings(activity, onCancel)
                } else {
                    ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        LOCATION_PERMISSION
                    )
                    sharedPreferences.edit()
                        .putBoolean(SharedPreferencesHelper.LOCATION_PERMISSION_ASKED, true).apply()
                }
            }
            return userAnswer
        }

        fun checkDocCapturePermission(activity : Activity) : Boolean {
            val havePermissions = DOC_CAPTURE_PERMISSIONS_REQUIRED.toList().all {
                ContextCompat.checkSelfPermission(activity,it) == PackageManager.PERMISSION_GRANTED
            }
            if (!havePermissions) {
                if(DOC_CAPTURE_PERMISSIONS_REQUIRED.toList().any {
                        ActivityCompat.shouldShowRequestPermissionRationale(activity, it)}
                ) {
                    val dialog = AlertDialog.Builder(activity)
                        .setTitle("Permission")
                        .setMessage("Permission needed! Do you want to grant them?")
                        .setPositiveButton("Yes") { id, v ->
                            ActivityCompat.requestPermissions(
                                activity, DOC_CAPTURE_PERMISSIONS_REQUIRED, DOC_CAPTURE_PERMISSION)
                        }
                        .setNegativeButton("No") { id, v -> }
                        .create()
                    dialog.show()
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        activity.requestPermissions(DOC_CAPTURE_PERMISSIONS_REQUIRED, DOC_CAPTURE_PERMISSION)
                    }
                }
                return false
            }
            return true
        }

        private fun openAppSettings(activity: Activity, onCancel: (() -> Unit)) {
            if (!userAnswered) {
                return
            }
            val alertDialogBuilder = AlertDialog.Builder(activity)
                .setTitle(activity.getString(R.string.permission_needed))
                .setMessage(activity.getString(R.string.location_permission_needed))
                .setPositiveButton(activity.getString(R.string.open_settings)) { id, i ->
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri: Uri = Uri.fromParts(
                        "package", activity.packageName,
                        null
                    )
                    intent.data = uri
                    activity.startActivity(intent)
                }
                .setNegativeButton("Cancel") { id, i ->
                    onCancel.invoke()
                }
            val dialog = alertDialogBuilder.create()
            dialog.show()
        }
    }

    interface UserAnswer {
        fun userAnswered()
    }
}
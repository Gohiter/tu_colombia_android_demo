package com.orugga.transunionsdk.util

import org.opencv.core.MatOfPoint2f
import org.opencv.core.MatOfPoint
import org.opencv.core.CvType
import org.opencv.core.Point
import org.opencv.core.Size
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

object MathUtils {
    fun toMatOfPointInt(mat: MatOfPoint2f): MatOfPoint {
        val matInt = MatOfPoint()
        mat.convertTo(matInt, CvType.CV_32S)
        return matInt
    }

    fun toMatOfPointFloat(mat: MatOfPoint): MatOfPoint2f {
        val matFloat = MatOfPoint2f()
        mat.convertTo(matFloat, CvType.CV_32FC2)
        return matFloat
    }

    fun angle(p1: Point, p2: Point, p0: Point): Double {
        val dx1 = p1.x - p0.x
        val dy1 = p1.y - p0.y
        val dx2 = p2.x - p0.x
        val dy2 = p2.y - p0.y
        return (dx1 * dx2 + dy1 * dy2) / sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10)
    }

    fun scaleRectangle(original: MatOfPoint2f, scale: Double): MatOfPoint2f {
        val originalPoints = original.toList()
        val resultPoints: MutableList<Point> = ArrayList()
        for (point in originalPoints) {
            resultPoints.add(Point(point.x * scale, point.y * scale))
        }
        val result = MatOfPoint2f()
        result.fromList(resultPoints)
        return result
    }

    fun maxCosine(approx: MatOfPoint2f): Double {
        val approxPoints = approx.toArray()
        var maxCosine = 0.0
        for (i in 2..4) {
            val cosine: Double = abs(angle(approxPoints[i % 4], approxPoints[i - 2], approxPoints[i - 1]))
            maxCosine = cosine.coerceAtLeast(maxCosine)
        }
        return maxCosine
    }

    fun sortPoints(src: MatOfPoint2f): MatOfPoint2f {
        val srcPoints = src.toArray()
        val result = MatOfPoint2f()
        val topLeft = srcPoints.minBy { it.x + it.y }
        val bottomRight = srcPoints.maxBy { it.x + it.y }
        val topRight = srcPoints.minBy { it.y - it.x }
        val bottomLeft = srcPoints.maxBy { it.y - it.x }
        result.fromArray(topLeft, topRight, bottomRight, bottomLeft)
        return result
    }

    fun sortPoints(src: MatOfPoint2f, initialX: Float, initialY: Float): MatOfPoint2f {
        val srcPoints = src.toArray()
        val result = MatOfPoint2f()
        val topLeft = srcPoints.minBy { it.x + it.y }
        topLeft?.x = topLeft?.x?.plus(initialX)
        topLeft?.y = topLeft?.y?.plus(initialY)
        val bottomRight = srcPoints.maxBy { it.x + it.y }
        bottomRight?.x = bottomRight?.x?.plus(initialX)
        bottomRight?.y = bottomRight?.y?.plus(initialY)
        val topRight = srcPoints.minBy { it.y - it.x }
        topRight?.x = topRight?.x?.plus(initialX)
        topRight?.y = topRight?.y?.plus(initialY)
        val bottomLeft = srcPoints.maxBy { it.y - it.x }
        bottomLeft?.x = bottomLeft?.x?.plus(initialX)
        bottomLeft?.y = bottomLeft?.y?.plus(initialY)
        result.fromArray(topLeft, topRight, bottomRight, bottomLeft)
        return result
    }

    fun getRectangleSize(points: List<Point>, margin: Double): Size {
        val averageWidth = (getPointsDistance(points[0], points[1]) + getPointsDistance(points[2], points[3])) / 2.0
        val averageHeight = (getPointsDistance(points[1], points[2]) + getPointsDistance(points[3], points[0])) / 2.0
        return Size(Point(averageWidth + margin, averageHeight+ margin))
    }

    private fun getPointsDistance(p1: Point, p2: Point): Double {
        val dx = p2.x - p1.x
        val dy = p2.y - p1.y
        return sqrt(dx.pow(2) + dy.pow(2))
    }
}

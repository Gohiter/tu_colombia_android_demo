package com.orugga.transunionsdk.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.graphics.PointF
import android.util.Log
import androidx.exifinterface.media.ExifInterface
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import org.opencv.utils.Converters
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


object ImageUtils {
    fun rotateBitmap(original: Bitmap, angle: Float, flipX: Boolean = false): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        if(flipX) {
            matrix.postScale(-1f, 1f)
        }
        return Bitmap.createBitmap(original, 0, 0, original.width, original.height, matrix, true)
    }

    fun getBitmapRotateAngle(imageFile: File): Int {
        var rotate = 0
        val exif = ExifInterface(imageFile.absolutePath)
        val orientation: Int =
            exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

        when (orientation) {
            ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
            ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180
            ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90
        }
        return rotate
    }

    fun flipX(original: Bitmap): Bitmap {
        val matrix = Matrix()
        matrix.postScale(-1f, 1f)
        return Bitmap.createBitmap(original, 0, 0, original.width, original.height, matrix, true)
    }

    fun bitmapToMat(bitmap: Bitmap): Mat {
        val mat = Mat(bitmap.height, bitmap.width, CvType.CV_8U, Scalar(4.toDouble()))
        val bitmap32 = bitmap.copy(Bitmap.Config.ARGB_8888, true)
        Utils.bitmapToMat(bitmap32, mat)
        return mat
    }

    fun matToBitmap(mat: Mat): Bitmap {
        val bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(mat, bitmap)
        return bitmap
    }

    fun resizeToScreenContentSize(bitmap: Bitmap, newWidth: Int, newHeight: Int): Bitmap? {
        if (bitmap.width <= 0 && bitmap.height <= 0) {
            return null
        }
        val scaleWidth = newWidth / bitmap.width
        val scaleHeight = newHeight / bitmap.height
        val matrix = Matrix()
        matrix.postScale(scaleWidth.toFloat(), scaleHeight.toFloat())
        return try {
            Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, false)
        } catch (e : IllegalArgumentException) {
            Log.e("resizeToScreenContent", e.message)
            null
        } finally {
            bitmap.recycle()
        }
    }

    fun getCroppedDocument(bitmap: Bitmap, points: List<PointF>, margin: Double): Bitmap {
        val dstMat: Mat = cropMat(bitmapToMat(bitmap), points, margin)
        return matToBitmap(dstMat)
    }

    fun cropMat(src: Mat, sourcePoints: List<PointF>, margin: Double): Mat {
        var points = ArrayList<Point>()
        points.add(Point(sourcePoints[0].x.toDouble() - margin, sourcePoints[0].y.toDouble() - margin))
        points.add(Point(sourcePoints[1].x.toDouble() + margin, sourcePoints[1].y.toDouble() - margin))
        points.add(Point(sourcePoints[2].x.toDouble() + margin, sourcePoints[2].y.toDouble() + margin))
        points.add(Point(sourcePoints[3].x.toDouble() - margin, sourcePoints[3].y.toDouble() + margin))
        val size = MathUtils.getRectangleSize(points, margin)
        val result = Mat.zeros(size, src.type())
        val imageOutline = getOutline(result)
        val inputDocMat = Converters.vector_Point2f_to_Mat(points)
        val transformation = Imgproc.getPerspectiveTransform(inputDocMat, imageOutline)
        Imgproc.warpPerspective(src, result, transformation, size)
        return result
    }

    private fun getOutline(image: Mat): MatOfPoint2f {
        val topLeft = Point(0.toDouble(), 0.toDouble())
        val topRight = Point(image.cols().toDouble(), 0.toDouble())
        val bottomRight = Point(image.cols().toDouble(), image.rows().toDouble())
        val bottomLeft = Point(0.toDouble(), image.rows().toDouble())
        val points = arrayOf(topLeft, topRight, bottomRight, bottomLeft)
        val result = MatOfPoint2f()
        result.fromArray(*points)
        return result
    }

    /** Helper function used to create a timestamped file */
    fun createFile(baseFolder: File, format: String, extension: String) =
        File(baseFolder, SimpleDateFormat(format, Locale.US)
            .format(System.currentTimeMillis()) + extension)

    fun getOutputDirectory(context: Context): File {
        val appContext = context.applicationContext
        val mediaDir = context.externalMediaDirs.firstOrNull()?.let {
            File(it, "transunionsdk").apply { mkdirs() } }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else appContext.filesDir
    }

    fun readFileToBytes(file: File) : ByteArray {
        val bytes = ByteArray(file.length().toInt())

        var fis: FileInputStream? = null
        try {
            fis = FileInputStream(file)
            //read file into bytes[]
            fis.read(bytes)
        } catch (e: IOException) {
            Log.e("readFileToBytes", e.message)
        }
        finally {
            fis?.close()
        }
        return bytes
    }
}
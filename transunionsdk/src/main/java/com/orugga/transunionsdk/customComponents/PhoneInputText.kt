package com.orugga.transunionsdk.customComponents

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.orugga.transunionsdk.R
import com.orugga.transunionsdk.databinding.CustomPhoneInputTextBinding
import com.orugga.transunionsdk.util.SharedPreferencesHelper
import com.orugga.transunionsdk.vo.Phone
import kotlinx.android.synthetic.main.custom_phone_input_text.view.*


class PhoneInputText@JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    private var phoneNumber: Phone? = null
    private var sharedPreferences: SharedPreferences
    private var maxSize = 20
    private val gson = Gson()
    val binding: CustomPhoneInputTextBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_phone_input_text,
            this, true)
    init {
        attrs?.let {
            val styledAttributes = context.obtainStyledAttributes(it, R.styleable.PhoneInputText, 0, 0)
            binding.etPhone.hint = styledAttributes.getString(R.styleable.PhoneInputText_tu_phone_hint)
            binding.txtPhone.text = styledAttributes.getString(R.styleable.PhoneInputText_tu_phone_label)
            binding.txtCode.text = styledAttributes.getString(R.styleable.PhoneInputText_tu_phone_code_label)
            val textSize = styledAttributes.getDimensionPixelSize(R.styleable.PhoneInputText_tu_phone_text_size, 0)
            if (textSize != 0){
                binding.etPhone.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize.toFloat())
                binding.spinnerCountryCode.setTextSize(textSize)
            }
            val labelTextSize = styledAttributes.getDimensionPixelSize(R.styleable.PhoneInputText_tu_phone_label_size, 0)
            if (labelTextSize != 0){
                binding.txtPhone.setTextSize(TypedValue.COMPLEX_UNIT_PX, labelTextSize.toFloat())
                binding.txtCode.setTextSize(TypedValue.COMPLEX_UNIT_PX, labelTextSize.toFloat())
            }
            val textColor = styledAttributes.getColor(R.styleable.PhoneInputText_tu_phone_text_color, -1)
            if (textColor != -1){
                binding.etPhone.setTextColor(textColor)
            }
            val labelTextColor = styledAttributes.getColor(R.styleable.PhoneInputText_tu_phone_label_color, -1)
            if (labelTextColor != -1){
                binding.txtPhone.setTextColor(labelTextColor)
                binding.txtCode.setTextColor(labelTextColor)
            }
            val labelFont = styledAttributes.getResourceId(R.styleable.PhoneInputText_tu_phone_label_font, -1)
            if(labelFont != -1){
                binding.txtPhone.typeface = ResourcesCompat.getFont(context, labelFont)
                binding.txtCode.typeface = ResourcesCompat.getFont(context, labelFont)
            }
            val textFont = styledAttributes.getResourceId(R.styleable.PhoneInputText_tu_phone_text_font, -1)
            if(textFont != -1){
                binding.etPhone.typeface = ResourcesCompat.getFont(context, textFont)
            }
            val drawableColor = styledAttributes.getColor(R.styleable.PhoneInputText_tu_phone_drawable_color, -1)
            if (drawableColor != -1){
                binding.ivPhone.setColorFilter(drawableColor)
            }
            val underlineColor = styledAttributes.getColor(R.styleable.PhoneInputText_tu_phone_underline_color, -1)
            if (underlineColor != -1){
                binding.vPhoneLine.setBackgroundColor(underlineColor)
                binding.vCodeLine.setBackgroundColor(underlineColor)
            }
            val threshold = styledAttributes.getInt(R.styleable.PhoneInputText_tu_phone_threshold, 1)
            binding.etPhone.threshold = threshold
            binding.etPhone.setOnFocusChangeListener { _, hasFocus ->
                if(hasFocus){
                    if(threshold == 0) {
                        val text = if(binding.etPhone.text.trim().isNotEmpty()) binding.etPhone.text.trim().toString() else " "
                        binding.etPhone.setText(text)
                    }
                }
            }
            val nextFocus = styledAttributes.getResourceId(R.styleable.PhoneInputText_tu_phone_next_focus, -1)
            if(nextFocus != -1){
                binding.etPhone.imeOptions = EditorInfo.IME_ACTION_NEXT
                binding.etPhone.nextFocusForwardId = nextFocus
            } else {
                binding.etPhone.imeOptions = EditorInfo.IME_ACTION_DONE
            }

            maxSize = styledAttributes.getInt(R.styleable.PhoneInputText_tu_phone_max_size, 20)
            binding.spinnerCountryCode.customMasterCountries = styledAttributes.getString(R.styleable.PhoneInputText_tu_phone_countries)
            binding.spinnerCountryCode.setDefaultCountryUsingNameCodeAndApply(styledAttributes.getString(R.styleable.PhoneInputText_tu_phone_default_country))
            styledAttributes.recycle()
        }
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.applicationContext)
        val phones = getPhones()
        val adapter = PhoneAdapter(context, android.R.layout.simple_list_item_1, phones)
        binding.etPhone.setAdapter(adapter)
        binding.etPhone.setOnItemClickListener { parent, _, position, _ ->
            phoneNumber = parent.adapter.getItem(position) as Phone?
            phoneNumber?.let {
                binding.etPhone.setText(it.nationalNumber)
                binding.spinnerCountryCode.setCountryForPhoneCode(it.countryCode.toInt())
            }
        }
        binding.root.setOnFocusChangeListener { _, hasFocus ->
            if(hasFocus){
                binding.etPhone.requestFocus()
            }
        }
    }

    fun getPhoneNumber(): String? {
        return getPhone()?.getPhoneNumber()
    }

    fun getPhone(): Phone? {
        val phone = Phone(binding.spinnerCountryCode.selectedCountryCode, etPhone.text.trim().toString())
        if(!phone.getPhoneNumber().isNullOrEmpty()) {
            addPhone(phone)
        }
        return phone
    }

    fun isValid(): Boolean {
        val phone = getPhoneNumber()
        return hasCountryCode() && hasRegionalNumber() && phone?.matches(Regex("\\+?[0-9]{4,}")) ?: false
    }

    fun hasCountryCode(): Boolean {
        return !binding.spinnerCountryCode.selectedCountryCode.isNullOrEmpty()
    }

    fun hasRegionalNumber(): Boolean {
        return !etPhone.text.trim().toString().isNullOrEmpty()
    }

    fun addPhones(phones: List<Phone>) {
        phones.forEach {phone ->
            addPhone(phone)
        }
    }

    private fun addPhone(phone: Phone?){
        if (phone != null) {
            val phonesList = getPhones()
            if(phonesList.size == maxSize){
                phonesList.remove(phonesList.first())
            }
            if(!phonesList.contains(phone)){
                phonesList.add(phone)
                sharedPreferences.edit().putString(SharedPreferencesHelper.PHONES_LIST_KEY, gson.toJson(phonesList)).commit()
            }
        }
    }

    fun getPhones(): MutableList<Phone>{
        val phonesJson = sharedPreferences.getString(SharedPreferencesHelper.PHONES_LIST_KEY, "[]")
        val type = object : TypeToken<List<Phone>>(){}.type
        return gson.fromJson(phonesJson, type)
    }
}
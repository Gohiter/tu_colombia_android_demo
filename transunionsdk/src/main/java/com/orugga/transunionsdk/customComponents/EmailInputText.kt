package com.orugga.transunionsdk.customComponents

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.AttributeSet
import android.util.Patterns
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.orugga.transunionsdk.R
import com.orugga.transunionsdk.databinding.CustomEmailInputTextBinding
import com.orugga.transunionsdk.util.SharedPreferencesHelper

class EmailInputText@JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    private var sharedPreferences: SharedPreferences
    private var maxSize = 20
    private val gson = Gson()
    val binding: CustomEmailInputTextBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.custom_email_input_text,
            this, true)
    init {
        attrs?.let {
            val styledAttributes = context.obtainStyledAttributes(it, R.styleable.EmailInputText, 0, 0)
            binding.etEmail.hint = styledAttributes.getString(R.styleable.EmailInputText_tu_email_hint)
            binding.txtEmail.text = styledAttributes.getString(R.styleable.EmailInputText_tu_email_label)
            val labelFont = styledAttributes.getResourceId(R.styleable.EmailInputText_tu_email_label_font, -1)
            if(labelFont != -1){
                binding.txtEmail.typeface = ResourcesCompat.getFont(context, labelFont)
            }
            val textSize = styledAttributes.getDimensionPixelSize(R.styleable.EmailInputText_tu_email_text_size, 0)
            if (textSize != 0){
                binding.etEmail.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize.toFloat())
            }
            val labelTextSize = styledAttributes.getDimensionPixelSize(R.styleable.EmailInputText_tu_email_label_size, 0)
            if (labelTextSize != 0){
                binding.txtEmail.setTextSize(TypedValue.COMPLEX_UNIT_PX, labelTextSize.toFloat())
            }
            val textColor = styledAttributes.getColor(R.styleable.EmailInputText_tu_email_text_color, -1)
            if (textColor != -1){
                binding.etEmail.setTextColor(textColor)
            }
            val labelTextColor = styledAttributes.getColor(R.styleable.EmailInputText_tu_email_label_color, -1)
            if (labelTextColor != -1){
                binding.txtEmail.setTextColor(labelTextColor)
            }
            val textFont = styledAttributes.getResourceId(R.styleable.EmailInputText_tu_email_text_font, -1)
            if(textFont != -1){
                binding.etEmail.typeface = ResourcesCompat.getFont(context, textFont)
            }
            val drawableColor = styledAttributes.getColor(R.styleable.EmailInputText_tu_email_drawable_color, -1)
            if (drawableColor != -1){
                binding.ivEmail.setColorFilter(drawableColor)
            }
            val underlineColor = styledAttributes.getColor(R.styleable.EmailInputText_tu_email_underline_color, -1)
            if (underlineColor != -1){
                binding.vEmail.setBackgroundColor(underlineColor)
            }
            val threshold = styledAttributes.getInt(R.styleable.EmailInputText_tu_email_threshold, 1)
            binding.etEmail.threshold = threshold
            binding.etEmail.setOnFocusChangeListener { _, hasFocus ->
                if(hasFocus){
                    if(threshold == 0) {
                        val text = if(binding.etEmail.text.trim().isNotEmpty()) binding.etEmail.text.trim().toString() else " "
                        binding.etEmail.setText(text)
                    }
                }
            }
            val nextFocus = styledAttributes.getResourceId(R.styleable.EmailInputText_tu_email_next_focus, -1)
            if(nextFocus != -1){
                binding.etEmail.imeOptions = EditorInfo.IME_ACTION_NEXT
                binding.etEmail.nextFocusForwardId = nextFocus
            } else {
                binding.etEmail.imeOptions = EditorInfo.IME_ACTION_DONE
            }
            maxSize = styledAttributes.getInt(R.styleable.EmailInputText_tu_email_max_size, 20)
            styledAttributes.recycle()
        }
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.applicationContext)
        val emailList = getEmailList()
        val adapter = EmailAdapter(context, android.R.layout.simple_list_item_1, emailList)
        binding.etEmail.setAdapter(adapter)
        binding.root.setOnFocusChangeListener { _, hasFocus ->
            if(hasFocus){
                binding.etEmail.requestFocus()
            }
        }
    }

    fun getEmail(): String? {
        val email = binding.etEmail.text.trim().toString()
        if(!email.isNullOrEmpty()) {
            addEmail(email)
        }
        return email
    }

    fun isValid(): Boolean {
        val email = getEmail()
        return !email.isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun addEmail(email: String){
        val emailList = getEmailList()
        if(emailList.size == maxSize){
            emailList.remove(emailList.first())
        }
        if(!emailList.contains(email)){
            emailList.add(email)
            sharedPreferences.edit().putString(SharedPreferencesHelper.EMAIL_LIST_KEY, gson.toJson(emailList)).commit()
        }
    }

    fun addEmails(emails: List<String>) {
        emails.forEach{ email ->
            addEmail(email)
        }
    }

    fun getEmailList(): ArrayList<String>{
        val emailListJson = sharedPreferences.getString(SharedPreferencesHelper.EMAIL_LIST_KEY, "[]")
        val type = object : TypeToken<List<String>>(){}.type
        return gson.fromJson(emailListJson, type)
    }

}
package com.orugga.transunionsdk.customComponents

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.orugga.transunionsdk.vo.Phone

class PhoneAdapter(context: Context, @LayoutRes private val layoutResource: Int, private val allPhones: List<Phone>):
    ArrayAdapter<Phone>(context, layoutResource, allPhones),
    Filterable {
    private var phones: List<Phone> = allPhones

    override fun getCount(): Int {
        return phones.size.coerceAtMost(2)
    }

    override fun getItem(p0: Int): Phone? {
        return phones[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: TextView = convertView as TextView? ?: LayoutInflater.from(context).inflate(layoutResource, parent, false) as TextView
        view.text = phones[position].getPhoneNumber()
        return view
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                phones = filterResults.values as List<Phone>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val queryString = charSequence?.toString()?.toLowerCase()?.trim()
                val filterResults = FilterResults()
                phones = allPhones
                filterResults.values = if (queryString==null || queryString.isEmpty()) {
                    phones
                } else {
                    phones.filter {
                        it.getPhoneNumber().contains(queryString)
                    }
                }
                return filterResults
            }
        }
    }
}
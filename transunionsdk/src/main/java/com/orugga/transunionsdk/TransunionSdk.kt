package com.orugga.transunionsdk

import android.content.Context
import android.util.Log
import com.google.android.gms.ads.internal.gmsg.HttpClient
import com.orugga.transunionsdk.api.TokenInterceptor
import com.orugga.transunionsdk.api.TransunionSdkService
import com.orugga.transunionsdk.helper.iovation.FraudManager
import com.orugga.transunionsdk.helper.verification.VerificationHelper
import com.orugga.transunionsdk.helper.session.SessionHelper
import com.orugga.transunionsdk.repository.AuthRepository
import com.orugga.transunionsdk.repository.VerificationRepository
import com.orugga.transunionsdk.util.SharedPreferencesHelper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import com.google.gson.GsonBuilder
import com.orugga.transunionsdk.helper.credolab.CredoLabManager
import com.orugga.transunionsdk.helper.documents.DocumentsHelper
import com.orugga.transunionsdk.repository.DocumentsRepository
import okhttp3.CertificatePinner
import java.net.HttpCookie
import java.security.KeyStore
import java.security.SecureRandom

import java.util.UUID
import javax.net.ssl.KeyManagerFactory
import kotlin.jvm.Throws


class TransunionSdk(appContext: Context, baseUrl: String, ccmUser: String = "", ccmPassword: String = "", ccmCertificatePassword: String = "" )
{
    private var authRepository: AuthRepository
    private var verificationRepository: VerificationRepository
    private var documenstRepository: DocumentsRepository
    private var sharedPreferencesHelper: SharedPreferencesHelper = SharedPreferencesHelper(
        appContext
    )
    private val baseUrl = baseUrl
    private val ccmUser = ccmUser
    private val ccmPassword = ccmPassword
    private val ccmCertificatePassword = ccmCertificatePassword
    private var context : Context = appContext

    private var transunionSdkService: TransunionSdkService
    var fraudManager = FraudManager
        get() = field
    var sessionHelper: SessionHelper
        get() = field
    var verificationHelper: VerificationHelper
        get() = field
    var documentsHelper: DocumentsHelper
        get() = field
    var credoLabManager = CredoLabManager
        get() = field
//    var onfido
    var logging : HttpLoggingInterceptor

    init {
        logging = HttpLoggingInterceptor()
        transunionSdkService = initializeTransunionSdkService(
            TokenInterceptor(
                sharedPreferencesHelper
            )
        )
        authRepository = AuthRepository(transunionSdkService, sharedPreferencesHelper)
        sessionHelper = SessionHelper.get(authRepository)

        verificationRepository = VerificationRepository(this, transunionSdkService, sharedPreferencesHelper)
        verificationHelper = VerificationHelper.get(verificationRepository)
        documenstRepository = DocumentsRepository(transunionSdkService, sharedPreferencesHelper)
        documentsHelper = DocumentsHelper.get(documenstRepository)

    }


    private fun initializeTransunionSdkService(tokenInterceptor: TokenInterceptor) : TransunionSdkService {
        logging = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {

            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return arrayOf()
            }

            @Throws(CertificateException::class)
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
            }

            @Throws(CertificateException::class)
            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
            }
        })
        // Install the all-trusting trust manager
        val sslContext = SSLContext.getInstance("SSL")
//        sslContext.init(null, trustAllCerts, java.security.SecureRandom())

        // Create an ssl socket factory with our all-trusting manager

//        val certpinner = CertificatePinner.
        val httpClient = OkHttpClient.Builder()





        val file_name = "TRANS371_SHA2.p12"
//        this.app
        val stringCertificate = context.assets.open(file_name).bufferedReader().use{
            it.readText()
        }
//        context.assets.open
//        KeyStore p12 = KeyStore.comp
        val p12 = KeyStore.getInstance("pkcs12")
        p12.load(context.assets.open(file_name), "TUoru99a".toCharArray())
//        val certificate = p12.getCertificate("{7bc07416-4e66-4e84-99d9-bf8cfd9c1172}".toUpperCase()) // Probar sin corchetes
            val kmf = KeyManagerFactory.getInstance("X509")
            kmf.init(p12, "TUoru99a".toCharArray())
            val keyManagers = kmf.keyManagers
            sslContext.init(keyManagers, trustAllCerts, SecureRandom())


//        p12.get
//        certificate.
//    p12.

//        keytool -list -v -keystore TRANS371_SHA2.p12 -storetype PKCS12 -storepass TUoru99a
//        val publicKey = certificate.publicKey.encoded

//        val hash = publicKey.hashCode()
//

        val certificatePinner = CertificatePinner.Builder()
//            .add("api-de-test.transunion.com", "sha256/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=")
            .add("api-de-test.transunion.com", "sha256/+Tk3bB7DDuMenIQqir2b5cDYjEoFEsZvmrqm4gumtps=")
//            .add("api-de-test.transunion.com", "sha256/980Ionqp3wkYtN9SZVgMzuWQzJta1nfxNPwTem1X0uc=")
//            .add("api-de-test.transunion.com", "sha256/du6FkDdMcVQ3u8prumAo6t3i3G27uMP2EOhR8R0at/U=")

        val sslSocketFactory = sslContext.socketFactory
        trustAllCerts.forEach { trustManager ->
            Log.d("509", if (trustManager is X509TrustManager) "ok" else "error")
            httpClient.sslSocketFactory(sslSocketFactory, trustManager as X509TrustManager)
        }

//        httpClient.certificatePinner(certificatePinner.build())
        httpClient.hostnameVerifier { _, _ -> true }
        httpClient.addInterceptor(logging).addInterceptor(tokenInterceptor)
        httpClient.addInterceptor(logging)
        httpClient.callTimeout(2, TimeUnit.MINUTES)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS);

        val gson = GsonBuilder()
            .setLenient()
            .create()
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient.build())
            .build()
            .create(TransunionSdkService::class.java)
    }

    fun restartUserSession() {
        val csid = UUID.randomUUID().toString()
        BiocatchSdk.updateCsid(csid)
        sharedPreferencesHelper.saveCurrentCSID(csid)
    }

    fun changeContext(contextName: String) {
        BiocatchSdk.changeContext(contextName)
    }
    fun configTransunion(user: String, password: String, oauthGranType: String, externalApllicationId: String, solutionSetName: String) {
        sharedPreferencesHelper.setTransunionSDK(user, password, oauthGranType, externalApllicationId, solutionSetName)
    }
    fun setCredoLab(credoLabKey: String, credoLabUser: String, credoLabPassword: String) {
        sharedPreferencesHelper.setCredoLab(credoLabKey, credoLabUser, credoLabPassword)
        credoLabManager.setReferenceKey(credoLabKey)
    }
    fun setBaseURL() {

    }
    fun setDemoSDK() {

    }
    companion object {
        fun create(appContext: Context, baseUrl: String, ccmUser: String = "", ccmPassword: String = "", ccmCertificatePassword: String = "" ) : TransunionSdk {
            return TransunionSdk(appContext, baseUrl, ccmUser, ccmPassword, ccmCertificatePassword )
        }
    }
}

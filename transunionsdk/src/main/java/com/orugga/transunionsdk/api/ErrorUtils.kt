package com.orugga.transunionsdk.api

import org.json.JSONArray
import retrofit2.Response
import java.lang.Exception

object ErrorUtils {
    private const val ERROR_OBJECT_INDEX = 0

    fun parseError(response: Response<*>): APIError {
        return try {
            val jsonError = JSONArray(response.errorBody()!!.string()).getJSONObject(ERROR_OBJECT_INDEX)
            APIError(jsonError.getString("Message"), response.code().toString())
        } catch (e: Exception) {
            APIError("", "")
        }

    }
}
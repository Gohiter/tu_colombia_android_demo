package com.orugga.transunionsdk.api

import android.util.Log
import com.orugga.transunionsdk.util.SharedPreferencesHelper
import okhttp3.Interceptor
import okhttp3.Response

class TokenInterceptor(private val sharedPreferencesHelper: SharedPreferencesHelper, private val isDemoEnvionemtn: Boolean = true, private val CCMUserName: String = "", private val CCMPassword: String = "", private val CCMClientUser: String = "") : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        val original = chain.request().newBuilder().build()

        if (original.url.encodedPath.contains("/Token")
            || original.url.encodedPath.contains("amazonaws")
            || original.url.encodedPath.contains("/account/login")) {
            return chain.proceed(original)
        }

        val originalHttpUrl = original.url.toUrl().toString()

        if (original.url.encodedPath.contains("/datasets")) {
            val requestBuilder = original.newBuilder()
                .addHeader("Authorization", "Bearer ${sharedPreferencesHelper.getCredoLabToken().access_token}")
                .url(originalHttpUrl)
            val request = requestBuilder.build()
            return chain.proceed(request)
        }
        if (original.url.toUrl().toString().contains("onfido")) {
            Log.d("Interceptor Onfido", "Interceptor Success")
            val requestBuilder = original.newBuilder()
                    .addHeader("Authorization", "Token  token=${sharedPreferencesHelper.getOnfidoToken()}")
                    .addHeader("CLIENT-USER", "OruggaUser")
                    .url(originalHttpUrl)
            val request = requestBuilder.build()
            return chain.proceed(request)
        }
        if (isDemoEnvionemtn) {
            Log.d("Interceptor DEMO-ENV", "Interceptor Success")
            val requestBuilder = original.newBuilder()
//                .addHeader("Authorization", "Token  token=${sharedPreferencesHelper.getOnfidoToken()}")
                .addHeader("Authorization", "Basic VFJBTlMzNzE6VFVvcnU5OWE=")
                .addHeader("CLIENT-USER", "OruggaUser")
                .url(originalHttpUrl)
            val request = requestBuilder.build()
            return chain.proceed(request)
        }
        val requestBuilder = original.newBuilder()
                .addHeader("Authorization", "Bearer ${sharedPreferencesHelper.getAuth().access_token}")
                .url(originalHttpUrl)
        val request = requestBuilder.build()
        val response = chain.proceed(request)

//        if (response.code() == 401) {
//            val httpClient = OkHttpClient.Builder()
//            val retrofit2 = Retrofit.Builder()
//                    .baseUrl(BuildConfig.BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .client(httpClient.build())
//                    .build()
//                    .create(TransunionSdkService::class.java)
//            val clientId = ""
//            val grantType = "refresh_token"
//            val refreshToken = sharedPreferencesHelper.getAuth().refresh_token
//            val responseNewToken = retrofit2.refreshToken(clientId, grantType, refreshToken).execute()
//            when {
//                responseNewToken.code() == 200 -> {
//                    val auth = responseNewToken.body()!!
//                    sharedPreferencesHelper.saveAuth(auth)
//                    return chain.proceed(request)
//                }
//                else -> {
//                }
//            }
//        }
        return response
    }
}
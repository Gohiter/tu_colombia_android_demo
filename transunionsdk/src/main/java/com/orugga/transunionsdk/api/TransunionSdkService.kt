package com.orugga.transunionsdk.api

import com.orugga.transunionsdk.vo.TokenResponse
import com.orugga.transunionsdk.vo.biocatch.BiocatchRequest
import com.orugga.transunionsdk.vo.biocatch.BiocatchResponse
import com.orugga.transunionsdk.vo.completeTransaction.CompleteTransactionRequest
import com.orugga.transunionsdk.vo.completeTransaction.CompleteTransactionResponse
import com.orugga.transunionsdk.vo.createApp.CreateAppRequest
import com.orugga.transunionsdk.vo.createApp.CreateAppResponse
import com.orugga.transunionsdk.vo.createDocument.CreateAttachDocumentResponse
import com.orugga.transunionsdk.vo.createDocument.CreateDocumentRequest
import com.orugga.transunionsdk.vo.credoLab.CredoLabTokenRequest
import com.orugga.transunionsdk.vo.credoLab.CredoLabTokenResponse
import com.orugga.transunionsdk.vo.credoLab.DataSetInsightsResponse
import com.orugga.transunionsdk.vo.deviceVerification.DeviceVerificationRequest
import com.orugga.transunionsdk.vo.deviceVerification.DeviceVerificationResponse
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceRequest
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceResponse
import com.orugga.transunionsdk.vo.docAuthenticationReport.DocAuthReportResponse
import com.orugga.transunionsdk.vo.docAuthenticationReport.DocAuthRequest
import com.orugga.transunionsdk.vo.emailVerification.EmailVerificationRequest
import com.orugga.transunionsdk.vo.emailVerification.EmailVerificationResponse
import com.orugga.transunionsdk.vo.facialSimilarityInvokeService.FacialSimilarityInvokeServiceRequest
import com.orugga.transunionsdk.vo.facialSimilarityInvokeService.FacialSimilarityInvokeServiceResponse
import com.orugga.transunionsdk.vo.facialSimilarityReport.FacialSimilarityReportResponse
import com.orugga.transunionsdk.vo.offers.OffersRequest
import com.orugga.transunionsdk.vo.offers.OffersResponse
import com.orugga.transunionsdk.vo.phoneVerification.PhoneVerificationRequest
import com.orugga.transunionsdk.vo.phoneVerification.PhoneVerificationResponse
import com.orugga.transunionsdk.vo.pullReport.PullReportRequest
import com.orugga.transunionsdk.vo.pullReport.PullReportResponse
import com.orugga.transunionsdk.vo.videoliveness.OnfidoSDKRequest
import com.orugga.transunionsdk.vo.videoliveness.OnfidoSDKResponse
import com.orugga.transunionsdk.vo.videoliveness.OnfidoVideoResponse
import com.orugga.transunionsdk.vo.watchlist.WatchlistRequest
import com.orugga.transunionsdk.vo.watchlist.WatchlistResponse
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

/**
 * REST API access points
 */
interface TransunionSdkService {

    @FormUrlEncoded
    @POST(Endpoints.getToken)
    suspend fun getToken(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("grant_type") grant_type: String): Response<TokenResponse>

    @POST(Endpoints.registerApp)
    suspend fun createApp(@Body createAppRequest: CreateAppRequest) : Response<CreateAppResponse>

    @POST(Endpoints.requestService)
    suspend fun sendEmail(@Path("applicationId") applicationId: String, @Body sendPhoneRequest: EmailVerificationRequest) : Response<EmailVerificationResponse>

    @POST(Endpoints.requestService)
    suspend fun sendPhone(@Path("applicationId") applicationId: String, @Body sendEmailRequest: PhoneVerificationRequest) : Response<PhoneVerificationResponse>

    @POST(Endpoints.requestService)
    suspend fun watchlist(@Path("applicationId") applicationId: String, @Body watchlistRequest: WatchlistRequest) : Response<WatchlistResponse>

    @POST(Endpoints.requestService)
    suspend fun invokeDocService(@Path("applicationId") applicationId: String, @Body docAuthInvokeServiceRequest: DocAuthInvokeServiceRequest) : Response<DocAuthInvokeServiceResponse>

    @POST(Endpoints.requestService)
    suspend fun invokeFacialSimilarityService(@Path("applicationId") applicationId: String, @Body facialSimilarityInvokeServiceRequest: FacialSimilarityInvokeServiceRequest) : Response<FacialSimilarityInvokeServiceResponse>

    @POST(Endpoints.createDocuments)
    suspend fun createDocument(@Path("applicationId") applicationId: String, @Body createDocumentRequest: CreateDocumentRequest) : Response<CreateAttachDocumentResponse>

    @POST(Endpoints.uploadDocument)
    suspend fun uploadDocument(@Path("documentLocationIdentifier") documentLocationIdentifier: String, @Body body: RequestBody) : Response<CreateAttachDocumentResponse>

    /**
     * The docAuthRequest object is the same for every document, it's necessary to set only one
     * of Front/Back/Selfie and the step as front/back/selfie
     *
     */
    @POST(Endpoints.docAuth)
    suspend fun documentAuthentication(@Path("applicationId") applicationId: String, @Body docAuthRequest: DocAuthRequest) : Response<DocAuthInvokeServiceResponse>

    /**
     * The docAuthRequest object is the same for every document, it's necessary to set the Front
     * object and the step as continue
     *
     */
    @POST(Endpoints.docAuth)
    suspend fun documentFrontReport(@Path("applicationId") applicationId: String, @Body docAuthRequest: DocAuthRequest) : Response<DocAuthReportResponse>

    /**
     * The docAuthRequest object is the same for every document, it's necessary to set only the Selfie
     * object and the step as continue
     *
     */
    @POST(Endpoints.docAuth)
    suspend fun facialSimilarityReport(@Path("applicationId") applicationId: String, @Body docAuthRequest: DocAuthRequest) : Response<FacialSimilarityReportResponse>

    @POST(Endpoints.requestService)
    suspend fun pullReport(@Path("applicationId") applicationId: String, @Body pullReportRequest: PullReportRequest) : Response<PullReportResponse>

    @GET(Endpoints.getPublicIp)
    suspend fun getPublicId() : Response<String>

    @POST(Endpoints.requestService)
    suspend fun deviceVerification(
        @Path("applicationId") appId: String,
        @Body deviceVerificationRequest: DeviceVerificationRequest
    )
            : Response<DeviceVerificationResponse>

    @POST(Endpoints.requestService)
    suspend fun getBiocatchScore(@Path("applicationId") applicationId: String, @Body biocatchRequest: BiocatchRequest) : Response<BiocatchResponse>

    @POST(Endpoints.requestService)
    suspend fun getOffers(@Path("applicationId") applicationId: String, @Body offersRequest: OffersRequest) : Response<OffersResponse>

    @POST(Endpoints.getCredoLabToken)
    suspend fun getCredoLabToken(@Body credoLabTokenRequest: CredoLabTokenRequest): Response<CredoLabTokenResponse>

    @GET(Endpoints.getCredoLabInsights)
    suspend fun getCredoLabInsights(@Path("referenceNumber") referenceNumber: String) : Response<DataSetInsightsResponse>

    @POST(Endpoints.requestService)
    suspend fun completeTransaction(@Path("applicationId") applicationId: String, @Body completeTransactionRequest: CompleteTransactionRequest) : Response<CompleteTransactionResponse>

    @POST(Endpoints.getOnfidoSDKToken)
    suspend fun getOnfidoSDKToken(@Body onfidoSDKRequest: OnfidoSDKRequest): Response<OnfidoSDKResponse>

    @GET(Endpoints.getOnfidoVideos)
    suspend fun getOnfidoVideos(@Path("applicant_id") applicantId: String) : Response<OnfidoVideoResponse>
  }
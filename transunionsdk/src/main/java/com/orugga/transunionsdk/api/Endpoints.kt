package com.orugga.transunionsdk.api

import com.orugga.transunionsdk.BuildConfig

object Endpoints {
    const val getPublicIp = "https://checkip.amazonaws.com"
    const val getToken = "Token"
    const val registerApp = "applications"
    const val requestService = "applications/{applicationId}/queues/TSO_ServiceInput"
    const val docAuth = "applications/{applicationId}/queues/TSO_DocAuth"
    const val uploadDocument = "documents/{documentLocationIdentifier}"
    const val createDocuments = "applications/{applicationId}/documents"
    const val getCredoLabToken = "${BuildConfig.CREDOLAB_URL}/v5.0/account/login"
    const val getCredoLabInsights = "${BuildConfig.CREDOLAB_URL}/v5.0/datasets/{referenceNumber}/datasetinsight"
    const val getOnfidoSDKToken = "${BuildConfig.ONFIDO_URL}/v3/sdk_token"
    const val getOnfidoVideos = "${BuildConfig.ONFIDO_URL}/v3/live_videos?applicant_id={applicant_id}"
}
package com.orugga.transunionsdk.docCapture

import android.graphics.Bitmap
import android.graphics.PointF
import com.orugga.transunionsdk.util.ImageUtils
import com.orugga.transunionsdk.util.MathUtils
import org.opencv.android.Utils
import org.opencv.core.MatOfPoint2f
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.core.CvType
import org.opencv.core.MatOfPoint
import org.opencv.core.MatOfInt
import org.opencv.core.Core
import org.opencv.imgproc.Imgproc
import kotlin.math.abs
import kotlin.math.ceil


object DocCaptureUtils {
    private const val THRESHOLD_LEVEL = 2
    private const val CANNY_THRESHOLD_1 = 10.0
    private const val CANNY_THRESHOLD_2 = 20.0
    private const val MAX_THRESHOLD = 255.0
    private const val K_SIZE = 9
    private const val AREA_LOWER_THRESHOLD = 0.2
    private const val AREA_UPPER_THRESHOLD = 0.98
    private const val DOWNSCALE_IMAGE_SIZE = 600.0
    private const val MAX_COS = 0.3

    fun getContourEdgePoints(src: Bitmap): List<PointF>? {
        val matOfPoint2f: MatOfPoint2f = getLargestContour(src) ?: return null
        val points = listOf(*matOfPoint2f.toArray())
        val result= ArrayList<PointF>()
        for (i in points.indices) {
            result.add(PointF(points[i].x.toFloat(), points[i].y.toFloat()))
        }
        return result
    }

    fun getLargestContour(bitmap: Bitmap): MatOfPoint2f? {
        val src = Mat(bitmap.height, bitmap.width, CvType.CV_8UC1)
        Utils.bitmapToMat(bitmap, src)
        // Downscale image for better performance.
        val ratio = DOWNSCALE_IMAGE_SIZE / src.width().coerceAtLeast(src.height())
        val downscaledSize = Size(src.width() * ratio, src.height() * ratio)
        val downscaled = Mat(downscaledSize, src.type())
        Imgproc.resize(src, downscaled, downscaledSize)
        val rectangles =
            getAllContours(
                downscaled
            )
        if (rectangles.isEmpty()) {
            return null
        }
        rectangles.sortWith(Comparator { p0, p1 -> ceil(Imgproc.contourArea(p1) - Imgproc.contourArea(p0)).toInt() })
        val largestRectangle = rectangles[0]
        return MathUtils.scaleRectangle(
            largestRectangle,
            1f / ratio
        )
    }

    private fun getAllContours(src: Mat): ArrayList<MatOfPoint2f> {
        // Blur the image to filter out the noise.
        val blurred = Mat()
        Imgproc.medianBlur(src, blurred,
            K_SIZE
        )
        // Initialize variables
        val gray0 = Mat(blurred.size(), CvType.CV_8U)
        val gray = Mat()
        val contours = ArrayList<MatOfPoint>()
        val rectangles = ArrayList<MatOfPoint2f>()
        val sources = ArrayList<Mat>()
        sources.add(blurred)
        val destinations: MutableList<Mat> = ArrayList()
        destinations.add(gray0)
        val srcArea = src.rows() * src.cols()
        // Find squares in every color plane of the image.
        for (c in 0..2) {
            val ch = intArrayOf(c, 0)
            val fromTo = MatOfInt(*ch)
            Core.mixChannels(sources, destinations, fromTo)
            for (l in 0 until THRESHOLD_LEVEL) {
                if (l == 0) {
                    // Use Canny instead of zero threshold level.
                    Imgproc.Canny(gray0, gray,
                        CANNY_THRESHOLD_1,
                        CANNY_THRESHOLD_2
                    )
                    // Dilate Canny output to remove potential holes between edge segments.
                    Imgproc.dilate(gray, gray, Mat.ones(Size(3.0, 3.0), 0))
                } else {
                    val threshold = (l + 1) * MAX_THRESHOLD / THRESHOLD_LEVEL
                    Imgproc.threshold(gray0, gray, threshold,
                        MAX_THRESHOLD, Imgproc.THRESH_BINARY)
                }
                Imgproc.findContours(gray, contours, Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE)
                for (contour in contours) {
                    val contourFloat: MatOfPoint2f =
                        MathUtils.toMatOfPointFloat(
                            contour
                        )
                    val arcLen = Imgproc.arcLength(contourFloat, true) * 0.02
                    // Approximate polygonal curves.
                    val approx = MatOfPoint2f()
                    Imgproc.approxPolyDP(contourFloat, approx, arcLen, true)
                    if (isRectangle(
                            approx,
                            srcArea
                        )
                    ) {
                        rectangles.add(approx)
                    }
                }
            }
        }
        return rectangles
    }

    private fun isRectangle(polygon: MatOfPoint2f, srcArea: Int): Boolean {
        return polygon.rows() == 4 && isAreaInThreshold(
            polygon,
            srcArea
        ) && Imgproc.isContourConvex(
            MathUtils.toMatOfPointInt(polygon)
        ) && MathUtils.maxCosine(polygon) < MAX_COS
    }

    private fun isAreaInThreshold(polygon: MatOfPoint2f, srcArea: Int): Boolean {
        val area = abs(Imgproc.contourArea(polygon))
        return area >= srcArea * AREA_LOWER_THRESHOLD && area <= srcArea * AREA_UPPER_THRESHOLD
    }
}


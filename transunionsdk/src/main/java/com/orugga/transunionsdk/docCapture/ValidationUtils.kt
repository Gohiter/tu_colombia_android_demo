package com.orugga.transunionsdk.docCapture

import android.util.Log
import androidx.camera.core.ImageProxy
import com.orugga.transunionsdk.util.MathUtils
import com.orugga.transunionsdk.vo.ValidationResult
import org.opencv.core.MatOfPoint2f
import org.opencv.core.Point
import org.opencv.core.Mat
import org.opencv.core.CvType
import org.opencv.core.MatOfDouble
import org.opencv.core.Core
import org.opencv.imgproc.Imgproc
import java.nio.ByteBuffer
import kotlin.math.abs
import kotlin.math.pow

object ValidationUtils {
    private const val minBelowAreaPercent = 0.20
    private const val maxCosine = 0.085
    private const val alternativeMaxCosine = 0.342
    private const val maxHeightPercent = 0.9
    private const val maxWidthPercent = 0.9
    private const val minOffCenter = 20
    private const val maxEdgeDistorted = 100
    private const val minBlur = 50
    private const val minLuminosity = 100.0
    private const val maxArea = 0.95

    fun isTooDark(image: ImageProxy): Boolean {
        // Since format in ImageAnalysis is YUV, image.planes[0] contains the luminance plane
        val buffer = image.planes[0].buffer
        // Extract image data from callback object
        val data = buffer.toByteArray()
        // Convert the data into an array of pixel values ranging 0-255
        val pixels = data.map { it.toInt() and 0xFF }
        // Compute average luminance for the image
        return pixels.average() < minLuminosity
    }

    fun isTooDark(src: Mat): Boolean {
        val matGray = Mat()
        Imgproc.cvtColor(src, matGray, Imgproc.COLOR_BGR2HSV)
        val mean = Core.mean(matGray)
        val luminosity = mean.`val`[2]
        return luminosity < minLuminosity
    }

    fun isBlurred(src: Mat): Boolean {
        return blurLevel(src) < minBlur
    }

    private fun blurLevel(src: Mat): Double {
        val destination = Mat()
        val matGray = Mat()
        val kernel = object : Mat(3, 3, CvType.CV_32F) {
            init {
                put(0, 0, 0.0)
                put(0, 1, -1.0)
                put(0, 2, 0.0)
                put(1, 0, -1.0)
                put(1, 1, 4.0)
                put(1, 2, -1.0)
                put(2, 0, 0.0)
                put(2, 1, -1.0)
                put(2, 2, 0.0)
            }
        }
        Imgproc.cvtColor(src, matGray, Imgproc.COLOR_BGR2GRAY)
        Imgproc.filter2D(matGray, destination, -1, kernel)
        val median = MatOfDouble()
        val std = MatOfDouble()
        Core.meanStdDev(destination, median, std)
        return std.get(0, 0)[0].pow(2.0)
    }

    fun validateContour(sortedMapOfPoint2f: MatOfPoint2f, previewWidth: Double, previewHeight: Double, previewStart: Double, previewTop: Double, imageArea: Double): ValidationResult {
        val points = sortedMapOfPoint2f.toArray()
        val topLeftPoint = points[0]
        val topRightPoint = points[1]
        val bottomRightPoint = points[2]
        val bottomLeftPoint = points[3]

        val resultArea = abs(Imgproc.contourArea(sortedMapOfPoint2f))
        val resultWidth = maxOf(topRightPoint.x - topLeftPoint.x, bottomRightPoint.x - bottomLeftPoint.x)
        val resultHeight = maxOf(topRightPoint.y - bottomRightPoint.y, topLeftPoint.y - bottomLeftPoint.y)
        val previewArea = previewWidth * previewHeight

        return when {
            isNotRectangle(points) -> ValidationResult.NO_CONTOUR
            isAboveMaxArea(resultArea, imageArea) -> ValidationResult.NO_CONTOUR
            isAlternativeMaxCosine(sortedMapOfPoint2f) -> ValidationResult.NO_CONTOUR
            isDetectedAreaBelowLimits(resultArea, previewArea) -> ValidationResult.MOVE_CLOSER
            isDetectedHeightAboveLimit(resultHeight, previewHeight) -> ValidationResult.MOVE_AWAY
            isDetectedWidthAboveLimit(resultWidth, previewWidth) -> ValidationResult.MOVE_AWAY
            isOffCenter(topRightPoint, bottomRightPoint, bottomLeftPoint, topLeftPoint, previewHeight, previewWidth, previewTop, previewStart) -> ValidationResult.OFF_CENTER
            isAngleIncorrect(sortedMapOfPoint2f, topRightPoint, bottomRightPoint, bottomLeftPoint, topLeftPoint) -> ValidationResult.ADJUST_ANGLE
            else -> ValidationResult.NO_MESSAGE
        }
    }

    fun validateBitmapContour(sortedMapOfPoint2f: MatOfPoint2f, previewArea: Double): ValidationResult {
        val points = sortedMapOfPoint2f.toArray()
        val topLeftPoint = points[0]
        val topRightPoint = points[1]
        val bottomRightPoint = points[2]
        val bottomLeftPoint = points[3]
        val resultArea = abs(Imgproc.contourArea(sortedMapOfPoint2f))
        return when {
            isNotRectangle(points) -> ValidationResult.NO_CONTOUR
            isAlternativeAngleIncorrect(sortedMapOfPoint2f, topRightPoint, bottomRightPoint, bottomLeftPoint, topLeftPoint) -> ValidationResult.ADJUST_ANGLE
            isDetectedAreaBelowLimits(resultArea, previewArea) -> ValidationResult.MOVE_CLOSER
            else -> ValidationResult.NO_MESSAGE
        }
    }

    private fun isMRZRatioCorrect(contour: MatOfPoint2f) : Boolean{
        val roiRect = Imgproc.boundingRect(contour)
        val aspectRatio = roiRect.width.toDouble() / roiRect.height.toDouble()
        return aspectRatio > 5
    }

    fun isNotRectangle(contour: MatOfPoint2f): Boolean {
        val points = contour.toArray().distinct()
        return points.size < 4
    }

    private fun isNotRectangle(points: Array<Point>): Boolean {
        return points.distinct().size < 4
    }

    private fun isAngleIncorrect(approx: MatOfPoint2f, topRightPoint: Point, bottomRightPoint: Point, bottomLeftPoint: Point, topLeftPoint: Point): Boolean {
        return isMaxCosine(approx) || isLeftEdgeDistorted(topLeftPoint, bottomLeftPoint) || isRightEdgeDistorted(topRightPoint, bottomRightPoint)
    }

    private fun isAlternativeAngleIncorrect(approx: MatOfPoint2f, topRightPoint: Point, bottomRightPoint: Point, bottomLeftPoint: Point, topLeftPoint: Point): Boolean {
        return isAlternativeMaxCosine(approx) || isLeftEdgeDistorted(topLeftPoint, bottomLeftPoint) || isRightEdgeDistorted(topRightPoint, bottomRightPoint)
    }

    private fun isRightEdgeDistorted(topRightPoint: Point, bottomRightPoint: Point): Boolean {
        return abs(topRightPoint.x - bottomRightPoint.x) > maxEdgeDistorted
    }

    private fun isLeftEdgeDistorted(topLeftPoint: Point, bottomLeftPoint: Point): Boolean {
        return abs(topLeftPoint.x - bottomLeftPoint.x) > maxEdgeDistorted
    }

    private fun isMaxCosine(approx: MatOfPoint2f): Boolean {
        return MathUtils.maxCosine(approx) >= maxCosine
    }

    private fun isAlternativeMaxCosine(approx: MatOfPoint2f): Boolean {
        return MathUtils.maxCosine(approx) >= alternativeMaxCosine
    }

    private fun isDetectedAreaBelowLimits(resultArea: Double, previewArea: Double): Boolean {
        return resultArea < (previewArea * minBelowAreaPercent)
    }

    private fun isDetectedHeightAboveLimit(resultHeight: Double, previewHeight: Double): Boolean {
        return (resultHeight / previewHeight) > maxHeightPercent
    }

    private fun isDetectedWidthAboveLimit(resultWidth: Double, previewWidth: Double): Boolean {
        return (resultWidth / previewWidth) > maxWidthPercent
    }

    private fun isOffCenter(topRightPoint: Point, bottomRightPoint: Point, bottomLeftPoint: Point, topLeftPoint: Point, previewHeight: Double, previewWidth: Double, previewTop: Double, previewStart: Double): Boolean {
        return isTopEdgeTouching(topLeftPoint, topRightPoint, previewTop)
                || isBottomEdgeTouching(bottomLeftPoint, bottomRightPoint, previewHeight, previewTop)
                || isLeftEdgeTouching(topLeftPoint, bottomLeftPoint, previewStart)
                || isRightEdgeTouching(topRightPoint, bottomRightPoint, previewWidth, previewStart)
    }

    private fun isBottomEdgeTouching(bottomLeftPoint: Point, bottomRightPoint: Point, previewHeight: Double, top: Double): Boolean {
        return maxOf(bottomLeftPoint.y, bottomRightPoint.y) >= previewHeight + top + minOffCenter
    }

    private fun isRightEdgeTouching(topRightPoint: Point, bottomRightPoint: Point, previewWidth: Double, start: Double): Boolean {
        return maxOf(topRightPoint.x, bottomRightPoint.x) >= previewWidth + start + minOffCenter
    }

    private fun isTopEdgeTouching(topLeftPoint: Point, topRightPoint: Point, top: Double): Boolean {
        return minOf(topLeftPoint.y, topRightPoint.y) <= top - minOffCenter
    }

    private fun isLeftEdgeTouching(topLeftPoint: Point, bottomLeftPoint: Point, start: Double): Boolean {
        return minOf(topLeftPoint.x, bottomLeftPoint.x) <= start - minOffCenter
    }

    private fun isAboveMaxArea(resultArea: Double, imageArea: Double): Boolean {
        return (resultArea / imageArea) > maxArea
    }

    private fun ByteBuffer.toByteArray(): ByteArray {
        rewind()    // Rewind the buffer to zero
        val data = ByteArray(remaining())
        get(data)   // Copy the buffer into a byte array
        return data // Return the byte array
    }
}
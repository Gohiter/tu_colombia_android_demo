package com.orugga.transunionsdk.docCapture

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.YuvImage
import android.graphics.ImageFormat
import android.graphics.Rect
import android.graphics.BitmapFactory
import android.graphics.PointF
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.orugga.transunionsdk.util.ImageUtils
import com.orugga.transunionsdk.util.YuvToRgbConverter
import com.orugga.transunionsdk.vo.ValidationResult
import java.io.ByteArrayOutputStream
import java.util.ArrayDeque
import kotlin.collections.ArrayList


/**
 * Our custom image analysis class.
 *
 * <p>All we need to do is override the function `analyze` with our desired operations. Here,
 * we compute the average luminosity of the image by looking at the Y plane of the YUV frame.
 */

class DocCaptureAnalyzer(listener: DocCaptureListener? = null, context: Context?, validate: Boolean, portrait: Boolean) : ImageAnalysis.Analyzer {
    private val frameRateWindow = 8
    private val frameTimestamps = ArrayDeque<Long>(5)
    private val listeners = ArrayList<DocCaptureListener>().apply { listener?.let { add(it) } }
    private var lastAnalyzedTimestamp = 0L
    private var context = context
    private var validate = validate
    private var portrait = portrait
    private var docCapture = true

    var framesPerSecond: Double = -1.0
        private set

    /**
     * Used to add listeners that will be called with each luma computed
     */
    fun onFrameAnalyzed(listener: DocCaptureListener) = listeners.add(listener)

    private fun ImageProxy.toBitmap(): Bitmap {
        val yBuffer = planes[0].buffer // Y
        val uBuffer = planes[1].buffer // U
        val vBuffer = planes[2].buffer // V
        val ySize = yBuffer.remaining()
        val uSize = uBuffer.remaining()
        val vSize = vBuffer.remaining()
        val nv21 = ByteArray(ySize + uSize + vSize)
        //U and V are swapped
        yBuffer.get(nv21, 0, ySize)
        vBuffer.get(nv21, ySize, vSize)
        uBuffer.get(nv21, ySize + vSize, uSize)
        val yuvImage = YuvImage(nv21, ImageFormat.NV21, this.width, this.height, null)
        val out = ByteArrayOutputStream()
        yuvImage.compressToJpeg(Rect(0, 0, yuvImage.width, yuvImage.height), 50, out)
        val imageBytes = out.toByteArray()
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
    }

    /**
     * Analyzes an image to produce a result.
     *
     * <p>The caller is responsible for ensuring this analysis method can be executed quickly
     * enough to prevent stalls in the image acquisition pipeline. Otherwise, newly available
     * images will not be acquired and analyzed.
     *
     * <p>The image passed to this method becomes invalid after this method returns. The caller
     * should not store external references to this image, as these references will become
     * invalid.
     *
     * @param image image being analyzed VERY IMPORTANT: Analyzer method implementation must
     * call image.close() on received images when finished using them. Otherwise, new images
     * may not be received or the camera may stall, depending on back pressure setting.
     *
     */
    @SuppressLint("UnsafeExperimentalUsageError")
    override fun analyze(imageProxy: ImageProxy) {
        // Keep track of frames analyzed
        val currentTime = System.currentTimeMillis()
        frameTimestamps.push(currentTime)

        // Compute the FPS using a moving average
        while (frameTimestamps.size >= frameRateWindow) frameTimestamps.removeLast()
        val timestampFirst = frameTimestamps.peekFirst() ?: currentTime
        val timestampLast = frameTimestamps.peekLast() ?: currentTime
        framesPerSecond = 1.0 / ((timestampFirst - timestampLast) /
                frameTimestamps.size.coerceAtLeast(1).toDouble()) * 30 * 1000.0

        // Analysis could take an arbitrarily long amount of time
        // Since we are running in a different thread, it won't stall other use cases

        lastAnalyzedTimestamp = frameTimestamps.first

        //if (listeners.isEmpty() || documentFound) {
            var result = ValidationResult.NO_MESSAGE
            var points: List<PointF>? = null
            var croppedBitmap: Bitmap? = null
            val image = imageProxy.image
            if(image != null) {
                var bitmap = Bitmap.createBitmap(image.width, image.height, Bitmap.Config.ARGB_8888)
                context?.let { YuvToRgbConverter(it).yuvToRgb(image, bitmap) }
                if(portrait) {
                    bitmap = ImageUtils.rotateBitmap(bitmap, 90f)
                }
                val contour = if(docCapture) {
                    DocCaptureLiveEdgeUtils.getContourEdgePoints(bitmap)
                } else {
                    DocCaptureLiveEdgeUtils.getMRZContourEdgePoints(bitmap)
                }
                docCapture = !docCapture
                if(contour != null){
                    points = DocCaptureLiveEdgeUtils.getPointsFromMat(contour)
                    result = if(validate){
                        ValidationUtils.validateContour(contour, bitmap.width * 0.6, bitmap.height * 0.6, bitmap.width * 0.2, bitmap.height * 0.2, (bitmap.width * bitmap.height).toDouble())
                    } else {
                        ValidationUtils.validateBitmapContour(contour, (bitmap.width * bitmap.height).toDouble())
                    }
                    if(result == ValidationResult.NO_MESSAGE){
                        val originalCropped = ImageUtils.getCroppedDocument(bitmap, points, 50.0)
                        val croppedMat = ImageUtils.bitmapToMat(originalCropped)
                        when {
                            ValidationUtils.isTooDark(croppedMat) -> result = ValidationResult.TOO_DARK
                            (validate && ValidationUtils.isBlurred(croppedMat)) -> result = ValidationResult.BLURRED
                            else -> croppedBitmap = originalCropped
                        }
                    }
                }
                listeners.forEach { it(result, points, bitmap, croppedBitmap) }
                image.close()
            }
            imageProxy.close()
        //}
    }

}

/** Helper type alias used for analysis use case callbacks */
typealias DocCaptureListener = (validationResult: ValidationResult, points: List<PointF>?, bitmap: Bitmap, cropped: Bitmap?) -> Unit
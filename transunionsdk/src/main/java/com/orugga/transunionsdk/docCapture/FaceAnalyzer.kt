package com.orugga.transunionsdk.docCapture

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.YuvImage
import android.graphics.ImageFormat
import android.graphics.Rect
import android.graphics.BitmapFactory
import android.graphics.PointF
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.orugga.transunionsdk.util.ImageUtils
import com.orugga.transunionsdk.util.YuvToRgbConverter
import java.io.ByteArrayOutputStream
import java.util.ArrayDeque
import kotlin.collections.ArrayList

class FaceAnalyzer(listener: FaceListener? = null, context: Context?, angle: Float) : ImageAnalysis.Analyzer {
    private val frameRateWindow = 8
    private val frameTimestamps = ArrayDeque<Long>(5)
    private val listeners = ArrayList<FaceListener>().apply { listener?.let { add(it) } }
    private var lastAnalyzedTimestamp = 0L
    private var context = context
    private var angle = angle
    var framesPerSecond: Double = -1.0
        private set

    /**
     * Used to add listeners that will be called with each computed
     */
    fun onFrameAnalyzed(listener: FaceListener) = listeners.add(listener)

    private fun ImageProxy.toBitmap(): Bitmap {
        val yBuffer = planes[0].buffer // Y
        val uBuffer = planes[1].buffer // U
        val vBuffer = planes[2].buffer // V
        val ySize = yBuffer.remaining()
        val uSize = uBuffer.remaining()
        val vSize = vBuffer.remaining()
        val nv21 = ByteArray(ySize + uSize + vSize)
        //U and V are swapped
        yBuffer.get(nv21, 0, ySize)
        vBuffer.get(nv21, ySize, vSize)
        uBuffer.get(nv21, ySize + vSize, uSize)
        val yuvImage = YuvImage(nv21, ImageFormat.NV21, this.width, this.height, null)
        val out = ByteArrayOutputStream()
        yuvImage.compressToJpeg(Rect(0, 0, yuvImage.width, yuvImage.height), 50, out)
        val imageBytes = out.toByteArray()
        return BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
    }

    @SuppressLint("UnsafeExperimentalUsageError")
    override fun analyze(imageProxy: ImageProxy) {
        // Keep track of frames analyzed
        val currentTime = System.currentTimeMillis()
        frameTimestamps.push(currentTime)

        // Compute the FPS using a moving average
        while (frameTimestamps.size >= frameRateWindow) frameTimestamps.removeLast()
        val timestampFirst = frameTimestamps.peekFirst() ?: currentTime
        val timestampLast = frameTimestamps.peekLast() ?: currentTime
        framesPerSecond = 1.0 / ((timestampFirst - timestampLast) /
                frameTimestamps.size.coerceAtLeast(1).toDouble()) * 1000.0

        // Analysis could take an arbitrarily long amount of time
        // Since we are running in a different thread, it won't stall other use cases
        lastAnalyzedTimestamp = frameTimestamps.first

        var croppedBitmap: Bitmap?= null
        val image = imageProxy.image
        if(image != null){
            val bitmap = Bitmap.createBitmap(image.width, image.height, Bitmap.Config.ARGB_8888)
            context?.let { YuvToRgbConverter(it).yuvToRgb(image, bitmap) }
            val rotated = ImageUtils.rotateBitmap(bitmap, angle)
            val facePoints = DocCaptureLiveEdgeUtils.findFacePoints(rotated)
            if (!facePoints.isNullOrEmpty()) {
                croppedBitmap = ImageUtils.getCroppedDocument(rotated, facePoints, 50.0)
            }
            listeners.forEach { it(facePoints, rotated, croppedBitmap) }
            image.close()
        }
        imageProxy.close()
    }
}

typealias FaceListener = (points: List<PointF>?, bitmap: Bitmap, cropped: Bitmap?) -> Unit
package com.orugga.transunionsdk.docCapture

import android.app.Activity
import android.graphics.Bitmap
import android.graphics.PointF
import com.orugga.transunionsdk.util.ImageUtils
import com.orugga.transunionsdk.util.MathUtils
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import org.opencv.objdetect.CascadeClassifier
import java.io.File
import kotlin.math.abs


object DocCaptureLiveEdgeUtils {
    private const val TOP_CONTOURS = 10
    private const val MAX_VAL = 255.0
    private const val ALPHA = 0.0
    private const val BETA = 255.0
    private const val KSIZE_BLUR = 3.0
    private const val KSIZE_CLOSE = 10.0
    private const val CANNY_THRESH_L = 85.0
    private const val CANNY_THRESH_U = 185.0
    private const val TRUNC_THRESH = 150.0
    private const val CUTOFF_THRESH = 155.0
    private const val MIN_FACE_AREA = 0.10
    private const val MRZ_WIDTH = 13.0
    private const val MRZ_HEIGHT = 5.0
    private const val SOBEL_DX = 1
    private const val SOBEL_DY = 0
    private const val SOBEL_KSIZE = -1
    private const val MRZ_TYPE_3 = 114.0 / 10.65
    private const val MRZ_TYPE_1 = 77.7 / 11.41
    private const val faceModel = "haarcascade_frontalface_alt.xml"
    private lateinit var faceCascade: CascadeClassifier

    fun getContourEdgePoints(bitmap: Bitmap): MatOfPoint2f? {
        val srcMat = Mat(bitmap.height, bitmap.width, CvType.CV_8UC1)
        Utils.bitmapToMat(bitmap, srcMat)
        var points = ArrayList<PointF>()
        val x = srcMat.cols() * 0.2F
        val endX = srcMat.cols() * 0.8F
        val y = srcMat.rows() * 0.2F
        val endY = srcMat.rows() * 0.8F
        points.add(PointF(x , y)) //topLeft
        points.add(PointF(endX , y)) //topRight
        points.add(PointF(endX , endY)) //bottomRight
        points.add(PointF(x , endY)) //bottomLeft
        val originalMat = ImageUtils.cropMat(srcMat, points, 0.0)
        Imgproc.cvtColor(originalMat, originalMat, Imgproc.COLOR_BGR2GRAY, 4)
        // step 1. Blur and normalize the image for uniformity
        Imgproc.blur(originalMat, originalMat, Size(
            KSIZE_BLUR,
            KSIZE_BLUR
        ))
        Core.normalize(originalMat, originalMat,
            ALPHA,
            BETA, Core.NORM_MINMAX)
        // step 2. Truncate to make it uniformly bright.
        Imgproc.threshold(originalMat, originalMat,
            TRUNC_THRESH,
            MAX_VAL, Imgproc.THRESH_TRUNC)
        Core.normalize(originalMat, originalMat,
            ALPHA,
            BETA, Core.NORM_MINMAX)
        // step 3. Apply canny edge detection
        Imgproc.Canny(originalMat, originalMat,
            CANNY_THRESH_U,
            CANNY_THRESH_L
        )
        // step 4. Cutoff weak edges
        Imgproc.threshold(originalMat, originalMat,
            CUTOFF_THRESH,
            MAX_VAL, Imgproc.THRESH_TOZERO)
        // step 5. Close small gaps
        val morphKernel = Mat(Size(
            KSIZE_CLOSE,
            KSIZE_CLOSE
        ), CvType.CV_8UC1, Scalar(MAX_VAL))
        val anchor = Point(-1.0, -1.0)
        Imgproc.morphologyEx(originalMat, originalMat, Imgproc.MORPH_CLOSE, morphKernel, anchor, 1)
        val largestContour =
            findLargestContours(
                originalMat,
                TOP_CONTOURS
            )
        return largestContour?.let {
            findPoints(it, x, y)
        }
    }

    fun getGrayMat(bitmap: Bitmap) : Mat {
        val mat = Mat(bitmap.height, bitmap.width, CvType.CV_8UC1)
        Utils.bitmapToMat(bitmap, mat)
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY, 4)
        return mat
    }

    private fun hull2Points(hull: MatOfInt, contour: MatOfPoint): MatOfPoint {
        val indexes = hull.toList()
        val points = ArrayList<Point>()
        val contours = contour.toList()
        for (index in indexes) {
            points.add(contours[index])
        }
        val point = MatOfPoint()
        point.fromList(points)
        return point
    }

    private fun findLargestContours(src: Mat, NUM_TOP_CONTOURS: Int): List<MatOfPoint>? {
        val auxMat = Mat()
        val contours = ArrayList<MatOfPoint>()
        Imgproc.findContours(src, contours, auxMat, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE)
        val hulls = ArrayList<MatOfPoint>()
        val tempHullIndices = MatOfInt()
        for (i in contours.indices) {
            Imgproc.convexHull(contours[i], tempHullIndices)
            hulls.add(
                hull2Points(
                    tempHullIndices,
                    contours[i]
                )
            )
        }
        for (c in contours) c.release()
        tempHullIndices.release()
        auxMat.release()
        if (hulls.size != 0) {
            hulls.sortWith(Comparator { lhs, rhs -> Imgproc.contourArea(rhs).compareTo(Imgproc.contourArea(lhs)) })
            return hulls.subList(0, hulls.size.coerceAtMost(NUM_TOP_CONTOURS))
        }
        return null
    }

    private fun findPoints(contours: List<MatOfPoint>, initialX: Float, initialY: Float): MatOfPoint2f? {
        for (contour in contours) {
            val matOfPoint2f = MatOfPoint2f(*contour.toArray())
            val peri = Imgproc.arcLength(matOfPoint2f, true)
            val approx = MatOfPoint2f()
            Imgproc.approxPolyDP(matOfPoint2f, approx, 0.02 * peri, true)
            if (approx.rows() == 4) {
                return MathUtils.sortPoints(approx, initialX, initialY)
            }
        }
        return null
    }

    fun getPointsFromMat(contour: MatOfPoint2f): List<PointF> {
        val result = ArrayList<PointF>()
        val points = listOf(*contour.toArray())
        for (i in points.indices) {
            result.add(PointF(points[i].x.toFloat(), points[i].y.toFloat()))
        }
        return result
    }



    fun loadFaceModel(activity: Activity) {
        faceCascade = CascadeClassifier(File(activity.filesDir, "model").apply { writeBytes(activity.assets.open(faceModel).readBytes()) }.path)
    }

    fun findFacePoints(bitmap: Bitmap): List<PointF>? {
        if(bitmap.width <= 0 || bitmap.height <= 0){
            return null
        }
        val originalMat = getGrayMat(bitmap)
        val side = minOf(bitmap.height, bitmap.width) * MIN_FACE_AREA
        return findFacePoints(originalMat, side)
    }

    fun findFacePoints(originalMat: Mat, side: Double): List<PointF>? {
        val face = ArrayList<PointF>()
        Imgproc.equalizeHist(originalMat, originalMat)
        val rectangles = MatOfRect()
        faceCascade.detectMultiScale(originalMat, rectangles, 1.1, 3, 0, Size(side, side))
        val facePoints = rectangles.toArray()
        if(facePoints.isNullOrEmpty()){
            return face
        }
        val tlx = facePoints[0].tl().x.toFloat()
        val tly = facePoints[0].tl().y.toFloat()
        val brx = facePoints[0].br().x.toFloat()
        val bry = facePoints[0].br().y.toFloat()
        val width = facePoints[0].width
        face.add(PointF(tlx, tly)) //topLeft
        face.add(PointF(tlx + width, tly)) //topRight
        face.add(PointF(brx, bry)) //bottomRight
        face.add(PointF(brx - width, bry)) //bottomLeft
        return face
    }

    fun getMRZContourEdgePoints(bitmap: Bitmap): MatOfPoint2f? {
        val originalMat = getGrayMat(bitmap)
        // step 1. Blur image
        Imgproc.blur(originalMat, originalMat, Size(
            KSIZE_BLUR,
            KSIZE_BLUR
        ))
        //Step 2. Find dark regions on a light background
        val blackhat = Mat()
        val rectKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, Size(MRZ_WIDTH, MRZ_HEIGHT))
        Imgproc.morphologyEx(originalMat, blackhat, Imgproc.MORPH_BLACKHAT, rectKernel)
        //Step 3. Compute the Scharr gradient
        val gradX = Mat()
        Imgproc.Sobel(blackhat, gradX, CvType.CV_32F, SOBEL_DX, SOBEL_DY, SOBEL_KSIZE)
        Core.convertScaleAbs(gradX, gradX, 1.0, 0.0)
        val minMax = Core.minMaxLoc(gradX)
        val alpha = 255 / (minMax.maxVal - minMax.minVal)
        val beta = -(minMax.minVal * 255 / (minMax.maxVal - minMax.minVal))
        Core.convertScaleAbs(gradX, gradX, alpha, beta)
        blackhat.release()
        //Step 4. Close gaps in between letters, then apply Otsu's thresholding method
        Imgproc.morphologyEx(gradX, gradX, Imgproc.MORPH_CLOSE, rectKernel)
        val thresh = Mat()
        Imgproc.threshold(gradX, thresh, 0.0, 255.0, Imgproc.THRESH_OTSU)
        gradX.release()
        rectKernel.release()
        //Step 5. Close gaps again, this time using the square kernel, then perform a series of erosions to break apart connected components
        val squareRectKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, Size(21.0, 21.0))
        Imgproc.morphologyEx(thresh, thresh, Imgproc.MORPH_CLOSE, squareRectKernel)
        Imgproc.erode(thresh, thresh, Mat(), Point(-1.0, -1.0), 4)
        squareRectKernel.release()
        //Step 6. Set 5% of the left and right borders to zero
        for (i in 0 until originalMat.size().height.toInt()) {
            for (j in 0 until ((originalMat.size().width * 0.05).toInt())) {
                thresh.put(i, j, 0.0)
                thresh.put(i, originalMat.size().width.toInt() - j, 0.0)
            }
        }
        //Step 7. Find contours
        val largestContour =
            findLargestContours(
                thresh,
                TOP_CONTOURS
            )
        thresh.release()
        //Step 8. Find MRZ points
        val mrzPoints = largestContour?.let {
            findPoints(it)
        }
        //Step 9. find face in Passport
        val side = minOf(bitmap.height, bitmap.width) * MIN_FACE_AREA
        mrzPoints?.let {
            if(ValidationUtils.isNotRectangle(mrzPoints)) {
                return null
            }
            val roiRect = Imgproc.boundingRect(mrzPoints)
            val aspectRatio = roiRect.width.toDouble() / roiRect.height.toDouble()
            //if(isFaceOnTheLeftSide(facePoints, mrzPoints) && isMRZType3(aspectRatio)) {
            if(isMRZType3(aspectRatio)) {
                //Step 10. find Passport points
                return findPassportPointsFromMRZ(mrzPoints)
            } else if (isMRZType1(aspectRatio)){
                //Step 10. ID Card points
                return findIDCardPointsFromMRZ(mrzPoints)
            }
        }
        originalMat.release()
        return null
    }

    private fun findPoints(contours: List<MatOfPoint>): MatOfPoint2f? {
        for (contour in contours) {
            val matOfPoint2f = MatOfPoint2f(*contour.toArray())
            val peri = Imgproc.arcLength(matOfPoint2f, true)
            val approx = MatOfPoint2f()
            Imgproc.approxPolyDP(matOfPoint2f, approx, 0.02 * peri, true)
            if (approx.rows() == 4) {
                return MathUtils.sortPoints(approx)
            }
        }
        return null
    }

    private fun findPassportPointsFromMRZ(mrzPoints: MatOfPoint2f): MatOfPoint2f {
        val result = MatOfPoint2f()
        val mrzArray = mrzPoints.toArray()
        val mrzTopLeft = mrzArray[0]
        val mrzTopRight = mrzArray[1]
        val mrzBottomRight = mrzArray[2]
        val mrzBottomLeft = mrzArray[3]

        //First extend the points on height
        val auxTopLeft = scalePoints(mrzBottomLeft, mrzTopLeft, 9.5)
        val auxBottomRight = scalePoints(mrzTopRight, mrzBottomRight, 1.75)
        val auxTopRight = scalePoints(mrzBottomRight, mrzTopRight, 9.5)
        val auxBottomLeft = scalePoints(mrzTopLeft, mrzBottomLeft, 1.75)

        //Second extend the points on width
        val topLeft = scalePoints(auxTopRight, auxTopLeft, 1.05)
        val bottomRight = scalePoints(auxBottomLeft, auxBottomRight, 1.05)
        val topRight = scalePoints(auxTopLeft, auxTopRight, 1.05)
        val bottomLeft = scalePoints(auxBottomRight, auxBottomLeft, 1.05)

        result.fromArray(topLeft, topRight, bottomRight, bottomLeft)
        return result
    }

    private fun findIDCardPointsFromMRZ(mrzPoints: MatOfPoint2f): MatOfPoint2f {
        val result = MatOfPoint2f()
        val mrzArray = mrzPoints.toArray()
        val mrzTopLeft = mrzArray[0]
        val mrzTopRight = mrzArray[1]
        val mrzBottomRight = mrzArray[2]
        val mrzBottomLeft = mrzArray[3]

        //First extend the points on height
        val auxTopLeft = scalePoints(mrzBottomLeft, mrzTopLeft, 5.0)
        val auxBottomRight = scalePoints(mrzTopRight, mrzBottomRight, 1.5)
        val auxTopRight = scalePoints(mrzBottomRight, mrzTopRight, 5.0)
        val auxBottomLeft = scalePoints(mrzTopLeft, mrzBottomLeft, 1.5)

        //Second extend the points on width
        val topLeft = scalePoints(auxTopRight, auxTopLeft, 1.1)
        val bottomRight = scalePoints(auxBottomLeft, auxBottomRight, 1.25)
        val topRight = scalePoints(auxTopLeft, auxTopRight, 1.25)
        val bottomLeft = scalePoints(auxBottomRight, auxBottomLeft, 1.1)

        result.fromArray(topLeft, topRight, bottomRight, bottomLeft)
        return result
    }

    private fun scalePoints(pointA: Point, pointB: Point, scale: Double) : Point{
        val newX = pointA.x + (pointB.x - pointA.x) * scale
        val newY = pointA.y + (pointB.y - pointA.y) * scale
        return Point(newX, newY)
    }
    
    private fun isFaceOnTheLeftSide(face: List<PointF>, mrz: MatOfPoint2f) : Boolean {
        val maxFaceX = face.maxByOrNull { point ->  point.x}?: PointF(0F, 0F)
        val mrzArray = mrz.toArray()
        val maxMRZX = mrzArray.maxByOrNull { point -> point.x }
        val minMRZX = mrzArray.minByOrNull { point -> point.x }
        return maxFaceX.x < (minMRZX?.x?.let { maxMRZX?.x?.minus(it) })?.toFloat()?: 0F
    }

    private fun isMRZType1(aspectRatio : Double) : Boolean {
        return abs(aspectRatio - MRZ_TYPE_1) <= 2
    }

    private fun isMRZType3(aspectRatio : Double) : Boolean {
        return abs(aspectRatio - MRZ_TYPE_3) <= 3
    }
}
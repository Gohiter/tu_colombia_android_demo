package com.orugga.transunionsdk.repository

import com.orugga.transunionsdk.BuildConfig
import com.orugga.transunionsdk.api.ErrorUtils
import com.orugga.transunionsdk.api.TransunionSdkService
import com.orugga.transunionsdk.util.SharedPreferencesHelper
import com.orugga.transunionsdk.vo.*
import com.orugga.transunionsdk.vo.createApp.*

/**
 * Repository that handles User objects.
 */
class AuthRepository(
    private val transunionSdkService: TransunionSdkService,
    private val sharedPreferencesHelper: SharedPreferencesHelper
){

    suspend fun getOauthToken() : TokenResponse {
        val username = sharedPreferencesHelper.getUser()
        val password = sharedPreferencesHelper.getPassword()
        val grantType = sharedPreferencesHelper.getGrantType()
        val response = transunionSdkService.getToken(username, password, grantType)
        if(response.isSuccessful){
            sharedPreferencesHelper.removeAuth()
            sharedPreferencesHelper.removeApplicationId()
            response.body()?.let { sharedPreferencesHelper.saveAuth(it)}
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }

    suspend fun createDCApplication(firstName: String, lastName: String) : CreateAppResponse {
        val solutionName = sharedPreferencesHelper.getSolutionName()
        val appRequestInfo = CreateAppRequestInfo(solutionSetName = solutionName)
        val applicant = Applicant(firstName, lastName)
        val appId = sharedPreferencesHelper.getAppId()
        val appFields = CreateAppFields(appId, ApplicantIO(applicant))
        val response = transunionSdkService.createApp(CreateAppRequest(appRequestInfo, appFields))
        if(response.isSuccessful) {
                response.body()?.let { sharedPreferencesHelper.saveApplicationId(it)}
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }
    fun getDCApplicationId(): String {
        val appId =  sharedPreferencesHelper.getApplicationId()
        return if(appId.responseInfo != null){
            appId.responseInfo.applicationId
        } else {
            "-"
        }
    }
}
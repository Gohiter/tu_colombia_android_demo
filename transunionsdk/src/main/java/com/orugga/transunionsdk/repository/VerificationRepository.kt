package com.orugga.transunionsdk.repository

import android.content.Context
import com.orugga.transunionsdk.BuildConfig
import com.orugga.transunionsdk.TransunionSdk
import com.orugga.transunionsdk.api.ErrorUtils
import com.orugga.transunionsdk.api.TransunionSdkService
import com.orugga.transunionsdk.util.SharedPreferencesHelper
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.biocatch.*
import com.orugga.transunionsdk.vo.credoLab.CredoLabTokenRequest
import com.orugga.transunionsdk.vo.credoLab.CredoLabTokenResponse
import com.orugga.transunionsdk.vo.credoLab.DataSetInsightsResponse
import com.orugga.transunionsdk.vo.completeTransaction.CompleteTransactionFields
import com.orugga.transunionsdk.vo.completeTransaction.CompleteTransactionRequest
import com.orugga.transunionsdk.vo.completeTransaction.CompleteTransactionResponse
import com.orugga.transunionsdk.vo.deviceVerification.*
import com.orugga.transunionsdk.vo.emailVerification.*
import com.orugga.transunionsdk.vo.offers.OffersFields
import com.orugga.transunionsdk.vo.offers.OffersRequest
import com.orugga.transunionsdk.vo.offers.OffersResponse
import com.orugga.transunionsdk.vo.phoneVerification.*
import com.orugga.transunionsdk.vo.videoliveness.OnfidoSDKRequest
import com.orugga.transunionsdk.vo.videoliveness.OnfidoSDKResponse
import com.orugga.transunionsdk.vo.videoliveness.OnfidoVideoResponse
import com.orugga.transunionsdk.vo.watchlist.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext

class VerificationRepository(
    private val transunionSdk: TransunionSdk,
    private val transunionSdkService: TransunionSdkService,
    private val sharedPreferencesHelper: SharedPreferencesHelper
){

    suspend fun sendEmail(email: String) : EmailVerificationResponse {
        val applicant = EmailVerificationApplicantItem(email)
        val applicants = EmailVerificationApplicants(applicant)
        val fields = EmailVerificationFields(applicants, "EmailVerification")
        val request = EmailVerificationRequest(fields)
        val response = transunionSdkService.sendEmail(sharedPreferencesHelper.getApplicationId().responseInfo.applicationId, request)
        if(response.isSuccessful){
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }

    suspend fun sendPhone(number: String, type: String, countryCode: String) : PhoneVerificationResponse {
        val telephone = PhoneVerificationTelephoneItem(number, type, countryCode)
        val telephones = PhoneVerificationTelephones(listOf(telephone))
        val applicant = PhoneVerificationApplicantItem(telephones)
        val applicants = PhoneVerificationApplicants(listOf(applicant))
        val fields = PhoneVerificationFields(applicants, "PhoneVerification")
        val request = PhoneVerificationRequest(fields)
        val response = transunionSdkService.sendPhone(sharedPreferencesHelper.getApplicationId().responseInfo.applicationId, request)
        if(response.isSuccessful){
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }

    suspend fun validateDevice(blackBox: String): DeviceVerificationResponse {
        var typedIp = ""
        withContext(Dispatchers.Default) {
            withContext(Dispatchers.Default) {
                typedIp = getPublicIp()
            }
        }
        val deviceRequest = DeviceRequest(typedIp, "", blackBox, "login", TransactionInsight("", ""))
        val deviceVerificationApplicantIO = DeviceVerificationApplicantIO(DeviceVerificationApplicant(deviceRequest))
        val deviceVerificationFields = DeviceVerificationFields(applicantsIO = deviceVerificationApplicantIO)
        val response = transunionSdkService.deviceVerification(sharedPreferencesHelper.getApplicationId().responseInfo.applicationId, DeviceVerificationRequest(deviceVerificationFields))
        if(response.isSuccessful) {
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }

    suspend fun watchlistVerification(firstName: String, lastName: String): WatchlistResponse {
        val applicant = Applicant(applicantFirstName = firstName, applicantLastName = lastName)
        val watchlistRequest = WatchlistRequest(WatchlistFields(applicantIO = ApplicantIO(applicant)))
        val response = transunionSdkService.watchlist(sharedPreferencesHelper.getApplicationId().responseInfo.applicationId, watchlistRequest)
        if(response.isSuccessful) {
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }

    suspend fun getBiocatchScore() : BiocatchResponse {
        val currentCSID = sharedPreferencesHelper.getUserCurrentCSID()
        val applicant = BiocatchApplicantItem(csid = currentCSID)
        val applicants = BiocatchApplicants(listOf(applicant))
        val fields = BiocatchFields(applicantsIO = applicants)
        val request = BiocatchRequest(fields)

        val response = transunionSdkService.getBiocatchScore(sharedPreferencesHelper.getApplicationId().responseInfo.applicationId, request)
        if(response.isSuccessful){
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }

    suspend fun getPublicIp(): String {
        val response = transunionSdkService.getPublicId()
        if(response.isSuccessful) {
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }

    suspend fun getOffers(firstName: String, lastName: String): OffersResponse {
        val applicant = Applicant(firstName, lastName)
        val offersRequest = OffersRequest(OffersFields(applicantIO = ApplicantIO(applicant)))
        val response = transunionSdkService.getOffers(sharedPreferencesHelper.getApplicationId().responseInfo.applicationId, offersRequest)
        if(response.isSuccessful) {
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }

    fun collectCredoLabData(appContext: Context, referenceKey: String) = flow {
        emit(Resource.loading(null))
        val credolabKey = sharedPreferencesHelper.getCredoLabKey()
        val credolabmail = sharedPreferencesHelper.getCredoLabUser()
        val credolabPassword = sharedPreferencesHelper.getCredoLabPassword()

        try {
            val result = transunionSdk.credoLabManager.collectData(appContext, referenceKey, BuildConfig.CREDOLAB_URL)
            emit(Resource.success(result))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    suspend fun getCredoLabToken(): CredoLabTokenResponse {
        val credolabmail = sharedPreferencesHelper.getCredoLabUser()
        val credolabPassword = sharedPreferencesHelper.getCredoLabPassword()
        val request = CredoLabTokenRequest(credolabmail, credolabPassword)
        val response = transunionSdkService.getCredoLabToken(request)
        if(response.isSuccessful) {
            sharedPreferencesHelper.removeCredoLabToken()
            return if (response.body() != null) {
                sharedPreferencesHelper.saveCredoLabToken(response.body()!!)
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }

    suspend fun getCredoLabDatasetInsight(referenceNumber: String): DataSetInsightsResponse {
        val response = transunionSdkService.getCredoLabInsights(referenceNumber)
        if(response.isSuccessful) {
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }

    suspend fun completeTransaction() : CompleteTransactionResponse {
        val fields = CompleteTransactionFields()
        val request = CompleteTransactionRequest(fields)
        val response = transunionSdkService.completeTransaction(sharedPreferencesHelper.getApplicationId().responseInfo.applicationId, request)
        if(response.isSuccessful){
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }
    suspend fun getSDKToken(onfidoSdkToken: String, applicantID: String,applicationBundleName: String) : OnfidoSDKResponse {
        //Guardamos el token asi se usa luego en otras calls
        sharedPreferencesHelper.saveOnfidoToken(onfidoSdkToken)
        val request = OnfidoSDKRequest(applicantID, applicationBundleName)
        val response = transunionSdkService.getOnfidoSDKToken(request)
        if (response.isSuccessful) {
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }
    suspend fun getVideo (applicantID: String) : OnfidoVideoResponse {
        val response = transunionSdkService.getOnfidoVideos(applicantID)
        if (response.isSuccessful) {
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }
}
package com.orugga.transunionsdk.repository

import android.util.Log
import com.orugga.transunionsdk.api.ErrorUtils
import com.orugga.transunionsdk.api.TransunionSdkService
import com.orugga.transunionsdk.util.ImageUtils
import com.orugga.transunionsdk.util.SharedPreferencesHelper
import com.orugga.transunionsdk.vo.UploadDocumentStep
import com.orugga.transunionsdk.vo.UploadableDocumentType
import com.orugga.transunionsdk.vo.createApp.Applicant
import com.orugga.transunionsdk.vo.createApp.ApplicantIO
import com.orugga.transunionsdk.vo.createDocument.CreateAttachDocumentResponse
import com.orugga.transunionsdk.vo.createDocument.CreateDocumentRequest
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceFields
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceRequest
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceResponse
import com.orugga.transunionsdk.vo.docAuthentication.DocAuthIdentityVerification
import com.orugga.transunionsdk.vo.docAuthenticationReport.DocAuthReportResponse
import com.orugga.transunionsdk.vo.docAuthenticationReport.DocAuthRequest
import com.orugga.transunionsdk.vo.docAuthenticationReport.DocAuthRequestField
import com.orugga.transunionsdk.vo.facialSimilarityInvokeService.FacialSimilarityInvokeServiceRequest
import com.orugga.transunionsdk.vo.facialSimilarityInvokeService.FacialSimilarityInvokeServiceResponse
import com.orugga.transunionsdk.vo.facialSimilarityReport.FacialSimilarityReportResponse
import com.orugga.transunionsdk.vo.fileAttach.DocAttachPhoto
import com.orugga.transunionsdk.vo.pullReport.PullReportRequest
import com.orugga.transunionsdk.vo.pullReport.PullReportRequestFields
import com.orugga.transunionsdk.vo.pullReport.PullReportResponse
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File


/**
 * Repository that handles User objects.
 */
class DocumentsRepository(
    private val transunionSdkService: TransunionSdkService,
    private val sharedPreferencesHelper: SharedPreferencesHelper
){

    suspend fun documentAuthentication(firstName: String, lastName: String) : DocAuthInvokeServiceResponse {
        val applicant = Applicant(firstName, lastName)
        val docAuthenticationFields = DocAuthInvokeServiceFields(applicantIO = ApplicantIO(applicant))
        val response = transunionSdkService.invokeDocService(
            sharedPreferencesHelper.getApplicationId().responseInfo.applicationId,
            DocAuthInvokeServiceRequest(docAuthenticationFields)
        )
        if(response.isSuccessful) {
            Log.i("documentAuthentication", "success")
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception (" ${error.code} ${error.message}")
        }
    }

    suspend fun createDocument(uploadableDocumentType: UploadableDocumentType, fileName: String) : CreateAttachDocumentResponse {
        val createDocumentRequest = CreateDocumentRequest("IDDocument", "IdDocumentFront.png", "Test")
        val response = transunionSdkService.createDocument(
            sharedPreferencesHelper.getApplicationId().responseInfo.applicationId,
            createDocumentRequest
        )
        if(response.isSuccessful){
            response.headers().get("Location")?.let {
                when (uploadableDocumentType) {
                    UploadableDocumentType.FRONT -> sharedPreferencesHelper.saveFrontPictureLocationId(it)
                    UploadableDocumentType.SELFIE -> sharedPreferencesHelper.saveSelfiePictureLocationId(it)
                }
            }
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            throw Exception(" ${response.code()} ${response.message()}")
        }
    }

    /**
     * The content type can be obtained using 'getContentResolver().getType(fileUri)'
     */
    suspend fun uploadDocument(file: File, type: MediaType, uploadableDocumentType: UploadableDocumentType) : CreateAttachDocumentResponse {
        val documentLocation =
            when (uploadableDocumentType) {
                UploadableDocumentType.FRONT -> sharedPreferencesHelper.getFrontPictureLocationId()
                UploadableDocumentType.SELFIE -> sharedPreferencesHelper.getSelfiePictureLocationId()
                else -> sharedPreferencesHelper.getFrontPictureLocationId()
            }
        val documentId = documentLocation.substring(documentLocation.lastIndexOf("/") + 1)
        val array = ImageUtils.readFileToBytes(file)
        val requestBody = RequestBody.create(type, array)

        val response = transunionSdkService.uploadDocument(
            documentId,
            requestBody
        )
        if(response.isSuccessful){
            Log.i("uploadDocument", "success")
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            Log.i("uploadDocument", "${response.raw().code} ${response.raw().message}")
            throw Exception(" ${response.raw().code} ${response.raw().message}")
        }
    }

    suspend fun processFrontDocument() : DocAuthInvokeServiceResponse {
        val documentLocation = sharedPreferencesHelper.getFrontPictureLocationId()
        val documentId = documentLocation.substring(documentLocation.lastIndexOf("/") + 1)
        val frontDoc = DocAttachPhoto(documentId, "")
        val docAuthIdentityVerification = DocAuthIdentityVerification(front = frontDoc, type = "tax_id",
            step = UploadableDocumentType.FRONT.value, currentStep = UploadDocumentStep.FRONT.value)
        val request = DocAuthRequest(DocAuthRequestField(docAuthIdentityVerification))
        val response = transunionSdkService.documentAuthentication(
            sharedPreferencesHelper.getApplicationId().responseInfo.applicationId, request
        )
        if(response.isSuccessful){
            Log.i("processDocument", "success")
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception(" ${error.code} ${error.message}")
        }
    }

    suspend fun getFrontDocumentReport(docType: String) : DocAuthReportResponse {
        val documentLocation = sharedPreferencesHelper.getFrontPictureLocationId()
        val documentId = documentLocation.substring(documentLocation.lastIndexOf("/") + 1)
        val frontDoc = DocAttachPhoto(documentId, "")
        val docAuthIdentityVerification = DocAuthIdentityVerification(front = frontDoc,
            type = docType, step = UploadableDocumentType.FRONT.value,
            currentStep = UploadDocumentStep.CONTINUE.value)
        val request = DocAuthRequest(DocAuthRequestField(docAuthIdentityVerification))
        val response = transunionSdkService.documentFrontReport(
            sharedPreferencesHelper.getApplicationId().responseInfo.applicationId, request
        )
        if(response.isSuccessful){
            Log.i("processDocument", "success")
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception(" ${error.code} ${error.message}")
        }
    }

    suspend fun facialSimilarityInvokeService(firstName: String, lastName: String) : FacialSimilarityInvokeServiceResponse {
        val applicant = Applicant(firstName, lastName)
        val response = transunionSdkService.invokeFacialSimilarityService(
            sharedPreferencesHelper.getApplicationId().responseInfo.applicationId,
            FacialSimilarityInvokeServiceRequest(applicantIO = ApplicantIO(applicant))
        )
        if(response.isSuccessful){
            Log.i("fsInvokeService", "success")
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception(" ${error.code} ${error.message}")
        }
    }

    suspend fun processSelfie() : DocAuthInvokeServiceResponse {
        val documentLocation = sharedPreferencesHelper.getSelfiePictureLocationId()
        val documentId = documentLocation.substring(documentLocation.lastIndexOf("/") + 1)
        val selfieDoc = DocAttachPhoto(documentId, "")
        val docAuthIdentityVerification = DocAuthIdentityVerification(selfie = selfieDoc, type = "",
            step = UploadableDocumentType.SELFIE.value, currentStep = UploadDocumentStep.SELFIE.value)
        val request = DocAuthRequest(DocAuthRequestField(docAuthIdentityVerification))
        val response = transunionSdkService.documentAuthentication(
            sharedPreferencesHelper.getApplicationId().responseInfo.applicationId, request
        )
        if(response.isSuccessful){
            Log.i("processSelfie", "success")
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception(" ${error.code} ${error.message}")
        }
    }

    suspend fun facialSimilarityReport() : FacialSimilarityReportResponse {
        val documentLocation = sharedPreferencesHelper.getSelfiePictureLocationId()
        val documentId = documentLocation.substring(documentLocation.lastIndexOf("/") + 1)
        val selfieDoc = DocAttachPhoto(documentId, "")
        val docAuthIdentityVerification = DocAuthIdentityVerification(
            selfie =  selfieDoc, step = UploadableDocumentType.SELFIE.value,
            currentStep = UploadDocumentStep.CONTINUE.value)
        val request = DocAuthRequest(DocAuthRequestField(docAuthIdentityVerification))
        val response = transunionSdkService.facialSimilarityReport(
            sharedPreferencesHelper.getApplicationId().responseInfo.applicationId, request
        )
        if(response.isSuccessful){
            Log.i("facialSimilarityReport", "success")
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception(" ${error.code} ${error.message}")
        }
    }

    suspend fun pullReport() : PullReportResponse {
        val response = transunionSdkService.pullReport(
            sharedPreferencesHelper.getApplicationId().responseInfo.applicationId,
            PullReportRequest(PullReportRequestFields())
        )
        if(response.isSuccessful){
            Log.i("pullReport", "success")
            return if (response.body() != null) {
                response.body()!!
            } else {
                throw Exception()
            }
        }else{
            val error = ErrorUtils.parseError(response)
            throw Exception(" ${error.code} ${error.message}")
        }
    }
}
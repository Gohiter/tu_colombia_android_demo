package com.orugga.transunion.splash

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import com.orugga.transunion.MainActivity
import com.orugga.transunion.R

class SplashActivity: Activity() {

    private lateinit var mDelayHandler: Handler
    private lateinit var imageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mDelayHandler = Handler()
        mDelayHandler.postDelayed(mRunnable, 2000)
    }

    private val mRunnable: Runnable = Runnable {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
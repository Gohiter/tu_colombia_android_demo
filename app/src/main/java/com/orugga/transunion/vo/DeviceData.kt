package com.orugga.transunion.vo

import android.os.Parcel
import android.os.Parcelable

data class DeviceData(
        var geoLocation: String? = null,
        var anonymizerDetected: String? = null,
        var browserType: String? = null,
        var browserVersion: String? = null,
        var operatingSystem: String? = null,
        var deviceType: String? = null,
        var ipAddress: String? = null,
        var ipCountry: String? = null,
        var ipCity: String? = null,
        var ipIsp: String? = null,
        var fraudForceScore: String? = null,
        var sureScoreScore: String? = null,
        var recommendation: String? = null,
        var mobileProvider: String? = null,
        var address: String? = null) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(geoLocation)
        parcel.writeString(anonymizerDetected)
        parcel.writeString(browserType)
        parcel.writeString(browserVersion)
        parcel.writeString(operatingSystem)
        parcel.writeString(deviceType)
        parcel.writeString(ipAddress)
        parcel.writeString(ipCountry)
        parcel.writeString(ipCity)
        parcel.writeString(ipIsp)
        parcel.writeString(fraudForceScore)
        parcel.writeString(sureScoreScore)
        parcel.writeString(recommendation)
        parcel.writeString(mobileProvider)
        parcel.writeString(address)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DeviceData> {
        override fun createFromParcel(parcel: Parcel): DeviceData {
            return DeviceData(parcel)
        }

        override fun newArray(size: Int): Array<DeviceData?> {
            return arrayOfNulls(size)
        }
    }

}
package com.orugga.transunion.vo

import android.os.Parcel
import android.os.Parcelable

data class VerificationName(
        var firstName: String? = null,
        var lastName: String? = null) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(firstName)
        parcel.writeString(lastName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<VerificationName> {
        override fun createFromParcel(parcel: Parcel): VerificationName {
            return VerificationName(parcel)
        }

        override fun newArray(size: Int): Array<VerificationName?> {
            return arrayOfNulls(size)
        }
    }

}
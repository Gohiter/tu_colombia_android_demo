package com.orugga.transunion.vo

import android.os.Parcel
import android.os.Parcelable

data class PhoneData(
        var number: String? = null,
        var locationCity: String? = null,
        var locationCountry: String? = null,
        var typeDescription: String? = null,
        var riskLevel: String? = null,
        var riskScore: String? = null,
        var recommendation: String? = null,
        var blacklistHit: String? = null) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(number)
        parcel.writeString(locationCity)
        parcel.writeString(locationCountry)
        parcel.writeString(typeDescription)
        parcel.writeString(riskLevel)
        parcel.writeString(riskScore)
        parcel.writeString(recommendation)
        parcel.writeString(blacklistHit)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PhoneData> {
        override fun createFromParcel(parcel: Parcel): PhoneData {
            return PhoneData(parcel)
        }

        override fun newArray(size: Int): Array<PhoneData?> {
            return arrayOfNulls(size)
        }
    }

}
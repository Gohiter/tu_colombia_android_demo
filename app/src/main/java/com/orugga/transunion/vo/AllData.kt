package com.orugga.transunion.vo

import android.os.Parcelable
import com.orugga.transunionsdk.vo.credoLab.CredoLabScores
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AllData(
        var verificationName: VerificationName? = null,
        var documentData: DocumentData? = null,
        var emailData: EmailData? = null,
        var deviceData: DeviceData? = null,
        var phoneData: PhoneData? = null,
        var watchlistData: WatchlistData? = null,
        var biocatchScore: String? = null,
        var credoLabScores: List<CredoLabScores>? = null) : Parcelable
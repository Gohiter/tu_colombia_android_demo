package com.orugga.transunion.vo

import android.os.Parcel
import android.os.Parcelable

data class EmailData(
        var address: String? = null,
        var status: String? = null,
        var statusCode: String? = null,
        var firstSeenDate: String? = null,
        var activity: String? = null,
        var descriptionAge: String? = null,
        var basicDecision: String? = null,
        var advancedDecision: String? = null,
        var blacklistHit: String? = null) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(address)
        parcel.writeString(status)
        parcel.writeString(statusCode)
        parcel.writeString(firstSeenDate)
        parcel.writeString(activity)
        parcel.writeString(descriptionAge)
        parcel.writeString(basicDecision)
        parcel.writeString(advancedDecision)
        parcel.writeString(blacklistHit)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EmailData> {
        override fun createFromParcel(parcel: Parcel): EmailData {
            return EmailData(parcel)
        }

        override fun newArray(size: Int): Array<EmailData?> {
            return arrayOfNulls(size)
        }
    }

}
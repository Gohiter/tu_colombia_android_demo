package com.orugga.transunion.vo

import android.os.Parcel
import android.os.Parcelable

data class IntroItem(
        var index: Int = 0,
        var title: String? = null,
        var subtitle: String? = null,
        var image: Int = 0) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(index)
        parcel.writeString(title)
        parcel.writeString(subtitle)
        parcel.writeInt(image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<IntroItem> {
        override fun createFromParcel(parcel: Parcel): IntroItem {
            return IntroItem(parcel)
        }

        override fun newArray(size: Int): Array<IntroItem?> {
            return arrayOfNulls(size)
        }
    }

}
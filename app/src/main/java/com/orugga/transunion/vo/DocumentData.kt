package com.orugga.transunion.vo

import android.os.Parcel
import android.os.Parcelable
import com.orugga.transunionsdk.vo.authExam.AuthExamResponse
import com.orugga.transunionsdk.vo.docAuthenticationReport.DocAuthReportVerification
import com.orugga.transunionsdk.vo.examRequest.ExamRequestResponse
import com.orugga.transunionsdk.vo.facialSimilarityReport.DSFacialSimilarityReport
import com.orugga.transunionsdk.vo.facialSimilarityReport.FacialSimilarityReportAttributes

data class DocumentData(
        var documentType: String? = null,
        var documentID: String? = null,
        var ocrName: String? = null,
        var ocrBirthDate: String? = null,
        var ocrID: String? = null,
        var fraudValidation: String? = null,
        var tamperDetection: String? = null,
        var tamperDescription: String? = null,
        var photoPresent: String? = null,
        var documentValidation: String? = null,
        var photoImage: String? = null,
        var photoStatic: String? = null,
        var photoVideo: String? = null,
        var photoAudibleChecks: String? = null,
        var photoVisualChecks: String? = null,
        var livenessDetection: String? = null,
        var liveTamperDetection: String? = null,
        var percentMatch: String? = null,
        var faceRecognitionResult: String? = null) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString())

    constructor(documentResponse: DocAuthReportVerification, examRequestResponse: ExamRequestResponse) : this(
        documentResponse.properties?.documentType,
        documentResponse.properties?.documentNumbers?.get(0)?.value,
        "${documentResponse.properties?.firstName} ${documentResponse.properties?.lastName}",
        documentResponse.properties?.dateOfBirth,
        documentResponse.properties?.documentNumbers?.get(0)?.value,
        documentResponse.subResult,
        documentResponse.visuaAutheDigTamRes,
        documentResponse.visuaAutheRes,
        documentResponse.visuaAutheFacDetRes,
        documentResponse.result,
        "-",
        "Yes",
        "-",
        "-",
        "-",
        "-",
        documentResponse.imageIntegRes)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(documentType)
        parcel.writeString(documentID)
        parcel.writeString(ocrName)
        parcel.writeString(ocrBirthDate)
        parcel.writeString(ocrID)
        parcel.writeString(fraudValidation)
        parcel.writeString(tamperDetection)
        parcel.writeString(tamperDescription)
        parcel.writeString(photoPresent)
        parcel.writeString(documentValidation)
        parcel.writeString(photoImage)
        parcel.writeString(photoStatic)
        parcel.writeString(photoVideo)
        parcel.writeString(photoAudibleChecks)
        parcel.writeString(photoVisualChecks)
        parcel.writeString(livenessDetection)
        parcel.writeString(liveTamperDetection)
        parcel.writeString(percentMatch)
        parcel.writeString(faceRecognitionResult)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DocumentData> {
        override fun createFromParcel(parcel: Parcel): DocumentData {
            return DocumentData(parcel)
        }

        override fun newArray(size: Int): Array<DocumentData?> {
            return arrayOfNulls(size)
        }
    }

}
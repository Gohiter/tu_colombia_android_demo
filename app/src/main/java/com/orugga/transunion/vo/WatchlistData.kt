package com.orugga.transunion.vo

import android.os.Parcel
import android.os.Parcelable

data class WatchlistData(
        var name: String? = null,
        var categoryCode: String? = null,
        var match: String? = null,
        var percentMatch: String? = null) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(categoryCode)
        parcel.writeString(match)
        parcel.writeString(percentMatch)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WatchlistData> {
        override fun createFromParcel(parcel: Parcel): WatchlistData {
            return WatchlistData(parcel)
        }

        override fun newArray(size: Int): Array<WatchlistData?> {
            return arrayOfNulls(size)
        }
    }

}
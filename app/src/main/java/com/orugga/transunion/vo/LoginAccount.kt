package com.orugga.transunion.vo

import android.os.Parcel
import android.os.Parcelable

data class LoginAccount(
        var emailAddressOrAlias: String? = null,
        var password: String? = null,
        var secretKey: String? = null) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(emailAddressOrAlias)
        parcel.writeString(password)
        parcel.writeString(secretKey)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LoginAccount> {
        override fun createFromParcel(parcel: Parcel): LoginAccount {
            return LoginAccount(parcel)
        }

        override fun newArray(size: Int): Array<LoginAccount?> {
            return arrayOfNulls(size)
        }
    }

}
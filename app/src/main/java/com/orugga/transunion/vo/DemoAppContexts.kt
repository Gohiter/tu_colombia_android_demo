package com.orugga.transunion.vo

enum class DemoAppContexts(val value: String) {
    WALKTHROUGH_1("Walkthrough_1"),
    WALKTHROUGH_2("Walkthrough_2"),
    WALKTHROUGH_3("Walkthrough_3"),
    PRE_SIGN_UP("Pre_Sign_Up"),
    SIGN_UP_EMPTY("Sign_Up_Empty"),
    STEP_1_1("Step_1_1"),
    STEP_1_2("Step_1_2"),
    STEP_2_1("Step_2_1"),
    STEP_2_2("Step_2_2"),
    STEP_3("Step_3"),
    STEP_4_1("Step_4_1"),
    STEP_4_2("Step_4_2"),
    STEP_4_3("Step_4_3"),
    STEP_4_4("Step_4_4"),
    STEP_4_5("Step_4_5"),
    STEP_4_6("Step_4_6"),
    STEP_5_1("Step_5_1"),
    STEP_5_2("Step_5_2"),
    STEP_6_1("Step_6_1"),
    STEP_6_2("Step_6_2"),
    STEP_6_3("Step_6_3"),
    STEP_6_4("Step_6_4"),
    STEP_6_5("Step_6_5"),
    CHECK_DATA_1("Check_data_1"),
    CHECK_DATA_2("Check_data_2"),
    //TODO ADD THESE FRAGMENTS
    INSTANT_CARD("Instant_Card"),
    INSTANT_CARD_TRANSFER("Instant_Card_Transfer"),
    CARDS_FOUND("Cards_found"),
    CARDS_FOUND_2("Cards_founds_2"),
    CARDS_FOUND_3("Cards_found_3"),
    SHARE_A_MY_CARD("Share_A_My_Card"),
    //
    PRIVACY_POLICY_ACCEPTED("Privacy_Policy_accepted")
}

package com.orugga.transunion.vo

data class DocType(
    val name: String,
    val id: String
)

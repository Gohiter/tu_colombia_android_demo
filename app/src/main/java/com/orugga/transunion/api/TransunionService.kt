package com.orugga.transunion.api

import com.orugga.transunion.vo.Auth
import com.orugga.transunion.vo.LoginAccount
import retrofit2.Response
import retrofit2.http.*

/**
 * REST API access points
 */
interface TransunionService {

    @POST(Endpoints.login)
    suspend fun login(@Body loginAccount: LoginAccount): Response<Auth>

}
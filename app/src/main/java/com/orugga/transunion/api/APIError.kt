package com.orugga.transunion.api
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class APIError(
    @field:SerializedName("Message") val message: String,
    @field:SerializedName("Code") val code: String): Parcelable {
    constructor(parcel: Parcel) : this(
    parcel.readString(),
    parcel.readString())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(message)
        parcel.writeString(code)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<APIError> {
        override fun createFromParcel(parcel: Parcel): APIError {
            return APIError(parcel)
        }

        override fun newArray(size: Int): Array<APIError?> {
            return arrayOfNulls(size)
        }
    }
}


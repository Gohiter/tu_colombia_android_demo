package com.orugga.transunion.ui.checkResult

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentCheckYourDataBinding
import com.orugga.transunion.ui.steps.step4.step4_2.VerifyAllData2FragmentArgs
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import kotlinx.android.synthetic.main.fragment_check_your_data.view.*
import java.io.File
import javax.inject.Inject


class CheckResultFragment : BaseFragment() {
    companion object {
        private const val TAG = "CheckResultFragment"
    }

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentCheckYourDataBinding
    private lateinit var checkResultViewModel: CheckResultViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_check_your_data, container, false)
        binding.cvDocumentAuth.setOnClickListener { toggleDocument() }
        binding.cvDeviceVerification.setOnClickListener { toggleDevice() }
        binding.cvEmailVerification.setOnClickListener { toggleEmail() }
        binding.cvPhoneVerification.setOnClickListener { togglePhone() }
        binding.cvWatchlist.setOnClickListener { toggleWatchlist() }
        binding.cvBiocatch.setOnClickListener { toggleBiocatch() }
        binding.btnConfirm.setOnClickListener { openDialog() }
        binding.context = context
        return binding.root
    }

    private fun setupSuccessAlertDialog() {
        binding.alertDialog.setAppId("#${checkResultViewModel.getApplicationId()}")
        binding.alertDialog.setPrimaryButtonAction(View.OnClickListener { goToAskId() })
    }

    private fun toggleDocument() {
        if(binding.lytDocument.constraintLyt.visibility == View.VISIBLE){
            binding.lytDocument.constraintLyt.visibility =View.GONE
            val drawable = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_expand) }
            binding.ivDocumentTitle.setImageDrawable(drawable)
        }else{
            binding.lytDocument.constraintLyt.visibility =View.VISIBLE
            val drawable = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_collapse) }
            binding.ivDocumentTitle.setImageDrawable(drawable)
        }
    }

    private fun toggleDevice() {
        if(binding.lytDevice.constraintLyt.visibility == View.VISIBLE){
            binding.lytDevice.constraintLyt.visibility =View.GONE
            val drawable = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_expand) }
            binding.ivDeviceTitle.setImageDrawable(drawable)
        }else{
            binding.lytDevice.constraintLyt.visibility =View.VISIBLE
            val drawable = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_collapse) }
            binding.ivDeviceTitle.setImageDrawable(drawable)
        }
    }

    private fun toggleEmail() {
        if(binding.lytEmail.constraintLyt.visibility == View.VISIBLE){
            binding.lytEmail.constraintLyt.visibility =View.GONE
            val drawable = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_expand) }
            binding.ivEmailTitle.setImageDrawable(drawable)
        }else{
            binding.lytEmail.constraintLyt.visibility =View.VISIBLE
            val drawable = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_collapse) }
            binding.ivEmailTitle.setImageDrawable(drawable)
        }
    }

    private fun togglePhone() {
        if(binding.lytPhone.constraintLyt.visibility == View.VISIBLE){
            binding.lytPhone.constraintLyt.visibility =View.GONE
            val drawable = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_expand) }
            binding.ivPhoneTitle.setImageDrawable(drawable)
        }else{
            binding.lytPhone.constraintLyt.visibility =View.VISIBLE
            val drawable = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_collapse) }
            binding.ivPhoneTitle.setImageDrawable(drawable)
        }
    }

    private fun toggleWatchlist() {
        if(binding.lytWatchlist.constraintLyt.visibility == View.VISIBLE){
            binding.lytWatchlist.constraintLyt.visibility =View.GONE
            val drawable = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_expand) }
            binding.ivWatchlistTitle.setImageDrawable(drawable)
        }else{
            binding.lytWatchlist.constraintLyt.visibility =View.VISIBLE
            val drawable = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_collapse) }
            binding.ivWatchlistTitle.setImageDrawable(drawable)
        }
    }

    private fun toggleBiocatch() {
        if(binding.lytBiocatch.constraintLyt.visibility == View.VISIBLE){
            binding.lytBiocatch.constraintLyt.visibility =View.GONE
            val drawable = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_expand) }
            binding.ivBiocatchTitle.setImageDrawable(drawable)
        }else{
            binding.lytBiocatch.constraintLyt.visibility =View.VISIBLE
            val drawable = context?.let { AppCompatResources.getDrawable(it, R.drawable.ic_collapse) }
            binding.ivBiocatchTitle.setImageDrawable(drawable)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        checkResultViewModel = ViewModelProvider(this, viewModelFactory).get(CheckResultViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        checkResultViewModel.fileFront.value = File(sharedPreferencesHelper.getFrontDocumentUri())
        checkResultViewModel.fileBack.value = File(sharedPreferencesHelper.getBackDocumentUri())
        checkResultViewModel.results.value = sharedPreferencesHelper.getExamResponse()
        checkResultViewModel.address.value = sharedPreferencesHelper.getAddress()
        checkResultViewModel.location.value = sharedPreferencesHelper.getLocation()
        binding.resultEmail = sharedPreferencesHelper.getEmail()
        binding.resultPhoneNumber = sharedPreferencesHelper.getPhone()?.nationalNumber
        binding.fileFront = checkResultViewModel.fileFront
        binding.fileBack = checkResultViewModel.fileBack
        binding.results = checkResultViewModel.results
        binding.address = checkResultViewModel.address
        binding.location = checkResultViewModel.location
        arguments?.let {binding.data =
            VerifyAllData2FragmentArgs.fromBundle(it).data
        }
        setupSuccessAlertDialog()
    }

    private fun openDialog() {
        binding.alertDialog.visibility = View.VISIBLE
    }

    private fun goToAskId() {
        findNavController().navigate(CheckResultFragmentDirections.actionCheckResultFragmentToAskIdFragment())
    }
}
package com.orugga.transunion.ui.steps.step3.step3_1

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.orugga.transunion.repository.DocumentsRepository
import com.orugga.transunion.repository.UserRepository
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.authExam.AuthExamResponse
import com.orugga.transunionsdk.vo.examRequest.ExamRequestResponse
import com.orugga.transunionsdk.vo.otpRequest.GetOTPResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class OTPViewModel
@Inject constructor(private val documentsRepository: DocumentsRepository,
                    private val userRepository: UserRepository,
                    private val sharedPreferencesHelper: SharedPreferencesHelper
) : ViewModel() {

    val showError = MutableLiveData<Boolean>()
    val getOTPResponse = MutableLiveData<Resource<GetOTPResponse>>()
    val sendOTPResponse = MutableLiveData<Resource<ExamRequestResponse>>()
    val resendOTPResponse = MutableLiveData<Resource<AuthExamResponse>>()
    val bypassOTPResponse = MutableLiveData<Resource<AuthExamResponse>>()
    val counterText = MutableLiveData<String>()


    fun getOTP(phoneNumber: String) {
        viewModelScope.launch {
            documentsRepository.getOTP("SMS", phoneNumber, "Mobile", "", "").collect {
                getOTPResponse.value = it
            }
        }
    }

    fun sendOTP(action: String, pinNumber: String) {
        viewModelScope.launch {
            documentsRepository.sendOTP(action, pinNumber).collect {
                sendOTPResponse.value = it
            }
        }
    }

    fun resendOTP(action: String, pinNumber: String) {
        viewModelScope.launch {
            documentsRepository.resendOTP(action, pinNumber).collect {
                resendOTPResponse.value = it
            }
        }
    }

    fun bypassOTP(action: String) {
        viewModelScope.launch {
            documentsRepository.bypassOTP(action).collect {
                bypassOTPResponse.value = it
            }
        }
    }

    fun showError(show: Boolean) {
        showError.value = show
    }
}
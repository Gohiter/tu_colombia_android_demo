package com.orugga.transunion.ui.steps.instantCard

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.orugga.transunion.repository.UserRepository
import javax.inject.Inject

class InstantCardViewModel
@Inject constructor(
    private val userRepository: UserRepository
): ViewModel(){

    val showDialog = MutableLiveData<Boolean>(false)

    init {

    }

    fun getApplicationId(): String {
        return userRepository.getAppId()
    }

}
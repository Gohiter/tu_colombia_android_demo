package com.orugga.transunion.ui.steps.step6.step6_2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep62ProofIncomeBinding
import com.orugga.transunion.ui.steps.step4.step4_2.VerifyAllData2FragmentArgs
import com.orugga.transunion.ui.util.PhotoFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import java.io.File
import javax.inject.Inject

class ProofIncomeFragment : PhotoFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentStep62ProofIncomeBinding
    private lateinit var viewModel: ProofIncomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_step_6_2_proof_income, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ProofIncomeViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnTakePic.setOnClickListener { dispatchTakePictureIntent(validation = false) }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        onSuccessTakenPicture = {file -> goToProofIncomeResult(file)}
    }

    private fun goToProofIncomeResult(file: File) {
        arguments?.let {
            findNavController().navigate(
                ProofIncomeFragmentDirections
                    .actionProofIncomeFragmentToProofIncomeResultFragment(
                        VerifyAllData2FragmentArgs.fromBundle(it).data, file
                    )
            )
        }
    }
}
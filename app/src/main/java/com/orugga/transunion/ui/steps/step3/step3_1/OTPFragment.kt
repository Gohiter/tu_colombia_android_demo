package com.orugga.transunion.ui.steps.step3.step3_1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentOtpBinding
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunionsdk.vo.Status
import javax.inject.Inject

class OTPFragment: BaseFragment() {
    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentOtpBinding
    private lateinit var viewModel: OTPViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_otp,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(OTPViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnContinue.setOnClickListener { onContinueClicked() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        binding.btnResened.setOnClickListener { onResendClicked() }
        binding.btnOmit.setOnClickListener { onOmitClicked() }
        binding.btnContinue.isEnabled = false
        binding.counterText = viewModel.counterText
        binding.getOTPResponse = viewModel.getOTPResponse
        binding.sendOTPResponse = viewModel.sendOTPResponse
        binding.resendOTPResponse = viewModel.resendOTPResponse
        binding.bypassOTPResponse = viewModel.bypassOTPResponse
        binding.showError = viewModel.showError
        binding.errorDialog.setPrimaryButtonAction { returnToAskId() }
        viewModel.counterText.value = "Presione reenviar código para reintentarlo"
        getOTP()
        observeSendOTPResponse()
        observeResendOTPResponse()
        observeBypassOTPResponse()
    }

    private fun getOTP() {
        val phone = sharedPreferencesHelper.getPhone()
        viewModel.getOTP(phone!!.nationalNumber)
    }

    private fun onContinueClicked() {
        val otp = binding.etCode.text.toString()
        viewModel.sendOTP("", otp)
    }

    private fun onResendClicked() {
        viewModel.resendOTP("ResendOTP", "")
    }

    private fun onOmitClicked() {
        viewModel.bypassOTP("Bypass")
    }

    private fun observeSendOTPResponse() {
        viewModel.sendOTPResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    result.data?.let {
                        sharedPreferencesHelper.saveExamResponse(it)
                    }
                    goToVerifyDataFragment()
                }
                Status.ERROR -> {
                    Toast.makeText(context, "OTP S Error", Toast.LENGTH_LONG)
                        .show()
                    viewModel.showError(true)
                }
            }
        })
    }

    private fun observeResendOTPResponse() {
        viewModel.resendOTPResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    getOTP()
                }
                Status.ERROR -> {
                    Toast.makeText(context, "OTP R Error", Toast.LENGTH_LONG)
                        .show()
                    viewModel.showError(true)
                }
            }
        })
    }

    private fun observeBypassOTPResponse() {
        viewModel.bypassOTPResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    val bypassOTPResponse =
                        result.data
                    if (bypassOTPResponse != null && bypassOTPResponse.status != "awaiting_approval") {
                        sharedPreferencesHelper.saveSelfieResponse(bypassOTPResponse)
                        goToQuestionsFragment()
                    } else {
                        Toast.makeText(
                            context,
                            "OTP B Exam Response Error",
                            Toast.LENGTH_LONG
                        )
                            .show()
                        viewModel.showError(true)
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, "OTP B Error", Toast.LENGTH_LONG)
                        .show()
                    viewModel.showError(true)
                }
            }
        })
    }

    private fun goToQuestionsFragment() {
        findNavController().navigate(OTPFragmentDirections.actionOTPFragmentToSingleQuestionFragment())
    }

    private fun goToVerifyDataFragment() {
        findNavController().navigate(OTPFragmentDirections.actionOTPFragmentToVerifyAllData1Fragment())
    }

    private fun returnToAskId() {
        findNavController().navigate(OTPFragmentDirections.actionOTPFragmentToAskIdFragment())
    }
}
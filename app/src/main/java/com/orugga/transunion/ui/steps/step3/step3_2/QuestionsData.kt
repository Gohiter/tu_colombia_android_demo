package com.orugga.transunion.ui.steps.step3.step3_2

import com.gitlab.mvysny.konsumexml.Konsumer

data class ExamDetails(
    val title: String,
    val language: String,
    val matchKeyId: String,
    val errorGeneratingQA: String,
    val timeTakenToAnswer: String,
    val questions: List<Question>) {
    companion object {
        fun xml(k: Konsumer): ExamDetails {
            k.checkCurrent("ExamDetails")
            val title = k.attributes.getValue("Title")
            val language = k.attributes.getValue("Language")
            val matchKeyId = k.attributes.getValue("MatchKeyId")
            val errorGeneratingQA = k.attributes.getValue("ErrorGeneratingQA")
            val timeTakenToAnswer = k.attributes.getValue("TimeTakenToAnswer")
            return ExamDetails(
                title,
                language,
                matchKeyId,
                errorGeneratingQA,
                timeTakenToAnswer,
                k.child("Questions") { children("Question") { Question.xml(this) } })
        }
    }
}

data class Question(
    val id: String,
    val questionId: String,
    val text: String,
    val type: String,
    val isDummy: String,
    val isQuestionTextHasListTypeDataElement: String,
    val listAnswerIndex: String,
    val expirationTime: String,
    val timeTakenToAnswer: String,
    val answers: List<Answer>) {
    companion object {
        fun xml(k: Konsumer): Question {
            k.checkCurrent("Question")
            return Question(
                k.attributes["Id"],
                k.attributes["QuestionId"],
                k.attributes["Text"],
                k.attributes["Type"],
                k.attributes["IsDummy"],
                k.attributes["isQuestionTextHasListTypeDataElement"],
                k.attributes["ListAnswerIndex"],
                k.attributes["ExpirationTime"],
                k.attributes["TimeTakenToAnswer"],
                k.children("Answer") { Answer.xml(this) })
        }
    }
}

data class Answer(var isSelected: String, val correctAnswer: String, val text: String) {
    companion object {
        fun xml(k: Konsumer): Answer {
            k.checkCurrent("Answer")
            return Answer(
                k.attributes["IsSelected"],
                k.attributes["CorrectAnswer"],
                k.text()
            )
        }
    }
}




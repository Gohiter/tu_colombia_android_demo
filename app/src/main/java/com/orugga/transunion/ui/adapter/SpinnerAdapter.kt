package com.orugga.transunion.ui.adapter

import android.content.Context
import android.graphics.Typeface
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.SpinnerAdapter
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.orugga.transunion.R


class SpinnerAdapter(
    private val mContext: Context,
    private val mData: Array<String>,
    private val mInitialSelection: String
) :
    BaseAdapter(), SpinnerAdapter {
    override fun getCount(): Int {
        return mData.size + 1
    }

    override fun getItem(position: Int): String {
        return if (position != 0) {
            mData[position - 1]
        } else {
            mInitialSelection
        }
    }

    override fun getItemId(position: Int): Long {
        return if (position != 0) {
            position.toLong()
        } else {
            -1
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var convertView = convertView
        if (convertView == null) convertView = LayoutInflater.from(mContext)
            .inflate(R.layout.support_simple_spinner_dropdown_item, parent, false)
        val text = convertView!!.findViewById<TextView>(android.R.id.text1)
        text.text = getItem(position)
        text.setTextColor(mContext.resources.getColor(R.color.tuBlack1))
        text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12f)
        val myTypeface = Typeface.create(
            mContext?.let { ResourcesCompat.getFont(it, R.font.intro_light_regular) },
            Typeface.NORMAL
        )
        text.setTypeface(myTypeface)
        return convertView
    }
}

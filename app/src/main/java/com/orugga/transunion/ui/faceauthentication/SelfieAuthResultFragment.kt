package com.orugga.transunion.ui.faceauthentication

import android.Manifest
import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.orugga.transunion.BuildConfig
import com.orugga.transunion.MainActivity
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentSelfieResultFaceAuthBinding
import com.orugga.transunion.databinding.FragmentStep2SelfieResultBinding
import com.orugga.transunion.ui.util.PermissionsHelper
import com.orugga.transunion.ui.util.PhotoFragment
import com.orugga.transunion.util.AddressHelper
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunion.vo.*
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
//import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.util.*
import javax.inject.Inject
import kotlin.concurrent.schedule

class SelfieAuthResultFragment : PhotoFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentSelfieResultFaceAuthBinding
    private lateinit var authViewModel: SelfieAuthViewModel
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private var firstName: String? = null
    private var lastName: String? = null
    private var lastLocation: Location? = null
    private var newValue = false
    private var data = AllData()

    companion object {
        const val AWAITING_APPROVAL = "awaiting_approval"
        const val AWAITING_DATA = "awaiting_data"
        const val REJECTED_SUBRESULT = "rejected"
        const val PULL_REPORT_RETRY_TIME = 10000L
        private const val TAG = "SelfieAuthFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_selfie_result_face_auth, container, false)
        return binding.root
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if (PermissionsHelper.checkPermission(
                requireActivity(),
                this,
                PermissionsHelper.LOCATION_PERMISSION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            if (AddressHelper.isLocationEnabled(requireActivity())) {
                LocationServices.getFusedLocationProviderClient(requireContext())
                    .requestLocationUpdates(locationRequest, locationCallback, null);

            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        authViewModel = ViewModelProvider(this, viewModelFactory).get(SelfieAuthViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.file = authViewModel.file
        binding.btnChangePicSelfie.setOnClickListener { dispatchTakePictureIntent(true) }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
//        binding.btnContinue.setOnClickListener { sendSelfie() }
        binding.btnContinue.setOnClickListener { this.goToNextStep() }
        binding.errorDialog.setPrimaryButtonAction(View.OnClickListener{ returnToAskId() })
        onSuccessTakenPictureAuth = {file -> authViewModel.file.value = file }
        binding.errorDialog.setAppId("#${authViewModel.getApplicationId()}")
        binding.showError = authViewModel.showError
        binding.invokeServiceResponse = authViewModel.invokeServiceResponse
        binding.createDocumentResponse = authViewModel.createSelfieResponse
        binding.uploadDocumentResponse = authViewModel.uploadSelfieResponse
        binding.processDocumentResponse = authViewModel.processSelfieResponse
        binding.facialSimilarityReportResponse = authViewModel.facialSimilarityReportResponse
        binding.pullReportResponse = authViewModel.pullReportResponse
        binding.pullRetrying = authViewModel.pullRetrying

        binding.createAppResponse = authViewModel.createAppResponse
        binding.tokenResponse = authViewModel.tokenResponse
        binding.googleAddressResponse = authViewModel.googleAddressResponse
        binding.emailVerificationResponse = authViewModel.emailVerificationResponse
        binding.phoneVerificationResponse = authViewModel.phoneVerificationResponse
        binding.blackBoxResponse = authViewModel.blackBoxResponse
        binding.deviceVerificationResponse = authViewModel.deviceVerificationResponse
        binding.watchlistResponse = authViewModel.watchlistResponse
        binding.offersResponse = authViewModel.offersResponse

        initializeLocationVariables()

        observeGoogleAddressResponse()
        observeWatchlistVerification()
        observeSendEmail()
        observeSendPhone()
        observeOffers()

//        observeInvokeServiceResponse()
//        observeCreateSelfieResponse()
//        observeUploadSelfieResponse()
//        observeProcessSelfieResponse()
//        observeSelfieReportResponse()
//        observePullReportResponse()
    }

//    private fun sendSelfie() {
//        newValue = true
//        authViewModel.invokeSelfieService()
//    }

    private fun returnToAskId() {
        authViewModel.showError(false)
//        findNavController().navigate(SelfieResultFragmentDirections.actionSelfieResultFragmentToAskIdFragment())
    }

    private fun observeInvokeServiceResponse() {
        authViewModel.invokeServiceResponse.observe(viewLifecycleOwner, Observer { result ->
            if (newValue) {
                when (result.status) {
                    Status.SUCCESS -> {
                        authViewModel.createSelfieDocument()
                    }
                    Status.ERROR -> {
                        Toast.makeText(context, "IS Error: ${result.message})", Toast.LENGTH_LONG).show()
                        authViewModel.showError(true)
                    }
                }
            }

        })
    }

    private fun observeCreateSelfieResponse() {
        authViewModel.createSelfieResponse.observe(viewLifecycleOwner, Observer { result ->
            if (newValue) {
                when (result.status) {
                    Status.SUCCESS -> {
//                        MediaType.parse("image/png")?.let { mediaType ->
                        "image/png".toMediaTypeOrNull()?.let { mediaType ->
                            authViewModel.uploadSelfie(
                                authViewModel.file.value!!,
                                mediaType
                            )
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(context, "CS Error: ${result.message})", Toast.LENGTH_LONG).show()
                        authViewModel.showError(true)
                    }
                }
            }

        })
    }

    private fun observeUploadSelfieResponse() {
        authViewModel.uploadSelfieResponse.observe(viewLifecycleOwner, Observer { result ->
            if (newValue) {
                when (result.status) {
                    Status.SUCCESS -> {
                        authViewModel.processSelfie()
                    }
                    Status.ERROR -> {
                        Toast.makeText(context, "US Error: ${result.message})", Toast.LENGTH_LONG).show()
                        authViewModel.showError(true)
                    }
                }
            }
        })
    }

//    private fun observeProcessSelfieResponse() {
//        authViewModel.processSelfieResponse.observe(viewLifecycleOwner, Observer { result ->
//            if (newValue) {
//                when (result.status) {
//                    Status.SUCCESS -> {
//                        authViewModel.getFacialSimilarityReport()
//                    }
//                    Status.ERROR -> {
//                        Toast.makeText(context, "PS Error: ${result.message})", Toast.LENGTH_LONG).show()
//                        authViewModel.showError(true)
//                    }
//                }
//            }
//        })
//    }

//    private fun observeSelfieReportResponse() {
//        authViewModel.facialSimilarityReportResponse.observe(viewLifecycleOwner, Observer { result ->
//            if (newValue) {
//                when (result.status) {
//                    Status.SUCCESS -> {
//                        val selfieResponse = result.data?.dsFacialSimilarityApplicanFields?.dsFacialSimilarityApplicantIO?.dsFacialSimilarityApplicant?.get(0)?.attributes?.dSFacialSimilarity
//                        if(selfieResponse != null && selfieResponse.status != "awaiting_approval"){
//                            sharedPreferencesHelper.saveSelfieResponse(selfieResponse)
//                        }
//                        authViewModel.pullReport()
//                    }
//                    Status.ERROR -> {
//                        Toast.makeText(context, "PR Error: ${result.message})", Toast.LENGTH_LONG).show()
//                        authViewModel.showError(true)
//                    }
//                }
//            }
//        })
//    }

//    private fun observePullReportResponse() {
//        authViewModel.pullReportResponse.observe(viewLifecycleOwner, Observer { result ->
//            if (newValue) {
//                when (result.status) {
//                    Status.SUCCESS -> {
//                        val attributes =
//                            result.data?.fields?.applicantIO?.applicant?.get(0)?.attributes
//                        val documentStatus = attributes?.dSIdDocumentReport?.status ?: "null"
//                        val selfieStatus = attributes?.dSFacialSimilarityReport?.status ?: "null"
//                        val documentsubResult = attributes?.dSIdDocumentReport?.subResult
//                        val selfieSubResult = attributes?.dSFacialSimilarityReport?.subResult
//                        Log.i(
//                            "REPORT STATUS",
//                            "document status: $documentStatus, selfie status: $selfieStatus"
//                        )
//                        if (documentStatus == AWAITING_APPROVAL || documentStatus == AWAITING_DATA || selfieStatus == AWAITING_APPROVAL) {
//                            runBlocking {
//                                delayedPullReport()
//                            }
//                        } else if (documentsubResult == REJECTED_SUBRESULT || selfieSubResult == REJECTED_SUBRESULT) {
//                            authViewModel.retryPull(false)
//                            authViewModel.showError(true)
//                        } else {
//                            authViewModel.retryPull(false)
//                            val selfieResponse = attributes?.dSFacialSimilarityReport
//                            val documentResponse = attributes?.dSIdDocumentReport
//                            if (selfieResponse != null && selfieResponse.status != AWAITING_APPROVAL) {
//                                sharedPreferencesHelper.saveSelfieResponse(selfieResponse)
//                            }
//                            if (documentResponse != null && documentResponse.status != AWAITING_APPROVAL) {
//                                sharedPreferencesHelper.saveDocumentResponse(documentResponse)
//                                firstName = documentResponse.properties?.firstName
//                                lastName = documentResponse.properties?.lastName
//                                getCurrentLocation()
//                            }
//                        }
//                    }
//                    Status.ERROR -> {
//                        authViewModel.showError(true)
//                    }
//                }
//            }
//        })
//    }

    private fun observeGoogleAddressResponse() {
        authViewModel.googleAddressResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    context?.let {
                        binding.progressDialog.setTitle(getString(R.string.verifying_your_device))
                        authViewModel.getBlackBox(it, BuildConfig.IOVATION_KEY)
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, "GA Error: ${result.message})", Toast.LENGTH_LONG).show()
                    (activity as MainActivity).showErrorAndReset(result.message.toString())
                }
            }
        })
    }

    private fun observeWatchlistVerification() {
        authViewModel.watchlistResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    binding.progressDialog.setTitle(getString(R.string.verifying_your_email))
                    val savedEmail = sharedPreferencesHelper.getEmail()
                    authViewModel.sendEmail(savedEmail)
                }
                Status.ERROR -> {
                    Toast.makeText(context, "WL Error: ${result.message})", Toast.LENGTH_LONG).show()
                    (activity as MainActivity).showErrorAndReset(result.message.toString())
                }
            }
        })
    }

    private fun observeSendEmail() {
        authViewModel.emailVerificationResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    binding.progressDialog.setTitle(getString(R.string.verifying_your_phone_number))
                    val savedPhone = sharedPreferencesHelper.getPhone()
                    if (savedPhone != null) {
                        authViewModel.sendPhone(savedPhone.nationalNumber, "2", savedPhone.countryCode)
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, "EV Error: ${result.message})", Toast.LENGTH_LONG).show()
                    (activity as MainActivity).showErrorAndReset(result.message.toString())
                }
            }
        })
    }

    private fun observeSendPhone() {
        authViewModel.phoneVerificationResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    firstName?.let {
                        binding.progressDialog.setTitle(getString(R.string.offers))
                        lastName?.let { lastName ->
                            authViewModel.getOffers(
                                it, lastName
                            )
                        }
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, "PV Error: ${result.message})", Toast.LENGTH_LONG).show()
                    (activity as MainActivity).showErrorAndReset(result.message.toString())
                }
            }
        })
    }

    private fun observeOffers() {
        authViewModel.offersResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    goToNextStep()
                }
                Status.ERROR -> {
                    Toast.makeText(context, "GO Error: ${result.message})", Toast.LENGTH_LONG).show()
                    (activity as MainActivity).showErrorAndReset(result.message.toString())
                }
            }
        })
    }

    private fun initializeLocationVariables() {
        locationRequest = LocationRequest.create()
        locationRequest.interval = 1000
        locationRequest.fastestInterval = 500
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){
                    lastLocation = location
                    Log.i("Location Update",location.toString())
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun getCurrentLocation() {
        authViewModel.googleAddressResponse.value = Resource.loading(null)
        if (PermissionsHelper.checkPermission(
                requireActivity(),
                this,
                PermissionsHelper.LOCATION_PERMISSION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            if (AddressHelper.isLocationEnabled(requireActivity())) {
                LocationServices.getFusedLocationProviderClient(
                    requireActivity()
                ).lastLocation.addOnSuccessListener(
                    requireActivity()
                ) { location: Location? ->
                    var address = ""
                    if (location == null) {
                        Log.e("showCurrentLocation", "Location is null")
                        if (lastLocation != null) {
                            address = AddressHelper.getAddressFromLocation(
                                lastLocation!!.latitude,
                                lastLocation!!.longitude,
                                requireContext()
                            )
                        }
                    } else location.apply {
                        lastLocation = location
                        address = AddressHelper.getAddressFromLocation(
                            location.latitude,
                            location.longitude,
                            requireContext()
                        )
                    }
                    Log.i(TAG, "Google Api obtained address: $address")
                    authViewModel.googleAddressResponse.value = Resource.success(address)
                }
            } else {
                authViewModel.googleAddressResponse.value = Resource.error("You need to grant location permission.", null)
            }
        }
    }



    private fun buildWatchlistData(): WatchlistData {
        // Get watchlist verification data
        val matches = authViewModel.watchlistResponse
            .value?.data?.responseFields?.applicantIO?.applicant
            ?.get(0)?.attributes?.dsWatchlistVerification?.matches
        if (matches?.isEmpty()!!) {
            return WatchlistData()
        }
        val watchlistResponse = matches[0]

        // Build Watchlist Data object
        var eventDescription = ""
        var eventCategoryCode = ""
        if (watchlistResponse.events.isNotEmpty()) {
            val event = watchlistResponse.events[0]
            eventDescription = event.category.categoryDesc
            eventCategoryCode = event.category.categoryCode
        }
        return WatchlistData(
            watchlistResponse.entityName,
            eventCategoryCode,
            eventDescription,
            watchlistResponse.matchScore
        )

    }

    private fun buildDeviceData(): DeviceData {
        // Get device verification data
        val deviceVerificationFields = authViewModel.deviceVerificationResponse
            .value?.data?.deviceVerificationResponseFields?.applicantIO
            ?.applicant?.get(0)?.deviceVerificationResponseApplicantAttributes?.dsPhoneVerification

        // Build device Data object
        return DeviceData(
            "${deviceVerificationFields?.deviceLatitud?.substring(0, deviceVerificationFields.deviceLatitud.lastIndexOf(".") + 3)}, " +
                    "${deviceVerificationFields?.deviceLongitud?.substring(0, deviceVerificationFields.deviceLongitud.lastIndexOf(".") + 3)}",
            "",
            "deviceVerificationFields?.deviceMoBr",
            "deviceVerificationFields?.devMoApVe",
            "deviceVerificationFields?.deviceOS",
            "deviceVerificationFields?.deviceType",
            "",
            "deviceVerificationFields?.devMoSyCa",
            "deviceVerificationFields?.devMoLoTi",
            "",
            "",
            "",
            "",
            "deviceVerificationFields?.devMoSyNe",
            authViewModel.googleAddressResponse.value?.data
        )
    }

    private fun buildEmailData(): EmailData {
        val emailResponse = authViewModel.emailVerificationResponse.value?.data
            ?.fields?.applicantsIO?.applicants?.get(0)?.attributes
        //Build email data object
        return EmailData(
            sharedPreferencesHelper.getEmail(), emailResponse?.dsVerification?.basicCheckStatus,
            emailResponse?.dsVerification?.basicCheckStatusCode,
            emailResponse?.dsVerification?.dateFirstSeen,
            "", "", "", "", ""
        )
    }

    private fun buildPhoneData(): PhoneData {
        val phoneResponse = authViewModel.phoneVerificationResponse.value?.data
            ?.fields?.applicantsIO?.applicants?.get(0)?.attributes
        // Build phone data object
        return PhoneData(
            "+${phoneResponse?.dsVerification?.phoneCountryCode}${phoneResponse?.dsVerification?.phoneNumber}",
            phoneResponse?.dsVerification?.phoneCity, phoneResponse?.dsVerification?.phoneCountry,
            phoneResponse?.dsVerification?.phoneTypeDescription,
            phoneResponse?.dsVerification?.phoneRiskLevel,
            phoneResponse?.dsVerification?.phoneRiskScore,
            phoneResponse?.dsVerification?.phoneRiskRecommendation,
            phoneResponse?.drVerification?.isPhoneBlackListCheckSuccess
        )
    }

//    private suspend fun delayedPullReport() {
//        withContext(Dispatchers.Unconfined){
//            authViewModel.retryPull(true)
//            Timer().schedule(PULL_REPORT_RETRY_TIME){
//                authViewModel.pullReport()
//            }
//        }
//    }

    private fun goToNextStep() {
        if(newValue){
            newValue = false
            data.phoneData = buildPhoneData()
            data.emailData = buildEmailData()
            data.deviceData = buildDeviceData()
            data.watchlistData = buildWatchlistData()
        }
        findNavController().navigate(SelfieAuthResultFragmentDirections.actionSelfieAuthFragmentFragmentToBankAccountFragment())
    }
}
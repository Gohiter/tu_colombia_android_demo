package com.orugga.transunion.ui.util

import android.app.Activity
import android.content.Intent
import android.os.Environment
import android.util.Log
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import com.orugga.transunionsdk.activity.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date


abstract class PhotoFragment: BaseFragment() {

    private lateinit var photoFile: File
    private lateinit var photoFileFront: File
    private lateinit var photoFileBack: File
    private lateinit var photoFileSelfie: File
    var onSuccessTakenPicture: ((file: File) -> Unit)? = null


    var onSuccessTakenPictureAuth: ((file: File) -> Unit)? = null


    var onSuccessTakenFrontBack: ((fileFront: File, fileBack: File) -> Unit)? = null

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionsHelper.CAMERA_PERMISSION
            && grantResults.isNotEmpty()) {
            dispatchTakePictureIntent()
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date()) + ".jpg"
        val storageDir: File = requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val newFile = File(storageDir, timeStamp)
        newFile.createNewFile()
        photoFile = newFile
        return newFile
    }

    fun dispatchDocPictureVeridas() {
        val intent = Intent(activity, DocActivity::class.java)
        startForResultDoc.launch(intent)

    }

    fun dispatchSelfiePictureVeridas() {
        val intent = Intent(activity, SelfieCaptureActivity::class.java)
        startforResultSelfie.launch(intent)
    }

    private val startForResultDoc = registerForActivityResult(StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data
                Log.i("PhotoFront", intent?.getStringExtra(DocActivity.KEY_FRONT_FILE_PATH)!!)
                Log.i("PhotoBack", intent?.getStringExtra(DocActivity.KEY_BACK_FILE_PATH)!!)
                photoFileFront = File(intent?.getStringExtra(DocActivity.KEY_FRONT_FILE_PATH))
                photoFileBack = File(intent?.getStringExtra(DocActivity.KEY_BACK_FILE_PATH))
                onSuccessTakenFrontBack?.invoke(photoFileFront, photoFileBack)
            }
        }

    private val startforResultSelfie = registerForActivityResult(StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data
                Log.i("PhotoSelfie", intent?.getStringExtra(SelfieCaptureActivity.KEY_SELFIE_FILE_PATH)!!)
                photoFileSelfie = File(intent?.getStringExtra(SelfieCaptureActivity.KEY_SELFIE_FILE_PATH))
                onSuccessTakenPicture?.invoke(photoFileSelfie)
            }
        }

    fun dispatchTakePictureIntent(frontal: Boolean = false, validation: Boolean = true) {
        val intent = when {
            frontal -> Intent(requireActivity(), SelfieActivity::class.java)
            (!frontal && validation) -> Intent(requireActivity(), DocCaptureActivity::class.java)
            else -> Intent(requireActivity(), ProofCaptureActivity::class.java)
        }
        startForResult.launch(intent)
    }

    private val startForResult = registerForActivityResult(StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val intent = result.data
            Log.i("PhotoFragment", intent?.getStringExtra(DocCaptureActivity.KEY_FILE_PATH))
            photoFile = File(intent?.getStringExtra(DocCaptureActivity.KEY_FILE_PATH))
            onSuccessTakenPicture?.invoke(photoFile)
        }
    }

//    fun dispatchTakePictureIntent(frontal: Boolean = false) {
//        if (PermissionsHelper.checkPermission(requireActivity(), this, PermissionsHelper.CAMERA_PERMISSION, Manifest.permission.CAMERA)) {
//            Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
//                takePictureIntent.resolveActivity(requireContext().packageManager)?.also {
//                    val photoFile: File? = try {
//                        createImageFile()
//                    } catch (ex: IOException) {
//                        null
//                    }
//                    photoFile?.also {
//                        val packageName = activity?.packageName
//                        val photoURI: Uri = FileProvider.getUriForFile(requireContext(),"$packageName.provider", it)
//                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
//                        when {
//                            Build.VERSION.SDK_INT < Build.VERSION_CODES.O -> {
//                                takePictureIntent.putExtra(
//                                    "android.intent.extras.CAMERA_FACING",
//                                    CameraCharacteristics.LENS_FACING_FRONT
//                                )  // Tested on API 24 Android version 7.0(Samsung S6)
//                            }
//                            Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> {
//                                takePictureIntent.putExtra(
//                                    "android.intent.extras.CAMERA_FACING",
//                                    CameraCharacteristics.LENS_FACING_FRONT
//                                ) // Tested on API 27 Android version 8.0(Nexus 6P)
//                                takePictureIntent.putExtra(
//                                    "android.intent.extra.USE_FRONT_CAMERA",
//                                    true
//                                )
//                            }
//                            else -> takePictureIntent.putExtra(
//                                "android.intent.extras.CAMERA_FACING",
//                                1
//                            )
//                        }
//                        startActivityForResult(takePictureIntent, PermissionsHelper.REQUEST_IMAGE_CAPTURE)
//                    }
//                }
//            }
//        }
//    }

}
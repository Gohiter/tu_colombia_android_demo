package com.orugga.transunion.ui.steps.step3.step3_2

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.gitlab.mvysny.konsumexml.konsumeXml
import com.orugga.transunion.repository.DocumentsRepository
import com.orugga.transunion.repository.UserRepository
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.examRequest.ExamRequestResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class QuestionsViewModel
@Inject constructor(private val documentsRepository: DocumentsRepository,
                    private val userRepository: UserRepository,
                    private val sharedPreferencesHelper: SharedPreferencesHelper
) : ViewModel() {

    val examSecondChance = MutableLiveData(false)
    val showError = MutableLiveData<Boolean>()
    val examDetail = MutableLiveData<ExamDetails>()
    val examQuestion = MutableLiveData<String>()
    val examAnswerOne = MutableLiveData<String>()
    val examAnswerTwo = MutableLiveData<String>()
    val examAnswerThree = MutableLiveData<String>()
    val examAnswerFour = MutableLiveData<String>()
    val questionIndex = MutableLiveData<Int>(0)
    val processExamResponse = MutableLiveData<Resource<ExamRequestResponse>>()

    init {
        getXml() }

    fun getApplicationId(): String {
        return userRepository.getAppId()
    }

    private fun getXml() {
        val getSelfieResponse = sharedPreferencesHelper.getSelfieResponse()
        val responseXml = getSelfieResponse.fields.examData
        parseXml(responseXml)
    }

    fun parseXml(responseXml: String?) {
        if (responseXml != null) {
            val examDetails =
                responseXml.konsumeXml().child("ExamDetails") { ExamDetails.xml(this) }
            examDetail.value = examDetails
        } else {
            showError(true)
        }
    }

    fun processExam(xml: String) {
        viewModelScope.launch {
            documentsRepository.processExam(xml).collect {
                processExamResponse.value = it
            }
        }
    }

    fun showError(show: Boolean) {
        showError.value = show
    }

    fun examSecondChance(chance: Boolean){
        examSecondChance.value = chance
    }
}

package com.orugga.transunion.ui.intro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentIntroBinding
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunion.vo.DemoAppContexts
import com.orugga.transunionsdk.TransunionSdk
import javax.inject.Inject

class IntroFragment : BaseFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    @Inject
    lateinit var transunionSdk: TransunionSdk
    private lateinit var binding: FragmentIntroBinding
    private var adapter: IntroItemPagerAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_intro, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        adapter = context?.let { IntroItemPagerAdapter(it) }
        initializeViewPager()
        binding.btnSkipIntro.setOnClickListener{ goToAskId() }
        binding.btnContinue.setOnClickListener{ goToAskId() }
        binding.btnGoNext.setOnClickListener{ nextPage() }
    }

    private fun nextPage() {
        binding.viewPager.currentItem +=1
    }

    private fun goToAskId() {
    }
    private fun initializeViewPager() {
        binding.viewPager.adapter = adapter
        binding.dotsIndicator.setViewPager(binding.viewPager)
        binding.viewPager.adapter?.registerDataSetObserver(binding.dotsIndicator.dataSetObserver)
        binding.viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                when(position) {
                    0 -> {
                        transunionSdk.changeContext(DemoAppContexts.WALKTHROUGH_1.value)
                        showNext()
                    }
                    1 -> {
                        transunionSdk.changeContext(DemoAppContexts.WALKTHROUGH_2.value)
                        showNext()
                    }
                    2 -> {
                        transunionSdk.changeContext(DemoAppContexts.WALKTHROUGH_3.value)
                        showContinue()
                    }
                }
            }

            fun showContinue() {
                binding.btnContinue.visibility = View.VISIBLE
                binding.btnGoNext.visibility = View.GONE
                binding.btnSkipIntro.visibility = View.GONE
            }

            fun showNext() {
                binding.btnContinue.visibility = View.GONE
                binding.btnGoNext.visibility = View.VISIBLE
                binding.btnSkipIntro.visibility = View.VISIBLE
            }

            override fun onPageScrollStateChanged(state: Int) {}

        })
    }

}
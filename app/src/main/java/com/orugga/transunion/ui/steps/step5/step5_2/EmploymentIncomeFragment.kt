package com.orugga.transunion.ui.steps.step5.step5_2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep52InfoEmploymentIncomeBinding
import com.orugga.transunion.ui.steps.step4.step4_2.VerifyAllData2FragmentArgs
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import javax.inject.Inject

class EmploymentIncomeFragment : BaseFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentStep52InfoEmploymentIncomeBinding
    private lateinit var viewModel: EmploymentIncomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_step_5_2_info_employment_income, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(EmploymentIncomeViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnContinue.setOnClickListener { goToSignatureFragment() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        setupSelector()
    }

    private fun goToSignatureFragment() {    }

    private fun setupSelector() {
        var incomeBarChanged: Boolean
        var incomeBarInitialChange = false

        val displayMetrics = requireActivity().resources.displayMetrics
        val minDpsX = 20
        val minDpsXFinal = minDpsX * displayMetrics.density
        val maxDpsX = 126
        val maxDpsXFinal = displayMetrics.widthPixels - maxDpsX * displayMetrics.density

        val minDpstoMin = 70
        val minDpstoMinFinal = minDpstoMin * displayMetrics.density
        val maxDpstoMax = 184
        val maxDpstoMaxFinal = displayMetrics.widthPixels - maxDpstoMax * displayMetrics.density

        val incomeBar = binding.incomeBar
        incomeBar.maxValue = getString(R.string.viewpager_employ_annual_income_card_max).toFloat()
        incomeBar.minValue = getString(R.string.viewpager_employ_annual_income_card_min).toFloat()
        incomeBar.setOnSeekbarChangeListener { value ->
            val rect = incomeBar.leftThumbRect

            var nextX: Float =
                rect.right - displayMetrics.density * 13 / 2 - binding.txtValActual.width / 4
            if (nextX < minDpsXFinal) {
                nextX = minDpsXFinal
            }
            if (nextX > maxDpsXFinal) {
                nextX = maxDpsXFinal
            }
            binding.txtValActual.x = nextX
            if (incomeBarInitialChange) {
                if (incomeBar.maxValue > DOLLAR_MAX) {
                    binding.txtValActual.text = getSimpleMillionsName(value.toInt())
                } else {
                    binding.txtValActual.text = getStringFromIntWithDot(value.toInt())
                }
                incomeBarChanged = true
                if (incomeBarChanged) {
                    binding.btnContinue.isEnabled = true
                }
            }
            incomeBarInitialChange = true
        }
        incomeBar.position = 0
    }

    private fun getSimpleMillionsName(value: Int): String? {
        return if (value > COLOMBIAN_PESOS_THRESHOLD) {
            getString(R.string.income_max)
        } else {
            val entireValue: Int = value / 1000000
            if (entireValue > 1) {
                "$ $entireValue millones"
            } else {
                "$ $entireValue millón"
            }
        }
    }

    private fun getStringFromIntWithDot(value: Int): String? {
        var intRes: String
        val nMaxWithoutDots = 10000
        intRes = value.toString()
        if (value >= nMaxWithoutDots) {
            val nLenInt = intRes.length
            if (nLenInt == 5) {
                intRes = intRes.substring(0, 2) + "," + intRes.substring(2, 5)
            } else if (nLenInt == 6) {
                intRes = intRes.substring(0, 3) + "," + intRes.substring(3, 6)
            }
        }
        return "$ $intRes"
    }

    companion object {
        const val DOLLAR_MAX = 150000
        const val COLOMBIAN_PESOS_THRESHOLD = 49999990
    }
}
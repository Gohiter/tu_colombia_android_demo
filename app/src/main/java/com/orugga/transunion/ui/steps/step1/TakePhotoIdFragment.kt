package com.orugga.transunion.ui.steps.step1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep1ScanIdBinding
import com.orugga.transunion.ui.util.PhotoFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import java.io.File
import javax.inject.Inject

class TakePhotoIdFragment : PhotoFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentStep1ScanIdBinding
    private lateinit var viewModel: PhotoIdViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_step_1_scan_id, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(PhotoIdViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnTakePic.setOnClickListener { dispatchDocPictureVeridas() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        onSuccessTakenFrontBack = { fileFront, fileBack -> goToTakePhotoIdResult(fileFront, fileBack)}
    }

    private fun goToTakePhotoIdResult(fileFront: File, fileBack: File) {
        sharedPreferencesHelper.saveFrontDocumentUri(fileFront.absolutePath)
        sharedPreferencesHelper.saveBackDocumentUri(fileBack.absolutePath)
        arguments?.let {
            findNavController().navigate(TakePhotoIdFragmentDirections.actionTakePhotoIdFragmentToTakePhotoIdResultFragment())
        }
    }
}
package com.orugga.transunion.ui.steps.step6.step6_3

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep63GetsignatureBinding
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunion.vo.AllData
import com.orugga.transunionsdk.vo.Status
import kotlinx.android.synthetic.main.fragment_step_6_3_getsignature.*
import javax.inject.Inject

class SignatureFragment : BaseFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentStep63GetsignatureBinding
    private lateinit var viewModel: SignatureViewModel
    private lateinit var data: AllData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_step_6_3_getsignature,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(SignatureViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.inkFilled = viewModel.inkFilled
        binding.biocatchResponse = viewModel.biocatchResponse
        binding.completeResponse = viewModel.completeTransactionResponse
        binding.credoLabTokenResponse = viewModel.credoLabTokenResponse
        binding.credoLabDataInsightResponse = viewModel.dataSetInsightResponse
        binding.btnClearSignature.setOnClickListener { clearInk() }
        binding.btnSubmit.setOnClickListener { goToCheckResultsFragment() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        viewModel.inkFilled.value = false
        arguments?.let {data =
            SignatureFragmentArgs.fromBundle(it).data!!
        }
        setupInk()
        observeBiocatch()
        observeCredoLabTokenResponse()
        observeCredoLabDataSetInsightResponse()
        observeCompleteTransaction()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupInk() {
        binding.ink.setOnTouchListener(
            object : OnTouchListener {
                override fun onTouch(v: View, event: MotionEvent): Boolean {
                    if (binding.ink.isViewEmpty) {
                        if (event.action == MotionEvent.ACTION_DOWN) {
                            viewModel.inkFilled.value = true
                            return false
                        }

                    }
                    return false
                }
            })
    }

    private fun clearInk() {
        btnClearSignature.setOnClickListener {
            binding.ink.clear()
            if (binding.ink.isViewEmpty) {
                viewModel.inkFilled.value = false
            }
        }
    }

    private fun observeBiocatch() {
        viewModel.biocatchResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    data.biocatchScore = viewModel.biocatchResponse.value?.data?.fields?.applicantsIO?.applicants?.get(0)?.attributes?.dsBiocatch?.score
                    viewModel.completeTransaction()
                }
                Status.ERROR -> {
                    Log.e(TAG, result.message)
                }
            }
        })
    }

    private fun observeCredoLabTokenResponse() {
        viewModel.credoLabTokenResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    Log.i(TAG, "Success CredoLab Token ${result.data}")
                    viewModel.getCredoLabDataSetInsight(sharedPreferencesHelper.getCredoLabReference())
                }
                Status.ERROR -> {
                    Log.e(TAG, result.message)
                }
            }
        })
    }

    private fun observeCredoLabDataSetInsightResponse() {
        viewModel.dataSetInsightResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    Log.i(TAG, "Success CredoLab DataSet Insight ${result.data}")
                    result.data?.scores?.let {
                        sharedPreferencesHelper.saveCredoLabScores(it)
                        data.credoLabScores = it
                    }
                    goToCheckResultsFragment()
                }
                Status.ERROR -> {
                    Log.e(TAG, result.message)
                }
            }
        })
    }

    private fun observeCompleteTransaction() {
        viewModel.completeTransactionResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    viewModel.getCredoLabToken()
                }
                Status.ERROR -> {
                    Log.e(TAG, result.message)
                    viewModel.getCredoLabToken()
                }
            }
        })
    }

    private fun goToCheckResultsFragment() {
        findNavController().navigate(
            SignatureFragmentDirections.actionSignatureFragmentToCheckResultFragment(data)
        )
    }

    private fun nextStep() {
        if(sharedPreferencesHelper.isHasId()) {
//            viewModel.getBiocatchScore()
            viewModel.completeTransaction()
        } else {
            goToCheckResultsFragment()
        }
    }

    companion object {
        private const val TAG = "SignatureFragment"
    }

}
package com.orugga.transunion.ui.steps.step4.step4_1

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.orugga.transunion.repository.UserRepository
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.deviceVerification.DeviceVerificationResponse
import com.orugga.transunionsdk.vo.docAuthenticationReport.Properties
import com.orugga.transunionsdk.vo.watchlist.WatchlistResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class VerifyAllData1ViewModel
@Inject constructor(private val userRepository: UserRepository): ViewModel(){

    val deviceVerificationResponse = MutableLiveData<Resource<DeviceVerificationResponse>>()

    val blackBoxResponse = MutableLiveData<Resource<String>>()

    val watchlistResponse = MutableLiveData<Resource<WatchlistResponse>>()

    val documentProperties = MutableLiveData<Properties>()

    init {

    }

    fun getBlackBox(appContext: Context, iovationSubscriberKey: String) {
        viewModelScope.launch {
            userRepository.getBlackBox(appContext, iovationSubscriberKey).collect {
                blackBoxResponse.value = it
            }
        }
    }

}
package com.orugga.transunion.ui.faceauthentication

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.orugga.transunion.repository.DocumentsRepository
import com.orugga.transunion.repository.UserRepository
import com.orugga.transunion.repository.VerificationRepository
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.TokenResponse
import com.orugga.transunionsdk.vo.UploadableDocumentType
import com.orugga.transunionsdk.vo.authExam.AuthExamResponse
import com.orugga.transunionsdk.vo.createApp.CreateAppResponse
import com.orugga.transunionsdk.vo.createDocument.CreateAttachDocumentResponse
import com.orugga.transunionsdk.vo.deviceVerification.DeviceVerificationResponse
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceResponse
import com.orugga.transunionsdk.vo.emailVerification.EmailVerificationResponse
import com.orugga.transunionsdk.vo.faceauthentication.FaceAuthenticationLoginResponse
import com.orugga.transunionsdk.vo.facialSimilarityInvokeService.FacialSimilarityInvokeServiceResponse
import com.orugga.transunionsdk.vo.facialSimilarityReport.FacialSimilarityReportResponse
import com.orugga.transunionsdk.vo.offers.OffersResponse
import com.orugga.transunionsdk.vo.passiveliveness.PassiveLivenessResponse
import com.orugga.transunionsdk.vo.phoneVerification.PhoneVerificationResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import okhttp3.MediaType
import com.orugga.transunionsdk.vo.pullReport.PullReportResponse
import com.orugga.transunionsdk.vo.watchlist.WatchlistResponse
import java.io.File
import javax.inject.Inject

class SelfieAuthViewModel
@Inject constructor(private val verificationRepository: VerificationRepository,
                    private val documentsRepository: DocumentsRepository,
                    private val userRepository: UserRepository): ViewModel(){

    val pullRetrying = MutableLiveData<Boolean>()
    val file = MutableLiveData<File>()
    val pullReportResponse = MutableLiveData<Resource<PullReportResponse>>()
    val showError = MutableLiveData<Boolean>()
    val invokeServiceResponse = MutableLiveData<Resource<FacialSimilarityInvokeServiceResponse>>()
    val createSelfieResponse = MutableLiveData<Resource<CreateAttachDocumentResponse>>()
    val uploadSelfieResponse = MutableLiveData<Resource<CreateAttachDocumentResponse>>()
    val processSelfieResponse = MutableLiveData<Resource<AuthExamResponse>>()
    val facialSimilarityReportResponse = MutableLiveData<Resource<FacialSimilarityReportResponse>>()

    val emailVerificationResponse = MutableLiveData<Resource<EmailVerificationResponse>>()
    val phoneVerificationResponse = MutableLiveData<Resource<PhoneVerificationResponse>>()
    val deviceVerificationResponse = MutableLiveData<Resource<DeviceVerificationResponse>>()
    val blackBoxResponse = MutableLiveData<Resource<String>>()
    val watchlistResponse = MutableLiveData<Resource<WatchlistResponse>>()
    val tokenResponse = MutableLiveData<Resource<TokenResponse>>()
    val createAppResponse = MutableLiveData<Resource<CreateAppResponse>>()
    val googleAddressResponse = MutableLiveData<Resource<String>>()
    val offersResponse = MutableLiveData<Resource<OffersResponse>>()
    val passiveLivenessResponse = MutableLiveData<Resource<PassiveLivenessResponse>>()
    val faceAuthenticationLoginResponse = MutableLiveData<Resource<FaceAuthenticationLoginResponse>>()

    init {

    }

    fun getApplicationId(): String {
        return userRepository.getAppId()
    }

//    fun pullReport() {
//        viewModelScope.launch {
//            documentsRepository.pullReport().collect {
//                pullReportResponse.value = it
//            }
//        }
//    }

    fun retryPull(show: Boolean) {
        pullRetrying.value = show
    }

    fun showError(show: Boolean) {
        showError.value = show
    }

//    fun invokeSelfieService() {
//        viewModelScope.launch {
//            documentsRepository.invokeFacialSimilarityService("", "", "", "").collect {
//                invokeServiceResponse.value = it
//            }
//        }
//    }

    fun createSelfieDocument() {
        viewModelScope.launch {
            documentsRepository.createDocument(UploadableDocumentType.SELFIE, "").collect {
                createSelfieResponse.value = it
            }
        }
    }

    fun uploadSelfie(file: File, mediaType: MediaType) {
        viewModelScope.launch {
            documentsRepository.uploadDocument(file, mediaType, UploadableDocumentType.SELFIE).collect {
                uploadSelfieResponse.value = it
            }
        }
    }

    fun processSelfie() {
        viewModelScope.launch {
            documentsRepository.processSelfie().collect {
                processSelfieResponse.value = it
            }
        }
    }

//    fun getFacialSimilarityReport() {
//        viewModelScope.launch {
//            documentsRepository.getFacialSimilarityReport().collect {
//                facialSimilarityReportResponse.value = it
//            }
//        }
//    }

//    fun getAuth() {
//        viewModelScope.launch {
//            userRepository.getAuthToken().collect {
//                tokenResponse.value = it
//            }
//        }
//    }

    fun createApp() {
        viewModelScope.launch {
            userRepository.createApp("santiagolky@gmail.com", "57", "3178343999", "0610IDXLAP3RgeO6+aHpGwrJS3BYmpfzhlZ4Xrr3mxaaF4CFsF0V6q4g1wlhwwmFPnn0d2/wOllLiuUNwobTVE7FietDKnTCjwwHkx8BLEyt88d9GIlnWUuHKQIlNqD3J4AFnzhqGiz54LroEW8akNQY7vID0zGGbwW+PgRYUjoXHkJ+PAD/vc36vNB3GEGzt34hmoTR8K0iB48NVwwZEQvQah/ptz+M3HyjxAVo922mlE87iig1MMa5e2MF1ACFvxdG1BVPJbnBJsW17Y+zbPJ1BS5YnsR6eZiVJlCZbiEq3no2uxm1Anw4xa9iKg2Mm2QMhKVwpiDyrvYjNb1g6se3k25n4OeHuMfJpoFwTlSuaek6jzj/oTMCPrLHjkpPesQfaoIvWZwF6QUuWlX4jdcEZMGXxNA9FjMGcEuQSqTrVJrXC9eAVFVwwnXjQA61v/iioCtOVD0t0u5LKa08NUWr430BDmuSyD9/vBzWtnk2fhet0kJSbQZiBiyRBaYnTLfXQe2ASPEC0iCuNtHocuq8DIPWM24q0Tg8+5tuil/qCC4f5D18cQSl2d5eGBj2vpUo33p8cN6z/cQrLp3vWLE2SQmfGN/lnNwAp03xwLSi16aYrVgoNNhkRsxkIMcJO6J+wS0Btoiw8UKmoueXUGDdECk1w9CnLW7HX2fxlB9GTDXBnU9HmK/9PCIi928YBLkZSRhEgIgY6zly0YM2ebOjLQGuNgApfwj2OxwPhx4lzXavNOj1A4oUeIURUYA4cTtEXipum8qvHFUghcQkrtzck3zqJxLQ5xHAaIxp/C+7qjDaL4+pEP9PAq7hljzckg/Gg12Gne1FzIv4vD//e2Pl5l4tWh0Z3P1P6xq/KjHVkQm6Z1BQ0n3HW9ZQxg62P4NJA9CiQpQeEFQk1Zpc37UTLdCtoSXO5fC8ApEMNUlG3mbOl6kqKC+YG8CORDsilWnbII5NqUK4szIa7/NvQOD/iSTJkJ+oGutBsAFN4bfKotGsCEh93Q5o2FORd30fJYtTWicAGejtmnDUtvufy5H9SuWso92boUxCMAGMD3roWrV0KuTSfwAc1cGWO1PBnYw3slAkO/qTW87OTJVG8DKwFSv42YqILhQgb2xfyEvsap+N4qk33dW/Gl+iD2BZ7if3XLGYeZuW1FqwXc2fJfIgs9q471629vmbs+VV4mOnS7GztxHBeAzZ9o8Dn399A38aLhsnikBTfyNy+15+1wZUli3NMPX6OAspPG7mqI8+B5JM+2BH6WSMlfbKGhK25e6QzuB3KF3iqP/+jlITQUm2o4lXw49O0QiNWjUmhzoVDR4Ig59DNvkJl1E2dMro8jaV2rHl4FgNF9Q0zJC9ayjdl6QFeKoSgYU9Vx17cECeRIKJ29CnDhj03eDSwkjxa/1b13rWZzWDzxOXpte0l4/3qtgrmcRnWlAyDtVqoA3ZuJVr+NAh/fDrsyeHAL7BNin8FEKD9DJZd2nCpp61Qlw0jzx9t5I9OwQCd50/n6Jrly7/gk4Lmy1oyw8zAkUKno1LMbwBmT1dNHYaSeHGPEjHT3IM55i7xVZT6hNRbcUCaKs9wU05kI1A00jfXOSHmm08YiQ4M77bqvmBkjzHzbifhw1rVX8VkpWIvSOJKeho+x4AM7Q6dY4ftgAAUFd6ruP3VVAVy+6SIYefLi9qwwYYkQ=="
            ).collect {
                createAppResponse.value = it
            }
        }
    }

    fun sendEmail(email: String) {
        viewModelScope.launch {
            verificationRepository.sendEmail(email).collect {
                emailVerificationResponse.value = it
            }
        }
    }

    fun sendPhone(number: String, type: String, countryCode: String) {
        viewModelScope.launch {
            verificationRepository.sendPhone(number, type, countryCode).collect {
                phoneVerificationResponse.value = it
            }
        }
    }

    fun getBlackBox(appContext: Context, iovationSubscriberKey: String) {
        viewModelScope.launch {
            userRepository.getBlackBox(appContext, iovationSubscriberKey).collect {
                blackBoxResponse.value = it
            }
        }
    }

    fun getOffers(firstName: String, lastName: String) {
        viewModelScope.launch {
            verificationRepository.getOffers(firstName, lastName).collect {
                offersResponse.value = it
            }
        }
    }
    fun passiveLivenessVerification(imageUrl: String) {
        viewModelScope.launch {
            verificationRepository.passiveLivenessVerification(imageUrl).collect {
                passiveLivenessResponse.value = it
            }
        }
    }

    fun faceAuthenticationLoginsVerification(imageUrl: String, customerId: String, clientId: String, emailAddress: String) {
        viewModelScope.launch {

            verificationRepository.faceAuthenticationLogin(imageUrl, clientId, customerId, emailAddress).collect {
                faceAuthenticationLoginResponse.value = it
            }
        }
    }
}
package com.orugga.transunion.ui.steps.askForId

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.orugga.transunion.repository.UserRepository
import com.orugga.transunion.repository.VerificationRepository
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.TokenResponse
import com.orugga.transunionsdk.vo.createApp.CreateAppResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class AskForIDViewModel
@Inject constructor(
    private val userRepository: UserRepository,
    private val verificationRepository: VerificationRepository
): ViewModel(){

    val tokenResponse = MutableLiveData<Resource<TokenResponse>>()
    val createAppResponse = MutableLiveData<Resource<CreateAppResponse>>()
    val credoLabResponse = MutableLiveData<Resource<String>>()

    init {

    }

    fun getAuth() {
        viewModelScope.launch {
            userRepository.getAuthToken().collect {
                tokenResponse.value = it
            }
        }
    }

    fun createApp() {
        viewModelScope.launch {
            userRepository.createApp("santiagolky@gmail.com", "57", "3178343999", "0610IDXLAP3RgeO6+aHpGwrJS3BYmpfzhlZ4Xrr3mxaaF4CFsF0V6q4g1wlhwwmFPnn0d2/wOllLiuUNwobTVE7FietDKnTCjwwHkx8BLEyt88d9GIlnWUuHKQIlNqD3J4AFnzhqGiz54LroEW8akNQY7vID0zGGbwW+PgRYUjoXHkJ+PAD/vc36vNB3GEGzt34hmoTR8K0iB48NVwwZEQvQah/ptz+M3HyjxAVo922mlE87iig1MMa5e2MF1ACFvxdG1BVPJbnBJsW17Y+zbPJ1BS5YnsR6eZiVJlCZbiEq3no2uxm1Anw4xa9iKg2Mm2QMhKVwpiDyrvYjNb1g6se3k25n4OeHuMfJpoFwTlSuaek6jzj/oTMCPrLHjkpPesQfaoIvWZwF6QUuWlX4jdcEZMGXxNA9FjMGcEuQSqTrVJrXC9eAVFVwwnXjQA61v/iioCtOVD0t0u5LKa08NUWr430BDmuSyD9/vBzWtnk2fhet0kJSbQZiBiyRBaYnTLfXQe2ASPEC0iCuNtHocuq8DIPWM24q0Tg8+5tuil/qCC4f5D18cQSl2d5eGBj2vpUo33p8cN6z/cQrLp3vWLE2SQmfGN/lnNwAp03xwLSi16aYrVgoNNhkRsxkIMcJO6J+wS0Btoiw8UKmoueXUGDdECk1w9CnLW7HX2fxlB9GTDXBnU9HmK/9PCIi928YBLkZSRhEgIgY6zly0YM2ebOjLQGuNgApfwj2OxwPhx4lzXavNOj1A4oUeIURUYA4cTtEXipum8qvHFUghcQkrtzck3zqJxLQ5xHAaIxp/C+7qjDaL4+pEP9PAq7hljzckg/Gg12Gne1FzIv4vD//e2Pl5l4tWh0Z3P1P6xq/KjHVkQm6Z1BQ0n3HW9ZQxg62P4NJA9CiQpQeEFQk1Zpc37UTLdCtoSXO5fC8ApEMNUlG3mbOl6kqKC+YG8CORDsilWnbII5NqUK4szIa7/NvQOD/iSTJkJ+oGutBsAFN4bfKotGsCEh93Q5o2FORd30fJYtTWicAGejtmnDUtvufy5H9SuWso92boUxCMAGMD3roWrV0KuTSfwAc1cGWO1PBnYw3slAkO/qTW87OTJVG8DKwFSv42YqILhQgb2xfyEvsap+N4qk33dW/Gl+iD2BZ7if3XLGYeZuW1FqwXc2fJfIgs9q471629vmbs+VV4mOnS7GztxHBeAzZ9o8Dn399A38aLhsnikBTfyNy+15+1wZUli3NMPX6OAspPG7mqI8+B5JM+2BH6WSMlfbKGhK25e6QzuB3KF3iqP/+jlITQUm2o4lXw49O0QiNWjUmhzoVDR4Ig59DNvkJl1E2dMro8jaV2rHl4FgNF9Q0zJC9ayjdl6QFeKoSgYU9Vx17cECeRIKJ29CnDhj03eDSwkjxa/1b13rWZzWDzxOXpte0l4/3qtgrmcRnWlAyDtVqoA3ZuJVr+NAh/fDrsyeHAL7BNin8FEKD9DJZd2nCpp61Qlw0jzx9t5I9OwQCd50/n6Jrly7/gk4Lmy1oyw8zAkUKno1LMbwBmT1dNHYaSeHGPEjHT3IM55i7xVZT6hNRbcUCaKs9wU05kI1A00jfXOSHmm08YiQ4M77bqvmBkjzHzbifhw1rVX8VkpWIvSOJKeho+x4AM7Q6dY4ftgAAUFd6ruP3VVAVy+6SIYefLi9qwwYYkQ=="
            ).collect {
                createAppResponse.value = it
            }
        }
    }

    fun collectCredoLabData(appContext: Context, referenceNumber: String) {
        viewModelScope.launch {
            verificationRepository.collectCredoLabData(appContext, referenceNumber).collect {
                Log.d("CREDOLAB", "Collect Success")
                Log.d("CREDOLAB", referenceNumber)
                Log.d("CREDOLAB", it.toString())
//                credoLabResponse.value = it
            }
        }
    }
}
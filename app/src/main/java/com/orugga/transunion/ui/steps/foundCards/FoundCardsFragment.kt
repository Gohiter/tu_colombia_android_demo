package com.orugga.transunion.ui.steps.foundCards

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentFoundCardsBinding
import com.orugga.transunion.ui.util.BaseFragment

class FoundCardsFragment : BaseFragment() {

    private lateinit var binding: FragmentFoundCardsBinding
    private lateinit var foundCardsViewModel: FoundCardsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_found_cards, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        foundCardsViewModel = ViewModelProvider(this, viewModelFactory).get(FoundCardsViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnSubmit.setOnClickListener { goToTransferBalances() }
        binding.card1 = foundCardsViewModel.card1
        binding.card2 = foundCardsViewModel.card2
        binding.card3 = foundCardsViewModel.card3
        binding.card4 = foundCardsViewModel.card4
        binding.cardItem1.cardV.setOnClickListener { selectCard(foundCardsViewModel.card1) }
        binding.cardItem2.cardV.setOnClickListener { selectCard(foundCardsViewModel.card2) }
        binding.cardItem3.cardV.setOnClickListener { selectCard(foundCardsViewModel.card3) }
        binding.cardItem4.cardV.setOnClickListener { selectCard(foundCardsViewModel.card4) }
    }

    private fun goToTransferBalances() {
    }

    private fun selectCard(cardItem: MutableLiveData<Boolean>) {
        foundCardsViewModel.card1.value = cardItem == foundCardsViewModel.card1
        foundCardsViewModel.card2.value = cardItem == foundCardsViewModel.card2
        foundCardsViewModel.card3.value = cardItem == foundCardsViewModel.card3
        foundCardsViewModel.card4.value = cardItem == foundCardsViewModel.card4
    }
}
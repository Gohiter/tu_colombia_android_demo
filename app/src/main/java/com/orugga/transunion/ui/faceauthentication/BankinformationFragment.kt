package com.orugga.transunion.ui.faceauthentication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentBankInformationBinding
import com.orugga.transunion.databinding.FragmentStep43VerifyAlldataBinding
import com.orugga.transunion.ui.adapter.SpinnerAdapter
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import javax.inject.Inject

class BankinformationFragment : BaseFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentBankInformationBinding
//    private lateinit var viewModel: VerifyAllData3ViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_bank_information, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        viewModel = ViewModelProvider(this, viewModelFactory).get(VerifyAllData3ViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
//        populateCountriesSpinner()
        binding.btnContinue.setOnClickListener { goToStep5() }
//        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
    }



    private fun goToStep5() {
//        arguments?.let {
//            findNavController().navigate(BankinformationFragmentDirections.actionBankInformationFragmentToIntroFragment()
//
//            )
//        }
    }
}
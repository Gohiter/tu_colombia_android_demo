package com.orugga.transunion.ui.steps.step6.step6_1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep61ProofAddressResultsBinding
import com.orugga.transunion.ui.util.PhotoFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import javax.inject.Inject

class ProofOfAddressResultFragment : PhotoFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentStep61ProofAddressResultsBinding
    private lateinit var viewModel: ProofOfAddressViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_step_6_1_proof_address_results, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ProofOfAddressViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.file.value = ProofOfAddressResultFragmentArgs.fromBundle(requireArguments()).file
        binding.file = viewModel.file
        binding.btnContinue.setOnClickListener { goToProofIncome() }
        binding.btnChangePicAddress.setOnClickListener { dispatchTakePictureIntent(validation = false) }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        onSuccessTakenPicture = {file -> viewModel.file.value = file }
    }

    private fun goToProofIncome() {
        findNavController().navigate(ProofOfAddressResultFragmentDirections
            .actionProofOfAddressResultToProofIncomeFragment(
                ProofOfAddressResultFragmentArgs.fromBundle(requireArguments()).data))
    }
}
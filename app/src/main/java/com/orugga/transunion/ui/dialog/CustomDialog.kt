package com.orugga.transunion.ui.dialog

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.orugga.transunion.R
import com.orugga.transunion.databinding.DialogCustomBinding


class CustomDialog(
    private val title: String?,
    private val text: String,
    private val buttonLabel: String,
    private val onConfirm: (() -> Unit),
    private val onDismiss: (() -> Unit)
) : DialogFragment() {

    private lateinit var binding: DialogCustomBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_custom, null, false)
        binding.txtDialogTitle.text = title
        binding.txtDialog.text = text
        binding.btnDialog.text = buttonLabel
        binding.btnDialog.setOnClickListener {
            dialog?.dismiss()
            onConfirm.invoke()
        }
        val dialogBuilder = context.let { AlertDialog.Builder(it) }
        dialogBuilder.setCancelable(true)
        dialogBuilder.setView(binding.root)
        val dialog = dialogBuilder.create()
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onDismiss.invoke()
    }
}
package com.orugga.transunion.ui.steps.step5.step5_1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep51InfoEmployIncomeStatusEstimateBinding
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import javax.inject.Inject

class EmployIncomeEstimateFragment : BaseFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentStep51InfoEmployIncomeStatusEstimateBinding
    private lateinit var viewModel: EmployIncomeEstimateViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_step_5_1_info_employ_income_status_estimate, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(EmployIncomeEstimateViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.selectedEmployeeStatus.value = null
        binding.selectedCardView = viewModel.selectedEmployeeStatus
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        setListenersToCards()
    }

    private fun setListenersToCards() {
        binding.cardEmployee.setOnClickListener { viewModel.selectedEmployeeStatus.value = binding.cardEmployee }
        binding.cardSelfEmployed.setOnClickListener { viewModel.selectedEmployeeStatus.value = binding.cardSelfEmployed }
        binding.cardNotWorking.setOnClickListener { viewModel.selectedEmployeeStatus.value = binding.cardNotWorking }
        binding.cardRetired.setOnClickListener { viewModel.selectedEmployeeStatus.value = binding.cardRetired }
        binding.cardStudent.setOnClickListener { viewModel.selectedEmployeeStatus.value = binding.cardStudent }
        binding.btnContinue.setOnClickListener { goToStep52() }
    }

    private fun goToStep52() {
        arguments?.let {
            findNavController().navigate(EmployIncomeEstimateFragmentDirections
                .actionEmployIncomeEstimateFragmentToEmploymentIncomeFragment(
                    EmployIncomeEstimateFragmentArgs.fromBundle(it).data
            ))

        }
    }
}
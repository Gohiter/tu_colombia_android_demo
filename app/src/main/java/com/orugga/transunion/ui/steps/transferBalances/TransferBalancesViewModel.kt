package com.orugga.transunion.ui.steps.transferBalances

import androidx.lifecycle.ViewModel
import com.orugga.transunion.repository.UserRepository
import javax.inject.Inject

class TransferBalancesViewModel
@Inject constructor(
    private val userRepository: UserRepository
): ViewModel(){

    init {

    }

    fun getApplicationId(): String {
        return userRepository.getAppId()
    }

}
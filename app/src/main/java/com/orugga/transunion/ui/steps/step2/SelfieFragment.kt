package com.orugga.transunion.ui.steps.step2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep2SelfieBinding
import com.orugga.transunion.ui.util.PhotoFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunionsdk.helper.onfido.OnfidoHelper
import java.io.File
import javax.inject.Inject

class SelfieFragment : PhotoFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentStep2SelfieBinding
    private lateinit var viewModel: SelfieSelectionViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_step_2_selfie, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(SelfieSelectionViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnTakePic.setOnClickListener { dispatchSelfiePictureVeridas() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        onSuccessTakenPicture = { file -> goToTakePhotoIdResult(file)}
        sharedPreferencesHelper.setVideoLiveness(false)
    }

//    private fun startVideoLiveness() {
//    sharedPreferencesHelper.setVideoLiveness(true)
//        viewModel.invokeSelfieService()
////     goToTakePhotoIdResult(null)
////        OnfidoHelper.runOnfidoVideoLiveness(requireContext(), this,"1234")
//    }

    private fun goToTakePhotoIdResult(file: File?) {
        arguments?.let {
            findNavController().navigate(
                SelfieFragmentDirections
                    .actionSelfieFragmentToSelfieResultFragment(
                        file
                    )
            )
        }
    }
}
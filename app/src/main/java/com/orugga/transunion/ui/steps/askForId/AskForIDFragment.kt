package com.orugga.transunion.ui.steps.askForId

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentAskIdBinding
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.ui.util.PermissionsHelper
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunionsdk.TransunionSdk
import com.orugga.transunionsdk.vo.Status
import javax.inject.Inject
import kotlin.random.Random.Default.nextInt

class AskForIDFragment : BaseFragment() {

    companion object {
        private const val TAG = "AskForIDFragment"
    }

    @Inject
    lateinit var transunionSdk: TransunionSdk

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentAskIdBinding
    private lateinit var askForIDViewModel: AskForIDViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding =
            DataBindingUtil.inflate(layoutInflater, R.layout.fragment_ask_id, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        askForIDViewModel =
            ViewModelProvider(this, viewModelFactory).get(AskForIDViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnHaveIdYes.setOnClickListener { goToStep1() }
        binding.btnHaveIdNo.setOnClickListener { goToCheckResults() }
        askForIDViewModel.getAuth()
        sharedPreferencesHelper.removeDocumentResponse()
        sharedPreferencesHelper.removeSelfieResponse()
        sharedPreferencesHelper.removeFrontDocumentUri()
        sharedPreferencesHelper.removeBackDocumentUri()
        sharedPreferencesHelper.removePhone()
        sharedPreferencesHelper.removeEmail()
        sharedPreferencesHelper.removeAddress()
        sharedPreferencesHelper.removeLocation()
        sharedPreferencesHelper.removeExamResponse()
        observeToken()
        observeCredoLabResponse()
        sharedPreferencesHelper.saveCredoLabReference("Android-id-" + nextInt().toString())
    }

    private fun observeToken() {
        askForIDViewModel.tokenResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    Log.i(TAG, "Success token ${result.data?.access_token}")
                }
                Status.ERROR -> {
                    Log.e(TAG, result.message)
                }
            }
        })
    }

    private fun observeCredoLabResponse() {
        askForIDViewModel.credoLabResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    Log.i(TAG, "Success CredoLab ${result.data}")
                }
                Status.ERROR -> {
                    Log.e(TAG, result.message)
                }
            }
        })
    }

    private fun goToStep1() {
        sharedPreferencesHelper.saveHasId(true)
        findNavController().navigate(AskForIDFragmentDirections.actionAskIdFragmentToWelcomeFragment())
    }

    private fun goToCheckResults() {
        findNavController().navigate(AskForIDFragmentDirections.actionAskIdFragmentToCheckResultFragment())
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if (PermissionsHelper.checkPermission(
                requireActivity(),
                this,
                PermissionsHelper.CREDOLAB_PERMISSIONS,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.READ_CALENDAR,
                Manifest.permission.GET_ACCOUNTS
            )
        ) {
            context?.let {
                askForIDViewModel.collectCredoLabData(
                    it,
                    sharedPreferencesHelper.getCredoLabReference()
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionsHelper.CREDOLAB_PERMISSIONS
            && grantResults.isNotEmpty()
        ) {
            context?.let {
                askForIDViewModel.collectCredoLabData(
                    it,
                    sharedPreferencesHelper.getCredoLabReference()
                )
            }
        }
    }

}
package com.orugga.transunion.ui.steps.step6.step6_1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep61ProofAddressBinding
import com.orugga.transunion.ui.steps.step4.step4_2.VerifyAllData2FragmentArgs
import com.orugga.transunion.ui.util.PhotoFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import java.io.File
import javax.inject.Inject

class ProofOfAddressFragment : PhotoFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentStep61ProofAddressBinding
    private lateinit var viewModel: ProofOfAddressViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_step_6_1_proof_address, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(ProofOfAddressViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnTakePic.setOnClickListener { dispatchTakePictureIntent(validation = false) }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        onSuccessTakenPicture = { file -> goToProofOfAddressResult(file)}
    }

    private fun goToProofOfAddressResult(file: File) {
        arguments?.let {
            findNavController().navigate(
                ProofOfAddressFragmentDirections
                    .actionProofOfAddressFragmentToProofOfAddressResultFragment(
                        VerifyAllData2FragmentArgs.fromBundle(it).data, file
                    )
            )
        }
    }
}
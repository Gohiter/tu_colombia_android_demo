package com.orugga.transunion.ui.steps.instantCard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.MainActivity
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentInstantCardBinding
import com.orugga.transunion.ui.util.BaseFragment

class InstantCardFragment : BaseFragment() {

    private lateinit var binding: FragmentInstantCardBinding
    private lateinit var instantCardViewModel: InstantCardViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_instant_card, container, false)
        binding.btnInfo.setOnClickListener { openInfoDialog() }
        binding.btnYes.setOnClickListener { openTransferDialog() }
        binding.btnNo.setOnClickListener { openSuccessDoneDialog() }
        binding.infoDialog.btnExit.setOnClickListener { closeInfoDialog() }
        binding.infoDialog.outsideArea.setOnClickListener { closeInfoDialog() }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        instantCardViewModel = ViewModelProvider(this, viewModelFactory).get(InstantCardViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.showDialog = instantCardViewModel.showDialog
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        setupTransferDialog()
        setupSuccessDoneDialog()
    }

    private fun openInfoDialog() {
        instantCardViewModel.showDialog.value = true
    }

    private fun closeInfoDialog() {
        instantCardViewModel.showDialog.value = false
    }

    private fun openTransferDialog() {
        binding.alertDialog.visibility = View.VISIBLE
    }

    private fun closeTransferDialog() {
        binding.alertDialog.visibility = View.GONE
    }

    private fun openSuccessDoneDialog() {
        binding.doneDialog.visibility = View.VISIBLE
    }

    private fun setupTransferDialog() {
        binding.alertDialog.setPrimaryButtonAction(View.OnClickListener { goToFoundCards() })
    }

    private fun setupSuccessDoneDialog() {
        binding.doneDialog.setAppId("#${instantCardViewModel.getApplicationId()}")
        binding.doneDialog.setPrimaryButtonAction(View.OnClickListener { goToAskForId() })
    }

    private fun goToFoundCards() {
    }

    private fun goToAskForId() {
        (activity as MainActivity).showErrorAndReset(resources.getString(R.string.done))
    }
}
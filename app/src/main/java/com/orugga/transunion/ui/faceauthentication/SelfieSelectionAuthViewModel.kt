package com.orugga.transunion.ui.faceauthentication

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.orugga.transunion.repository.DocumentsRepository
import com.orugga.transunion.repository.UserRepository
import com.orugga.transunion.repository.VerificationRepository
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.facialSimilarityInvokeService.FacialSimilarityInvokeServiceResponse
import com.orugga.transunionsdk.vo.facialSimilarityReport.FacialSimilarityReportResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class SelfieSelectionAuthViewModel @Inject constructor(private val verificationRepository: VerificationRepository,
                                                       private val documentsRepository: DocumentsRepository,
                                                       private val userRepository: UserRepository
): ViewModel() {
    val invokeServiceResponse = MutableLiveData<Resource<FacialSimilarityInvokeServiceResponse>>()
    init {

    }

//    fun invokeSelfieService() {
//        viewModelScope.launch {
//            documentsRepository.invokeFacialSimilarityService("", "", "", "").collect {
//                invokeServiceResponse.value = it
//            }
//        }
//    }
}
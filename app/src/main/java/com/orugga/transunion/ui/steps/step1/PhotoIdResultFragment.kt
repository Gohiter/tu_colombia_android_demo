package com.orugga.transunion.ui.steps.step1

//import okhttp3.MediaType.Companion.toMediaTypeOrNull
import android.content.res.AssetManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.BuildConfig
import com.orugga.transunion.MainActivity
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep1ScanIdResultBinding
import com.orugga.transunion.ui.util.PhotoFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunionsdk.vo.Status
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.io.File
import java.io.InputStream
import javax.inject.Inject


class PhotoIdResultFragment : PhotoFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentStep1ScanIdResultBinding
    private lateinit var viewModel: PhotoIdViewModel
    private var newValue = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_step_1_scan_id_result, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(PhotoIdViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.fileFront.value = File(sharedPreferencesHelper.getFrontDocumentUri())
        viewModel.fileBack.value = File(sharedPreferencesHelper.getBackDocumentUri())
        binding.fileFront = viewModel.fileFront
        binding.fileBack = viewModel.fileBack
        binding.btnChangePic.setOnClickListener { dispatchDocPictureVeridas() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        binding.btnContinue.setOnClickListener { uploadDocument() }
        binding.showError = viewModel.showError
        binding.createAppResponse = viewModel.createAppResponse
        binding.createFrontDocumentResponse = viewModel.createFrontDocumentResponse
        binding.createBackDocumentResponse = viewModel.createBackDocumentResponse
        binding.uploadFrontDocumentResponse = viewModel.uploadFrontDocumentResponse
        binding.uploadBackDocumentResponse = viewModel.uploadBackDocumentResponse
        binding.processDocumentResponse = viewModel.processDocumentResponse
        binding.blackBoxResponse = viewModel.blackBoxResponse
        binding.errorDialog.setPrimaryButtonAction { returnToAskId() }
        onSuccessTakenFrontBack = { fileFront, fileBack ->
            viewModel.fileFront.value = fileFront
            viewModel.fileBack.value = fileBack
        }
        observeCreateAppResponse()
        observeCreateFrontDocumentResponse()
        observeCreateBackDocumentResponse()
        observeUploadFrontDocumentResponse()
        observeUploadBackDocumentResponse()
        observeProcessDocumentResponse()
        observeBlackBox()
    }

    private fun uploadDocument() {
        newValue = true
        context?.let {
            viewModel.getBlackBox(it, BuildConfig.IOVATION_KEY)
        }
    }

    private fun returnToAskId() {
        viewModel.showError(false)
        findNavController().navigate(PhotoIdResultFragmentDirections.actionPhotoIdResultFragmentToAskIdFragment())
    }

    private fun observeBlackBox() {
        viewModel.blackBoxResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    val email = sharedPreferencesHelper.getEmail()
                    val phone = sharedPreferencesHelper.getPhone()
                    val blackbox = result.data!!
                    viewModel.createApp(
                        email,
                        phone!!.countryCode,
                        phone.nationalNumber,
                        blackbox
                    )
                }
                Status.ERROR -> {
                    Log.d("Ale", "BB")
                    Toast.makeText(context, "BB Error: ${result.message})", Toast.LENGTH_LONG)
                        .show()
                    (activity as MainActivity).showErrorAndReset(result.message.toString())
                }
            }
        })
    }

    private fun observeCreateAppResponse() {
        viewModel.createAppResponse.observe(viewLifecycleOwner, Observer { result ->
            if (newValue) {
                when (result.status) {
                    Status.SUCCESS -> {
                        result.data?.responseInfo?.applicationId?.let {
                            binding.errorDialog.setAppId(
                                "#$it"
                            )
                        }
                        viewModel.createFrontDocument()
                    }
                    Status.ERROR -> {
                        viewModel.showError(true)
                        Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun observeCreateFrontDocumentResponse() {
        viewModel.createFrontDocumentResponse.observe(viewLifecycleOwner, Observer {result ->
            if (newValue) {
                when (result.status) {
                    Status.SUCCESS -> {
                        "image/jpg".toMediaTypeOrNull()?.let { mediaType ->
//                            val am: AssetManager = requireActivity().assets
//                            val inputStream: InputStream = am.open("Front Juan.JPG")
//                            val file: File = File.createTempFile("front", "jpg")
//
//                            inputStream.use { input ->
//                                file.outputStream().use { output ->
//                                    input.copyTo(output)
//                                }
//                            }
                            viewModel.uploadFrontDocument(
                                viewModel.fileFront.value!!,
//                                file,
                                mediaType
                            )
                        }
                    }
                    Status.ERROR -> {
                        viewModel.showError(true)
                        Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }

        })
    }

    private fun observeCreateBackDocumentResponse() {
        viewModel.createBackDocumentResponse.observe(viewLifecycleOwner, Observer {result ->
            if (newValue) {
                when (result.status) {
                    Status.SUCCESS -> {
                        "image/jpg".toMediaTypeOrNull()?.let { mediaType ->
//                            val am: AssetManager = requireActivity().assets
//                            val inputStream: InputStream = am.open("Back Juan.JPG")
//                            val file: File = File.createTempFile("front", "jpg")
//
//                            inputStream.use { input ->
//                                file.outputStream().use { output ->
//                                    input.copyTo(output)
//                                }
//                            }
                            viewModel.uploadBackDocument(
                                viewModel.fileBack.value!!,
//                                file,
                                mediaType
                            )
                        }
                    }
                    Status.ERROR -> {
                        viewModel.showError(true)
                        Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }

        })
    }

    private fun observeUploadFrontDocumentResponse() {
        viewModel.uploadFrontDocumentResponse.observe(viewLifecycleOwner, Observer {result ->
            if (newValue) {
                when (result.status) {
                    Status.SUCCESS -> {
                        viewModel.createBackDocument()
                    }
                    Status.ERROR -> {
                        viewModel.showError(true)
                        Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun observeUploadBackDocumentResponse() {
        viewModel.uploadBackDocumentResponse.observe(viewLifecycleOwner, Observer {result ->
            if (newValue) {
                when (result.status) {
                    Status.SUCCESS -> {
                        viewModel.processDocument()
                    }
                    Status.ERROR -> {
                        viewModel.showError(true)
                        Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun observeProcessDocumentResponse() {
        viewModel.processDocumentResponse.observe(viewLifecycleOwner, Observer {result ->
            if (newValue) {
                when (result.status) {
                    Status.SUCCESS -> {
                        val documentResponse = result.data
                        if(documentResponse != null && documentResponse.status != "awaiting_approval"){
                            sharedPreferencesHelper.saveDocumentResponse(documentResponse)
                        }
                        goToStep2()
                    }
                    Status.ERROR -> {
                        viewModel.showError(true)
                        Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }

    private fun goToStep2() {
        newValue = false
        findNavController().navigate(PhotoIdResultFragmentDirections.actionPhotoIdResultFragmentToSelfieFragment())
    }
}

//        private fun observeDocAuthReportResponse() {
//        viewModel.docAuthReportResponse.observe(viewLifecycleOwner, Observer {result ->
//            if (newValue) {
//                when (result.status) {
//                    Status.SUCCESS -> {
//                        val documentResponse = result.data?.fieldsReport?.reportApplicantIO?.attributes?.get(0)?.reportAttributes?.dSDocAuthReportVerification
//                        if(documentResponse != null && documentResponse.status != "awaiting_approval"){
//                            sharedPreferencesHelper.saveDocumentResponse(documentResponse)
//                        }
//                        goToStep2()
//                    }
//                    Status.ERROR -> {
//                        viewModel.showError(true)
//                        Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
//                    }
//                }
//            }
//        })
//    }

//        private fun observeInvokeServiceResponse() {
//        viewModel.invokeServiceResponse.observe(viewLifecycleOwner, Observer {result ->
//            if (newValue) {
//                when (result.status) {
//                    Status.SUCCESS -> {
//                        viewModel.createDocument()
//                    }
//                    Status.ERROR -> {
//                        viewModel.showError(true)
//                        Toast.makeText(context, result.message, Toast.LENGTH_SHORT).show()
//                    }
//                }
//            }
//
//        })
//    }
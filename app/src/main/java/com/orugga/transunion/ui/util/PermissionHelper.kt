package com.orugga.transunion.ui.util

import android.app.Activity
import android.content.pm.PackageManager
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

class PermissionsHelper {
    companion object {

        const val CAMERA_PERMISSION = 42
        const val LOCATION_PERMISSION = 43
        const val CREDOLAB_PERMISSIONS = 44

        fun checkPermission(activity : Activity, fragment:Fragment, permissionId : Int, vararg perm : String) : Boolean {
            val havePermissions = perm.toList().all {
                ContextCompat.checkSelfPermission(activity,it) == PackageManager.PERMISSION_GRANTED
            }
            if (!havePermissions) {
                if(perm.toList().any {
                            ActivityCompat.shouldShowRequestPermissionRationale(activity, it)}
                ) {
                    val dialog = AlertDialog.Builder(activity)
                            .setTitle("Permission")
                            .setMessage("Permission needed! Do you want to grant them?")
                            .setPositiveButton("Yes") { id, v ->
                                ActivityCompat.requestPermissions(
                                        activity, perm, permissionId)
                            }
                            .setNegativeButton("No") { id, v -> }
                            .create()
                    dialog.show()
                } else {
                    fragment.requestPermissions(perm, permissionId)
                }
                return false
            }
            return true
        }
    }
}
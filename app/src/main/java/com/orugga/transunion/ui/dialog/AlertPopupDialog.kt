package com.orugga.transunion.ui.dialog

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.orugga.transunion.R
import com.orugga.transunion.databinding.DialogAlertPopupBinding

class AlertPopupDialog(
    private val title: String?,
    private val text: String,
    private val onConfirm: (() -> Unit)
) : DialogFragment() {

    private lateinit var binding: DialogAlertPopupBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_alert_popup, null, false)
        binding.txtDialogTitle.text = title
        binding.txtDialog.text = text
        binding.btnOK.setOnClickListener {
            dialog?.dismiss()
            onConfirm.invoke()
        }
        val dialogBuilder = context.let { AlertDialog.Builder(it) }
        dialogBuilder.setCancelable(true)
        dialogBuilder.setView(binding.root)
        val dialog = dialogBuilder.create()
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }
}
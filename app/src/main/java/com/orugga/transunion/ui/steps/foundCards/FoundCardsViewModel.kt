package com.orugga.transunion.ui.steps.foundCards

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.material.card.MaterialCardView
import com.orugga.transunion.repository.UserRepository
import javax.inject.Inject

class FoundCardsViewModel
@Inject constructor(
    private val userRepository: UserRepository
): ViewModel(){

    val card1 = MutableLiveData<Boolean>(false)
    val card2 = MutableLiveData<Boolean>(false)
    val card3 = MutableLiveData<Boolean>(false)
    val card4 = MutableLiveData<Boolean>(false)

    init {

    }

}
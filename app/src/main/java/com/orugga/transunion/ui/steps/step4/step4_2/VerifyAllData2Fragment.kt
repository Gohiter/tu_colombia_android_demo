package com.orugga.transunion.ui.steps.step4.step4_2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep42VerifyAlldataBinding
import com.orugga.transunion.ui.steps.step4.step4_1.VerifyAllData1FragmentDirections
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunion.vo.*
import com.orugga.transunionsdk.vo.Phone
import com.orugga.transunionsdk.vo.docAuthenticationReport.AddressLines
import com.orugga.transunionsdk.vo.examRequest.ExamRequestResponse
import javax.inject.Inject

class VerifyAllData2Fragment : BaseFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentStep42VerifyAlldataBinding
    private lateinit var viewModel: VerifyAllData2ViewModel
    private lateinit var examResponse: ExamRequestResponse
    private lateinit var data: AllData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_step_4_2_verify_alldata,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(VerifyAllData2ViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnContinue.setOnClickListener { goToStep5() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        arguments?.let {
            data =
                VerifyAllData2FragmentArgs.fromBundle(it).data!!
        }
        examResponse = sharedPreferencesHelper.getExamResponse()
        getProperties(examResponse)
    }

    private fun getProperties(properties: ExamRequestResponse) {
        binding.etContactEmail.setText(sharedPreferencesHelper.getEmail())
        binding.etContactCellphoneNumber.setText(sharedPreferencesHelper.getPhone()?.getPhoneNumber())
        binding.etContactPhoneNumber.setText(properties.fields.applicants.applicant[0].idData.examResponsePhones[0].telephoneNumber)
        loop@ for (address in properties.fields.applicants.applicant[0].idData.examResponseAddresses) {
            if (address.addressType == "RES" || address.addressType == "RES-LAB") {
                binding.etContactAdress.setText(address.addressLocation)
                binding.etContactCity.setText(address.addressCity)
                break@loop
            }
        }
    }

    private fun goToStep5() {
        findNavController().navigate(
            VerifyAllData2FragmentDirections.actionVerifyAllData2FragmentToSignatureFragment(
                data
            )
        )
    }
//        sharedPreferencesHelper.saveEmail(binding.etContactEmail.text.toString())
//        val etPhone = binding.etContactPhoneNumber.text.toString().trim()
//        val savedPhone = sharedPreferencesHelper.getPhone()
//        val phone = if(savedPhone != null && etPhone == savedPhone.getPhoneNumber().trim()){
//            savedPhone
//        } else {
//            Phone("91", etPhone)
//        }
//        sharedPreferencesHelper.savePhone(phone)
//        val direction = if(sharedPreferencesHelper.isHasId()){
//            VerifyAllData2FragmentDirections.actionVerifyAllData2FragmentToEmployIncomeEstimateFragment(
//                data
//            )
//        } else {
//            VerifyAllData2FragmentDirections.actionVerifyAllData2FragmentToVerifyAllData3Fragment(
//                data
//            )
//        }
//        findNavController().navigate(direction)

    companion object {
        private const val TAG = "VerifyAllData2Fragment"
    }
}

//    private fun setAddress(addressLines: AddressLines?) {
//        if(addressLines != null) {
//            binding.etContactAdress1.setText(addressLines.streetAddress)
//            val line2 = "${addressLines.city} ${addressLines.postalCode}"
//            binding.etContactAdress2.setText(line2)
//        }
//    }
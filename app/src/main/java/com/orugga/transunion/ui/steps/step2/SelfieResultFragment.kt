package com.orugga.transunion.ui.steps.step2

import android.Manifest
import android.annotation.SuppressLint
import android.content.res.AssetManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.*
import com.orugga.transunion.BuildConfig
import com.orugga.transunion.MainActivity
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep2SelfieResultBinding
import com.orugga.transunion.ui.faceauthentication.SelfieAuthResultFragment
import com.orugga.transunion.ui.util.PermissionsHelper
import com.orugga.transunion.ui.util.PhotoFragment
import com.orugga.transunion.util.AddressHelper
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunion.vo.*
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.Status
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import java.io.File
import java.io.InputStream
import java.util.*
import javax.inject.Inject

class SelfieResultFragment : PhotoFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentStep2SelfieResultBinding
    private lateinit var viewModel: SelfieViewModel
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var newValue = false
    private var lastLocation: Location? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_step_2_selfie_result,
            container,
            false
        )
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        return binding.root
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if (PermissionsHelper.checkPermission(
                requireActivity(),
                this,
                PermissionsHelper.LOCATION_PERMISSION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            if (AddressHelper.isLocationEnabled(requireActivity())) {
                LocationServices.getFusedLocationProviderClient(requireContext())
                    .requestLocationUpdates(locationRequest, locationCallback, null);

            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(SelfieViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.file.value = SelfieResultFragmentArgs.fromBundle(requireArguments()).file
        binding.file = viewModel.file
        binding.btnChangePicSelfie.setOnClickListener { dispatchSelfiePictureVeridas() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        binding.btnContinue.setOnClickListener { sendSelfie() }
        binding.errorDialog.setPrimaryButtonAction(View.OnClickListener { returnToAskId() })
        onSuccessTakenPicture = { file -> viewModel.file.value = file }
        binding.errorDialog.setAppId("#${viewModel.getApplicationId()}")
        binding.showError = viewModel.showError
        binding.createDocumentResponse = viewModel.createSelfieResponse
        binding.uploadDocumentResponse = viewModel.uploadSelfieResponse
        binding.processDocumentResponse = viewModel.processSelfieResponse

        observeCreateSelfieResponse()
        observeUploadSelfieResponse()
        observeProcessSelfieResponse()
        initializeLocationVariables()
    }

    private fun sendSelfie() {
        newValue = true
        viewModel.createSelfieDocument()
    }

    private fun observeCreateSelfieResponse() {
        viewModel.createSelfieResponse.observe(viewLifecycleOwner, Observer { result ->
            if (newValue) {
                when (result.status) {
                    Status.SUCCESS -> {
                        "image/jpg".toMediaTypeOrNull()?.let { mediaType ->
//                            val am: AssetManager = requireActivity().assets
//                            val inputStream: InputStream = am.open("Selfie1 Juan.JPG")
//                            val file: File = File.createTempFile("front", "jpg")
//
//                            inputStream.use { input ->
//                                file.outputStream().use { output ->
//                                    input.copyTo(output)
//                                }
//                            }
                            viewModel.uploadSelfie(
                                viewModel.file.value!!,
//                                file,
                                mediaType
                            )
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(context, "CS Error: ${result.message})", Toast.LENGTH_LONG)
                            .show()
                        viewModel.showError(true)
                    }
                }
            }

        })
    }

    private fun observeUploadSelfieResponse() {
        viewModel.uploadSelfieResponse.observe(viewLifecycleOwner, Observer { result ->
            if (newValue) {
                when (result.status) {
                    Status.SUCCESS -> {
                        viewModel.processSelfie()
                    }
                    Status.ERROR -> {
                        Toast.makeText(context, "US Error: ${result.message})", Toast.LENGTH_LONG)
                            .show()
                        viewModel.showError(true)
                    }
                }
            }
        })
    }

    private fun observeProcessSelfieResponse() {
        viewModel.processSelfieResponse.observe(viewLifecycleOwner, Observer { result ->
            if (newValue) {
                when (result.status) {
                    Status.SUCCESS -> {
                        val selfieResponse =
                            result.data
                        if (selfieResponse == null || selfieResponse.status == "awaiting_approval") {
                            Toast.makeText(
                                context,
                                "PS Error: ${result.message}",
                                Toast.LENGTH_LONG
                            )
                                .show()
                            viewModel.showError(true)
                        } else if (selfieResponse.responseInfo.currentQueue == "OTP_PhoneSelection") {
                            goToOTPStep()
                        } else if (selfieResponse.fields.examData != null) {
                            sharedPreferencesHelper.saveSelfieResponse(selfieResponse)
                            getLastKnownLocation()
                        } else {
                            Toast.makeText(
                                context,
                                "ED Error: ${result.message}",
                                Toast.LENGTH_LONG
                            )
                                .show()
                            viewModel.showError(true)
                        }
                    }
                    Status.ERROR -> {
                        Toast.makeText(context, "PS Error: ${result.message})", Toast.LENGTH_LONG)
                            .show()
                        viewModel.showError(true)
                    }
                }
            }
        })
    }

    @SuppressLint("MissingPermission")
    fun getLastKnownLocation() {
        if (PermissionsHelper.checkPermission(
                requireActivity(),
                this,
                PermissionsHelper.LOCATION_PERMISSION,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            if (AddressHelper.isLocationEnabled(requireActivity())) {
                LocationServices.getFusedLocationProviderClient(
                    requireActivity()
                ).lastLocation.addOnSuccessListener(
                    requireActivity()
                ) { location: Location? ->
                    var address = ""
                    if (location == null) {
                        Log.e("showCurrentLocation", "Location is null")
                        if (lastLocation != null) {
                            address = AddressHelper.getAddressFromLocation(
                                lastLocation!!.latitude,
                                lastLocation!!.longitude,
                                requireContext()
                            )
                        }
                    } else location.apply {
                        lastLocation = location
                        address = AddressHelper.getAddressFromLocation(
                            location.latitude,
                            location.longitude,
                            requireContext()
                        )
                    }
                    sharedPreferencesHelper.saveLocation(location)
                    sharedPreferencesHelper.saveAddress(address)
                    goToNextStep()
                }
            } else {
                sharedPreferencesHelper.saveAddress("You need to grant location permission.")
                goToNextStep()
            }
        }
    }
    private fun initializeLocationVariables() {
        locationRequest = LocationRequest.create()
        locationRequest.interval = 1000
        locationRequest.fastestInterval = 500
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){
                    lastLocation = location
                    Log.i("Location Update",location.toString())
                }
            }
        }
    }

    private fun goToNextStep() {
        if (newValue) {
            newValue = false
   }
        findNavController().navigate(SelfieResultFragmentDirections.actionSelfieResultFragmentToSingleQuestionFragment())
    }

    private fun goToOTPStep() {
        if (newValue) {
            newValue = false
        }
        findNavController().navigate(SelfieResultFragmentDirections.actionSelfieResultFragmentToOTPFragment())

    }

    private fun returnToAskId() {
        viewModel.showError(false)
        findNavController().navigate(SelfieResultFragmentDirections.actionSelfieResultFragmentToAskIdFragment())
    }
}
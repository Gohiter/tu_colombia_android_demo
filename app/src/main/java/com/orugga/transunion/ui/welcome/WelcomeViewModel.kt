package com.orugga.transunion.ui.welcome

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.orugga.transunion.repository.UserRepository
import com.orugga.transunion.repository.VerificationRepository
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.TokenResponse
import com.orugga.transunionsdk.vo.createApp.CreateAppResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class WelcomeViewModel
@Inject
constructor(
    private val userRepository: UserRepository,
    private val verificationRepository: VerificationRepository
) : ViewModel() {

    val tokenResponse = MutableLiveData<Resource<TokenResponse>>()
    val credoLabResponse = MutableLiveData<Resource<String>>()

    init {

    }

    fun getAuth() {
        viewModelScope.launch {
            userRepository.getAuthToken().collect {
                tokenResponse.value = it
            }
        }
    }

    fun collectCredoLabData(appContext: Context, referenceNumber: String) {
        viewModelScope.launch {
            verificationRepository.collectCredoLabData(appContext, referenceNumber).collect {
                Log.d("CREDOLAB", "Collect Success")
                Log.d("CREDOLAB", referenceNumber)
                Log.d("CREDOLAB", it.toString())
//                credoLabResponse.value = it
            }
        }
    }
}

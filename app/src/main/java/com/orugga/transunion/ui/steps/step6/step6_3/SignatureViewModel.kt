package com.orugga.transunion.ui.steps.step6.step6_3

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.orugga.transunion.repository.VerificationRepository
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.biocatch.BiocatchResponse
import com.orugga.transunionsdk.vo.credoLab.CredoLabTokenResponse
import com.orugga.transunionsdk.vo.credoLab.DataSetInsightsResponse
import com.orugga.transunionsdk.vo.completeTransaction.CompleteTransactionResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class SignatureViewModel
@Inject constructor(private val verificationRepository: VerificationRepository): ViewModel(){

    var inkFilled = MutableLiveData<Boolean>(false)
    val biocatchResponse = MutableLiveData<Resource<BiocatchResponse>>()
    val completeTransactionResponse = MutableLiveData<Resource<CompleteTransactionResponse>>()
    val credoLabTokenResponse = MutableLiveData<Resource<CredoLabTokenResponse>>()
    val dataSetInsightResponse = MutableLiveData<Resource<DataSetInsightsResponse>>()

    init {

    }

    fun getBiocatchScore() {
        viewModelScope.launch {
            verificationRepository.getBiocatchScore().collect {
                biocatchResponse.value = it
            }
        }
    }

    fun getCredoLabToken() {
        viewModelScope.launch {
            verificationRepository.getCredoLabToken().collect {
                credoLabTokenResponse.value = it
            }
        }
    }

    fun getCredoLabDataSetInsight(referenceNumber: String) {
        viewModelScope.launch {
            verificationRepository.getCredoLabDataSetInsight(referenceNumber).collect {
                dataSetInsightResponse.value = it
            }
        }
    }

    fun completeTransaction() {
        viewModelScope.launch {
            verificationRepository.completeTransaction().collect {
                completeTransactionResponse.value = it
            }
        }
    }

}
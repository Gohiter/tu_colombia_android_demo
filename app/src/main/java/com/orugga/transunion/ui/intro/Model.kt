package com.orugga.transunion.ui.intro

import com.orugga.transunion.R

enum class Model(val layoutResId: Int) {
    FIRST(R.layout.intro_item_1),
    SECOND(R.layout.intro_item_2),
    THIRD(R.layout.intro_item_3)
}
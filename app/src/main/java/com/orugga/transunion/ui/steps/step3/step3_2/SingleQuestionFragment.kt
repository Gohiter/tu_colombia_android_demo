package com.orugga.transunion.ui.steps.step3.step3_2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentSingleQuestionBinding
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunionsdk.vo.Status
import kotlinx.android.synthetic.main.fragment_single_question.*
import org.redundent.kotlin.xml.xml
import javax.inject.Inject

class SingleQuestionFragment : BaseFragment() {
    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentSingleQuestionBinding
    private lateinit var viewModel: QuestionsViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_single_question,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(QuestionsViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnContinue.setOnClickListener { onContinueClicked() }
        binding.btnFinish.setOnClickListener { onFinishClicked() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        binding.checkboxOne.setOnClickListener { checkBox -> onCheckboxChange(checkBox) }
        binding.checkboxTwo.setOnClickListener { checkBox -> onCheckboxChange(checkBox) }
        binding.checkboxThree.setOnClickListener { checkBox -> onCheckboxChange(checkBox) }
        binding.checkboxFour.setOnClickListener { checkBox -> onCheckboxChange(checkBox) }
        binding.question = viewModel.examQuestion
        binding.answerOne = viewModel.examAnswerOne
        binding.answerTwo = viewModel.examAnswerTwo
        binding.answerThree = viewModel.examAnswerThree
        binding.answerFour = viewModel.examAnswerFour
        binding.processExamResponse = viewModel.processExamResponse
        binding.showError = viewModel.showError
        binding.errorDialog.setPrimaryButtonAction(View.OnClickListener { returnToAskId() })
        binding.errorDialog.setAppId("#${viewModel.getApplicationId()}")
        btnFinish.visibility = View.GONE
        btnContinue.isEnabled = false
        btnFinish.isEnabled = false
        observeExamDetail()
        observeProcessExamResponse()
    }

    private fun observeExamDetail() {
        viewModel.examDetail.observe(viewLifecycleOwner, Observer {
            viewModel.questionIndex.value = 0
            setQuestion(viewModel.questionIndex.value!!)
        })
    }

    private fun setQuestion(index: Int) {
        binding.groupOne.visibility = View.GONE
        binding.groupTwo.visibility = View.GONE
        binding.groupThree.visibility = View.GONE
        binding.groupFour.visibility = View.GONE
        viewModel.examQuestion.value = viewModel.examDetail.value!!.questions[index].text
        val answerSize = viewModel.examDetail.value!!.questions[index].answers.size

        if (answerSize >= 1) {
            binding.groupOne.visibility = View.VISIBLE
            viewModel.examAnswerOne.value =
                viewModel.examDetail.value!!.questions[index].answers[0].text
        }

        if (answerSize >= 2) {
            binding.groupTwo.visibility = View.VISIBLE
            viewModel.examAnswerTwo.value =
                viewModel.examDetail.value!!.questions[index].answers[1].text
        }

        if (answerSize >= 3) {
            binding.groupThree.visibility = View.VISIBLE
            viewModel.examAnswerThree.value =
                viewModel.examDetail.value!!.questions[index].answers[2].text
        }

        if (answerSize >= 4) {
            binding.groupFour.visibility = View.VISIBLE
            viewModel.examAnswerFour.value =
                viewModel.examDetail.value!!.questions[index].answers[3].text
        }
    }

    private fun onCheckboxChange(checkBox: View?) {
        if (checkBox != checkboxOne) checkboxOne.isChecked = false
        if (checkBox != checkboxTwo) checkboxTwo.isChecked = false
        if (checkBox != checkboxThree) checkboxThree.isChecked = false
        if (checkBox != checkboxFour) checkboxFour.isChecked = false

        toggleContinueButton()
    }

    private fun toggleContinueButton() {
        btnContinue.isEnabled = checkboxOne.isChecked || checkboxTwo.isChecked ||
                checkboxThree.isChecked || checkboxFour.isChecked
        btnFinish.isEnabled = checkboxOne.isChecked || checkboxTwo.isChecked ||
                checkboxThree.isChecked || checkboxFour.isChecked
    }

    private fun onContinueClicked() {
        storeSelectedAnswer()
        viewModel.questionIndex.value = viewModel.questionIndex.value!! + 1
        val questionSize = viewModel.examDetail.value!!.questions.size
        if (viewModel.questionIndex.value!! == questionSize - 1) {
            hideContinue()
        }
        setQuestion(viewModel.questionIndex.value!!)
        onCheckboxChange(null)
    }

    private fun hideContinue() {
        btnContinue.visibility = View.GONE
        btnFinish.visibility = View.VISIBLE
    }

    private fun hideFinish() {
        btnFinish.visibility = View.GONE
        btnContinue.visibility = View.VISIBLE
    }

    private fun storeSelectedAnswer() {
        val answerIndex = if (checkboxOne.isChecked) {
            0
        } else if (checkboxTwo.isChecked) {
            1
        } else if (checkboxThree.isChecked) {
            2
        } else {
            3
        }
        val selectedAnswer =
            viewModel.examDetail.value!!.questions[viewModel.questionIndex.value!!].answers[answerIndex]
        selectedAnswer.isSelected = "true"
    }

    private fun onFinishClicked() {
        storeSelectedAnswer()
        sendXml()
    }

    private fun sendXml() {
        val examXml = xml("ExamDetails") {
            attribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
            attribute("xmlns:xsd", "http://www.w3.org/2001/XMLSchema")
            attribute("Title", viewModel.examDetail.value!!.title)
            attribute("Language", viewModel.examDetail.value!!.language)
            attribute("MatchKeyId", viewModel.examDetail.value!!.matchKeyId)
            attribute("ErrorGeneratingQA", viewModel.examDetail.value!!.errorGeneratingQA)
            attribute("TimeTakenToAnswer", viewModel.examDetail.value!!.timeTakenToAnswer)
            "Questions" {
                for (question in viewModel.examDetail.value!!.questions) {
                    "Question" {
                        attribute("Id", question.id)
                        attribute("QuestionId", question.questionId)
                        attribute("Text", question.text)
                        attribute("Type", question.type)
                        attribute("IsDummy", question.isDummy)
                        attribute(
                            "isQuestionTextHasListTypeDataElement",
                            question.isQuestionTextHasListTypeDataElement
                        )
                        attribute("ListAnswerIndex", question.listAnswerIndex)
                        attribute("ExpirationTime", question.expirationTime)
                        attribute("TimeTakenToAnswer", question.timeTakenToAnswer)
                        for (answer in question.answers) {
                            "Answer" {
                                attribute("IsSelected", answer.isSelected)
                                attribute("CorrectAnswer", answer.correctAnswer)
                                -answer.text
                            }
                        }
                    }
                }
            }
        }
        val xmlString = examXml.toString(false)
        replacer(xmlString)
    }

    private fun replacer(xmlStringOld: String) {
        val xmlStringNew = xmlStringOld.replace(
            "\"",
            "'"
        )
        viewModel.processExam(xmlStringNew)
    }

    private fun observeProcessExamResponse() {
        viewModel.processExamResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    val examResponse =
                        result.data
                    if (examResponse == null || examResponse.status == "awaiting_approval") {
                        Toast.makeText(
                            context,
                            "Exam Response Null Error: ${result.message}",
                            Toast.LENGTH_LONG
                        )
                            .show()
                        viewModel.showError(true)
                    } else if (examResponse.responseInfo.currentQueue == "IDA_ShowExam") {
                        hideFinish()
                        onCheckboxChange(null)
                        viewModel.parseXml(examResponse.fields.examData)
                    } else if (examResponse.fields.applicants.applicant[0].idData.examResponseResponse == null) {
                        viewModel.showError(true)
                    } else {
                        result.data?.let {
                            sharedPreferencesHelper.saveExamResponse(it)
                        }
                        goToVerifyDataFragment()
                    }
                }
                Status.ERROR -> {
                    Toast.makeText(context, "Exam Error: ${result.message}", Toast.LENGTH_LONG)
                        .show()
                    viewModel.showError(true)
                }
            }
        })
    }

    private fun returnToAskId() {
        viewModel.showError(false)
        viewModel.examSecondChance(false)
        findNavController().navigate(SingleQuestionFragmentDirections.actionSingleQuestionFragmentToAskIdFragment())
    }

    private fun goToVerifyDataFragment() {
        viewModel.examSecondChance(false)
        findNavController().navigate(SingleQuestionFragmentDirections.actionSingleQuestionFragmentToVerifyAllData1Fragment())
    }
}
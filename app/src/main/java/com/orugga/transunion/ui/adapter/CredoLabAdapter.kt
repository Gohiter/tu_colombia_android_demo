package com.orugga.transunion.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.orugga.transunion.R
import com.orugga.transunion.databinding.ItemScoresBinding
import com.orugga.transunionsdk.vo.credoLab.CredoLabScores

class CredoLabAdapter() : ListAdapter<CredoLabScores, CredoLabAdapter.ViewHolder>(object : DiffUtil.ItemCallback<CredoLabScores>() {
    override fun areItemsTheSame(oldItem: CredoLabScores, newItem: CredoLabScores): Boolean {
        return oldItem.code == newItem.code
    }

    override fun areContentsTheSame(oldItem: CredoLabScores, newItem: CredoLabScores): Boolean {
        return oldItem.code == newItem.code
    }
}) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemScoresBinding>(layoutInflater, R.layout.item_scores, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val score = getItem(position)
        holder.binding.score = score
    }

    class ViewHolder(binding: ItemScoresBinding) : RecyclerView.ViewHolder(binding.root) {
        val binding = binding
    }

}
package com.orugga.transunion.ui.steps.step4.step4_1

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep41VerifyAlldataBinding
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunion.vo.DemoAppContexts
import com.orugga.transunionsdk.TransunionSdk
import com.orugga.transunionsdk.vo.docAuthenticationReport.DocAuthReportVerification
import com.orugga.transunion.vo.AllData
import com.orugga.transunion.vo.VerificationName
import com.orugga.transunionsdk.vo.examRequest.ExamRequestResponse
import java.text.SimpleDateFormat
import java.util.Calendar
import javax.inject.Inject

class VerifyAllData1Fragment : BaseFragment(), DatePickerDialog.OnDateSetListener {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    @Inject
    lateinit var transunionSdk: TransunionSdk
    private lateinit var binding: FragmentStep41VerifyAlldataBinding
    private lateinit var viewModel: VerifyAllData1ViewModel
    private lateinit var docResponse: DocAuthReportVerification
    private lateinit var examResponse: ExamRequestResponse
    private lateinit var data: AllData

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_step_4_1_verify_alldata, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(VerifyAllData1ViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnContinue.setOnClickListener { goToStep42() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        docResponse = sharedPreferencesHelper.getDocumentResponse()
        examResponse = sharedPreferencesHelper.getExamResponse()
        getProperties(examResponse)
        data = arguments?.let { VerifyAllData1FragmentArgs.fromBundle(it).data } ?: AllData()
        buildDatePickerDialog()
    }

    private fun getProperties(properties: ExamRequestResponse) {
        val nameOne = properties.fields.applicants.applicant[0].idData.examResponseResponse!!.examResponseTercero.nameOne
        val nameTwo = properties.fields.applicants.applicant[0].idData.examResponseResponse!!.examResponseTercero.nameTwo
        val lastNameOne = properties.fields.applicants.applicant[0].idData.examResponseResponse!!.examResponseTercero.lastNameOne
        val lastNameTwo = properties.fields.applicants.applicant[0].idData.examResponseResponse!!.examResponseTercero.lastNameTwo
        binding.etName.setText(getString(
            R.string.name_placeholder,
            nameOne,
            if (nameTwo != "null") { nameTwo } else {""}))
        binding.etLastName.setText(getString(
            R.string.name_placeholder,
            lastNameOne,
            if (lastNameTwo != "null") { lastNameTwo } else {""}))
        binding.etIdNumber.setText(properties.fields.applicants.applicant[0].idData.examResponseResponse!!.examResponseTercero.idNumber)
        binding.etBirthDate.setText(properties.fields.applicants.applicant[0].applicantAttributes.veridasReport.birthDate)
        binding.etBirthPlace.setText(properties.fields.applicants.applicant[0].applicantAttributes.veridasReport.birthPlaceDistrict)
        binding.etExpeditionDate.setText(properties.fields.applicants.applicant[0].idData.examResponseResponse!!.examResponseTercero.expeditionDate)
        binding.etExpeditionPlace.setText(properties.fields.applicants.applicant[0].idData.examResponseResponse!!.examResponseTercero.expeditionPlace)
        binding.etIdType.setText(properties.fields.applicants.applicant[0].idData.examResponseResponse!!.examResponseTercero.idType)
    }


    private fun buildDatePickerDialog() {
        //TODO check if hasid is false to show dialog
        val newCalendar = Calendar.getInstance()
        val datePickerDialog = context?.let {DatePickerDialog(it, this, newCalendar.get(Calendar.YEAR),
            newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH))}
        binding.etBirthDate.setOnClickListener {
            datePickerDialog?.setOnDismissListener {
                transunionSdk.changeContext(DemoAppContexts.STEP_4_3.value)
            }
            datePickerDialog?.datePicker?.maxDate = System.currentTimeMillis()
            datePickerDialog?.show()
            transunionSdk.changeContext(DemoAppContexts.STEP_4_4.value)
        }
    }


    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, dayOfMonth)
        val formatter = SimpleDateFormat("dd/MM/yyyy")
        binding.etBirthDate.setText(formatter.format((calendar.time)))
    }

    private fun goToStep42() {
        val verificationName = VerificationName(binding.etName.text.toString(), binding.etLastName.text.toString())
        data.verificationName = verificationName
        findNavController().navigate(
            VerifyAllData1FragmentDirections.actionVerifyAllData1FragmentToVerifyAllData2Fragment(
                data
            )
        )
    }

}
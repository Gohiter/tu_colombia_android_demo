package com.orugga.transunion.ui.steps.step2

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.orugga.transunion.repository.DocumentsRepository
import com.orugga.transunion.repository.UserRepository
import com.orugga.transunion.repository.VerificationRepository
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.TokenResponse
import com.orugga.transunionsdk.vo.UploadableDocumentType
import com.orugga.transunionsdk.vo.authExam.AuthExamResponse
import com.orugga.transunionsdk.vo.createApp.CreateAppResponse
import com.orugga.transunionsdk.vo.createDocument.CreateAttachDocumentResponse
import com.orugga.transunionsdk.vo.deviceVerification.DeviceVerificationResponse
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceResponse
import com.orugga.transunionsdk.vo.emailVerification.EmailVerificationResponse
import com.orugga.transunionsdk.vo.facialSimilarityInvokeService.FacialSimilarityInvokeServiceResponse
import com.orugga.transunionsdk.vo.facialSimilarityReport.FacialSimilarityReportResponse
import com.orugga.transunionsdk.vo.offers.OffersResponse
import com.orugga.transunionsdk.vo.phoneVerification.PhoneVerificationResponse
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import okhttp3.MediaType
import com.orugga.transunionsdk.vo.pullReport.PullReportResponse
import com.orugga.transunionsdk.vo.watchlist.WatchlistResponse
import java.io.File
import javax.inject.Inject

class SelfieViewModel
@Inject constructor(
    private val documentsRepository: DocumentsRepository,
    private val userRepository: UserRepository
) : ViewModel() {

    val file = MutableLiveData<File>()
    val showError = MutableLiveData<Boolean>()
    val createSelfieResponse = MutableLiveData<Resource<CreateAttachDocumentResponse>>()
    val uploadSelfieResponse = MutableLiveData<Resource<CreateAttachDocumentResponse>>()
    val processSelfieResponse = MutableLiveData<Resource<AuthExamResponse>>()

    init {

    }

    fun getApplicationId(): String {
        return userRepository.getAppId()
    }

    fun showError(show: Boolean) {
        showError.value = show
    }

    fun createSelfieDocument() {
        viewModelScope.launch {
            documentsRepository.createDocument(UploadableDocumentType.SELFIE, "Selfie.jpg")
                .collect {
                    createSelfieResponse.value = it
                }
        }
    }

    fun uploadSelfie(file: File, mediaType: MediaType) {
        viewModelScope.launch {
            documentsRepository.uploadDocument(file, mediaType, UploadableDocumentType.SELFIE)
                .collect {
                    uploadSelfieResponse.value = it
                }
        }
    }

    fun processSelfie() {
        viewModelScope.launch {
            documentsRepository.processSelfie().collect {
                processSelfieResponse.value = it
            }
        }
    }
}
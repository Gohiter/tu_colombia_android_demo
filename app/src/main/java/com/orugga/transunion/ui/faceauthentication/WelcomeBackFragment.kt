package com.orugga.transunion.ui.faceauthentication

import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentWelcomeBackBinding
import com.orugga.transunion.databinding.FragmentWelcomeBinding
import com.orugga.transunion.ui.CustomTypefaceSpan
import com.orugga.transunion.ui.dialog.AlertPopupDialog
import com.orugga.transunion.ui.dialog.CustomDialog
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.ui.welcome.WelcomeFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunion.vo.DemoAppContexts
import com.orugga.transunionsdk.TransunionSdk
import javax.inject.Inject


class WelcomeBackFragment : BaseFragment() {
    @Inject
    lateinit var transunionSdk: TransunionSdk

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper

    private lateinit var binding: FragmentWelcomeBackBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_welcome_back, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnConfirm.setOnClickListener { validateForm() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        setToSText()
    }

    private fun goToNextFragment() {

        findNavController().navigate(WelcomeBackFragmentDirections.actionWelcomeBackFragmentToTakeSelfieFragment())
    }

    private fun setToSText() {
        val tosText = getString(R.string.get_mail_phone_frag_txt_tos)
        val tosClickableTxt = getString(R.string.get_mail_phone_frag_txt_clickable_tos)
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                openTosDialog()
            }
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                val tublue2 = context?.let { ContextCompat.getColor(it, R.color.tuBlue2) }
                if (tublue2 != null) {
                    ds.color = tublue2
                }
                ds.isUnderlineText = false
            }
        }
        val myTypeface = Typeface.create(
            context?.let { ResourcesCompat.getFont(it, R.font.intro_regular_regular) },
            Typeface.NORMAL
        )
        val startIndex = tosText.indexOf(tosClickableTxt)
        val endIndex = startIndex + tosClickableTxt.length
        val spannableString = SpannableString(tosText)
        spannableString.setSpan(clickableSpan, startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(CustomTypefaceSpan(myTypeface), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        binding.txtTos.text = spannableString
        binding.txtTos.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun openTosDialog() {
        CustomDialog(getString(R.string.get_mail_phone_frag_tos_title),
            getString(R.string.TYC_get_mail_phone_content),
            getString(R.string.get_mail_phone_frag_btn_ok), {}, {
                transunionSdk.changeContext(DemoAppContexts.SIGN_UP_EMPTY.value)
            })
            .show(parentFragmentManager, "dialogToS")
        transunionSdk.changeContext(DemoAppContexts.PRIVACY_POLICY_ACCEPTED.value)
    }

    private fun validateForm() {
        val title = getString(R.string.welcome_validation_dialog_title)
        val email = binding.emailInputText.getEmail()
//        val phone = binding.phoneInputText.getPhone()
        if(email.isNullOrEmpty()){
            openValidationDialog(title, getString(R.string.welcome_validation_dialog_empty_email))
            return
        }else if(!binding.emailInputText.isValid()){
            openValidationDialog(title, getString(R.string.welcome_validation_dialog_invalid_email))
            return
        }
//        if(!binding.phoneInputText.hasCountryCode()){
//            openValidationDialog(title, getString(R.string.welcome_validation_dialog_empty_country_code))
//            return
//        }else if(!binding.phoneInputText.hasRegionalNumber()){
//            openValidationDialog(title, getString(R.string.welcome_validation_dialog_empty_phone_number))
//            return
//        }else if(!binding.phoneInputText.isValid()){
//            openValidationDialog(title, getString(R.string.welcome_validation_dialog_invalid_phone))
//            return
//        }
        if(!binding.checkbox.isChecked){
            openValidationDialog(title, getString(R.string.welcome_validation_dialog_tos_unchecked))
            return
        }
        email?.let { sharedPreferencesHelper.saveEmail(it) }
//        phone?.let { sharedPreferencesHelper.savePhone(it) }
        goToNextFragment()
    }

    private fun openValidationDialog(title: String?, text: String) {
        AlertPopupDialog(title, text) {}.show(parentFragmentManager, "dialogValidation")
    }
}
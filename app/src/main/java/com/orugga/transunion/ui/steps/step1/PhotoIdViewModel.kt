package com.orugga.transunion.ui.steps.step1

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.orugga.transunion.repository.DocumentsRepository
import com.orugga.transunion.repository.UserRepository
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.UploadableDocumentType
import com.orugga.transunionsdk.vo.createApp.CreateAppResponse
import com.orugga.transunionsdk.vo.createDocument.CreateAttachDocumentResponse
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceResponse
import com.orugga.transunionsdk.vo.docAuthentication.DocAuthPhotoFrontResponse
import com.orugga.transunionsdk.vo.docAuthenticationReport.DocAuthReportResponse
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect
import okhttp3.MediaType
import java.io.File
import javax.inject.Inject
import android.content.res.AssetManager
import java.io.InputStream


class PhotoIdViewModel
@Inject constructor(private val documentsRepository: DocumentsRepository,
                    private val userRepository: UserRepository): ViewModel() {

    val fileFront = MutableLiveData<File>()
    val fileBack = MutableLiveData<File>()
    val showError = MutableLiveData<Boolean>()
    val blackBoxResponse = MutableLiveData<Resource<String>>()
    val createAppResponse = MutableLiveData<Resource<CreateAppResponse>>()
    val createFrontDocumentResponse = MutableLiveData<Resource<CreateAttachDocumentResponse>>()
    val createBackDocumentResponse = MutableLiveData<Resource<CreateAttachDocumentResponse>>()
    val uploadFrontDocumentResponse = MutableLiveData<Resource<CreateAttachDocumentResponse>>()
    val uploadBackDocumentResponse = MutableLiveData<Resource<CreateAttachDocumentResponse>>()
    val processDocumentResponse = MutableLiveData<Resource<DocAuthInvokeServiceResponse>>()

    init {

    }

    fun showError(show: Boolean) {
        showError.value = show
    }

    fun getBlackBox(appContext: Context, iovationSubscriberKey: String) {
        viewModelScope.launch {
            userRepository.getBlackBox(appContext, iovationSubscriberKey).collect {
                blackBoxResponse.value = it
            }
        }
    }

    fun createApp(email: String, phoneCode: String, phoneNumber: String, blackbox: String) {
        viewModelScope.launch {
            userRepository.createApp(email, phoneCode, phoneNumber, blackbox).collect {
                createAppResponse.value = it
            }
        }
    }

    fun createFrontDocument() {
        viewModelScope.launch {
            documentsRepository.createDocument(UploadableDocumentType.FRONT, "Front.jpg").collect {
                createFrontDocumentResponse.value = it
            }
        }
    }

    fun createBackDocument() {
        viewModelScope.launch {
            documentsRepository.createDocument(UploadableDocumentType.BACK, "Back.jpg").collect {
                createBackDocumentResponse.value = it
            }
        }
    }

    fun uploadFrontDocument(file: File, mediaType: MediaType) {
        viewModelScope.launch {
            documentsRepository.uploadDocument(file, mediaType, UploadableDocumentType.FRONT).collect {
                uploadFrontDocumentResponse.value = it
            }
        }
    }

    fun uploadBackDocument(file: File, mediaType: MediaType) {
        viewModelScope.launch {
            documentsRepository.uploadDocument(file, mediaType, UploadableDocumentType.BACK).collect {
                uploadBackDocumentResponse.value = it
            }
        }
    }

    fun processDocument() {
        viewModelScope.launch {
            documentsRepository.processDocument().collect {
                processDocumentResponse.value = it
            }
        }
    }
}

//    fun getDocAuthReport() {
//        viewModelScope.launch {
//            documentsRepository.getDocAuthReport().collect {
//                docAuthReportResponse.value = it
//            }
//        }
//    }

//    fun invokeDocumentService() {
//        viewModelScope.launch {
//            documentsRepository.invokeService("", "", "", "").collect {
//                invokeServiceResponse.value = it
//            }
//        }
//    }
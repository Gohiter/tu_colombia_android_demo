package com.orugga.transunion.ui.checkResult

import android.location.Location
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.orugga.transunion.repository.UserRepository
import com.orugga.transunionsdk.vo.examRequest.ExamRequestResponse
import java.io.File
import javax.inject.Inject

class CheckResultViewModel
@Inject constructor(
    private val userRepository: UserRepository
): ViewModel(){

    val fileFront = MutableLiveData<File>()
    val fileBack = MutableLiveData<File>()
    val results = MutableLiveData<ExamRequestResponse>()
    val address = MutableLiveData<String>()
    val location = MutableLiveData<Location>()

    init {

    }

    fun getApplicationId(): String {
        return userRepository.getAppId()
    }
}
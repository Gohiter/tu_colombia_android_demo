package com.orugga.transunion.ui.steps.step4.step4_3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentStep43VerifyAlldataBinding
import com.orugga.transunion.ui.adapter.SpinnerAdapter
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.util.SharedPreferencesHelper
import javax.inject.Inject

class VerifyAllData3Fragment : BaseFragment() {

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var binding: FragmentStep43VerifyAlldataBinding
    private lateinit var viewModel: VerifyAllData3ViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_step_4_3_verify_alldata, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this, viewModelFactory).get(VerifyAllData3ViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        populateCountriesSpinner()
        binding.btnContinue.setOnClickListener { goToStep5() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
    }

    private fun populateCountriesSpinner() {
        val types = resources.getStringArray(R.array.countries)
        binding.spinnerCitizenshipCountry.adapter = context?.let {
            SpinnerAdapter(requireActivity(), types,
                resources.getString(R.string.viewpager_item_step_verify_alldata_citizen_spinner_hint))
        }
        binding.spinnerCitizenshipCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                binding.btnContinue.isEnabled = false
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    binding.btnContinue.isEnabled = position != 0
            }
        }
    }

    private fun goToStep5() {  }
}
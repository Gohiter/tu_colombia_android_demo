package com.orugga.transunion.ui.welcome

import android.Manifest
import android.annotation.SuppressLint
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentWelcomeBinding
import com.orugga.transunion.ui.CustomTypefaceSpan
import com.orugga.transunion.ui.dialog.AlertPopupDialog
import com.orugga.transunion.ui.dialog.CustomDialog
import com.orugga.transunion.ui.util.BaseFragment
import com.orugga.transunion.ui.util.PermissionsHelper
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunion.vo.DemoAppContexts
import com.orugga.transunionsdk.TransunionSdk
import com.orugga.transunionsdk.vo.Status
import javax.inject.Inject
import kotlin.random.Random


class WelcomeFragment : BaseFragment() {

    companion object {
        private const val TAG = "WelcomeFragment"
    }

    @Inject
    lateinit var transunionSdk: TransunionSdk

    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var welcomeViewModel: WelcomeViewModel
    private lateinit var binding: FragmentWelcomeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_welcome, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        welcomeViewModel = ViewModelProvider(this, viewModelFactory).get(WelcomeViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnConfirm.setOnClickListener { validateForm() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        welcomeViewModel.getAuth()
        sharedPreferencesHelper.removeDocumentResponse()
        sharedPreferencesHelper.removeSelfieResponse()
        sharedPreferencesHelper.removeFrontDocumentUri()
        sharedPreferencesHelper.removeBackDocumentUri()
        sharedPreferencesHelper.removePhone()
        sharedPreferencesHelper.removeEmail()
        sharedPreferencesHelper.removeLocation()
        sharedPreferencesHelper.removeAddress()
        sharedPreferencesHelper.removeExamResponse()
        observeToken()
        observeCredoLabResponse()
        sharedPreferencesHelper.saveCredoLabReference("Android-id-" + Random.nextInt().toString())
        setToSText()
    }

    private fun observeToken() {
        welcomeViewModel.tokenResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    Log.i(TAG, "Success token ${result.data?.access_token}")
                }
                Status.ERROR -> {
                    Log.e(TAG, result.message)
                }
            }
        })
    }

    private fun observeCredoLabResponse() {
        welcomeViewModel.credoLabResponse.observe(viewLifecycleOwner, Observer { result ->
            when (result.status) {
                Status.SUCCESS -> {
                    Log.i(TAG, "Success CredoLab ${result.data}")
                }
                Status.ERROR -> {
                    Log.e(TAG, result.message)
                }
            }
        })
    }

    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        if (PermissionsHelper.checkPermission(
                requireActivity(),
                this,
                PermissionsHelper.CREDOLAB_PERMISSIONS,
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.READ_CALENDAR,
                Manifest.permission.GET_ACCOUNTS
            )
        ) {
            context?.let { welcomeViewModel.collectCredoLabData(it, sharedPreferencesHelper.getCredoLabReference()) }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionsHelper.CREDOLAB_PERMISSIONS
            && grantResults.isNotEmpty()) {
            context?.let { welcomeViewModel.collectCredoLabData(it, sharedPreferencesHelper.getCredoLabReference()) }
        }
    }

    private fun goToNextFragment() {
        findNavController().navigate(WelcomeFragmentDirections.actionWelcomeFragmentToScanIdFragment())
    }

    private fun setToSText() {
        val tosText = getString(R.string.get_mail_phone_frag_txt_tos)
        val tosClickableTxt = getString(R.string.get_mail_phone_frag_txt_clickable_tos)
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                openTosDialog()
            }
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                val tublue2 = context?.let { ContextCompat.getColor(it, R.color.tuBlue2) }
                if (tublue2 != null) {
                    ds.color = tublue2
                }
                ds.isUnderlineText = false
            }
        }
        val myTypeface = Typeface.create(
            context?.let { ResourcesCompat.getFont(it, R.font.intro_regular_regular) },
            Typeface.NORMAL
        )
        val startIndex = tosText.indexOf(tosClickableTxt)
        val endIndex = startIndex + tosClickableTxt.length
        val spannableString = SpannableString(tosText)
        spannableString.setSpan(clickableSpan, startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(CustomTypefaceSpan(myTypeface), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        binding.txtTos.text = spannableString
        binding.txtTos.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun openTosDialog() {
        CustomDialog(getString(R.string.get_mail_phone_frag_tos_title),
            getString(R.string.TYC_get_mail_phone_content),
            getString(R.string.get_mail_phone_frag_btn_ok), {}, {
                transunionSdk.changeContext(DemoAppContexts.SIGN_UP_EMPTY.value)
            })
            .show(parentFragmentManager, "dialogToS")
        transunionSdk.changeContext(DemoAppContexts.PRIVACY_POLICY_ACCEPTED.value)
    }

    private fun validateForm() {
        val title = getString(R.string.welcome_validation_dialog_title)
        val email = binding.emailInputText.getEmail()
        val phone = binding.phoneInputText.getPhone()
        if(email.isNullOrEmpty()){
            openValidationDialog(title, getString(R.string.welcome_validation_dialog_empty_email))
            return
        }else if(!binding.emailInputText.isValid()){
            openValidationDialog(title, getString(R.string.welcome_validation_dialog_invalid_email))
            return
        }
        if(!binding.phoneInputText.hasCountryCode()){
            openValidationDialog(title, getString(R.string.welcome_validation_dialog_empty_country_code))
            return
        }else if(!binding.phoneInputText.hasRegionalNumber()){
            openValidationDialog(title, getString(R.string.welcome_validation_dialog_empty_phone_number))
            return
        }else if(!binding.phoneInputText.isValid()){
            openValidationDialog(title, getString(R.string.welcome_validation_dialog_invalid_phone))
            return
        }
        if(!binding.checkbox.isChecked){
            openValidationDialog(title, getString(R.string.welcome_validation_dialog_tos_unchecked))
            return
        }
        email?.let { sharedPreferencesHelper.saveEmail(it) }
        phone?.let { sharedPreferencesHelper.savePhone(it) }
        goToNextFragment()
    }

    private fun openValidationDialog(title: String?, text: String) {
        AlertPopupDialog(title, text) {}.show(parentFragmentManager, "dialogValidation")
    }
}
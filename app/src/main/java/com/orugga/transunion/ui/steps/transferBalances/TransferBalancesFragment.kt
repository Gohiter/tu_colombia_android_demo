package com.orugga.transunion.ui.steps.transferBalances

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.orugga.transunion.MainActivity
import com.orugga.transunion.R
import com.orugga.transunion.databinding.FragmentTransferBalancesBinding
import com.orugga.transunion.ui.util.BaseFragment

class TransferBalancesFragment : BaseFragment() {

    private lateinit var binding: FragmentTransferBalancesBinding
    private lateinit var transferBalancesViewModel: TransferBalancesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.hide()
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_transfer_balances, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        transferBalancesViewModel = ViewModelProvider(this, viewModelFactory).get(TransferBalancesViewModel::class.java)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnConfirm.setOnClickListener { openSuccessDoneDialog() }
        binding.btnBack.setOnClickListener { requireActivity().onBackPressed() }
        setupSuccessDoneDialog()
    }

    private fun setupSuccessDoneDialog() {
        binding.doneDialog.setAppId("#${transferBalancesViewModel.getApplicationId()}")
        binding.doneDialog.setPrimaryButtonAction(View.OnClickListener { goToAskForId() })
    }

    private fun openSuccessDoneDialog() {
        binding.doneDialog.visibility = View.VISIBLE
    }

    private fun goToAskForId() {
        (activity as MainActivity).showErrorAndReset(resources.getString(R.string.done))
    }
}
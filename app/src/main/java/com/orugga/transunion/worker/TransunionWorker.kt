package com.orugga.transunion.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.orugga.transunion.repository.UserRepository
import kotlinx.coroutines.coroutineScope
import javax.inject.Inject

class TransunionWorker constructor(
        val userRepository: UserRepository,
        private val appContext: Context,
        workerParams: WorkerParameters
): CoroutineWorker(appContext, workerParams) {

    /**
     * Here we have to run the logic that should run in the worker.
     */
    override suspend fun doWork(): Result = coroutineScope {
        Result.success()
    }

    /**
     * A factory class needed to provide all the dependencies needed by this particular worker,
     * in this case the ContestRepository.
     */
    class Factory @Inject constructor(
            val userRepository: UserRepository
            ): ChildWorkerFactory {

        override fun create(appContext: Context, params: WorkerParameters): CoroutineWorker {
            return TransunionWorker(userRepository, appContext, params)
        }
    }
}
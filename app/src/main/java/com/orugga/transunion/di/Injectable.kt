package com.orugga.transunion.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable

package com.orugga.transunion.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.orugga.transunion.ui.checkResult.CheckResultViewModel
import com.orugga.transunion.ui.faceauthentication.SelfieAuthViewModel
import com.orugga.transunion.ui.faceauthentication.SelfieSelectionAuthViewModel
import com.orugga.transunion.ui.steps.askForId.AskForIDViewModel
import com.orugga.transunion.ui.steps.foundCards.FoundCardsViewModel
import com.orugga.transunion.ui.steps.instantCard.InstantCardViewModel
import com.orugga.transunion.ui.steps.step1.PhotoIdViewModel
import com.orugga.transunion.ui.steps.step2.SelfieSelectionViewModel
import com.orugga.transunion.ui.steps.step2.SelfieViewModel
import com.orugga.transunion.ui.steps.step3.step3_1.OTPViewModel
import com.orugga.transunion.ui.steps.step3.step3_2.QuestionsViewModel
import com.orugga.transunion.ui.steps.step4.step4_1.VerifyAllData1ViewModel
import com.orugga.transunion.ui.steps.step4.step4_2.VerifyAllData2ViewModel
import com.orugga.transunion.ui.steps.step4.step4_3.VerifyAllData3ViewModel
import com.orugga.transunion.ui.steps.step5.step5_1.EmployIncomeEstimateViewModel
import com.orugga.transunion.ui.steps.step5.step5_2.EmploymentIncomeViewModel
import com.orugga.transunion.ui.steps.step6.step6_1.ProofOfAddressViewModel
import com.orugga.transunion.ui.steps.step6.step6_2.ProofIncomeViewModel
import com.orugga.transunion.ui.steps.step6.step6_3.SignatureViewModel
import com.orugga.transunion.ui.welcome.WelcomeViewModel

import com.orugga.transunion.viewmodel.TransunionViewModelFactory

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(AskForIDViewModel::class)
    abstract fun bindAskForId(askForIDViewModel: AskForIDViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PhotoIdViewModel::class)
    abstract fun bindPhotoIdViewModel(photoIdViewModel: PhotoIdViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(WelcomeViewModel::class)
    abstract fun bindWelcomeViewModel(welcomeViewModel: WelcomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelfieViewModel::class)
    abstract fun bindSelfieViewModel(selfieViewModel: SelfieViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelfieSelectionViewModel::class)
    abstract fun bindSelfieSelectionViewModel(selfieSelectionViewModel: SelfieSelectionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QuestionsViewModel::class)
    abstract fun bindQuestionsViewModel(questionsViewModel: QuestionsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OTPViewModel::class)
    abstract fun bindOTPViewModel(oTPViewModel: OTPViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VerifyAllData1ViewModel::class)
    abstract fun bindVerifyAllData1ViewModel(verifyAllDataViewModel: VerifyAllData1ViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VerifyAllData2ViewModel::class)
    abstract fun bindVerifyAllData2ViewModel(verifyAllDataViewModel: VerifyAllData2ViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VerifyAllData3ViewModel::class)
    abstract fun bindVerifyAllData3ViewModel(verifyAllDataViewModel: VerifyAllData3ViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EmployIncomeEstimateViewModel::class)
    abstract fun bindEmployIncomeEstimateViewModel(employIncomeEstimateViewModel: EmployIncomeEstimateViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EmploymentIncomeViewModel::class)
    abstract fun bindEmploymentIncomeViewModel(employmentIncomeViewModel: EmploymentIncomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProofOfAddressViewModel::class)
    abstract fun bindProofOfAddressViewModel(proofOfAddressViewModel: ProofOfAddressViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProofIncomeViewModel::class)
    abstract fun bindVProofIncomeViewModel(proofIncomeViewModel: ProofIncomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SignatureViewModel::class)
    abstract fun bindSignatureViewModel(signatureViewModel: SignatureViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CheckResultViewModel::class)
    abstract fun bindCheckResultViewModel(checkResultViewModel: CheckResultViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InstantCardViewModel::class)
    abstract fun bindInstantCardViewModel(instantCardViewModel: InstantCardViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FoundCardsViewModel::class)
    abstract fun bindFoundCardsViewModel(foundCardsViewModel: FoundCardsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelfieAuthViewModel::class)
    abstract fun bindTransferBalancesViewModel(selfieAuthViewModel: SelfieAuthViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SelfieSelectionAuthViewModel::class)
    abstract fun selfieSelectionAuthViewModel(selfieSelectionAuthViewModel: SelfieSelectionAuthViewModel): ViewModel

//    @Binds
//    @IntoMap
//    @ViewModelKey(TransferBalancesViewModel::class)
//    abstract fun bindTransferBalancesViewModel(transferBalancesViewModel: TransferBalancesViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: TransunionViewModelFactory): ViewModelProvider.Factory
}

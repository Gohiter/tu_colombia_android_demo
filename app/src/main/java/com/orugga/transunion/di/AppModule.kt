package com.orugga.transunion.di

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import com.orugga.transunion.BuildConfig
import com.orugga.transunion.api.TransunionService
import com.orugga.transunion.db.TransunionDb
import com.orugga.transunion.db.UserDao
import com.orugga.transunionsdk.TransunionSdk
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
//import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.inject.Singleton
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


@Module(includes = [ViewModelModule::class, WorkerModule::class])
class AppModule {
//    @Singleton
//    @Provides
//    fun provideTransunionService(): TransunionService {
////        val logging = HttpLoggingInterceptor()
//        if (BuildConfig.DEBUG) {
////            logging.level = HttpLoggingInterceptor.Level.BODY
//        } else {
////            logging.level = HttpLoggingInterceptor.Level.NONE
//        }
//        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
//
//            override fun getAcceptedIssuers(): Array<X509Certificate> {
//                return arrayOf()
//            }
//
//            @Throws(CertificateException::class)
//            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
//            }
//
//            @Throws(CertificateException::class)
//            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
//            }
//        })
//        // Install the all-trusting trust manager
//        val sslContext = SSLContext.getInstance("SSL")
//        sslContext.init(null, trustAllCerts, java.security.SecureRandom())
//
//        // Create an ssl socket factory with our all-trusting manager
//        val sslSocketFactory = sslContext.socketFactory
////        val httpClient = OkHttpClient.Builder()
////        httpClient.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
////        httpClient.hostnameVerifier { _, _ -> true }
////        httpClient.addInterceptor(logging).addInterceptor(tokenInterceptor)
////        httpClient.addInterceptor(logging)
////        httpClient.callTimeout(2, TimeUnit.MINUTES)
////                .connectTimeout(20, TimeUnit.SECONDS)
////                .readTimeout(30, TimeUnit.SECONDS)
////                .writeTimeout(30, TimeUnit.SECONDS);
//
//        return Retrofit.Builder()
//                .baseUrl(BuildConfig.BASE_URL)
//                .addConverterFactory(GsonConverterFactory.create())
////                .client(httpClient.build())
//                .build()
//                .create(TransunionService::class.java)
//    }

    @Singleton
    @Provides
    fun provideDb(app: Application): TransunionDb {
        return Room
            .databaseBuilder(app, TransunionDb::class.java, "transunion.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideUserDao(db: TransunionDb): UserDao {
        return db.userDao()
    }

    @Singleton
    @Provides
    fun provideSharedPreference(app: Application) : SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(app.applicationContext)
    }

    @Singleton
    @Provides
    fun provideSDK(app: Application) : TransunionSdk {
        val url = "https://www.transuniondecisioncentreuat.com.mx/V35LATAM/TU.DE.Pont/"
        return TransunionSdk.create(app.applicationContext, url)
    }
}

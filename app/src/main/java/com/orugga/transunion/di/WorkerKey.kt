package com.orugga.transunion.di

import androidx.work.CoroutineWorker
import dagger.MapKey
import kotlin.reflect.KClass

/**
 * Key to be user for the Worker Annotations.
 */
@MustBeDocumented
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class WorkerKey(val value: KClass<out CoroutineWorker>)
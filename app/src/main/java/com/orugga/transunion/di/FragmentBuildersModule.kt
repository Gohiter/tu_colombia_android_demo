package com.orugga.transunion.di

import com.orugga.transunion.ui.checkResult.CheckResultFragment
import com.orugga.transunion.ui.faceauthentication.BankinformationFragment
import com.orugga.transunion.ui.faceauthentication.SelfieAuthFragment
import com.orugga.transunion.ui.faceauthentication.SelfieAuthResultFragment
import com.orugga.transunion.ui.faceauthentication.WelcomeBackFragment
import com.orugga.transunion.ui.intro.IntroFragment
import com.orugga.transunion.ui.steps.askForId.AskForIDFragment
import com.orugga.transunion.ui.steps.foundCards.FoundCardsFragment
import com.orugga.transunion.ui.steps.instantCard.InstantCardFragment
import com.orugga.transunion.ui.steps.step1.PhotoIdResultFragment
import com.orugga.transunion.ui.steps.step1.TakePhotoIdFragment
import com.orugga.transunion.ui.steps.step2.SelfieFragment
import com.orugga.transunion.ui.steps.step2.SelfieResultFragment
import com.orugga.transunion.ui.steps.step3.step3_1.OTPFragment
import com.orugga.transunion.ui.steps.step3.step3_2.SingleQuestionFragment
import com.orugga.transunion.ui.steps.step4.step4_1.VerifyAllData1Fragment
import com.orugga.transunion.ui.steps.step4.step4_2.VerifyAllData2Fragment
import com.orugga.transunion.ui.steps.step4.step4_3.VerifyAllData3Fragment
import com.orugga.transunion.ui.steps.step5.step5_1.EmployIncomeEstimateFragment
import com.orugga.transunion.ui.steps.step5.step5_2.EmploymentIncomeFragment
import com.orugga.transunion.ui.steps.step6.step6_1.ProofOfAddressFragment
import com.orugga.transunion.ui.steps.step6.step6_1.ProofOfAddressResultFragment
import com.orugga.transunion.ui.steps.step6.step6_2.ProofIncomeFragment
import com.orugga.transunion.ui.steps.step6.step6_2.ProofIncomeResultFragment
import com.orugga.transunion.ui.steps.step6.step6_3.SignatureFragment
import com.orugga.transunion.ui.steps.transferBalances.TransferBalancesFragment
import com.orugga.transunion.ui.welcome.WelcomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeIntroFragment(): IntroFragment

    @ContributesAndroidInjector
    abstract fun contributeAskForIDFragment(): AskForIDFragment

    @ContributesAndroidInjector
    abstract fun contributeTakePhotoIdFragment(): TakePhotoIdFragment

    @ContributesAndroidInjector
    abstract fun contributePhotoIdResultFragment(): PhotoIdResultFragment

    @ContributesAndroidInjector
    abstract fun contributeSelfieFragment(): SelfieFragment

    @ContributesAndroidInjector
    abstract fun contributeSelfieResultFragment(): SelfieResultFragment

    @ContributesAndroidInjector
    abstract fun contributeVerifyAllDataFragment(): VerifyAllData1Fragment

    @ContributesAndroidInjector
    abstract fun contributeVerifyAllData2Fragment(): VerifyAllData2Fragment

    @ContributesAndroidInjector
    abstract fun contributeVerifyAllData3Fragment(): VerifyAllData3Fragment

    @ContributesAndroidInjector
    abstract fun contributeOTPFragment(): OTPFragment

    @ContributesAndroidInjector
    abstract fun contributeSingleQuestionFragment(): SingleQuestionFragment

    @ContributesAndroidInjector
    abstract fun contributeEmployIncomeEstimateFragment(): EmployIncomeEstimateFragment

    @ContributesAndroidInjector
    abstract fun contributeEmploymentIncomeFragment(): EmploymentIncomeFragment

    @ContributesAndroidInjector
    abstract fun contributeProofOfAddressFragment(): ProofOfAddressFragment

    @ContributesAndroidInjector
    abstract fun contributeProofOfAddressResultFragment(): ProofOfAddressResultFragment

    @ContributesAndroidInjector
    abstract fun contributeProofIncomeResultFragment(): ProofIncomeResultFragment

    @ContributesAndroidInjector
    abstract fun contributeProofIncomeFragment(): ProofIncomeFragment

    @ContributesAndroidInjector
    abstract fun contributeSignatureFragment(): SignatureFragment

    @ContributesAndroidInjector
    abstract fun contributeWelcomeFragment(): WelcomeFragment

    @ContributesAndroidInjector
    abstract fun contributeCheckResultFragment(): CheckResultFragment

    @ContributesAndroidInjector
    abstract fun contributeInstantCardFragment(): InstantCardFragment

    @ContributesAndroidInjector
    abstract fun contributeFoundCardsFragment(): FoundCardsFragment

    @ContributesAndroidInjector
    abstract fun contributeTransferBalancesFragment(): TransferBalancesFragment

    @ContributesAndroidInjector
    abstract fun welcomeBackFragment(): WelcomeBackFragment

    @ContributesAndroidInjector
    abstract fun selfieAuthFragment(): SelfieAuthFragment

    @ContributesAndroidInjector
    abstract fun selfieAuthResultFragment(): SelfieAuthResultFragment

    @ContributesAndroidInjector
    abstract fun bankInformationFragment(): BankinformationFragment
}
package com.orugga.transunion.di

import com.orugga.transunion.di.WorkerKey
import com.orugga.transunion.worker.ChildWorkerFactory
import com.orugga.transunion.worker.TransunionWorker
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class WorkerModule {

    @Binds
    @IntoMap
    @WorkerKey(TransunionWorker::class)
    internal abstract fun bindMyWorkerFactory(worker: TransunionWorker.Factory): ChildWorkerFactory
}
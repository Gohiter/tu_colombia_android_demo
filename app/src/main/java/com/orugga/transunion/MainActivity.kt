package com.orugga.transunion

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.orugga.transunion.util.SharedPreferencesHelper
import com.orugga.transunion.vo.DemoAppContexts
import com.orugga.transunionsdk.TransunionSdk
import com.orugga.transunionsdk.activity.TransunionActivity
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class MainActivity : TransunionActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>
    @Inject
    lateinit var transunionSdk: TransunionSdk
    @Inject
    lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var navController: NavController
    private lateinit var containerFragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        setContentView(R.layout.activity_main)
        initComponents()
    }

    private fun initComponents(){
        navController = Navigation.findNavController(this, R.id.container)
        supportFragmentManager.findFragmentById(R.id.container)?.let{ containerFragment = it }
        supportFragmentManager.beginTransaction().setPrimaryNavigationFragment(containerFragment).commit()
        //transunionSdk.setCredoLab("f4ca28d9-888e-4436-8e26-34cba396ceb9", "paola.bocalandro@orugga.com", "0rrUg@", "https://scoring-sales.credolab.com/account/login")
        //transunionSdk.configTransunion("OruggaUser", "W3lc0me5pring@2019", "password", "SDKApp02", "TSOGlobalSDKVideo_p_AGSS")
        transunionSdk.configTransunion("Orugga.TSO", "mfjL2PkhC8W9cUD.", "password", "SDKApp02", "363")

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when(destination.id) {
                R.id.verifyAllData1Fragment -> {
                    if(sharedPreferencesHelper.isHasId()){
                        transunionSdk.changeContext(destination.label.toString())
                    }else{
                        transunionSdk.changeContext(DemoAppContexts.STEP_4_3.value)
                    }
                }
                R.id.verifyAllData2Fragment -> {
                    if(sharedPreferencesHelper.isHasId()){
                        transunionSdk.changeContext(destination.label.toString())
                    }else{
                        transunionSdk.changeContext(DemoAppContexts.STEP_4_5.value)
                    }
                }
                else -> transunionSdk.changeContext(destination.label.toString())
            }
        }
        transunionSdk.restartUserSession()
    }

    override fun onBackPressed() {
        hideKeyboard()
        super.onBackPressed()
    }

    private fun hideKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        val currentFocusedView = currentFocus
        if (currentFocusedView != null) {
            imm.hideSoftInputFromWindow(currentFocusedView.windowToken, 0)
        }
    }

    override fun onLocationPermissionDenied() {
        super.onLocationPermissionDenied()
    }

    override fun onLocationPermissionAccepted() {
        super.onLocationPermissionAccepted()
    }


    override fun supportFragmentInjector() = dispatchingAndroidInjector

    fun showErrorAndReset(errorMsg: String){
        transunionSdk.restartUserSession()
        Log.d("transunionError", errorMsg)
        navController.navigate(R.id.welcomeFragment)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }
}

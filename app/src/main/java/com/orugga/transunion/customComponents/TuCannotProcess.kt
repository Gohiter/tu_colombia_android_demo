package com.orugga.transunion.customComponents

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import com.orugga.transunion.R
import com.orugga.transunion.databinding.DialogCanNotProcessBinding


class TuCannotProcess@JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    val binding: DialogCanNotProcessBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_can_not_process,
            this, true)
    init {
        attrs?.let {
            val styledAttributes =
                context.obtainStyledAttributes(it, R.styleable.TuCannotProcess, 0, 0)
            binding.txtAppId.text = styledAttributes.getString(R.styleable.TuCannotProcess_can_not_process_app_id)
        }
    }

    fun setPrimaryButtonAction(onClickListener: OnClickListener) {
        binding.btnDone.setOnClickListener(onClickListener)
    }

    fun setAppId(appId: String) {
        binding.txtAppId.text = appId
    }
}
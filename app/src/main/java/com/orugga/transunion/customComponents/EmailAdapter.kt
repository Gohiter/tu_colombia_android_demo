package com.orugga.transunion.customComponents

import android.content.Context
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.Filterable
import androidx.annotation.LayoutRes

class EmailAdapter(context: Context, @LayoutRes private val layoutResource: Int, private val allEmails: List<String>):
    ArrayAdapter<String>(context, layoutResource, allEmails),
    Filterable {
    private var emailList: List<String> = allEmails

    override fun getCount(): Int {
        return emailList.size.coerceAtMost(2)
    }

    override fun getItem(p0: Int): String? {
        return emailList[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                emailList = filterResults.values as List<String>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val queryString = charSequence?.toString()?.toLowerCase()?.trim()
                val filterResults = FilterResults()
                emailList = allEmails
                filterResults.values = if (queryString==null || queryString.isEmpty()) {
                    emailList
                } else {
                    emailList.filter {
                        it.contains(queryString)
                    }
                }
                return filterResults
            }
        }
    }
}
package com.orugga.transunion.customComponents

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import com.orugga.transunion.R
import com.orugga.transunion.databinding.DialogAlertBinding


class TuAlertDialog@JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    val binding: DialogAlertBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.dialog_alert,
            this, true)
    init {
        attrs?.let {
            val styledAttributes =
                context.obtainStyledAttributes(it, R.styleable.TuAlertDialog, 0, 0)
            binding.isSuccessDialog = styledAttributes.getBoolean(R.styleable.TuAlertDialog_is_success_dialog, true)
            binding.txtTitle.text = styledAttributes.getString(R.styleable.TuAlertDialog_alert_title)
            binding.txtAppId.text = styledAttributes.getString(R.styleable.TuAlertDialog_app_id)
            binding.txtSubtitle.text = styledAttributes.getString(R.styleable.TuAlertDialog_alert_subtitle)
            binding.btnPrimaryAction.text = styledAttributes.getString(R.styleable.TuAlertDialog_primary_button_text)
            val icon = styledAttributes.getResourceId(R.styleable.TuAlertDialog_icon, -1)
            if (icon != -1) {
                binding.icon.setImageResource(icon)
                binding.checkIcon.visibility = View.GONE
            }
            binding.outsideArea.setOnClickListener { this.visibility = View.GONE }
        }
    }

    fun setPrimaryButtonAction(onClickListener: OnClickListener) {
        binding.btnPrimaryAction.setOnClickListener(onClickListener)
    }

    fun setAppId(appId: String) {
        binding.txtAppId.text = appId
    }

}
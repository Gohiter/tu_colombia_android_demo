package com.orugga.transunion.customComponents

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import com.orugga.transunion.R
import com.orugga.transunion.databinding.ProgressVerificationBinding


class ProgressDialog@JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {
    val binding: ProgressVerificationBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.progress_verification,
            this, true)
    init {
        attrs?.let {
            val styledAttributes =
                context.obtainStyledAttributes(it, R.styleable.ProgressDialog, 0, 0)
            binding.txtTitle.text = styledAttributes.getString(R.styleable.ProgressDialog_title)
            if (styledAttributes.getBoolean(R.styleable.ProgressDialog_has_subtitle, false)) {
                binding.txtSubtitle.visibility = View.VISIBLE
                binding.txtSubtitle.text =
                    styledAttributes.getString(R.styleable.ProgressDialog_subtitle)
            }
        }
    }

    fun setSubtitle(subtitle: String) {
        binding.txtSubtitle.visibility = View.VISIBLE
        binding.txtSubtitle.text = subtitle
    }

    fun setTitle(title: String) {
        binding.txtTitle.text = title
    }

}
package com.orugga.transunion.views

import android.content.Context
import android.util.AttributeSet
import com.crystal.crystalrangeseekbar.widgets.CrystalSeekbar

class CustomCrystalSeekbar : CrystalSeekbar {

    constructor(context: Context?) : super(context)

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    constructor (context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun getBarHeight(): Float {
        return context.resources.displayMetrics.density * 3.8f
    }

    override fun getThumbWidth(): Float {
        return context.resources.displayMetrics.density * 17
    }

    override fun getThumbHeight(): Float {
        return context.resources.displayMetrics.density * 17
    }
}
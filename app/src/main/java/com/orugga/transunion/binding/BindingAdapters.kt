package com.orugga.transunion.binding

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.android.material.card.MaterialCardView
import com.orugga.transunion.R
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


/**
 * Data Binding adapters specific to the app.
 */
object BindingAdapters {
    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("visibleInvisible")
    fun showHideInvisible(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.INVISIBLE
    }

    @JvmStatic
    @BindingAdapter("setTextWithoutNull")
    fun setTextWithoutNull(textView: TextView, text: String?) {
        if (TextUtils.isEmpty(text)) {
            textView.text = "-"
        } else {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter("setTextEmptyWithoutNull")
    fun setTextEmptyWithoutNull(textView: TextView, text: String?) {
        if (TextUtils.isEmpty(text)) {
            textView.text = ""
        } else {
            textView.text = text
        }
    }

    @SuppressLint("SetTextI18n")
    @JvmStatic
    @BindingAdapter("setScoreWithoutNull")
    fun setScoreWithoutNull(textView: TextView, number: Double?) {
        if (number == null) {
            textView.text = "-"
        } else {
            val numberPercent = (number?.times(100))
            val numberString = numberPercent.toInt().toString()
            textView.text = "$numberString%"
        }
    }

    @SuppressLint("SetTextI18n")
    @JvmStatic
    @BindingAdapter("setScoreIntWithoutNull")
    fun setScoreIntWithoutNull(textView: TextView, number: Int?) {
        if (number == null) {
            textView.text = "-"
        } else {
            val numberPercent = (number?.times(100))
            val numberString = numberPercent.toString()
            textView.text = "$numberString%"
        }
    }

    @JvmStatic
    @BindingAdapter("setBooleanWithoutNull")
    fun setBooleanWithoutNull(textView: TextView, text: Boolean?) {
        val stringText = text.toString()

        if (TextUtils.isEmpty(stringText)) {
            textView.text = "-"
        } else {
            textView.text = stringText
        }
    }

    @JvmStatic
    @BindingAdapter("setIntWithoutNull")
    fun setIntWithoutNull(textView: TextView, text: Int?) {
        val stringText = text.toString()
        if (TextUtils.isEmpty(stringText)) {
            textView.text = "-"
        } else {
            textView.text = stringText
        }
    }

    @JvmStatic
    @BindingAdapter("setDoubleWithoutNull")
    fun setDoubleWithoutNull(textView: TextView, text: Double?) {
        val stringText = text.toString()
        if (TextUtils.isEmpty(stringText)) {
            textView.text = "-"
        } else {
            textView.text = stringText
        }
    }

    @SuppressLint("SimpleDateFormat")
    @JvmStatic
    @BindingAdapter("setDateFormatted")
    fun setDateFormatted(textView: TextView, text: String?) {
        if (TextUtils.isEmpty(text)) {
            textView.text = "-"
        } else {
            val date: Date = SimpleDateFormat("dd MM yyyy").parse(text)
            val formattedDate = SimpleDateFormat("dd/MM/yyyy").format(date)
            textView.text = formattedDate
        }
    }

    @SuppressLint("SimpleDateFormat")
    @JvmStatic
    @BindingAdapter("setDateFormattedHyphen")
    fun setDateFormattedHyphen(textView: TextView, text: String?) {
        if (TextUtils.isEmpty(text)) {
            textView.text = "-"
        } else {
            val date: Date = SimpleDateFormat("yyyy-MM-dd").parse(text)
            val formattedDate = SimpleDateFormat("dd/MM/yyyy").format(date)
            textView.text = formattedDate
        }
    }

    @JvmStatic
    @BindingAdapter("setTextCodeWithoutNull")
    fun setTextCodeWithoutNull(textView: TextView, text: String?) {
        if (TextUtils.isEmpty(text)) {
            textView.text = "-"
        } else {
            textView.text = "#$text"
        }
    }

    @JvmStatic
    @BindingAdapter("setTextWithEllipses")
    fun setTextWithEllipses(textView: TextView, text: String?) {
        if (TextUtils.isEmpty(text)) {
            textView.text = ""
        } else if (text != null && text.length > 21) {
            val sb = StringBuilder(text.subSequence(0, 20))
            sb.append("...")
            textView.text = sb.toString()
        } else {
            textView.text = text
        }
    }

    @JvmStatic
    @BindingAdapter("recommendationLevel", "context")
    fun setRecommendationLevel(textView: TextView, text: String?, context: Context) {
        when (text) {
            "A" -> textView.text = context.getString(R.string.iovation_review_recommendation_allow)
            "R" -> textView.text = context.getString(R.string.iovation_review_recommendation_review)
            else -> textView.text = context.getString(R.string.iovation_review_recommendation_deny)
        }
    }


    @JvmStatic
    @BindingAdapter("loadImage")
    fun loadImage(view: ImageView, imageResource: Int?) {
        imageResource?.let { view.setImageResource(it) }
    }

    @JvmStatic
    @BindingAdapter("setStrokeColor")
    fun setStrokeColor(view: MaterialCardView, selectedCardView: MaterialCardView?) {
        if (selectedCardView !== view) {
            view.strokeColor = Color.WHITE
            view.invalidate()
        } else {
            view.strokeColor = Color.parseColor("#fcd800")
        }
    }

    @JvmStatic
    @BindingAdapter("setSelectedStroke")
    fun setSelectedStroke(view: MaterialCardView, selected: Boolean) {
        if (selected) {
            view.strokeColor = Color.parseColor("#fcd800")
        } else {
            view.strokeColor = Color.WHITE
            view.invalidate()
        }
    }

    @JvmStatic
    @BindingAdapter("setSelectedTint")
    fun setSelectedTint(view: AppCompatImageView, selected: Boolean) {
        var color = Color.parseColor("#000000")
        if (selected) {
            color = Color.parseColor("#0021a4")
        }
        view.imageTintList = ColorStateList.valueOf(color)
    }


    @JvmStatic
    @BindingAdapter("loadImage")
    fun loadImage(view: ImageView, file: File?) {
        Glide.with(view.context).load(file?.path).into(view)
    }

}

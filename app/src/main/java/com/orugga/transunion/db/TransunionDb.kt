package com.orugga.transunion.db


import androidx.room.Database
import androidx.room.RoomDatabase
import com.orugga.transunion.db.UserDao
import com.orugga.transunion.vo.User

/**
 * Main database description.
 */
@Database(
    entities = [
        User::class
    ],
    version = 1,
    exportSchema = false
)
abstract class TransunionDb : RoomDatabase() {

    abstract fun userDao(): UserDao
}

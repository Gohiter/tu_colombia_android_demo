package com.orugga.transunion.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.orugga.transunion.vo.User

/**
 * Interface for database access for User related operations.
 */
@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(user: User)

    @Query("SELECT * FROM user WHERE id = :id")
    fun findById(id: Int): LiveData<User>

    @Query("SELECT * FROM user")
    fun findLoggedUser(): LiveData<User>

    @Query("DELETE FROM user")
    fun delete()
}

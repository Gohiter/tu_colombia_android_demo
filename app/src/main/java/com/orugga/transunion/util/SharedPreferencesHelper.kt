package com.orugga.transunion.util

import android.content.SharedPreferences
import android.location.Location
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.orugga.transunion.vo.Auth
import com.orugga.transunionsdk.vo.Phone
import com.orugga.transunionsdk.vo.authExam.AuthExamResponse
import com.orugga.transunionsdk.vo.credoLab.CredoLabScores
import com.orugga.transunionsdk.vo.docAuthInvokeService.DocAuthInvokeServiceResponse
import com.orugga.transunionsdk.vo.docAuthenticationReport.DocAuthReportVerification
import com.orugga.transunionsdk.vo.examRequest.ExamRequestResponse
import com.orugga.transunionsdk.vo.facialSimilarityReport.DSFacialSimilarityReport
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Class to access using methods to Shared preferences.
 */
@Singleton
class SharedPreferencesHelper @Inject constructor(
        private val sharedPreferences: SharedPreferences){

    companion object {
        /**
         * String key for the auth model
         */
        private const val AUTH_KEY = "sharedPreferences.auth"
        private const val LOGGED_IN = "sharedPreferences.logged"
        private const val HAS_ID = "sharedPreferences.hasId"
        const val PHONES_LIST_KEY = "sharedPreferences.phones"
        const val EMAIL_LIST_KEY = "sharedPreferences.emails"
        private const val PHONES_MAX_SIZE = 20
        private const val EMAIL_MAX_SIZE = 20
        const val PHONE_KEY = "sharedPreferences.phone"
        const val EMAIL_KEY = "sharedPreferences.email"
        const val ADDRESS_KEY = "sharedPreferences.address"
        const val LOCATION_KEY = "sharedPreferences.location"
        private const val DOCUMENT_RESPONSE_KEY = "sharedPreferences.documentResponse"
        private const val SELFIE_RESPONSE_KEY = "sharedPreferences.selfieResponse"
        private const val DOCUMENT_FILE_FRONT = "sharedPreferences.documentFileFront"
        private const val DOCUMENT_FILE_BACK = "sharedPreferences.documentFileBack"
        private const val CREDOLAB_REFERENCE = "sharedPreferences.credoLabReference"
        private const val CREDOLAB_SCORES = "sharedPreferences.credoLabScores"
        private const val EXAM_RESPONSE_KEY = "saredPreferences.examResponse"
        private const val VIDEOLIVENESS_SELECTED = "sharedPreferences.videolivenessSelected"
    }

    private val gson = Gson()

    /**
     * clears the shared prefs
     */
    fun clearSharedPrefs(): Boolean {
        val editor = sharedPreferences.edit()
        editor.clear()
        return editor.commit()
    }

    /**
     * saves the auth
     */
    fun saveAuth(auth: Auth) {
        val editor = sharedPreferences.edit()
        editor.putString(AUTH_KEY, gson.toJson(auth)).commit()
    }

    /**
     * clears the auth
     */
    fun removeAuth(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(AUTH_KEY).commit()
    }

    /**
     * returns the auth
     */
    fun getAuth(): Auth {
        val auth = sharedPreferences.getString(AUTH_KEY, "{}")
        return gson.fromJson(auth, Auth::class.java)
    }


    fun saveLogged(logged: Boolean){
        sharedPreferences.edit().putBoolean(LOGGED_IN, logged).commit()
    }

    fun setVideoLiveness(isSelected: Boolean){
        sharedPreferences.edit().putBoolean(VIDEOLIVENESS_SELECTED, isSelected).commit()
    }
    fun getVideoLivenessSelection(): Boolean {
        return sharedPreferences.getBoolean(VIDEOLIVENESS_SELECTED, false)
    }
    fun isLoggedIn(): Boolean{
        return sharedPreferences.getBoolean(LOGGED_IN, false)
    }

    fun saveHasId(hasId: Boolean){
        sharedPreferences.edit().putBoolean(HAS_ID, hasId).commit()
    }

    fun removeHasId(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(HAS_ID).commit()
    }

    fun isHasId(): Boolean{
        return sharedPreferences.getBoolean(HAS_ID, false)
    }

    fun addPhone(phone: Phone){
        val phones = getPhones()
        if(PHONES_MAX_SIZE == phones.size){
            phones.remove(phones.first())
        }
        phones.add(phone)
        sharedPreferences.edit().putString(PHONES_LIST_KEY, gson.toJson(phones)).commit()
    }

    fun getPhones(): MutableList<Phone>{
        val phonesJson = sharedPreferences.getString(PHONES_LIST_KEY, "[]")
        val type = object : TypeToken<List<Phone>>(){}.type
        return gson.fromJson(phonesJson, type)
    }

    fun addEmail(email: String){
        val emailList = getEmailList()
        if(EMAIL_MAX_SIZE == emailList.size){
            emailList.remove(emailList.first())
        }
        emailList.add(email)
        sharedPreferences.edit().putString(EMAIL_LIST_KEY, gson.toJson(email)).commit()
    }

    fun getEmailList(): MutableList<String>{
        val emailListJson = sharedPreferences.getString(EMAIL_LIST_KEY, "[]")
        val type = object : TypeToken<List<String>>(){}.type
        return gson.fromJson(emailListJson, type)
    }

    fun saveEmail(email: String) {
        val editor = sharedPreferences.edit()
        editor.putString(EMAIL_KEY, email).commit()
    }

    fun removeEmail(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(EMAIL_KEY).commit()
    }

    fun getEmail(): String {
        return sharedPreferences.getString(EMAIL_KEY, "")
    }

    fun savePhone(phone: Phone) {
        val editor = sharedPreferences.edit()
        editor.putString(PHONE_KEY, gson.toJson(phone)).commit()
    }

    fun removePhone(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(PHONE_KEY).commit()
    }

    fun getPhone(): Phone? {
        val phone = sharedPreferences.getString(PHONE_KEY, "")
        return gson.fromJson(phone, Phone::class.java)
    }

    fun saveDocumentResponse(response: DocAuthInvokeServiceResponse) {
        val editor = sharedPreferences.edit()
        editor.putString(DOCUMENT_RESPONSE_KEY, gson.toJson(response)).commit()
    }

    fun removeDocumentResponse(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(DOCUMENT_RESPONSE_KEY).commit()
    }

    fun getDocumentResponse(): DocAuthReportVerification {
        val auth = sharedPreferences.getString(DOCUMENT_RESPONSE_KEY, "{}")
        return gson.fromJson(auth, DocAuthReportVerification::class.java)
    }

    fun getSelfieResponse(): AuthExamResponse {
        val auth = sharedPreferences.getString(SELFIE_RESPONSE_KEY, "{}")
        return gson.fromJson(auth, AuthExamResponse::class.java)
    }

    fun saveSelfieResponse(response: AuthExamResponse) {
        val editor = sharedPreferences.edit()
        editor.putString(SELFIE_RESPONSE_KEY, gson.toJson(response)).commit()
    }

    fun removeSelfieResponse(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(SELFIE_RESPONSE_KEY).commit()
    }

    fun saveFrontDocumentUri(frontUri: String) {
        val editor = sharedPreferences.edit()
        editor.putString(DOCUMENT_FILE_FRONT, frontUri).commit()
    }

    fun saveBackDocumentUri(frontUri: String) {
        val editor = sharedPreferences.edit()
        editor.putString(DOCUMENT_FILE_BACK, frontUri).commit()
    }

    fun removeFrontDocumentUri(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(DOCUMENT_FILE_FRONT).commit()
    }

    fun removeBackDocumentUri(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(DOCUMENT_FILE_BACK).commit()
    }

    fun getFrontDocumentUri(): String {
        return sharedPreferences.getString(DOCUMENT_FILE_FRONT, "")
    }

    fun getBackDocumentUri(): String {
        return sharedPreferences.getString(DOCUMENT_FILE_BACK, "")
    }

    fun saveCredoLabReference(reference: String) {
        Log.d("CREDOLAB", reference)
        val editor = sharedPreferences.edit()
        editor.putString(CREDOLAB_REFERENCE, reference).commit()
    }

    fun removeCredoLabReference(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(CREDOLAB_REFERENCE).commit()
    }

    fun getCredoLabReference(): String {

        return sharedPreferences.getString(CREDOLAB_REFERENCE, "")
    }

    fun saveCredoLabScores(scores: List<CredoLabScores>) {
        val editor = sharedPreferences.edit()
        editor.putString(CREDOLAB_SCORES, gson.toJson(scores)).commit()
    }

    fun removeCredoLabScores(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(CREDOLAB_SCORES).commit()
    }

    fun getCredoLabScores(): List<CredoLabScores> {
        val auth = sharedPreferences.getString(CREDOLAB_SCORES, "{}")
        val type = object : TypeToken<List<CredoLabScores>>(){}.type
        return gson.fromJson(auth, type)
    }

    fun saveExamResponse(response: ExamRequestResponse) {
        val editor = sharedPreferences.edit()
        editor.putString(EXAM_RESPONSE_KEY, gson.toJson(response)).commit()
    }

    fun removeExamResponse(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(EXAM_RESPONSE_KEY).commit()
    }

    fun getExamResponse(): ExamRequestResponse {
        val auth = sharedPreferences.getString(EXAM_RESPONSE_KEY, "{}")
        val type = object : TypeToken<ExamRequestResponse>(){}.type
        return gson.fromJson(auth, type)
    }

    fun saveLocation(location: Location?) {
        val editor = sharedPreferences.edit()
        editor.putString(LOCATION_KEY, gson.toJson(location)).commit()
    }

    fun removeLocation(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(LOCATION_KEY).commit()
    }

    fun getLocation():  Location? {
        val auth = sharedPreferences.getString(LOCATION_KEY, "{}")
        return gson.fromJson(auth, Location::class.java)
    }

    fun saveAddress(address: String) {
        val editor = sharedPreferences.edit()
        editor.putString(ADDRESS_KEY, address).commit()
    }

    fun removeAddress(): Boolean {
        val editor = sharedPreferences.edit()
        return editor.remove(ADDRESS_KEY).commit()
    }

    fun getAddress(): String {
        return sharedPreferences.getString(ADDRESS_KEY, "")
    }

}
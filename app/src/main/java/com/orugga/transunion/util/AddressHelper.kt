package com.orugga.transunion.util

import android.app.Activity
import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.util.Log
import java.io.IOException
import java.util.*


class AddressHelper {

    companion object {
        fun isLocationEnabled(activity: Activity): Boolean {
            var locationManager: LocationManager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
            )
        }

        fun getAddressFromLocation(latitude: Double, longitude: Double, context: Context?): String {
            val geocoder = Geocoder(context, Locale.getDefault())
            var result: String? = null
            try {
                val addressList: List<Address>? = geocoder.getFromLocation(latitude, longitude, 1)
                if (addressList != null && addressList.isNotEmpty()) {
                    val address: Address = addressList[0]
                    val sb = StringBuilder()
                    for (i in 0..address.maxAddressLineIndex) {
                        sb.append(address.getAddressLine(i))
                    }
                    result = sb.toString()
                }
            } catch (e: IOException) {
                Log.e("Location Address Loader", "Unable connect to Geocoder", e)
            } finally {
                if (result == null) {
                    result = " Unable to get address for this location."
                }
                return result
            }
        }
    }
}
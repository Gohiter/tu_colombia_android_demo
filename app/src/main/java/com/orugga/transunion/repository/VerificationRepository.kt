package com.orugga.transunion.repository

import android.content.Context
import com.orugga.transunionsdk.BuildConfig
import com.orugga.transunionsdk.TransunionSdk
import com.orugga.transunionsdk.util.SharedPreferencesHelper
import com.orugga.transunionsdk.vo.Resource
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VerificationRepository  @Inject constructor(
        private val transunionSdk: TransunionSdk,
//        private val sharedPreferenceHelper: SharedPreferencesHelper
){

    fun sendEmail(email: String) = flow {
        emit(Resource.loading(null))
        try {
            val response = transunionSdk.verificationHelper.sendEmail(email)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun sendPhone(number: String, type: String, countryCode: String) = flow {
        emit(Resource.loading(null))
        try {
            val response = transunionSdk.verificationHelper.sendPhone(number, type, countryCode)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun getBiocatchScore() = flow {
        emit(Resource.loading(null))
        try {
            val response = transunionSdk.verificationHelper.getBiocatchScore()
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun getOffers(firstName: String, lastName: String) = flow {
        emit(Resource.loading(null))
        try {
            val response = transunionSdk.verificationHelper.getOffers(firstName, lastName)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun collectCredoLabData(appContext: Context, referenceKey: String) = flow {
        emit(Resource.loading(null))
//        sharedPreferenceHelper
//        val credolabKey = sharedPreferenceHelper.getCredoLabKey()
//        val credolabmail = sharedPreferenceHelper.getCredoLabUser()
//        val credolabPassword = sharedPreferenceHelper.getCredoLabPassword()
        try {
//            val result = transunionSdk.credoLabManager.collectData(appContext, referenceKey, BuildConfig.CREDOLAB_URL)
            val result = transunionSdk.verificationRepository.collectCredoLabData(appContext, referenceKey)
            emit(Resource.success(result))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun getCredoLabToken() = flow {
        emit(Resource.loading(null))
//                val credolabmail = sharedPreferenceHelper.getCredoLabUser()
//        val credolabPassword = sharedPreferenceHelper.getCredoLabPassword()
        try {
            val response = transunionSdk.verificationHelper.getCredoLabToken()
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun getCredoLabDataSetInsight(referenceNumber: String) = flow {
        emit(Resource.loading(null))
        try {
            val response = transunionSdk.verificationHelper.getCredoLabDataSetInsight(referenceNumber)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun completeTransaction() = flow {
        emit(Resource.loading(null))
        try {
            val response = transunionSdk.verificationHelper.completeTransaction()
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun faceAuthenticationLogin(imageFile: String, clientID: String, customerId: String, emailAddress: String)  = flow {
        emit(Resource.loading(null))
        try {
            val response = transunionSdk.verificationHelper.faceAuthenticationLogin(imageFile, clientID, customerId, emailAddress)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }
    fun faceAuthenticationRegister(clientID: String, customerId: String, emailAddress: String)  = flow {
        emit(Resource.loading(null))
        try {
            val response = transunionSdk.verificationHelper.faceAuthenticationRegister(clientID, customerId, emailAddress)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }
    fun passiveLivenessVerification(imageUri: String)  = flow {
        emit(Resource.loading(null))
        try {
            val response = transunionSdk.verificationHelper.passiveLivenessVerification(imageUri)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }
}
package com.orugga.transunion.repository

import com.orugga.transunionsdk.TransunionSdk
import com.orugga.transunionsdk.vo.Resource
import com.orugga.transunionsdk.vo.UploadableDocumentType
import kotlinx.coroutines.flow.flow
import okhttp3.MediaType
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DocumentsRepository  @Inject constructor(
        private val transunionSdk: TransunionSdk
){
//    fun invokeService(emailAddress: String, telephoneCode: String, telephoneNumber: String, blackboxValue: String) = flow {
//        emit(Resource.loading(null))
//        try {
//            val response = transunionSdk.documentsHelper.documentAuthentication(emailAddress, telephoneCode, telephoneNumber, blackboxValue)
//            emit(Resource.success(response))
//        }catch (ex:Exception){
//            emit(Resource.error(ex.toString(), null))
//        }
//    }

    fun createDocument(uploadableDocumentType: UploadableDocumentType, fileName: String) = flow {
        emit(Resource.loading(null))
        try {
            val response =
                transunionSdk.documentsHelper.createDocument(uploadableDocumentType, fileName)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun uploadDocument(file: File, mediaType: MediaType, uploadableDocumentType: UploadableDocumentType) = flow {
        emit(Resource.loading(null))
        try {
            val response =
                transunionSdk.documentsHelper.uploadDocument(file, mediaType, uploadableDocumentType)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun processDocument() = flow {
        emit(Resource.loading(null))
        try {
            val response =
                transunionSdk.documentsHelper.processDocument()
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun processSelfie() = flow {
        emit(Resource.loading(null))
        try {
            val response =
                transunionSdk.documentsHelper.processSelfie()
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun processExam(xml: String) = flow {
        emit(Resource.loading(null))
        try {
            val response =
                transunionSdk.documentsHelper.processExam(xml)
            emit(Resource.success(response))
        } catch (ex: Exception) {
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun getOTP(validationMethod: String, phoneNumber: String, phoneType: String, returnMessage: String, action: String) = flow {
        emit(Resource.loading(null))
        try {
            val response =
                transunionSdk.documentsHelper.getOTP(validationMethod, phoneNumber, phoneType, returnMessage, action)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun sendOTP(action: String, pinNumber: String) = flow {
        emit(Resource.loading(null))
        try {
            val response =
                transunionSdk.documentsHelper.sendOTP(action, pinNumber)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun resendOTP(action: String, pinNumber: String) = flow {
        emit(Resource.loading(null))
        try {
            val response =
                transunionSdk.documentsHelper.resendOTP(action, pinNumber)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun bypassOTP(action: String) = flow {
        emit(Resource.loading(null))
        try {
            val response =
                transunionSdk.documentsHelper.bypassOTP(action)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }
}

//    fun getDocAuthReport() = flow {
//        emit(Resource.loading(null))
//        try {
//            val response =
//                transunionSdk.documentsHelper.getDocumentReport()
//            emit(Resource.success(response))
//        }catch (ex:Exception){
//            emit(Resource.error(ex.toString(), null))
//        }
//    }

//    fun pullReport() = flow {
//        emit(Resource.loading(null))
//        try {
//            val response =
//                transunionSdk.documentsHelper.pullReport()
//            emit(Resource.success(response))
//        }catch (ex:Exception){
//            emit(Resource.error(ex.toString(), null))
//        }
//    }


//    //Facial Similarity
//    fun invokeFacialSimilarityService(emailAddress: String, telephoneCode: String, telephoneNumber: String, blackboxValue: String) = flow {
//        emit(Resource.loading(null))
//        try {
//            val response = transunionSdk.documentsHelper.invokeFacialSimilarityService(emailAddress, telephoneCode, telephoneNumber, blackboxValue)
//            emit(Resource.success(response))
//        }catch (ex:Exception){
//            emit(Resource.error(ex.toString(), null))
//        }
//    }

//    fun getFacialSimilarityReport() = flow {
//        emit(Resource.loading(null))
//        try {
//            val response =
//                transunionSdk.documentsHelper.getFacialSimilarityReport()
//            emit(Resource.success(response))
//        }catch (ex:Exception){
//            emit(Resource.error(ex.toString(), null))
//        }
//    }
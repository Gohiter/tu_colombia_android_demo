package com.orugga.transunion.repository

import android.content.Context
import com.orugga.transunionsdk.TransunionSdk
import com.orugga.transunionsdk.vo.Resource
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository that handles User objects.
 */
@Singleton
class UserRepository  @Inject constructor(
        private val transunionSdk: TransunionSdk
){

    fun getAuthToken() = flow {
        emit(Resource.loading(null))
        try {
            val response = transunionSdk.sessionHelper.getAppToken()
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun createApp(emailAddress: String, telephoneCode: String, telephoneNumber: String, blackboxValue: String) = flow {
        emit(Resource.loading(null))
        try {
            val response = transunionSdk.sessionHelper.createDCApplication(emailAddress, telephoneCode, telephoneNumber, blackboxValue)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun getBlackBox(appContext: Context, iovationSubscriberKey: String) = flow {
        emit(Resource.loading(null))
        try {
            val response = transunionSdk.fraudManager.getBlackBoxFromDevice(appContext, iovationSubscriberKey)
            emit(Resource.success(response))
        }catch (ex:Exception){
            emit(Resource.error(ex.toString(), null))
        }
    }

    fun getAppId() : String{
        return transunionSdk.sessionHelper.getDCApplicationId()
    }
}